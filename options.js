
(function() {
    'use strict';

    /* Define important iframe contents and add all non-onstart scripts and
     * styles. */
    const frame = document.querySelector('#defaultTranscriptFrame');
    if (!frame || !frame.contentDocument) {
        return;
    }

    const frameHTML = frame.contentDocument.body.parentElement;
    const frameHead = frameHTML.querySelector('head');
    frameHead.appendChild(document.createElement('style'));
    const frameBody = frameHTML.querySelector('body');
    frame.contentDocument.styleSheets[0].insertRule(
        'html, body {' +
            'position: absolute !important;' +
            'left: 0 !important;' +
            'top: 0 !important;' +
            'width: 100% !important;' +
            'height: 100% !important;' +
            'background: white !important;' +
        '}', 0);
    
    const passagePane = $('<div id="passagePane"></div>')[0];
    frameBody.appendChild(passagePane);

    const $displayPane = $('<div id="displayPane"></div>');
    frameBody.appendChild($displayPane[0]);

    let selector = '<tw-tabcontrol id="transcriptTabControl"></tw-tabcontrol>';
    const tabControl = $(selector)[0];
    $displayPane.append(tabControl);

    // detail tab
    selector = '<tw-tab id="detailsTab" ' +
        'class="active"' +
        'data-selector="#transcriptDetailsDiv">Details</tw-tab>';
    const detailsTab = $(selector)[0];
    tabControl.appendChild(detailsTab);

    // detail tab container
    selector = '<div id="transcriptDetailsDiv" class="active"></div>';
    const $transcriptDetailsDiv = $(selector);
    $displayPane.append($transcriptDetailsDiv);

    // frames tab
    selector = '<tw-tab id="framesTab" ' +
        'data-selector="#transcriptFramesDiv">Frames</tw-tab>';
    const framesTab = $(selector)[0];
    tabControl.appendChild(framesTab);

    // frames tab container
    const $transcriptFramesDiv = $('<div id="transcriptFramesDiv"></div>');
    $displayPane.append($transcriptFramesDiv);

    // flat tab
    selector = '<tw-tab id="flatTab" ' +
        'data-selector="#transcriptFlatDiv">Flat</tw-tab>';
    const animatedTab = $(selector)[0];
    tabControl.appendChild(animatedTab);

    // flat tab container
    selector = '<div id="transcriptFlatDiv"></div>';
    const $transcriptFlatDiv = $(selector);
    $displayPane.append($transcriptFlatDiv);

    // settings tab
    selector = '<tw-tab id="settingsTab" ' +
        'data-selector="#transcriptSettingsDiv">Settings</tw-tab>';
    const settingsTab = $(selector)[0];
    tabControl.appendChild(settingsTab);

    // settings tab container
    selector = '<div id="transcriptSettingsDiv"></div>';
    const transcriptSettingsDiv = $(selector)[0];
    $displayPane.append(transcriptSettingsDiv);

    // transcript display settings
    const displaySettingsSpan = $('<span>Display Settings</span>')[0];
    transcriptSettingsDiv.appendChild(displaySettingsSpan);

    const dispTextChangesSpan = $('<span></span>')[0];
    transcriptSettingsDiv.appendChild(dispTextChangesSpan);

    selector = '<input id="dispTextChangesCheckbox" type="checkbox"></input>';
    const dispTextChangesCheckbox = $(selector)[0];
    dispTextChangesSpan.appendChild(dispTextChangesCheckbox);

    selector = '<label for="dispTextChangesCheckbox">' +
        'Display Text Changes</label>';
    const dispTextChangesLabel = $(selector)[0];
    dispTextChangesSpan.appendChild(dispTextChangesLabel);

    const dispPageEventsSpan = $('<span></span>')[0];
    transcriptSettingsDiv.appendChild(dispPageEventsSpan);

    selector = '<input id="dispPageEventsCheckbox" type="checkbox"></input>';
    const dispPageEventsCheckbox = $(selector)[0];
    dispPageEventsSpan.appendChild(dispPageEventsCheckbox);

    selector = '<label for="dispPageEventsCheckbox">' +
        'Display Page Events</label>';
    const dispPageEventsLabel = $(selector)[0];
    dispPageEventsSpan.appendChild(dispPageEventsLabel);

    const dispStateChangesSpan = $('<span></span>')[0];
    transcriptSettingsDiv.appendChild(dispStateChangesSpan);

    selector = '<input id="dispStateChangesCheckbox" type="checkbox"></input>';
    const dispStateChangesCheckbox = $(selector)[0];
    dispStateChangesSpan.appendChild(dispStateChangesCheckbox);

    selector = '<label for="dispStateChangesCheckbox">' +
        'Display State Changes</label>';
    const dispStateChangesLabel = $(selector)[0];
    dispStateChangesSpan.appendChild(dispStateChangesLabel);

    const dispLoadSpan = $('<span></span>')[0];
    transcriptSettingsDiv.appendChild(dispLoadSpan);

    selector = '<input id="dispLoadCheckbox" type="checkbox"></input>';
    const dispLoadCheckbox = $(selector)[0];
    dispLoadSpan.appendChild(dispLoadCheckbox);

    selector = '<label for="dispLoadCheckbox">' +
        'Display Load</label>';
    const dispLoadLabel = $(selector)[0];
    dispLoadSpan.appendChild(dispLoadLabel);

    const dispParseSpan = $('<span></span>')[0];
    transcriptSettingsDiv.appendChild(dispParseSpan);

    selector = '<input id="dispParseCheckbox" type="checkbox"></input>';
    const dispParseCheckbox = $(selector)[0];
    dispParseSpan.appendChild(dispParseCheckbox);

    selector = '<label for="dispLoadCheckbox">' +
        'Display Parse</label>';
    const dispParseLabel = $(selector)[0];
    dispParseSpan.appendChild(dispParseLabel);

    const dispRenderSpan = $('<span></span>')[0];
    transcriptSettingsDiv.appendChild(dispParseSpan);

    selector = '<input id="dispRenderCheckbox" type="checkbox"></input>';
    const dispRenderCheckbox = $(selector)[0];
    dispRenderSpan.appendChild(dispRenderCheckbox);

    selector = '<label for="dispRenderCheckbox">' +
        'Display Render</label>';
    const dispRenderLabel = $(selector)[0];
    dispRenderSpan.appendChild(dispRenderLabel);

    transcriptSettingsDiv.appendChild(document.createElement('br'));

    // transcript recording settings
    selector = '<span class="title">Recording Settings</span>';
    const recordingSettingsTitle = $(selector)[0];
    transcriptSettingsDiv.appendChild(recordingSettingsTitle);

    const recTextChangesSpan = $('<span></span>')[0];
    transcriptSettingsDiv.appendChild(recTextChangesSpan);

    selector = '<input id="recTextChangesCheckbox" type="checkbox"' +
        'data-selector="#transcriptSettingsDiv"></input>';
    const recTextChangesCheckbox = $(selector)[0];
    recTextChangesSpan.appendChild(recTextChangesCheckbox);

    selector = '<label for="recTextChangesCheckbox">' +
        'Record Text Changes</label>';
    const recTextChangesLabel = $(selector)[0];
    recTextChangesSpan.appendChild(recTextChangesLabel);

    const recPageEventsSpan = $('<span></span>')[0];
    transcriptSettingsDiv.appendChild(recPageEventsSpan);

    selector = '<input id="recPageEventsCheckbox" type="checkbox"></input>';
    const recPageEventsCheckbox = $(selector)[0];
    recPageEventsSpan.appendChild(recPageEventsCheckbox);

    selector = '<label for="recPageEventsCheckbox">' +
        'Record Page Events</label>';
    const recPageEventsLabel = $(selector)[0];
    recPageEventsSpan.appendChild(recPageEventsLabel);

    const recStateChangesSpan = $('<span></span>')[0];
    transcriptSettingsDiv.appendChild(recStateChangesSpan);

    selector = '<input id="recStateChangesCheckbox" type="checkbox"></input>';
    const recStateChangesCheckbox = $(selector)[0];
    recStateChangesSpan.appendChild(recStateChangesCheckbox);

    selector = '<label for="recStateChangesCheckbox">' +
        'Record State Changes</label>';
    const recStateChangesLabel = $(selector)[0];
    recStateChangesSpan.appendChild(recStateChangesLabel);

    const recLoadSpan = $('<span></span>')[0];
    transcriptSettingsDiv.appendChild(recLoadSpan);

    selector = '<input id="recLoadCheckbox" type="checkbox"></input>';
    const recLoadCheckbox = $(selector)[0];
    recLoadSpan.appendChild(recLoadCheckbox);

    selector = '<label for="recLoadCheckbox">' +
        'Record Load</label>';
    const recLoadLabel = $(selector)[0];
    recLoadSpan.appendChild(recLoadLabel);

    const recParseSpan = $('<span></span>')[0];
    transcriptSettingsDiv.appendChild(recParseSpan);

    selector = '<input id="recParseCheckbox" type="checkbox"></input>';
    const recParseCheckbox = $(selector)[0];
    recParseSpan.appendChild(recParseCheckbox);

    selector = '<label for="recLoadCheckbox">' +
        'Record Parse</label>';
    const recParseLabel = $(selector)[0];
    recParseSpan.appendChild(recParseLabel);

    const recRenderSpan = $('<span></span>')[0];
    transcriptSettingsDiv.appendChild(recParseSpan);

    selector = '<input id="recRenderCheckbox" type="checkbox"></input>';
    const recRenderCheckbox = $(selector)[0];
    recRenderSpan.appendChild(recRenderCheckbox);

    selector = '<label for="recRenderCheckbox">' +
        'Record Render</label>';
    const recRenderLabel = $(selector)[0];
    recRenderSpan.appendChild(recRenderLabel);

    frameBody.style.overflowY = 'hidden';

    selector = 'style';
    $(selector).each((_, style) => {
        const styleClone = style.cloneNode(true);
        // style does not work on append with type
        styleClone.removeAttribute('type');
        frameHead.appendChild(styleClone);
    });

    const script = document.createElement('script');
    function resizeIframeHeight() {
        let lastHeight = 0;
        window.sizeID = window.setInterval(() => {
            let compatibility;
            let highest = 0;
            for (let ii = 0; ii < document.body.childNodes.length; ii++) {
                const child = document.body.childNodes[ii];
                compatibility = Math.max( 
                    child.clientHeight,
                    child.scrollHeight);
                    //, child.offsetHeight);
                if (highest < compatibility) {
                    highest = compatibility;
                }
            }
            if (!highest) {
                return;
            }

            const frameDisplay = 
                top.document.querySelector('#defaultTranscriptDisplay');
            frameDisplay.style.height = highest + 'px';
            const maxHeight = top.document.body.clientHeight * 0.85 + 'px';
            frameDisplay.style.maxHeight = maxHeight;

            compatibility = Math.max(
                top.document.body.clientHeight,
                top.document.body.scrollHeight, 
                //body.offsetHeight, 
                top.document.body.parentElement.clientHeight, 
                top.document.body.parentElement.scrollHeight);//, html.offsetHeight );

            if (passagePane) {
                passagePane.style.maxHeight = maxHeight;
            }

            const displayPane = document.querySelector('#displayPane');
            if (displayPane) {
                displayPane.style.maxHeight = maxHeight;
            }

            lastHeight = compatibility;
        }, 1000);
    };
    script.innerHTML = resizeIframeHeight.toString() +
        '\nresizeIframeHeight()';
    frameBody.appendChild(script);

    const $defaultMenu = $('#defaultMenu');
    
    window.Options = {
        recordingLevels: {
            textChanges: true,
            pageEvents: true,
            stateChanges: true,
            load: false,
            render: false,
        },

        transcriptMutationObserver: null,

        toggleTranscriptRecording() {
            State.recordingTranscript = !State.recordingTranscript;
        },

        startTranscriptEntry() {
        },

        endTranscriptEntry() {
            // remove mutation observer
            if (Options.transcriptMutationObserver &&
                Options.transcriptMutationObserver.disconnect)
            {
                Options.transcriptMutationObserver.disconnect();
            }

            // remove event checker
            $('tw-passage.current').off('click keydown');
        },

        printTranscript(type) {
            let printed;
            let lowerTrimType;
            if (typeof(type) === 'string') {
                lowerTrimType = type.trim().toLowerCase();
            }

            if (lowerTrimType === 'html') {
                printed = Options.printTranscriptToHTML();
            } else {
                wideAlert('The type argument passed to ' +
                    'Options.printTranscript is not recognized. Cannot print.');
            }

            return printed;
        },

        printTranscriptToHTML() {
            // generate primary container
            const $node = $('<div id="container"></div>');

            const past = State.stacks.past;

            for (let ii = 0; ii < past.transcriptStates.length; ii++) {
                const $entry = $('<div class="entry"></div>');
                $node.append($entry);

                let id = ii.toString();
                const $title = $('<p data-transcript-identifier="' + id +
                    '" class="title">' +
                    State.stacks.past.passageNames[ii] +
                    '</p>');
                $title.css('cursor', 'pointer');
                $title.appendTo($entry);

                const $collapser = $('<button>-</button>');
                $collapser.attr({
                    class: 'collapser',
                    'data-on': '-',
                    'data-off': '+',
                });
                $collapser.appendTo($entry);

                const item = State.stacks.past.transcriptStates[ii];
                // show the last relevant change – macros if there was one, 
                // nothing otherwise
                if (item.textChanges.length) {
                    id += '_textChanges_-1';
                } else if (item.macros.length) {
                    id += '_macros_-1';
                } else {
                    continue;
                }

                const transcriptEntry = past.transcriptStates[ii];
                if (!transcriptEntry) {
                    $entry.append('<p>Empty!</p>');
                    $node.append($entry);
                    continue;
                }

                const changes = transcriptEntry.textChanges;
                        
                for (let jj = 0; jj < changes.length; jj++) {
                    const change = changes[jj];
                    const subID = id + '_' + jj;
                    const $state = 
                        $('<div class="state" id="' + subID + '"></div>');

                    $state[0].innerText = change.text;

                    for (let kk = 0; kk < change.pageEvents.length; kk++) {
                        const evt = change.pageEvents[kk];
                    }

                    $state.find('tw-transcript-event').each((_, evt) => {
                        evt.removeAttribute('unprocessed');
                        evt.innerHTML =
                            evt.getAttribute('data-transcript-index');
                    });

                    const $subtitle = $('<div class="subtitle">textchange</div>');
                    $subtitle.attr({
                        'data-transcript-time': transcriptEntry.time,
                        'data-transcript-identifier':
                            ii.toString() + '_textChanges_0',
                    });

                    $state.prepend($subtitle);

                    $entry.append($state);
                }
            }

            return $node.html();
        },

        setTranscriptDisplay(clicked) {
            if (!clicked) {
                /*passagePane.innerHTML = 'Empty! Did you turn on transcript ' +
                    'recording?';*/
                return;
            }

            const $currentTarget = $(clicked);
            const identifier = 
                $currentTarget.attr('data-transcript-identifier');
            if (!identifier) {
                wideAlert('A valid data-transcript-identifier does not exist ' +
                    'in the $currentTarget variable derived from the ' +
                    'clicked argument.');
                return;
            }

            let dataSrc = State.stacks.past.transcriptStates;
            identifier.split('_').filter(aa => {
                return aa !== '';
            }).forEach(id => {
                if (id === '-1' && dataSrc.length) {
                    dataSrc = dataSrc[dataSrc.length - 1];
                } else if (id in dataSrc) {
                    dataSrc = dataSrc[id];
                } else {
                    wideAlert('invalid data-transcript-identifier!');
                    return;
                }
            });

            function doNothing(str) {
                return str;
            }

            const funcs = {
                jqueryevt: saferJSONParse,
                state: saferJSONParse,
                time: Number.parseFloat,
                name: doNothing,
                macroName: doNothing,
                passageName: doNothing,
                type: doNothing,
                errors: doNothing,
                arguments: doNothing,
            };

            const aliases = {
                name: 'Passage Name',
                macroName: 'Macro Name',
                passageName: 'Passage Name',
                jqueryevt: 'jQuery Event',
                state: 'Current State',
                time: 'Time Recorded',
                type: 'Record Type',
                errors: 'Errors',
                arguments: 'Arguments',
            };

            const detailsElem = document.createElement('div');
            getOwnPropertyNames(dataSrc).forEach(name => {
                const div = document.createElement('div');
                const func = funcs[name];
                if (!func) {
                    return;
                }

                const alias = aliases[name.toLowerCase()];
                const processed = func(dataSrc[name]);
                if (alias) {
                    Options.varToHTML(div, processed, alias);
                } else {
                    Options.varToHTML(div, processed, name);
                }

                detailsElem.appendChild(div);
            });

            const frame = $('#defaultTranscriptFrame')[0];
            const frameBody = frame.contentDocument.body;

            const $displayPane = $(frameBody.querySelector('#displayPane'));
            
            const $details = $displayPane.find('#transcriptDetailsDiv');
            $details.html(detailsElem.innerHTML);
                
            const framesElem = document.createElement('div');
            const flatElem = document.createElement('div');
            dataSrc = State.stacks.past.transcriptStates[
                identifier.split('_')[0]];
            getOwnPropertyNames(dataSrc).forEach(collection => {
                if (typeof(dataSrc[collection]) !== 'object' ||
                    !('forEach' in dataSrc[collection]) ||
                    !dataSrc[collection].length ||
                    collection === 'textChanges' ||
                    collection === 'stateChanges' ||
                    collection === 'pageEvents')
                {
                    return;
                }

                dataSrc[collection].forEach(record => {
                    const flatDiv = document.createElement('div');
                    flatDiv.className = 'frame';

                    const framesDiv = document.createElement('div');
                    framesDiv.className = 'frame';

                    getOwnPropertyNames(record).forEach(prop => {
                        if (prop === 'name' ||
                            prop === 'passageName')
                        {
                            return;
                        }

                        let func = funcs[prop];

                        if (!func) {
                            func = noop;
                        }

                        if (prop === 'text') {
                            flatDiv.innerHTML = record[prop]
                                .replace(/\n/g, '<br>')
                                .replace(/\t/g, '&nbsp;&nbsp;&nbsp;&nbsp;');

                            const content = document.createElement('div');
                            content.className = 'content';
                            content.dataset.rawData = record[prop];
                            content.textContent = record[prop];
                            content.innerHTML = content.innerHTML
                                .replace(/\n/g, '<br>')
                                .replace(/\t/g, '&nbsp;&nbsp;&nbsp;&nbsp;');
                            framesDiv.appendChild(content);
                        } else {
                            const alias = aliases[prop.toLowerCase()];
                            const processed = func(record[prop]) ||
                                record[prop];
                            if (alias) {
                                Options.varToHTML(flatDiv, processed, alias);
                                Options.varToHTML(framesDiv, processed, alias);
                            } else {
                                Options.varToHTML(flatDiv, processed, prop);
                                Options.varToHTML(framesDiv, processed, prop);
                            }
                        }

                    });
                    
                    flatElem.appendChild(flatDiv);
                    framesElem.appendChild(framesDiv);
                });
            });

            const combined = dataSrc.textChanges
                .concat(dataSrc.stateChanges)
                .concat(dataSrc.macros)
                .sort((aa, bb) => {
                    if (aa.time < bb.time) {
                        return -1;
                    } else if (aa.time === bb.time) {
                        return 0;
                    } else {
                        return 1;
                    }
                });

            combined.forEach(record => {
                const div = document.createElement('div');
                div.className = 'frame';

                getOwnPropertyNames(record).forEach(prop => {
                    if (prop === 'name' || prop === 'passageName') {
                        return;
                    }

                    let func = funcs[prop];
                    if (!func) {
                        func = noop;
                    }

                    if (prop === 'text') {
                        div.innerHTML = record[prop];
                    } else {
                        const alias = aliases[prop.toLowerCase()];
                        const processed = func(record[prop]) || record[prop];
                        if (alias) {
                            Options.varToHTML(div, processed, alias);
                        } else {
                            Options.varToHTML(div, processed, prop);
                        }
                    }
                });
                    
                flatElem.appendChild(div);
                framesElem.appendChild(div.cloneNode(true));
            });

            const $frames = $displayPane.find('#transcriptFramesDiv');
            $frames.html('');

            const upButton = document.createElement('button');
            upButton.className = 'disabled';
            upButton.textContent = '↑';
            $frames.append(upButton);

            const downButton = document.createElement('button');
            downButton.textContent = '↓';
            $frames.append(downButton);
            // if there's 0 or 1 frames, don't allow traversal at all
            if (framesElem.querySelectorAll('.frame').length < 2) {
                downButton.className = 'disabled';
            }

            $(upButton).click(() => {
                const $prev = $frames.find('.frame.active')
                    .prev('.frame')
                    .first();
                if ($prev.length) {
                    $frames.find('.frame').removeClass('active');
                    $prev.addClass('active');
                }

                $(downButton).removeClass('disabled');
                if (!$prev.prev('.frame').first().length) {
                    $(upButton).addClass('disabled');
                }
            });

            $(downButton).click(() => {
                const $next = $frames.find('.frame.active')
                    .next('.frame')
                    .first();
                if ($next.length) {
                    $frames.find('.frame').removeClass('active');
                    $next.addClass('active');
                }

                $(upButton).removeClass('disabled');
                if (!$next.next('.frame').first().length) {
                    $(downButton).addClass('disabled');
                }
            });

            $(framesElem).find('script').remove();
            $frames.append(framesElem);
            $frames.find('.frame').first().addClass('active');

            $(flatElem).find('script').remove();
            const $flat = $displayPane.find('#transcriptFlatDiv');
            $flat.html('');
            $flat.append(flatElem);

            // open all Errors by default
            const selector = '.container[data-property-name="Errors"] > ' +
                '.collapsed > ' +
                '.collapser';
            $displayPane.find(selector).click();
        },

        varToHTML(node, prop, propName) {
            const container = document.createElement('div');
            container.className = 'container';
            container.dataset.propertyName = propName;
            node.appendChild(container);

            const textField = document.createElement('div');
            container.appendChild(textField);

            if (propName) {
                const title = document.createElement('b');
                let formattedName = propName;
                title.className = 'title';
                if ((prop && typeof(prop) === 'object') &&
                    prop.constructor &&
                    prop.constructor.name)
                {
                     formattedName += ' (' + prop.constructor.name + ')';
                }

                formattedName += ': ';
                title.innerHTML = '<span class="titleText">' +
                    formattedName +
                    '</span>';
                textField.appendChild(title);
            }

            if (typeof(prop) === 'object' && prop !== null) {
                const spanner = document.createElement('div');
                spanner.className = 'spanner';
                textField.appendChild(spanner);

                const subEl = document.createElement('div');
                container.appendChild(subEl);
                subEl.className = 'collapsed';

                const collapser = document.createElement('button');
                collapser.className = 'collapser';
                collapser.innerText = '+';
                collapser.setAttribute('data-on', '-');
                collapser.setAttribute('data-off', '+');
                subEl.appendChild(collapser);

                const subProps = Object.getOwnPropertyNames(prop);
                if (!subProps.length) {
                    const empty = document.createElement('div');
                    empty.innerText = 'Empty!';
                    subEl.appendChild(empty);
                }

                subProps.forEach(subProp => {
                    if (subProp === 'length' && prop instanceof Array) {
                        if (!prop.length) {
                            const empty = document.createElement('div');
                            empty.innerText = 'Empty!';
                            subEl.appendChild(empty);
                        } else {
                            return;
                        }
                    } else {
                        Options.varToHTML(subEl, prop[subProp], subProp);
                    }
                });

            } else {
                let valText;
                
                if (prop === null || prop === undefined) {
                    valText = 'null';
                } else if (isFunction(prop.toString)) {
                    valText = prop.toString();
                }

                if (typeof(prop) === 'string') {
                    valText = '"' + valText + '"';
                }
                const valNode = document.createTextNode(valText);
                textField.appendChild(valNode);

                const spacer = document.createElement('p');
                spacer.className = 'spacer';
                container.appendChild(spacer);
            }
        },

        updateTranscriptVisuals() {
            const passagePane = frameBody.querySelector('#passagePane');
            passagePane.innerHTML = Options.printTranscriptToHTML();

            const choice = $defaultMenu.attr('data-choice');
            if (choice === 'transcript') {
                $defaultMenu.attr('data-choice', 'transcript');
                if (!$(defaultMenu).find('.destructor').length) {
                    $defaultMenu.append('<button ' +
                        'class="destructor">X</button>');
                }

                $defaultMenu.find('tw-element-resizer')
                    .attr('data-selector', '#defaultTranscriptDisplay');
            
                Options.setTranscriptDisplay(passagePane.querySelector('.title'));
            }
        },

        captureTextChanges() {
            const $current = $('tw-passage.current');
            if (!$current.length) {
                return;
            }
             
            let lastText = $current[0].innerText;

            Options.eventIndex = 1;

            // create an observer instance
            Options.transcriptMutationObserver =
                new MutationObserver(mutations => {
                    if (!Options.recordingLevels.textChanges ||
                        (!State.recordingTranscript && !window.DEBUG))
                    {
                        return;
                    }

                    const entry = State.stacks.present.transcriptState;

                    const text = $current[0].innerText;

                    // if the text has changed, add a new entry
                    if (!lastText) {
                        lastText = text;
                    } else if (text !== lastText) {
                        const $current = $('tw-passage.current');
                        
                        // do not allow the events to persist
                        $current.find('tw-transcript-event').remove();

                        entry.textChanges.push({
                            text,
                            name: State.stacks.present.passageName,
                            time: new Date().getTime().toString(),
                            type: 'textchange',
                            pageEvents: [],
                            errors: from(State.compilerStatus.errors),
                        });

                        State.compilerStatus.errors = [];

                        Options.updateTranscriptVisuals();

                        lastText = $current.innerText;

                        Options.eventIndex = 1;
                    }
                });
             
            // configuration of the observer:
            const observerConfig = {
                childList: true,
                characterData: true,
                subtree: true,
            };
             
            // pass in the target node, as well as the observer options
            Options.transcriptMutationObserver.observe(
                document.body, observerConfig);
        },

        haltCaptureTextChanges() {
            Options.transcriptMutationObserver.disconnect();
        },

        capturePageEvents() {
            const $current = $('tw-passage.current');
            $current.on('click keydown', e => {
                const valid = window.DEBUG || 
                    (Options.recordingLevels.textChanges &&
                    State.recordingTranscript);
                // don't register clicks on the passage element, and don't
                // record unless the user state calls for it
                if (e.target === $current[0] || !valid) {
                    return;
                }

                const presentEntry = State.stacks.present.transcriptState;

                const time = new Date().getTime().toString();
                const $html = $(e.target);

                const $evt =
                    $('<tw-transcript-event unprocessed>__EVT' +
                    '</tw-transcript-event>');

                const identifier = 
                    State.stacks.past.transcriptState
                        .indexOf(presentEntry).toString() +
                    '_pageEvents_' +
                    String(Options.eventIndex - 1);

                $evt.text($evt.text() + '_' + identifier + '__');
               
                const decircled = e;
                delete decircled.originalEvent;
                delete decircled.view;
                delete decircled.target;
                delete decircled.currentTarget;
                delete decircled.handleObj;
                delete decircled.delegateTarget;
                delete decircled.relatedTarget;
                delete decircled.toElement;
                const decircTxt = JSON.stringify(decircled);

                $html.append($evt);

                presentEntry.textChanges.slice(-1)[0].pageEvents.push({
                    time,
                    textBefore: $current.text().split($evt.text())[0],
                    jqueryevt: decircTxt,
                    type: 'pageEvent',
                    errors: from(State.compilerStatus.errors),
                });

                $evt.remove();

                State.compilerStatus.errors = [];

                Options.updateTranscriptVisuals();

                Options.eventIndex++;
            });
        },

        captureStateChange(name, target, property, value) {
            const regex = /transcript|binding|compiler/
            if (State.stacks.present.transcriptState &&
                !regex.test(property.toLowerCase()) &&
                property.toLowerCase().startsWith('current') &&
                target[property] !== value)
            {
                State.stacks.present.stateChanges.push({
                    passageName: State.stacks.present.passageName,
                    name,
                    property,
                    value,
                    type: 'stateChange',
                    errors: from(State.compilerStatus.errors),
                });

                State.compilerStatus.errors = [];
            }
        },
    };

    const containers = $('#defaultMenu')
        .add($displayPane)
        .add(passagePane);

    // checks for collapser clicks
    containers.on('click', '.collapser', e => {
        const $target = $(e.currentTarget);
        const on = $target.attr('data-on');
        const off = $target.attr('data-off');
        const $parent = $target.parent();
        if ($parent.hasClass('collapsed')) {
            $target.text(on);
            $parent.removeClass('collapsed');
        } else {
            $target.text(off);
            $parent.addClass('collapsed');
        }
    });

    $(containers).on('click', '.destructor', e => {
        const $defaultMenu = $('#defaultMenu')
        $defaultMenu.attr('data-choice', 'options');
        $defaultMenu.find(':not(tw-element-resizer):not(.collapser)')
            .removeClass('active');
        $defaultMenu.find('#defaultOptions').addClass('active');
        $defaultMenu.find('tw-element-resizer')
            .attr('data-selector', '#defaultOptions');
        $(e.currentTarget).remove();
    });

    containers.on('change mousemove', 'input[type="range"]', e => {
        const $target = $(e.currentTarget);
        $(e.currentTarget).next('.rangeReadout').text($target.val());
    });

    $('#gotoAudio').click(e => {
        $defaultMenu.attr('data-choice', 'audio');
        $defaultMenu.find(':not(tw-element-resizer):not(.collapser)')
            .removeClass('active');
        $defaultMenu.find('#defaultAudioDisplay').addClass('active');
        $defaultMenu.find('tw-element-resizer')
            .attr('data-selector', 'tw-audio-display');
        if (!$(defaultMenu).find('.destructor').length) {
            $defaultMenu.append('<button class="destructor">X</button>');
        }
    });

    $('#gotoAccessibility').click(e => {
        $defaultMenu.attr('data-choice', 'accessibility');
        $defaultMenu.find(':not(tw-element-resizer):not(.collapser)')
            .removeClass('active');
        $defaultMenu.find('#defaultAccessibilityDisplay').addClass('active');
        $defaultMenu.find('tw-element-resizer')
            .attr('data-selector', '#defaultAccessibilityDisplay');
        if (!$(defaultMenu).find('.destructor').length) {
            $defaultMenu.append('<button class="destructor">X</button>');
        }
    });

    const $colorActive = $('#optionsBackgroundColorActive');
    $colorActive.click(() => {
        if ($colorActive.prop('checked')) {
            accessibilitySetBackgroundColor();
        } else {
            $('html, body, tw-story, tw-passage').css('background', '');
        }
    });

    $('.accessibilityInput').on('change mousemove', e => {
        const parentIDs = from($(e.currentTarget).parents())
            .map(parent => parent.id);

        if (parentIDs.indexOf('optionsFontSize') !== -1) {
            accessibilitySetFontSize(e.currentTarget.value);
        } else if (parentIDs.indexOf('optionsBackgroundColor') !== -1) {
            if (!$colorActive.prop('checked')) {
                return;
            }

            accessibilitySetBackgroundColor();
        } else if (parentIDs.indexOf('optionsBrightness') !== -1 ||
            parentIDs.indexOf('optionsContrast') !== -1 ||
            parentIDs.indexOf('optionsSaturation') !== -1)
        {
            const valueObj = {
                brightness: $('#optionsBrightness')
                    .find('.accessibilityInput').val(),
                contrast: $('#optionsContrast')
                    .find('.accessibilityInput').val(),
                saturation: $('#optionsSaturation')
                    .find('.accessibilityInput').val(),
            };

            accessibilitySetFilter(valueObj);
        }
    });
    
    let $htmls;

    window.accessibilitySetFontSize = () => {};
    if (document.body && document.body.parentElement) {
         $htmls = $(document.body.parentElement).add(frameHTML);
        window.accessibilitySetFontSize = value => {
            $htmls.css('font-size', value + '%');
        };
    }

    window.accessibilitySetBackgroundColor = () => {
        let colors = [];
        $('#optionsBackgroundColor').find('.accessibilityInput')
            .each((_, input) => {
                colors.push(input.value);
            });

        if (colors.length < 3) {
            throw new Error('One of the accessibility color ' +
                'sliders no longer exists. Cannot change color.');
        }

        const colorString = 'rgb(' +
            colors[0] + ', ' +
            colors[1] + ', ' +
            colors[2] + ')';
        $htmls.add($('body, tw-story, tw-passage'))
            .css('background', colorString);
    };

    window.accessibilitySetFilter = valueObject => {
        const filterString =
            'brightness(' + valueObject.brightness + ') ' +
            'contrast(' + valueObject.contrast + ') ' +
            'saturate(' + valueObject.saturation + ')';
        $('tw-story').css({
            '-webkit-filter': filterString,
            'filter': filterString,
        });
    };

    $('#gotoAddNodes').click(e => {
        $defaultMenu.attr('data-choice', 'addnodes');
        $defaultMenu.find(':not(tw-element-resizer):not(.collapser)')
            .removeClass('active');
        $defaultMenu.find('#defaultNodeadderDisplay').addClass('active');
        $defaultMenu.find('tw-element-resizer')
            .attr('data-selector', '#defaultNodeadderDisplay');
        if (!$(defaultMenu).find('.destructor').length) {
            $defaultMenu.append('<button class="destructor">X</button>');
        }
    });

    $('#addNodesAndSave').click(e => {
        const PIDs = [];
        let maxPID = 0;
        let $storydata = $('tw-storydata');
        const $storyPassages = $storydata.find('tw-passagedata');
        const names = [];
        for (let ii = 0; ii < $storyPassages.length; ii++) {
            const child = $storyPassages[ii];
            let pid = child.getAttribute('pid');
            if (!pid) {
                new wideAlert('One of the tw-passagedata nodes in the loaded story ' +
                    'lacks a pid attribute. Cannot add nodes.');
                return;
            }

            pid = Number.parseInt(pid); 
            if (Number.isNaN(pid)) {
                new wideAlert('One of the tw-passagedata nodes in the loaded ' +
                    'story has a pid attribute whose value is not a string ' +
                    'representation of an integer (1, 2, 3, etc.). Cannot ' +
                    'add nodes.');
                return;
            } else if (PIDs.indexOf(pid) !== -1) {
                new wideAlert('At least two of the tw-passagedata nodes in the ' +
                    'loaded story have an identical pid attribute. Cannot ' +
                    'add nodes.');
                return;
            }

            PIDs.push(pid);
            maxPID = Math.max(maxPID, pid);

            const name = child.getAttribute('name');
            if (!name) {
                wideAlert('One of the tw-passagedata nodes in the loaded story ' +
                    'lacks a name attribute. Cannot add nodes.');
                return;
            }

            if (names.indexOf(name) !== -1) {
                new wideAlert('At least two of the tw-passagedata nodes in the ' +
                    'loaded story have an identical name attribute. Cannot ' +
                    'add nodes.');
                return;
            }

            names.push(name);
        }

        // go to the next unused value
        maxPID++;

        const $textarea = $(e.target).parent().find('#nodeAdderTextarea');
        const content = $textarea.val();
        const splitContent = content.split('::').filter(a => a.trim() !== '');
        
        const newPassages = [];
        for (let ii = 0; ii < splitContent.length; ii++) {
            const content = splitContent[ii];
            const passageName = content.split(/\s/)[0];
            const passageText = content.slice(content.search(/\n/) + 1);

            if (passageName && names.indexOf(passageName) !== -1) {
                new wideAlert('The name, ' + passageName + ', already exists ' +
                    'in a tw-passagedata within the presently loaded ' +
                    'story. Cannot add nodes.');
                return;
            }

            newPassages.push({
                text: passageText,
                name: passageName,
                pid: null,
            });
        }

        const $html = $(document.body.parentElement.cloneNode(true));
        const selector = 'body > :not(tw-storydata), head';
        $html.find(selector).remove();
        $storydata = $html.find('tw-storydata');
        $storydata.attr('flags', 'nodeexpanded');
        let index = 0;
        newPassages.forEach(passage => {
            const pid = maxPID++;
            if (!passage.name) {
                passage.name = 'nodeMungerTemp' + pid;
            }

            const $newPassage = $('<tw-passagedata></tw-passagedata>');
            $newPassage.attr({
                pid,
                name: passage.name,
                position: '1000, ' + 80 * index++,
                tags: '',
            });
            
            $newPassage.text(passage.text);

            $storydata.append($newPassage);
        });

        saveFile(
            $html.find('tw-storydata').attr('name') + 'Expanded',
            'html',
            $html[0].outerHTML);
    });

    $('#toggleTranscript').click(e => {
        Options.toggleTranscriptRecording();
        if (State.recordingTranscript) {
            e.currentTarget.innerHTML = 'Off';
            $('#printTranscriptToConsole').removeClass('hidden');
        } else {
            e.currentTarget.innerHTML = 'On';
            $('#printTranscriptToConsole').addClass('hidden');
        }
    });

    $('#gotoTranscript').click(() => {
        $defaultMenu.attr('data-choice', 'transcript');
        $defaultMenu.find(':not(tw-element-resizer):not(.collapser)')
            .removeClass('active');
        $defaultMenu.find('#defaultTranscriptDisplay').addClass('active');
        Options.updateTranscriptVisuals();
        $defaultMenu.find('tw-element-resizer')
            .attr('data-selector', 'tw-transcript-display');

        if (!$(defaultMenu).find('.destructor').length) {
            $defaultMenu.append('<button class="destructor">X</button>');
        }
    });

    $('#printTranscript').click(() => {
        console.log(Options.printTranscript('text'));
    });

    $(frameBody).on('click', '[data-transcript-identifier]', e => {
        Options.setTranscriptDisplay(e.target);
    });

    $(frameBody).on('click', '.spanner', e => {
        const tgt = e.target;
        if (tgt && tgt.parentNode && tgt.parentNode.nextSibling) {
            const toClick =
                tgt.parentNode.nextSibling.querySelector('.collapser');
            if (toClick) {
                toClick.click();
            }
        }
    });

    window.gatelyElementResizing = null;
    $('tw-element-resizer').mousedown(e => {
        window.gatelyElementResizing = e.target;
        $(this).addClass('active');
    });

    window.widthElementToPercentage = (element, e) => {
        if (!element) {
            gatelyLog('The element argument passed to ' +
                'window.widthMenuElementToPercentage is not valid. Cannot ' +
                'change element parent width.');
            return;
        }

        const width = $('html').width() - e.screenX;
        $($(element).attr('data-selector')).css({
            'width': width + 'px',
            'width': 'calc(' + width + 'px - 4.67em)',
        });
    };

    $('#saveGame').click(e => {
        saveFile($('#saveFilename').val(), 'html');
        $('#saveFilename').val('');
    });

    $('#saveJSONP').click(() => {
        const str = 'gatelyInclude("' + $('tw-storydata').html().replace(/"/g, '\\"').replace(/'/g, "\\'").replace(/\n/g, '\\n').replace(/\t/g, '\\t').replace(/\r/g, '\\r') + '")';

        saveFile($('#saveJSONPFilename').val(), 'js', str);
        $('#saveJSONPFilename').val('');
    });
}());