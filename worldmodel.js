(function() {
    'use strict';

    // add errors and types collections to the virtual passage state
    const present = State.stacks.present;
    present.virtualPassageState.errors = {};
    present.virtualPassageState.types = {};

    Macro.addNamespace('world',
        null,
        function(actionType) {
            if (!actionType) {
                wideAlert('The actionType value passed to ' +
                    'Macro.world.handler is not valid.');
                return;
            } else if (typeof(actionType) !== 'string') {
                wideAlert('The actionType value passed to ' +
                    'Macro.world.handler is not a string. ' +
                    'Cannot handle macro.');
                return;
            }

            actionType = actionType.trim();
            const action = worldActions[actionType];
            const args = from(arguments).slice(1);

            if (isFunction(action)) {
                return action.apply(
                    {
                        twine: 'twine',
                        node: this,
                    },
                    args);
            } else {
                wideAlert('Can\'t do worldmodel action: ' +
                    action +
                    '. No handler exists for this action.');
            }
        }
    );

    Macro.add('add',
        {
            handler(name, type, objectOrParentModelArg) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }

                let context = 'Macro.world.add';
                if (typeof(name) !== 'string' || name === '') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The name argument provided to ' +
                            'Macro.world.add is not valid.'));
                } else if (/[#!.:*?%@]/.test(name)) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The name argument provided to ' +
                            'Macro.world.add contains one of ' +
                            'the reserved characters. The reserved ' +
                            'characters for names and types are ' +
                            '#, ., :, !, *, ?, %, and @.'));
                } else if (!Number.isNaN(Number(name))) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'You cannot give a name that is only ' +
                            'numbers to a model, given that this could ' +
                            'conflict with current or future model ID ' +
                            'values.'));
                } else if (typeof(type) !== 'string' || type === '') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The type argument provided to ' +
                            'Macro.world.add is not valid.'));
                } else if (/[#!.:*?%@]/.test(type)) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The type argument provided to ' +
                            'Macro.world.add contains one of the ' +
                            'reserved characters. The reserved ' +
                            'characters for names and types are ' +
                            '#, ., :, !, *, ?, %, and @.'));
                } else if (!worldActions.validateSetValue(name) ||
                    !worldActions.validateSetValue(type) ||
                    !worldActions.validateSetValue(
                        objectOrParentModelArg))
                {
                    let names = getOwnPropertyNames(reserved);
                    // do not return array prototype properties and only
                    // return strings
                    names = names
                        .filter(aa => !Array.prototype.hasOwnProperty(aa) &&
                            typeof(reserved[aa]) === 'string')
                        .map(aa => reserved[aa])
                        .join(', ');

                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'You cannot add an object which shares ' +
                            'a name with one of the reserved words. ' +
                            'The reserved words are: ' + names + '.'));
                }

                let existingModel = worldActions.get('#' + name);
                
                if (existingModel.type !== 'error') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'There is already a model by that name. ' +
                            'Each model must have a unique name.'));
                } else {
                    let constructor = modelConstructors[type];
                    if (!constructor) {
                        constructor = modelConstructors.model;
                    }

                    let model = new constructor(name, type);
                    present.virtualPassageState.all[name] = model;

                    if (!(model.type in present.virtualPassageState.types)) {
                        present.virtualPassageState.types[model.type] = {};
                    }

                    present.virtualPassageState.types[model.type][model.name] =
                        '#' + model.name;
                  
                    if (objectOrParentModelArg) {
                        let parent = worldActions.get(objectOrParentModelArg);

                        if (!parent || parent.type === 'error') {
                            const type = model.type;
                            const curState = present.virtualPassageState;
                            if (type in curState.types) {
                                delete curState.types[type][model.name]
                                delete curState.types[type][model.id]
                            }

                            delete curState.all[model.name];
                            delete curState.all[model.id];

                            return worldActions.formatOutput(
                                this, parent);
                        }

                        worldActions.take(parent, model);
                    }

                    return worldActions.formatOutput(this, model);
                }
            }
        },
        'world'
    );


    Macro.add('remove',
        {
            handler(modelArg) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }

                let model = worldActions.get(modelArg);
                let context = 'Macro.world.remove';
                if (!worldActions.validateSetValue(modelArg) ||
                    !worldActions.validateSetValue(model))
                {
                    let names = getOwnPropertyNames(reserved);
                    names = names
                        .filter(aa => !Array.prototype.hasOwnProperty(aa) &&
                            !reserved[aa].name)
                        .map(aa => reserved[aa])
                        .join(', ');

                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'You cannot remove an object that is, or ' +
                            'is named, one of the reserved words. The ' +
                            'reserved words are: ' + names + '.'));
                }

                if (!model || model.type === 'error') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The modelArg argument passed to ' +
                            'Macro.world.remove is not valid, or ' +
                            'the model does not exist: ' +
                            modelArg + '.'));
                }


                const type = model.type;
                const curState = present.virtualPassageState;
                if (type in curState.types) {
                    delete curState.types[type][model.name]
                    delete curState.types[type][model.id]
                }

                delete curState.all[model.name];
                delete curState.all[model.id];

                model.parentSelector = null;

                return worldActions.formatOutput(this, model);
            },
        },
        'world'
    );

    Macro.add('get',
        {
            handler(modelArg, propertyName) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }

                let context = 'Macro.world.get';
              
                if (!modelArg) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The modelArg argument passed to ' +
                            'Macro.world.get is not valid.'));
                } else if (typeof(modelArg) === 'string') {
                    modelArg = modelArg.replace(/&gt;/g, '>');
                }
                
                let model = modelArg;
                if (typeof(model) === 'string') {
                    try {
                        model = saferJSONParse(model);
                    } catch (e) {
                        // do nothing
                    }
                }

                let selector;
                if (typeof(model) === 'string' &&
                    /[#!.:*?]/.test(model.trim()[0]))
                {
                    selector = model.trim().split(/\s/);
                }

                if (selector &&
                    selector.length)
                {
                    model = worldActions.processSelector(selector);
                }

                if (worldActions.validateModel(model) &&
                    model.type !== 'collection')
                {
                    if (model.generic === true) {
                        model =
                            present.virtualPassageState.all[model.id];
                    } else {
                        model =
                            present.virtualPassageState.all[model.name];
                    }
                }

                if (!worldActions.validateModel(model)) {
                    return {
                        type: 'error',
                        message: 'The modelArg argument passed to ' +
                        'Macro.world.get does not correspond to ' +
                        'an existing model.'
                    };
                } else if (propertyName) {
                    if (propertyName[0] === '@') {
                        return worldActions.formatOutput(this,
                            worldActions.getAlias(
                                model,
                                propertyName.slice(1)));
                    } else {
                        return worldActions.formatOutput(this,
                            model[propertyName]);
                    }
                } else {
                    return worldActions.formatOutput(this, model);
                }
            },
        },
        'world'
    );

    Macro.add('processselector',
        {
            handler(selector) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }

                const context = 'Macro.world.processselector/get';

                // if the argument is a string and not an array, convert it
                // to an array
                if (typeof(selector) === 'string') {
                    selector = selector
                        .split(' ')
                        .filter(temp => temp !== '');
                }

                if (!('0' in selector)) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(context,
                            from(arguments),
                            'The selector argument passed to ' +
                            'Macro.world.processselector is not ' +
                            'valid. The argument must be a non-empty string' +
                            'or array.'));
                }

                const store = {
                    type: 'collection',
                    children: {}
                };

                let model;

                let componentType = selector[0][0];
                let componentValue = selector[0].slice(1);
                const curVPState = present.virtualPassageState;
                if (componentType === '#') {
                    model = curVPState.all[componentValue];
                    if (model === undefined &&
                        !Number.isNaN(Number(componentValue)))
                    {
                        const names = getOwnPropertyNames(curVPState.all);
                        model = names
                            .map(name => curVPState.all[name])
                            .filter(model => model.id ===
                                Math.floor(Number(componentValue))
                            )[0];
                    }
                } else if (componentType === '?') {
                    const player = curVPState.all.player;
                    if (!('current' in player)) {
                        throw new Error('There is no current property in ' +
                            'the player model. Cannot perform ? selectors.');
                    } else if (!(componentValue in player.current)) {
                        return worldActions.formatOutput(this,
                            new ErrorModel(
                                context,
                                from(arguments),
                                'There is no current property by the name ' +
                                componentValue +
                                ' in the player\'s current property.'));
                    }

                    const current = player.current;
                    const name = current[componentValue].slice(1);
                    model = curVPState.all[name];
                } else if (componentType === '.') {
                    model = {
                        type: 'collection',
                        children: {}
                    };

                    if (!(componentValue in curVPState.types)) {
                        wideAlert('There is no virtual passage store of ' +
                            'type ' + componentValue + '.');
                        return;
                    }

                    const namesOrIDs =
                        getOwnPropertyNames(curVPState.types[componentValue]);
                    namesOrIDs.forEach(nameOrID => {
                        model.children[nameOrID] = '#' + nameOrID;
                    });
                } else if (componentType === '*' && componentValue === '') {
                    model = {
                        type: 'collection',
                        children: {}
                    };

                    const names = getOwnPropertyNames(curVPState.all);
                    names.forEach(nameOrID => {
                        model.children[nameOrID] = '#' + nameOrID;
                    });
                } else if (componentType === '!') {
                    if (componentValue === '*') {
                        return worldActions.formatOutput(this,
                            new ErrorModel(context,
                                from(arguments),
                                'The * symbol cannot be part of a not (!) ' +
                                'selector.'));
                    } else if (componentValue[0] === '#') {
                        model = {
                            type: 'collection',
                            children: {}
                        };

                        const namesOrIDs =
                            getOwnPropertyNames(curVPState.all);
                        namesOrIDs.forEach(nameOrID => {
                            if (componentValue.slice(1) !== nameOrID) {
                                model.children[nameOrID] = '#' + nameOrID;
                            }
                        });
                    } else if (componentValue[0] === '?') {
                        const player = curVPState.all.player;
                        const nameOrID = componentValue.slice(1);
                        if (!('current' in player)) {
                            throw new Error('There is no current property ' +
                                'in the player model. Cannot perform ? ' +
                                'selectors.');
                        } else if (!(nameOrID in player.current)) {
                            return worldActions.formatOutput(this,
                                new ErrorModel(
                                    context,
                                    from(arguments),
                                    'There is no current property by the' +
                                    'name ' + componentValue + ' in the ' +
                                    'player\'s current property.'));
                        }

                        const newNameOrID = player.current[nameOrID].slice(1);
                        const current = curVPState.all[newNameOrID];
                        model = {
                            type: 'collection',
                            children: {}
                        };

                        const namesOrIDs = getOwnPropertyNames(curVPState.all);
                        namesOrIDs.forEach(nameOrID => {
                            if (current.name !== nameOrID &&
                                current.id !== nameOrID)
                            {
                                model.children[nameOrID] = '#' + nameOrID;
                            }
                        });
                    } else if (componentValue[0] === '.') {
                        model = {
                            type: 'collection',
                            children: {}
                        };

                        const namesOrIDs =
                            getOwnPropertyNames(curVPState.all);
                        namesOrIDs.forEach(nameOrID => {
                            if (curVPState.all[nameOrID].type !==
                                componentValue.slice(1))
                            {
                                model.children[nameOrID] = '#' + nameOrID;
                            }
                        });
                    } else if (componentValue[0] === ':') {
                        return worldActions.formatOutput(this,
                            new ErrorModel(
                                context,
                                from(arguments),
                                'A property cannot be part of a not ' +
                                'selector.'));
                    }
                } else if (componentType === ':') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'A property cannot be the first portion of a ' +
                            'selector.'));
                }

                if (!model) {
                    return {
                        context,
                        type: 'error',
                        message: 'The model(s) indicated by the ' +
                            componentType + componentValue +
                            ' portion of the selector do not exist.',
                    };
                }

                for (let ii = 1; ii < selector.length; ii++) {
                    store.children = {};
                    let toCheck = 'descendant';
                    if (selector[ii - 1] === '>') {
                        toCheck = 'child';
                    }

                    componentType = selector[ii][0];
                    componentValue =
                        selector[ii].slice(1, selector[ii].length);
                    if (componentType === '>' &&
                        componentValue === '' &&
                        ii + 1 in selector)
                    {
                        continue;
                    }

                    if (componentType === '#' &&
                        componentValue === 'root')
                    {
                        if (model.type === 'collection') {
                            return worldActions.formatOutput(this,
                                new ErrorModel(
                                    context,
                                    from(arguments),
                                    'It is not possible to produce a root ' +
                                    'from a collection of models.'));
                        }

                        let parents = [];
                        let newParent = model.parent();
                        while (newParent && newParent.type !== 'error') {
                            const parentSelector = newParent.parentSelector;
                            if (parents.indexOf(parentSelector) !== -1 ||
                                parents.indexOf(newParent.parent()) !== -1)
                            {
                                throw new Error('A parent of one of the ' +
                                    'specified models has a corrupted ' +
                                    'descendence tree and contains itself, ' +
                                    'or its selector, as a parent.');
                            }

                            parents.push(newParent.selector());
                            parents.push(newParent);
                            model = newParent;
                            newParent = newParent.parent();
                        }
                    } else if (componentType === '#') {
                        if (toCheck === 'child' ||
                            model.type === 'collection')
                        {
                            if (!('children' in model)) {
                                throw new Error('The model indicated by ' +
                                    'the portion of the selector prior to ' +
                                    componentType + componentValue +
                                    ' does not have a children property.');
                            }
                           
                            const names = getOwnPropertyNames(model.children);
                            let temp = null;
                            for (let ii = 0; ii < names.length; ii++) {
                                const nameOrID = names[ii];
                                if (!(nameOrID in model.children)) {
                                    break;
                                }

                                const childName =
                                    model.children[nameOrID].slice(1);
                                temp = curVPState.all[childName];
                            }

                            model = temp;
                        } else {
                            if (!('descendants' in model)) {
                                throw new Error('The model indicated by ' +
                                    'the portion of the selector prior to ' +
                                    componentType + componentValue +
                                    ' does not have a descendants property.');
                            }

                            const names =
                                getOwnPropertyNames(model.descendants);
                            let temp = null;
                            for (let ii = 0; ii < names.length; ii++) {
                                const nameOrID = names[ii];
                                if (!(nameOrID in model.descendants)) {
                                    break;
                                }

                                const descendantName =
                                    model.descendants[nameOrID].slice(1);
                                temp = curVPState.all[descendantName];
                            }

                            model = temp;
                        }
                    } else if (componentType === '@') {
                        model = worldActions.getAlias(model, componentValue);
                    } else if (componentType === '?') {
                        return worldActions.formatOutput(this,
                            new ErrorModel(
                                context,
                                from(arguments),
                                'A current (?) selector cannot occur after ' +
                                'the first portion of a selector.'));
                    } else if (componentType === '!') {
                        if (componentValue === '#root') {
                            return worldActions.formatOutput(this,
                                new ErrorModel(context,
                                    from(arguments),
                                    'A #root selector cannot have NOT (!) ' +
                                    'applied to it.'));
                        } else if (componentValue[0] === '#') {
                            if (toCheck === 'child' ||
                                model.type === 'collection')
                            {
                                if (!model.children) {
                                    throw new Error('The model indicated ' +
                                        'by the portion of the selector ' +
                                        'prior to ' +
                                        componentType + componentValue +
                                        ' does not have a children ' +
                                        'property.');
                                }
                                
                                const children = model.children;
                                const namesOrIDs =
                                    getOwnPropertyNames(children);
                                namesOrIDs.forEach(nameOrID => {
                                    if (componentValue.slice(1) !== nameOrID) {
                                        store.children[nameOrID] =
                                            '#' + nameOrID;
                                    }
                                });
                                model = store;
                            } else {
                                if (!('descendants' in model)) {
                                    throw new Error('The model indicated ' +
                                        'by the portion of the selector ' +
                                        'prior to ' +
                                        componentType + componentValue +
                                        ' does not have a descendants ' +
                                        'property.');
                                }

                                const descendants = model.descendants;
                                const namesOrIDs =
                                    getOwnPropertyNames(descendants);
                                namesOrIDs.forEach(nameOrID => {
                                    const sliced = componentValue.slice(1);
                                    if (sliced !== nameOrID) {
                                        store.children[nameOrID] =
                                            '#' + nameOrID;
                                    }
                                });
                                model = store;
                            }
                        } else if (componentValue[0] === '?') {
                            return worldActions.formatOutput(this,
                                new ErrorModel(context,
                                    from(arguments),
                                    'A current (?) selector cannot occur ' +
                                    'after the first portion of a ' +
                                    'selector.'));
                        } else if (componentValue[0] === '.') {
                            if (toCheck === 'child' ||
                                model.type === 'collection')
                            {
                                if (!('children' in model)) {
                                    throw new Error('The model indicated ' +
                                        'by the portion of the selector ' +
                                        'prior to ' +
                                        componentType + componentValue +
                                        ' does not have a children ' +
                                        'property.');
                                }
                                const children = model.children;
                                const namesOrIDs =
                                    getOwnPropertyNames(children);
                                namesOrIDs.forEach(nameOrID => {
                                    if (curVPState.all[nameOrID].type !==
                                        componentValue.slice(1))
                                    {
                                        store.children[nameOrID] =
                                            '#' + nameOrID;
                                    }
                                });
                                model = store;
                            } else {
                                if (!('descendants' in model)) {
                                    throw new Error('The model indicated ' +
                                        'by the portion of the selector ' +
                                        'prior to ' +
                                        componentType + componentValue +
                                        ' does not have a descendants ' +
                                        'property.');
                                }
                                const descendants = model.descendants;
                                const namesOrIDs =
                                    getOwnPropertyNames(descendants);
                                namesOrIDs.forEach(nameOrID => {
                                    if (curVPState.all[nameOrID].type !==
                                        componentValue.slice(1))
                                    {
                                        store.children[nameOrID] =
                                            '#' + nameOrID;
                                    }
                                });
                                model = store;
                            }
                        } else if (componentValue[0] === ':') {
                            return worldActions.formatOutput(this,
                                new ErrorModel(context,
                                    from(arguments),
                                    'A property cannot be part of a not ' +
                                    'selector.'));
                        }
                        model = store;
                    } else if (componentType === '.') {
                        if (toCheck === 'child' ||
                            model.type === 'collection')
                        {
                            if (!('children' in model)) {
                                throw new Error('The model indicated by ' +
                                    'the portion of the selector prior to ' +
                                    componentType + componentValue +
                                    ' does not have a children property.');
                            }

                            const namesOrIDs =
                                getOwnPropertyNames(model.children);
                            namesOrIDs.forEach(nameOrID => {
                                if (curVPState.types[componentValue] &&
                                    typeof(curVPState.types[componentValue]) === 'object' &&
                                    nameOrID in curVPState.types[componentValue])
                                {
                                    store.children[nameOrID] = '#' + nameOrID;
                                }
                            });
                            model = store;
                        } else {
                            if (!('descendants' in model)) {
                                throw new Error('The model indicated by ' +
                                    'the portion of the selector prior to ' +
                                    componentType + componentValue +
                                    ' does not have a descendants property.');
                            }

                            const namesOrIDs =
                                getOwnPropertyNames(model.descendants);
                            namesOrIDs.forEach(nameOrID => {
                                if (nameOrID in curVPState.types[componentValue]) {
                                    store.children[nameOrID] = '#' + nameOrID;
                                }
                            });
                            model = store;
                        }
                    } else if (componentType === ':') {
                        model = model[componentValue];
                    } else if (componentType === '*' &&
                        componentValue === '')
                    {
                        if (toCheck === 'child' ||
                            model.type === 'collection')
                        {
                            if (!('children' in model)) {
                                throw new Error('The model indicated by ' +
                                    'the portion of the selector prior to ' +
                                    componentType + componentValue +
                                    ' does not have a children property.');
                            }
                            model = {
                                type: 'collection',
                                children: model.children
                            };
                        } else {
                            if (!('descendants' in model)) {
                                throw new Error('The model indicated by ' +
                                    'the portion of the selector prior to ' +
                                    componentType + componentValue +
                                    ' does not have a descendants property.');
                            }
                            model = {
                                type: 'collection',
                                children: model.descendants
                            };
                        }
                    } else {
                        return worldActions.formatOutput(this,
                            new ErrorModel(
                                context,
                                from(arguments),
                                'The character indicating the selector ' +
                                'portion type ' +
                                componentType +
                                ' is not valid.'));
                    }
                    if (model === undefined ||
                        model === null)
                    {
                        return worldActions.formatOutput(this,
                            new ErrorModel(
                                context,
                                from(arguments),
                                'The model(s) indicated by the ' +
                                componentValue +
                                ' portion of the selector do not exist.'));
                    }
                }

                return worldActions.formatOutput(this, model);
            }
        },
        'world'
    );
    Macro.world.processSelector = Macro.world.processselector;
    Macro.world['process-selector'] = Macro.world.processselector;

    Macro.add('set',
        {
            handler(modelArg, propertyName, value) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }
                let context = 'Macro.world.set';
                let model = worldActions.get(modelArg);
                if (!model || model.type === 'error') {
                    return {
                        type: 'error',
                        context: context,
                        message: 'The modelArg argument passed to ' +
                        'Macro.world.set is not valid.'
                    };
                } else if (!worldActions.validateSetValue(propertyName)) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'You can\'t set this value. The property ' +
                            propertyName + ' is reserved.'));
                }

                model[propertyName] = value;
                return worldActions.formatOutput(this, model);
            },
        },
        'world'
    );

    Macro.add('unset',
        {
            handler(modelArg, propertyName) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }
                let context = 'Macro.world.unset';
                let model = worldActions.get(modelArg);
                if (!model || model.type === 'error') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(context,
                            from(arguments),
                            'The modelArg argument passed to ' +
                            'Macro.world.set is not valid.'));
                } else if (!worldActions.validateSetValue(propertyName)) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(context,
                            from(arguments),
                            'You can\'t set this value. The property ' +
                            propertyName + ' is reserved.'));
                }

                delete model[propertyName];
                return worldActions.formatOutput(this, model);
            }
        },
        'world'
    );

    Macro.add('contains',
        {
            handler(parentModelArg, childModelArg, children) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }
                let context = 'Macro.world.contains';

                let parentModel = worldActions.get(parentModelArg);
                if (!parentModel || parentModel.type === 'error') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The parentModelArg passed to ' +
                            'Macro.world.contains is not valid.'));
                } else if (parentModel.type === 'collection') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The parentModelArg passed to ' +
                            'Macro.world.contains represents a ' +
                            'collection. In can only be used on single ' +
                            'parent models.'));
                }

                let childModel = worldActions.get(childModelArg);
                if (!childModel || childModel.type === 'error') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The childModelArg passed to ' +
                            'Macro.world.contains is not valid.'));
                }

                if (children === true || children === 'children') {
                    if (childModel.type === 'collection') {
                        const children = childModel.children;
                        const names = getOwnPropertyNames(children);
                        for (let ii = 0; ii < names.length; ii++) {
                            if (names[ii] in parentModel.children) {
                                return true;
                            }
                        }
                        return false;
                    } else {
                        const child = parentModel.children[childModel.name];
                        return typeof(child) === 'object';
                    }
                } else {
                    if (childModel.type === 'collection') {
                        let retVal = false;
                        const children = childModel.children;
                        const names = getOwnPropertyNames(children);
                        for (let ii = 0; ii < names.length; ii++) {
                            if (names[ii] in parentModel.descendants) {
                                return true;
                            }
                        }
                        return false;
                    } else {
                        return childModel.name in parentModel.descendants;
                    }
                }
            }
        },
        'world'
    );

    Macro.add('isset',
        {
            handler(modelArg, propertyName) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }
                let context = 'Macro.world.isSet';
                let model = worldActions.get(modelArg);
                if (model === undefined || model.type === 'error') {
                    return {
                        context,
                        type: 'error',
                        message: 'The modelArg argument passed to ' +
                            'Macro.world.isset is not valid.'
                        };
                }

                return propertyName in model;
            }
        },
        'world'
    );
    Macro.world.isSet = Macro.world.isset;
    Macro.world['is-set'] = Macro.world.isset;

    Macro.add('exists',
        {
            handler(modelArg) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }
                let model = worldActions.get(modelArg);

                if (model === undefined || model.type === 'error') {
                    return false;
                } else {
                    return true;
                }
            }
        },
        'world'
    );

    Macro.add('give',
        {
            handler(modelArg, receiverModelArg) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }

                return worldActions.take.call(this,
                    receiverModelArg,
                    modelArg);
            }
        },
        'world'
    );

    Macro.add('take',
        {
            handler(modelArg, childModelArg) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }
                let context = 'Macro.world.take';

                let owner = worldActions.get(modelArg);
                if (!owner || owner.type === 'error') {
                    return new ErrorModel(
                        context,
                        from(arguments),
                        'The model referred to by the modelArg ' +
                        'argument ' +
                        modelArg +
                        ' does not exist.');
                }

                let child = worldActions.get(childModelArg);
                if (child === undefined || child.type === 'error') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The model referred to by the ' +
                            'childModelArg argument ' +
                            childModelArg +
                            ' does not exist.'));
                } else if ('children' in owner &&
                    child.name in owner.children &&
                    (child.generic !== true ||
                        worldActions.get(
                            owner.children[child.name]).generic !== true))
                {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'There is already a model with the same ' +
                            'name in the provided owner, ' +
                            owner.name +
                            ', and one or both are not generic.'));
                }

                let forbidden =
                    worldActions.isForbidden(owner, 'take', child);
                if (forbidden) {
                    return worldActions.formatOutput(new ErrorModel(
                        context,
                        from(arguments),
                        'A model of type ' +
                        owner.type +
                        ' cannot take a model of type ' +
                        child.type + '.'));
                }

                let parents = [];

                let oldParent = child.parent();
                while (oldParent && oldParent.type !== 'error') {
                    const selectorIndex =
                        parents.indexOf(oldParent.parentSelector);
                    const parentIndex =
                        parents.indexOf(oldParent.parent());
                    if (selectorIndex !== -1 || parentIndex !== -1) {
                        throw new Error('A parent of one of the ' +
                            'specified models has a corrupted ' +
                            'descendence tree and contains itself, ' +
                            'or its selector, as a parent.');
                    }

                    parents.push(oldParent.selector());
                    parents.push(oldParent);

                    if (child.generic) {
                        delete oldParent.children[child.id];
                    } else {
                        delete oldParent.children[child.name];
                        delete oldParent.descendants[child.name];
                    }

                    oldParent = oldParent.parent();
                }

                child.parentSelector = owner.selector();

                parents = [];

                let newParent = owner;
                if (child.generic) {
                    if (!(child.name in newParent.children)) {
                        newParent.children[child.name] = '#' + child.id;
                    }

                    if (child.name in newParent.children) {
                        const _child = newParent.children[child.name];
                        worldActions.get(_child).quantity +=
                            _child.quantity;
                        worldActions.remove(_child);

                        return worldActions.formatOutput(this, owner);
                    }
                } else {
                    newParent.children[child.name] = '#' + child.name;
                }

                return worldActions.formatOutput(this, owner);
            }
        },
        'world'
    );

    Macro.add('isgeneric',
        {
            handler(modelArg) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }
                let context = 'Macro.world.isgeneric';

                let model = worldActions.get(modelArg);
                if (!model || model.type === 'error') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The model referred to by the modelArg ' +
                            'argument ' + modelArg + ' does not exist.'));
                }
                if (model.generic === true) {
                    return true;
                } else {
                    return false;
                }
            }
        },
        'world'
    );
    Macro.world.isGeneric = Macro.world.isgeneric;
    Macro.world['is-generic'] = Macro.world.isgeneric;

    Macro.add('makegeneric',
        {
            handler(modelArg) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }
                let context = 'Macro.world.makegeneric';

                let model = worldActions.get(modelArg);
                if (!model || model.type === 'error') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The model referred to by the modelArg ' +
                            'argument ' + modelArg + ' does not exist.'));
                }

                model.generic = true;

                const curVPState = present.virtualPassageState;

                // this model can no longer be searched by name
                delete curVPState.all[model.name];
                delete curVPState[model.type][model.name];

                curVPState.all[model.id] = model;
                curVPState[model.type][model.id] = '#' + model.id;

                let newParent = model.parent();
                if (typeof(newParent) === 'object' &&
                    newParent !== null &&
                    'children' in newParent)
                {
                    newParent.children[model.name] = '#' + model.id;
                }

                const parents = [];
                while (typeof(newParent) === 'object' &&
                    newParent !== null &&
                    newParent.type !== 'error')
                {
                    const selectorIndex =
                        parents.indexOf(oldParent.parentSelector);
                    const parentIndex =
                        parents.indexOf(oldParent.parent());
                    if (selectorIndex !== -1 || parentIndex !== -1) {
                        throw new Error('A parent of one of the ' +
                            'specified models has a corrupted ' +
                            'descendence tree and contains itself, ' +
                            'or its selector, as a parent.');
                    }
                    parents.push(newParent.selector());
                    parents.push(newParent);

                    delete newParent.descendants[model.name];
                  
                    newParent = newParent.parent();
                }

                return worldActions.formatOutput(this, model);
            }
        },
        'world'
    );
    Macro.world.makegeneric = Macro.world.makegeneric;
    Macro.world['make-generic'] = Macro.world.makegeneric;

    Macro.add('print',
        {
            handler(modelArg, string, newLine) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }
               
                let context = 'Macro.world.print';
                if (!modelArg) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The modelArg argument passed to ' +
                            'Macro.world.print is not valid.'));
                } else if (typeof(string) !== 'string' || string === '') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The string argument passed to ' +
                            'Macro.world.print is not valid.'));
                }

                let model = worldActions.get(modelArg);
                if (model.type === 'error') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The modelArg argument passed to ' +
                            'Macro.world.print does not ' +
                            'correspond to an existing model.'));
                }

                const elem = document.createElement('tw-match');
                elem.dataset.blockDefinitions =
                    JSON.stringify(State.getBlockScope(this.node));
                elem.className = 'worldmodelPrintContainer';
                elem.innerHTML = string;

                let treeWalker = document.createTreeWalker(
                    elem,
                    NodeFilter.SHOW_TEXT,
                    {
                        acceptNode: node => {
                            const parent = node.parentNode;
                            if (parent &&
                                parent.tagName !== 'SCRIPT' &&
                                parent.tagName !== 'STYLE')
                            {
                                return NodeFilter.FILTER_ACCEPT;
                            } else {
                                return NodeFilter.FILTER_REJECT;
                            }
                        },
                    },
                    false
                );

                let nodeList = [];
                while (treeWalker.nextNode()) {
                    nodeList.push(treeWalker.currentNode);
                }

                nodeList.forEach(node => {
                    let regex = /%(.+?)%/;
                    let match;
                    let string = node.textContent;

                    while ((match = regex.exec(string)) !== null) {
                        let inner = match[1].trim();

                        if (inner[0] === ':') {
                            inner = inner.slice(1, inner.length);
                        } else if (inner[0] === '|' &&
                            inner[inner.length - 1] === '|')
                        {
                            try {
                                inner = eval(inner);
                            } catch (e) {
                                wideAlert('The Javascript raw string within ' +
                                    'the Macro.world.print template ' +
                                    'string is not well-formed. Please ' +
                                    'note the compiler error in the next ' +
                                    'alert.');
                                wideAlert(e.message);
                                inner = e.message;
                            }
                        }

                        string = string.replace(match[0],
                            worldActions.get(model, inner));
                    }

                    regex = /\{\{\S?.+?\S?\}\}/;

                    while ((match = regex.exec(string)) !== null) {
                        let inner = match[0].trim().slice(2, -2);

                        let link = '<tw-link class="passage" passage-name="';

                        if (inner.indexOf('|') !== -1) {
                            const split = inner.split('|');
                            link += split[split.length - 1] + '">';
                            link +=
                                split.slice(0, -1).join('|') + '</tw-link>';
                        } else if (inner.indexOf('->') !== -1) {
                            const split = inner.split('->');
                            link += split[split.length - 1] + '">';
                            link += split.slice(0, -1).join('->') +
                                '</tw-link>';
                        } else if (inner.indexOf('<-') !== -1) {
                            const split = inner.split('<-');
                            link += split[0] + '">';
                            link += split.slice(1).join('<-') + '</tw-link>';
                        } else {
                            link += inner + '">' + inner + '</tw-link>';
                        }

                        string = string.replace(match[0], link);
                    }

                    $(node).replaceWith('<tw-match>' + string + '</tw-match>');
                });

                if (newLine === true ||
                    (typeof(newLine) === 'string' &&
                        newLine.toLowerCase() === 'newline'))
                {
                    elem.appendChild(document.createElement('br'));
                } else if (typeof(newLine) === 'number' && newLine > 0) {
                    for (let ii = 0; ii < newLine; ii++) {
                        elem.appendChild(document.createElement('br'));
                    }
                }

                Compiler.compile(elem);

                return elem;
            },
        },
        'world'
    );

    Macro.add('getalias',
        {
            handler(modelArg, propertyName) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }

                let context = 'Macro.world.getAlias';

                if (!modelArg) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The modelArg argument passed to ' +
                            'Macro.world.getAlias is not valid.'));
                }
                let model = worldActions.get(modelArg);
                if (!model || model.type === 'error') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The model referred to by the modelArg ' +
                            'argument passed to ' +
                            'Macro.world.getalias ' +
                            modelArg +
                            ' does not exist.'));
                }

                if (!('aliases' in model)) {
                    model.aliases = {};
                }

                if (!propertyName) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The propertyName argument passed to ' +
                            'Macro.world.getalias is not valid.'));
                }

                if (propertyName in model.aliases) {
                    return worldActions.formatOutput(this,
                        model.aliases[propertyName]);
                } else {
                    return worldActions.formatOutput(this,
                        model[propertyName]);
                }
            },
        },
        'world'
    );
    Macro.world.getAlias = Macro.world.getalias;
    Macro.world['get-alias'] = Macro.world.getAlias;

    Macro.add('addalias',
        {
            handler(modelArg, propertyName, alias) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }

                let context = 'Macro.world.addalias';

                if (!modelArg) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The modelArg argument passed to ' +
                            'Macro.world.addalias is not valid.'));
                }
                let model = worldActions.get(modelArg);
                if (!model || model.type === 'error') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The model referred to by the modelArg ' +
                            'argument ' + modelArg + ' does not exist.'));
                }

                if (!propertyName) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The propertyName argument passed to ' +
                            'Macro.world.addalias is not valid.'));
                } else if (!(propertyName in model)) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The model represented by the modelArg ' +
                            'argument provided to ' +
                            'Macro.world.addalias does not have a ' +
                            'property matching the propertyName ' +
                            'argument. Cannot add alias.'));
                }

                if (!('aliases' in model)) {
                    model.aliases = {};
                }

                if (typeof(alias) !== 'string' || alias === '') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The alias argument passed to ' +
                            'Macro.world.addAlias is not a valid, ' +
                            'non-empty string. Cannot add alias.'));
                }

                model.aliases[propertyName] = alias;

                return worldActions.formatOutput(this, model);
            },
        },
        'world'
    );
    Macro.world.addAlias = Macro.world.addalias;
    Macro.world['add-alias'] = Macro.world.addalias;

    Macro.add('removealias',
        {
            handler(modelArg, propertyName) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }

                let context = 'Macro.world.removealias';

                if (!modelArg) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The modelArg argument passed to ' +
                            'Macro.world.removealias is not ' +
                            'valid.'));
                }
                let model = worldActions.get(modelArg);
                if (!model || model.type === 'error') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The model referred to by the modelArg ' +
                            'argument ' + modelArg + ' does not exist.'));
                }

                if (!propertyName) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The propertyName argument passed to ' +
                            'Macro.world.removealias is not ' +
                            'valid.'));
                } else if (!(propertyName in model)) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The model represented by the modelArg ' +
                            'argument provided to ' +
                            'Macro.world.removealias does not ' +
                            'have a property matching the propertyName ' +
                            'argument. Cannot add alias.'));
                }

                if (!('aliases' in model)) {
                    model.aliases = {};
                }

                delete model.aliases[propertyName];

                return worldActions.formatOutput(this, model);
            }
        },
        'world'
    );
    Macro.world.removeAlias = Macro.world.removealias;
    Macro.world['remove-alias'] = Macro.world.removealias;

    Macro.add('associated',
        {
            handler(modelArg, association, associatedModelArg) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }

                let context = 'Macro.world.is';
                if (!modelArg) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The modelArg argument passed to ' +
                            'Macro.world.is is not valid.'));
                } else if (!association) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The association argument passed to ' +
                            'Macro.world.is is not valid.'));
                }

                let model = worldActions.get(modelArg);
                if (model.type === 'error') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The modelArg argument passed to ' +
                            'Macro.world.is does not correspond ' +
                            'to an existing model.'));
                }

                if (associatedModelArg) {
                    if (!(association in model.associations)) {
                        return false;
                    }

                    let associatedModel =
                        worldActions.get(associatedModelArg);
                    if (associatedModel.type === 'error') {
                        return worldActions.formatOutput(this,
                            new ErrorModel(
                                context,
                                from(arguments),
                                'The associatedModelArg argument ' +
                                'passed to Macro.world.is does ' +
                                'not correspond to an existing model.'));
                    }

                    if (model.associations[association]
                        .indexOf(associatedModel.selector()) === -1)
                    {
                        return false;
                    }

                    return true;
                } else {
                    let associateds = model.associations[association];
                    let collection = {
                        type: 'collection',
                        children: {}
                    };

                    if (!associateds) {
                        return worldActions.formatOutput(
                            this, collection);
                    }

                    for (let ii = 0; ii < associateds.length; ii++) {
                        collection.children[associateds[ii]] =
                            worldActions.get(associateds[ii]);
                    }

                    return worldActions.formatOutput(this, collection);
                }
            }
        },
        'world'
    );

    Macro.add('associate',
        {
            handler(modelArg, association, associatedModelArg, converse) {
                let context = 'Macro.world.associate';
                if (!modelArg) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The modelArg argument passed to ' +
                            'Macro.world.associate is not valid.'));
                } else if (!association) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The association argument passed to ' +
                            'Macro.world.associate is not valid.'));
                } else if (!associatedModelArg) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The associatedModelArg argument passed to ' +
                            'Macro.world.associate is not valid.'));
                }

                let model = worldActions.get(modelArg);
                if (model.type === 'error') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The modelArg argument passed to ' +
                            'Macro.world.associate does not ' +
                            'correspond to an existing model.'));
                }

                let associatedModel =
                    worldActions.get(associatedModelArg);
                if (associatedModel.type === 'error') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The associatedModel argument passed to ' +
                            'Macro.world.associate does not ' +
                            'correspond to an existing model.'));
                }
                if (!(association in model.associations)) {
                    model.associations[association] = [];
                }
                const assoc = model.associations[association];
                if (assoc.indexOf(associatedModel.selector()) === -1) {
                    model.associations[association].push(
                        associatedModel.selector());
                }

                if (converse === true || converse === 'converse') {
                    let converse = worldActions.getConverse(association);
                    if (converse.type === 'error') {
                        return worldActions.formatOutput(this, converse);
                    }
                    if (!associatedModel.associations[converse]) {
                        associatedModel.associations[converse] = [];
                    }
                    let temp = associatedModel.associations[converse];
                    if (temp && temp.indexOf(model.selector()) === -1) {
                        associatedModel.associations[converse].push(
                            model.selector());
                    }
                }

                return worldActions.formatOutput(this, model);
            }
        },
        'world'
    );

    Macro.add('disassociate',
        {
            handler(modelArg, association, associatedModelArg, converse) {
                let context = 'Macro.world.disassociate';
                if (!modelArg) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The modelArg argument passed to ' +
                            'Macro.world.disassociate is not ' +
                            'valid.'));
                } else if (!association) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The association argument passed to ' +
                            'Macro.world.disassociate is not ' +
                            'valid.'));
                } else if (!associatedModelArg) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The associatedModelArg argument passed ' +
                            'to Macro.world.disassociate is not ' +
                            'valid.'));
                }

                let model = worldActions.get(modelArg);
                if (model.type === 'error') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The modelArg argument passed to ' +
                            'Macro.world.disassociate does not ' +
                            'correspond to an existing model.'));
                }

                let associatedModel =
                    worldActions.get(associatedModelArg);
                if (associatedModel.type === 'error') {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            context,
                            from(arguments),
                            'The associatedModel argument passed to ' +
                            'Macro.world.disassociate does not ' +
                            'correspond to an existing model.'));
                }

                if (!(association in model.associations)) {
                    return worldActions.formatOutput(this, model);
                }

                let associations = model.associations[association];
                if (associations.indexOf(associatedModel) === -1) {
                    return worldActions.formatOutput(this, model);
                }

                const toConcat =
                    associations.slice(
                        associations.indexOf(associatedModel + 1));
                associations = associations
                    .slice(0, associations.indexOf(associatedModel))
                    .concat(toConcat);

                if (converse) {
                    if (!associatedModel.associations[association]) {
                        return worldActions.formatOutput(this, model);
                    }

                    let converse = worldActions.getConverse(association);
                    if (converse.type === 'error') {
                        return converse;
                    }

                    let associations =
                        associatedModel.associations[converse];
                    if (associations.indexOf(model) === -1) {
                        return worldActions.formatOutput(this, model);
                    }

                    const toConcat =
                        associations.slice(
                            associations.indexOf(model + 1));
                    associations = associations
                        .slice(0, associations.indexOf(model))
                        .concat(toConcat);
                }

                return worldActions.formatOutput(this, model);
            }
        },
        'world'
    );

    const converses = {
        north: 'south',
        east: 'west',
        south: 'north',
        west: 'east',
        above: 'below',
        below: 'above',
        left: 'right',
        right: 'left',
        forward: 'back',
        back: 'forward',
        over: 'under',
        under: 'over'
    };
    
    Macro.add('getconverse',
        {
            handler(normal) {
                if (!converses[normal]) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            'Macro.world.getconverse',
                            from(arguments),
                            'There is no converse relation for that ' +
                            'word.'));
                }

                return converses[normal];
            }
        },
        'world'
    );
    Macro.world.getConverse = Macro.world.getconverse;
    Macro.world['get-converse'] = Macro.world.getconverse;

    Macro.add('addconverse',
        {
            handler(normal, converse) {
                if (normal in converses) {
                    return worldActions.formatOutput(this,
                        new ErrorModel(
                            'Macro.world.addconverse',
                            from(arguments),
                            'There is already a converse relation for ' +
                            'that word. Remove the old one first.'));
                }

                converses[normal] = converse;
                return true;
            }
        },
        'world'
    );
    Macro.world.addConverse = Macro.world.addconverse;
    Macro.world['add-converse'] = Macro.world.addconverse;

    Macro.add('removeconverse',
        {
            handler(name) {
                delete converses[name];
                return true;
            }
        },
        'world'
    );
    Macro.world.removeConverse = Macro.world.removeconverse;
    Macro.world['remove-converse'] = Macro.world.removeConverse;

    Macro.add('forbid',
        {
            handler(actorType, acteeType, action) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }

                if (!actorType) {
                    wideAlert('The actorType argument passed to ' +
                        'Macro.world.forbid is not valid.');
                    return false;
                } else if (actorType === '*') {
                    wideAlert('The only object that can have all actions ' +
                        'set to * is the builtin ErrorModel.');
                    return false;
                } else if (!acteeType) {
                    wideAlert('The acteeType argument passed to ' +
                        'Macro.world.forbid is not valid.');
                    return false;
                } else {
                    const curVPState = present.virtualPassageState;
                    let value = curVPState.forbidden[actorType];
                    if (!value) {
                        wideAlert('The actorType, ' +
                            actorType +
                            ', does not exist in ' +
                            'Macro.present.virtualPassageState' +
                            '.forbidden.');
                        return false;
                    }
                    value = value[acteeType];
                    if (!value) {
                        wideAlert('The acteeType, ' +
                            acteeType +
                            ', does not exist in ' +
                            'Macro.present.virtualPassageState' +
                            '.forbidden.' + actorType + '.');
                        return false;
                    }
                    if (value.indexOf(action) === -1) {
                        value.push(action);
                    }
                }
                return true;
            }
        },
        'world'
    );

    Macro.add('unforbid',
        {
            handler(actorType, acteeType, action) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }

                if (!actorType) {
                    wideAlert('The actorType argument passed to ' +
                        'Macro.world.unforbid is not valid.');
                    return false;
                } else if (!acteeType) {
                    wideAlert('The acteeType argument passed to ' +
                        'Macro.world.unforbid is not valid.');
                    return false;
                } else if (!action) {
                    wideAlert('The action argument passed to ' +
                        'Macro.world.unforbid is not valid.');
                    return false;
                } else {
                    const curVPState = present.virtualPassageState;
                    let value = curVPState.forbidden[actorType];
                    if (!value) {
                        wideAlert('The actorType, ' +
                            acteeType +
                            ', does not exist in Macro.State' +
                            '.stack.current.virtualPassageState.forbidden.');
                        return false;
                    }

                    value = value[acteeType];
                    if (!value) {
                        wideAlert('The action, ' +
                            action +
                            ', does not exist in ' +
                            'Macro.present.virtualPassageState' +
                            '.forbidden.' + actorType + '.');
                        return false;
                    } else if (!value.filter) {
                        wideAlert('The specified collection in ' +
                            'Macro.present.virtualPassageState' +
                            '.forbidden.' +
                            actorType + '.' + acteeType +
                            ' lacks a filter function property, ' +
                            'and is therefore invalid.');
                        return false;
                    }

                    value = value.filter(act => act !== action);
                }

                return true;
            }
        },
        'world'
    );

    Macro.add('isforbidden',
        {
            handler(actorType, action, acteeType) {
                for (let ii = 0; ii < arguments.length; ii++) {
                    if (arguments[ii] && arguments[ii].type === 'error') {
                        return arguments[ii];
                    }
                }

                if (actorType && actorType.type) {
                    actorType = actorType.type;
                }
                if (acteeType && acteeType.type) {
                    acteeType = acteeType.type;
                }

                if (!actorType) {
                    wideAlert('The actorType argument passed to ' +
                        'Macro.world.isForbidden is not valid.');
                    return false;
                } else if (!action) {
                    wideAlert('The action argument passed to ' +
                        'Macro.world.isForbidden is not valid.');
                    return false;
                } else if (!acteeType) {
                    wideAlert('The acteeType argument passed to ' +
                        'Macro.world.isForbidden is not valid.');
                    return false;
                } else {
                    const curVPState = present.virtualPassageState;
                    let value = curVPState.forbidden[actorType];
                    if (!value) {
                        //gatelyLog('The actorType, ' + actorType +
                        //', does not exist in Macro.State' +
                        //'.stack.current.virtualPassageState.forbidden.');
                        return false;
                    } else if (value === '*') {
                        return true;
                    }

                    value = value[action];
                    if (!value) {
                        //gatelyLog('The acteeType, ' + acteeType +
                        //', does not exist in Macro.State' +
                        //'.stack.current.virtualPassageState.forbidden.' +
                        //actorType + '.');
                        return false;
                    } else if (value === '*') {
                        return true;
                    }

                    return from(value).indexOf(acteeType) !== -1;
                }
            }
        },
        'world'
    );
    Macro.world.isForbidden = Macro.world.isforbidden;
    Macro.world['is-forbidden'] = Macro.world.isforbidden;

    Macro.add('populatefrompassages',
        {
            handler(type) {
                const vpoState = present.virtualPassageState;
                for (let name in vpoState.all) {
                    const vpo = vpoState.all[name];
                    if (!vpo || !vpo.tags || typeof(vpo.tags) !== 'object') {
                        return;
                    }

                    const regex = /^(_world-)(.+)/;
                    for (let ii = 0; ii < vpo.tags.length; ii++) {
                        const tag = vpo.tags[ii];
                        const match = tag.match(regex);
                        if (match && type === match[2]) {
                            vpo.type = match[2];
                            vpo.associations = {};
                            vpo.aliases = {};
                            vpo._internalSelector = '#' + name;
                            vpo.generic = false;
                            vpo.quantity = 1;
                            vpo.id = getNextUnusedId();
                            vpo.isWorldModel = true;
                            vpo.isWorldModel = true;

                            if (!(vpo.type in vpoState.types)) {
                                vpoState.types[vpo.type] = {};
                            }

                            vpoState.types[vpo.type][vpo.name] =
                                '#' + vpo.name;
                        }
                    }
                }
            }
        },
        'world'
    );
    Macro.world.populateFromPassages = Macro.world.populatefrompassages;
    Macro.world['populate-from-passages'] = Macro.world.populatefrompassages;

    /* Macro.add('definehandler',
        {
            handler(name, codeStr, isDoubleTag) {
                name = name.trim();
                if (typeof(name) !== 'string' || name === '') {
                    wideAlert('The name argument passed to ' +
                        'Macro.world.defineHandler is not valid.');
                    return false;
                } else if (typeof(codeStr) !== 'string' || name === '') {
                    wideAlert('The codeStr argument passed to ' +
                        'Macro.world.defineHandler is empty or ' +
                        'not a valid string.');
                    return false;
                }
                try {
                    worldModel[name] = {
                        isDoubleTag: isDoubleTag,
                        handler: new Function(codeStr),
                    };
                } catch (e) {
                    wideAlert(e.message);
                    return false;
                }
                return true;
            }
        },
        'world'
    );
    Macro.world.defineHandler = Macro.world.definehandler;
    Macro.world['define-handler'] = Macro.world.definehandler; */

    Macro.add('validatemodel',
        {
            handler(model) {
                if (!model || typeof(model) !== 'object') {
                    return false;
                }

                const jsType = typeof(model) === 'object' && model !== null;
               
                const modelType = 'type' in model &&
                    typeof(model.type) === 'string' &&
                    model.type &&
                    model.type !== 'error';
               
                const modelName = 'name' in model &&
                    typeof(model.name) === 'string' &&
                    model.name;

                if (model.name === '_nothing') {
                    wideAlert('The _nothing model cannot be modified or ' +
                        'validated in any fashion, and attempting to ' +
                        'validate it will always return false.');
                    return false;
                }

                const modelChildren = 'children' in model &&
                    model.children &&
                    typeof(model.children) === 'object';

                const modelDescendants = 'descendants' in model &&
                    model.descendants &&
                    typeof(model.descendants) === 'object';

                const isCollection = 'type' in model &&
                    model.type === 'collection' &&
                    'children' in model &&
                    model.children &&
                    typeof(model.children) === 'object';

                const isWorldModel = model.isWorldModel === true;

                if ((jsType &&
                    modelType &&
                    modelName &&
                    modelChildren &&
                    modelDescendants &&
                    isWorldModel) ||
                    isCollection)
                {
                    return true;
                } else {
                    return false;
                }
            },
        },
        'world'
    );
    Macro.world.validateModel = Macro.world.validatemodel;
    Macro.world['validate-model'] = Macro.world.validatemodel;

    Macro.add('validatesetvalue',
        {
            handler(modelArg) {
                return reserved.indexOf(modelArg) === -1;
            },
        },
        'world'
    );
    Macro.world.validateSetValue = Macro.world.validatesetvalue;
    Macro.world['validate-set-value'] = Macro.world.validatesetvalue;

    Macro.add('formatoutput',
        {
            handler(caller, output) {
                if (caller &&
                    caller.twine &&
                    typeof(output) !== 'string' &&
                    !isNode(output) &&
                    !isElement(output))
                {
                    return JSON.stringify(output);
                } else {
                    return output;
                }
            },
        },
        'world'
    );
    Macro.world.formatOutput = Macro.world.formatoutput;
    Macro.world['format-output'] = Macro.world.formatoutput;

    Macro.add('addmodelconstructor',
        {
            handler(type, constructor, forbidden) {
                if (!type) {
                    wideAlert('The type argument provided to ' +
                        'Macro.world.addmodelconstructor');
                } else if (type in modelConstructors) {
                    wideAlert('There is already a constructor for models of ' +
                        'type ' + type + '.');
                    return false;
                } else if (!isFunction(constructor)) {
                    wideAlert('The constructor argument,' +
                        constructor.toString() +
                        ', provided to Macro.world.addmodelconstructor' +
                        ' is not a valid constructor function.');
                    return false;
                }

                if (forbidden && forbidden.length) {
                    for (let ii = 0; ii < forbidden.length; ii++) {
                        // FILL IN HERE
                    }
                }

                modelConstructors[type] = constructor;

                return true;
            },
        },
        'world'
    );
    Macro.world.addModelConstructor = Macro.world.addmodelconstructor;
    Macro.world['add-model-constructor'] = Macro.world.addmodelconstructor;

    Macro.add('restoremodelprotofuncs',
        {
            handler() {
                const curVPState = present.virtualPassageState;
                const protoFuncs = getOwnPropertyNames(modelProtoFuncs);
                const models = getOwnPropertyNames(curVPState.all)
                    .map(nameOrID => curVPState.all[nameOrID]);
                models.forEach(function(model) {
                    if (!('parent' in model)) {
                        model.parent = present.virtualPassageState._nothing;
                    }

                    if (!('selector' in model)) {
                        model.selector = curVPState._nothing.selector;
                    }

                    protoFuncs.forEach(func => {
                        const protoFunc = modelProtoFuncs[func];
                        model[func] = protoFunc;
                    });
                });
            },
        },
        'world'
    );
    Macro.world.restoreModelProtoFuncs = Macro.world.restoremodelprotofuncs;
    Macro.world['restore-model-proto-funcs'] =
        Macro.world.restoremodelprotofuncs;

    // a list of all worldmodel functions
    const worldActions = {};
    getOwnPropertyNames(Macro.world).map(name => {
        if (name === 'handler') {
            return;
        }

        if (typeof(Macro.world[name]) === 'function') {
            worldActions[name] = Macro.world[name];
        } else if (typeof(Macro.world[name].handler) === 'function') {
            worldActions[name] = Macro.world[name].handler;
        }
    }).filter(aa => aa !== undefined);

    // forbidden [actorType] [action] [acteeType]
    present.virtualPassageState.forbidden = {
        location: {
            take: ['location', 'error'],
            contains: ['location', 'error'],
            give: '*',
            add: '*',
            remove: '*',
            forbid: '*',
            unforbid: '*',
            each: '*'
        },
        entity: {
            take: ['location', 'entity', 'error'],
            contains: ['location', 'entity', 'error'],
            give: ['entity', 'error'],
            add: '*',
            remove: '*',
            forbid: '*',
            unforbid: '*',
            each: '*'
        },
        object: {
            take: ['location', 'entity', 'error'],
            contains: ['location', 'entity', 'error'],
            give: ['error'],
            add: '*',
            remove: '*',
            forbid: '*',
            unforbid: '*',
            each: '*'
        },
        collection: {
            set: ['error'],
            unset: ['error'],
            give: '*',
            take: '*',
            add: '*',
            remove: '*',
            forbid: '*',
            unforbid: '*'
        },
        error: {
            give: '*',
            take: '*',
            contains: '*',
            add: '*',
            remove: '*',
            forbid: '*',
            unforbid: '*',
            set: '*',
            unset: '*',
            each: '*'
        },
    };

    let nextUnusedID = 0;
    function getNextUnusedId() {
        return nextUnusedID++;
    }

    window.World = {};
    World.Model = class Model {
        constructor(name, type) {
            this.name = name;
            this.type = type;
            this.parentSelector = null;
            this.children = {};
            this.descendants = {};
            this.associations = {};
            this.aliases = {};
            this._internalSelector = '#' + name;
            this.generic = false;
            this.quantity = 1;
            this.id = getNextUnusedId();
            this.isWorldModel = true;
        }
    };

    const modelProtoFuncs = {
        selector(newSelector) {
            if (newSelector) {
                this._internalSelector = newSelector;
                return;
            }
            if (!this._internalSelector) {
                throw new Error('The model, ' + this +
                    ', has no _internalSelector property. ' +
                    'Cannot get selector.');
            }
            if (this.generic) {
                return this.parentSelector + ' #' + this.id;
            } else {
                return this._internalSelector;
            }
        },

        parent() {
            if (this.parentSelector === null) {
                return null;
            }
            let parent = worldActions.get(this.parentSelector);
            if (parent.type === 'error') {
                return null;
            }
            return parent;
        },

        add(name, type) {
            for (let ii = 0; ii < arguments.length; ii++) {
                if (arguments[ii] && arguments[ii].type === 'error') {
                    return arguments[ii];
                }
            }

            let thisObj;
            let callObj = {};
            if (this instanceof String) {
                thisObj = saferJSONParse(this.toString());
                if (typeof(thisObj) !== 'object' ||
                    thisObj === null ||
                    !worldActions.validateModel(thisObj))
                {
                    return;
                }
                callObj = { twine: 'twine' };
            } else {
                thisObj = this;
            }

            return worldActions.add.call(callObj, name, type, thisObj);
        },

        addAlias(propertyName, alias) {
            for (let ii = 0; ii < arguments.length; ii++) {
                if (arguments[ii] && arguments[ii].type === 'error') {
                    return arguments[ii];
                }
            }

            let thisObj;
            let callObj = {};
            if (this instanceof String) {
                thisObj = saferJSONParse(this.toString());
                if (typeof(thisObj) !== 'object' ||
                    thisObj === null ||
                    !worldActions.validateModel(thisObj))
                {
                    return;
                }
                callObj = { twine: 'twine' };
            } else {
                thisObj = this;
            }

            return worldActions.addAlias.call(
                callObj,
                thisObj,
                propertyName,
                alias);
        },

        getAlias(propertyName) {
            for (let ii = 0; ii < arguments.length; ii++) {
                if (arguments[ii] && arguments[ii].type === 'error') {
                    return arguments[ii];
                }
            }

            let thisObj;
            let callObj = {};
            if (this instanceof String) {
                thisObj = saferJSONParse(this.toString());
                if (typeof(thisObj) !== 'object' ||
                    thisObj === null ||
                    !worldActions.validateModel(thisObj))
                {
                    return;
                }
                callObj = { twine: 'twine' };
            } else {
                thisObj = this;
            }

            return worldActions.getAlias.call(callObj, thisObj, propertyName);
        },

        associate(association, associatedModelArg, converse) {
            for (let ii = 0; ii < arguments.length; ii++) {
                if (arguments[ii] && arguments[ii].type === 'error') {
                    return arguments[ii];
                }
            }

            let thisObj;
            let callObj = {};
            if (this instanceof String) {
                thisObj = saferJSONParse(this.toString());
                if (typeof(thisObj) !== 'object' ||
                    thisObj === null ||
                    !worldActions.validateModel(thisObj))
                {
                    return;
                }
                callObj = { twine: 'twine' };
            } else {
                thisObj = this;
            }

            return worldActions.associate.call(
                callObj,
                thisObj,
                association,
                associatedModelArg,
                converse);
        },
        /*associated: '',
        contains: '',
        disassociate: '',
        each: '',
        forbid: '',
        get: '',
        give: '',
        isForbidden: '',
        isGeneric: '',
        isSet: '',
        makeGeneric: '',
        print: '',
        remove: '',
        removeAlias: '',
        set: '',
        take: '',
        unforbid: '',
        unset: ''*/
    };

    const protoFuncs = getOwnPropertyNames(modelProtoFuncs);
    protoFuncs.forEach(func => {
        const protoFunc = modelProtoFuncs[func];
        // don't overwrite pre-existing properties
        if (!(func in String.prototype)) {
            String.prototype[func] = protoFunc;
        }

        World.Model.prototype[func] = protoFunc;
    });

    World.LocationModel = class LocationModel extends World.Model {
        constructor(name) {
            super(name, 'location');
        }
    };

    World.EntityModel = class EntityModel extends World.Model {
        constructor(name) {
            super(name, 'entity');
        }
    };

    World.PlayerModel = class PlayerModel extends World.EntityModel {
        constructor(name) {
            super(name, 'player');
            this.current = {};
        }
    };

    World.ObjectModel = class ObjectModel extends World.Model {
        constructor(name) {
            super(name, 'object');
        }
    };

    let errorID = 0;
    class ErrorModel extends World.Model {
        constructor(pointOfFailure, args, errorMessage) {
            super('error' + errorID, 'error');
            errorID++;
            this.pointOfFailure = pointOfFailure;
            this.arguments = args;
            this.errorMessage = errorMessage;
            present.virtualPassageState.errors[this.name] = this;
            this.time = new Date();
            
            delete this.children;
            delete this.descendants;
            delete this.associations;
            delete this.aliases;
            delete this.generic;
            delete this.quantity;
        }
    };
    World.ErrorModel = ErrorModel;

    const modelConstructors = {
        location: World.LocationModel,
        entity: World.EntityModel,
        object: World.ObjectModel,
        error: World.ErrorModel,
        model: World.Model
    };

    const _nothing = new World.EntityModel('_nothing');
    present.virtualPassageState.all._nothing = _nothing;
    present.virtualPassageState._nothing = '#_nothing';
  
    const player = new World.PlayerModel('player');
    present.virtualPassageState.all.player = player;
    present.virtualPassageState.types.entity = {};
    present.virtualPassageState.types.entity.player = '#player';

    const reserved = [
        '_nothing',
        'error',
        _nothing,
    ];
}());