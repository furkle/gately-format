(function() {
    'use strict';
    
    const $ = window.jQuery;

    window.wideAlert = message => {
        const present = State.stacks.present;
        if (present.storyTheme.errorLevels.console) {
            console.log(message);
            console.trace('stack trace');
        }

        if (present.storyTheme.errorLevels.alert) {
            window.alert(message);
        }

        State.compilerStatus.errors.push(message);

        if (present.storyTheme.debug.failHard) {
            throw new Error(message);
        }
    };
    
    window.gatelyLog = message => {
        const present = State.stacks.present;
        if (present.storyTheme.errorLevels.console) {
            console.log(message);
            console.trace('stack trace');
        }

        State.compilerStatus.errors.push(message);

        if (present.storyTheme.debug.failHard) {
            throw new Error(message);
        }
    };

    window.gatelyAlert = message => {
        const present = State.stacks.present;
        if (State.currentTheme.errorLevels.alert) {
            window.alert(message);
        }

        State.compilerStatus.errors.push(message);

        if (State.currentTheme.debug.failHard) {
            throw new Error(message);
        }
    }

    window.noop = () => {
        /* do nothing */
    };

    window.from = Array.from;

    window.getOwnPropertyNames = Object.getOwnPropertyNames;

    window.$storydata = $('tw-storydata');

    window.$startPassage = $('tw-passagedata[pid="' +
        $storydata.attr('startNode') + '"]');

    window.isFunction = toCheck =>
        toCheck && {}.toString.call(toCheck) === '[object Function]';

    window.isNode = target => {
        let node;
        if (typeof(Node) === 'object') {
            node = target instanceof Node;
        } else {
            node = target;
        }

        return node &&
            typeof(target) === 'object' &&
            typeof(target.nodeType) === 'number' &&
            typeof(target.nodeName) === 'string';
    };

    // Returns true if it is a DOM element
    window.isElement = target => {
        let elem;
        if (typeof(HTMLElement) === 'object') {
            elem = target instanceof HTMLElement;
        } else {
            elem = target;
        }
        
        return elem &&
            typeof(target) === "object" &&
            target !== null &&
            target.nodeType === 1 &&
            typeof(target.nodeName) === 'string';
    };
        
    window.saferEval = str => {
        try {
            return eval(str);
        } catch (e) {
            try {
                return eval(
                    str.replace(/\n/g, '\\n')
                        .replace(/\t/g, '\\t')
                        .replace(/&quot;/g, '"')
                );
            } catch (e) {
                return str;
            }
        }
    };

    window.saferEvalToString = str => {
        if (typeof(str) !== 'string') {
            return str;
        }

        let evalled;

        try {
            evalled = eval(str);
        } catch (e) {
            try {
                evalled = eval(
                    str.replace(/\n/g, '\\n')
                        .replace(/\t/g, '\\t')
                        .replace(/&quot;/g, '"')
                );
            } catch (e) { /* do nothing */ }
        }

        if (typeof(evalled) === 'string') {
            return evalled;
        } else {
            return str;
        }
    };

    window.saferJSONParse = str => {
        if (typeof(str) !== 'string') {
            return str;
        }

        try {
            return JSON.parse(str);
        } catch (e) {
            try {
                return JSON.parse(
                    str.replace(/\n/g, '\\n')
                        .replace(/\t/g, '\\t')
                        .replace(/&quot;/g, '"')
                );
            } catch (e) {
                return str;
            }
        }
    };

    window.saferCombinedParse = str => {
        try {
            return JSON.parse(str);
        } catch (e) {
            try {
                return JSON.parse(
                    str.replace(/\n/g, '\\n')
                        .replace(/\t/g, '\\t')
                        .replace(/&quot;/g, '"')
                );
            } catch (e) {
                try {
                    return eval(str);
                } catch (e) {
                    try {
                        return eval(
                            str.replace(/\n/g, '\\n')
                                .replace(/\t/g, '\\t')
                                .replace(/&quot;/g, '"')
                        );
                    } catch (e) {
                        return str;
                    }
                }
            }
        }
    };

    window.getAllContainedTextNodes = nodeOrSelector => {
        if (typeof(nodeOrSelector) !== 'object' ||
            nodeOrSelector === null)
        {
            wideAlert('The nodeOrSelector argument passed to ' +
                'getAllContainedTextNodes is not valid. ' +
                'Cannot get text nodes.');
            return;
        }
        const $node = $(nodeOrSelector);
        if (!$node.length) {
            wideAlert('The nodeOrSelector argument passed to ' +
                'getAllContainedTextNodes is not valid, or is ' +
                'not a selector representative of an existing element. ' +
                'Cannot get text nodes.');
            return;
        }

        return $node.find('*')
            .contents()
            .filter((_, node) =>
                node.nodeType === 3 &&
                node.parentNode.tagName !== 'SCRIPT' &&
                node.parentNode.tagName !== 'STYLE');
    };

    window.saveFile = (fileName, extension, textOrElement) => {
        const $a = $('<a class="downloader"></a>');
        $('head').append($a);

        let text;
        if (typeof(textOrElement) === 'string' && text !== '') {
            text = textOrElement;
        } else if (isElement(textOrElement)) {
            text = textOrElement.outerHTML;
        } else if (textOrElement &&
            textOrElement[0] &&
            isElement(textOrElement[0]))
        {
            text = textOrElement[0].outerHTML;
        } else {
            const html = $('html')[0].cloneNode(true);
            // prevent scripts from re-executing on reload
            $(html).find('tw-passage script').remove();
            const selector = 'tw-save#defaultSave, a.downloader';
            $(html).find(selector).remove();
            Mixer.serializeMetadata();
            const stateStr = JSON.stringify(State);

            const save = document.createElement('tw-save');
            save.id = 'defaultSave';
            save.setAttribute('data-save', stateStr);
            html.querySelector('head').appendChild(save);

            const storydata = html.querySelector('tw-storydata');

            storydata.setAttribute('flags', 'save');
            text = html.outerHTML;
        }

        const blob = new Blob([text],
            {
                type: 'octet/stream',
            });
        const url = window.URL.createObjectURL(blob);

        $a[0].href = url;
        if (!fileName) {
            fileName = 'save';
        }

        if (extension) {
            extension = '.' + extension;
        } else {
            extension = '';
        }

        $a[0].download = fileName + extension;
        $a[0].click();
        $a.remove();

        window.URL.revokeObjectURL(url);
    };

    window.isElementRawString = str => {
        try {
            return $(str)[0] !== $(str)[0];
        } catch (e) {
            return false;
        }
    };
    
    window.deepCopy = obj => {
        return JSON.parse(JSON.stringify(obj));
    };

    window.deepFreeze = obj => {
        for (let name in obj) {
            // Freeze prop if it is an object
            if (typeof(obj[name]) === 'object' && obj[name] !== null) {
                deepFreeze(obj[name]);
            }
        }

        // Freeze self (no-op if already frozen)
        return Object.freeze(obj);
    };

    window.gatelyInclude = htmlStr => {
        const $elem = $('<div>' + htmlStr + '</div>');
        $elem.find('tw-passagedata').each((_, elem) => {
            $('tw-storydata').append(elem);
        });
    };

    window.quoteIsEscaped = (text, index) => {
        if (!text || typeof(text) === 'undefined') {
            throw new Error('The text argument was not provided to ' +
                'singleQuoteIsNotEscaped or is not a valid, non-empty ' +
                'string.');
            return null;
        } else if (text[index] !== "'" && text[index] !== '"') {
            throw new Error('The character in the text argument at the index ' +
                'indicated by the index argument is not a single quote.');
            return null;
        } else if (Number(index) < 0) {
            throw new Error('The index argument was not provided to ' +
                'singleQuoteIsNotEscaped or is not a valid, positive ' +
                'integer.');
            return null;
        } else if (/[^'"]/.test(text[index])) {
            return false;
        }

        let backslashes = 0;
        index--;
        for (; index >= 0; index--) {
            if (text[index] === '\\') {
                backslashes++;
            } else {
                break;
            }
        }

        return backslashes % 2 !== 0;
    };

    window.serializeElement = element => {
        if (!isElement(element)) {
            throw new Error('Use JSON.stringify instead.');
        }

        const copy = {};
        for (let name in element) {
            copy[name] = element[name];
        }

        return copy;
    };
}());