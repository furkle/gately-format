(function() {
    'use strict';

    const equalTypes = [
        '=',
        '+=',
        '-=',
        '*=',
        '/=',
        '%=',
    ];

    window.Macro = {
        handlerID: 0,

        global: {
            namespace: 'global',
            handler: null,
        },

        getNewHandlerId() {
            return this.handlerID++;
        },

        has(name, namespace) {
            if (!name) {
                wideAlert('No name provided to Macro.has.');
                return;
            }

            let nsVal;
            if (namespace) {
                nsVal = namespace;
            } else {
                nsVal = 'global';
            }

            if (!(nsVal in Macro)) {
                wideAlert('The namespace argument provided to ' +
                    'Macro.has, ' + nsVal + ', does not exist in ' +
                    'the Macro object.');
                return false;
            }

            const ns = Macro[nsVal];
            if (!ns || typeof(ns) !== 'object') {
                wideAlert('The value at Macro.' + nsVal + ' is ' +
                    'not a valid object. Cannot execute Macro.has.');
                return false;
            }

            if (!(name in ns)) {
                gatelyLog('The name argument provided to Macro.has, ' + name +
                    ', does not exist in Macro.' + nsVal + '.');
                return false;
            }

            const macro = ns[name];
            if (!macro || typeof(macro) !== 'object') {
                gatelyLog('The value at Macro.' + nsVal + '.' + name + ' is ' +
                    'not a valid object.');
                return false;
            } else if (!('handler' in macro)) {
                gatelyLog('The value at Macro.' + nsVal + '.' + name + 
                    'does not have a handler property.');
                return false;
            } else if (!macro.handler || !isFunction(macro.handler)) {
                gatelyLog('The value at Macro.' + nsVal + '.' + name +
                    'has a handler property, but it is not a valid ' +
                    'Javascript function.');
                return false;
            }

            return true;
        },

        add(name, defObj, namespace, doOverride, noFreeze) {
            if (!name) {
                wideAlert('The name argument must be provided to the add ' +
                    'function.');
                return;
            } else if (!defObj || !isFunction(defObj.handler)) {
                wideAlert('The defObj must exist and must have a valid ' +
                    'handler property.');
                return;
            }

            defObj.handler = Object.freeze(defObj.handler);

            let namespaceObj;
            if (!namespace ||
                (typeof(namespace) === 'string' &&
                    namespace.toLowerCase() === 'global'))
            {
                namespaceObj = Macro.global;
            } else if (!(namespace in Macro)) {
                wideAlert('There is no property in the Macro object with ' +
                    'the name ' + namespace + '. Cannot execute.');
                return;
            } else if (!('namespace' in Macro[namespace])) {
                wideAlert('An object was found in the Macro object ' +
                    'with a name matching the namespace argument, ' +
                    namespace + ', but it has no namespace property, ' +
                    'and does not appear to be a namespace. Cannot execute.');
                return;
            } else {
                namespaceObj = Macro[namespace];
            }

            if (name in namespaceObj &&
                doOverride !== true &&
                (typeof(doOverride) !== 'string' ||
                    doOverride.toLowerCase() !== 'override'))
            {
                wideAlert('The ' + namespaceObj.namespace + ' object ' +
                    'already contains a ' + name + ' object. If you want ' +
                    'to overwrite a pre-existing macro, please provide a ' +
                    'doOverride argument to Macro.add with a value of true ' +
                    'or "override".');
                return;
            }

            if (noFreeze === true ||
                (typeof(noFreeze) === 'string' &&
                    noFreeze.toLowerCase() === 'nofreeze'))
            {
                namespaceObj[name] = defObj;
            } else {
                namespaceObj[name] = Object.freeze(defObj);
            }
        },

        addNamespace(name, macros, handler) {
            if (typeof(name) !== 'string') {
                wideAlert('The name argument provided to ' +
                    'Macro.addNamespace is not a string. Cannot execute.');
                return;
            } else if (!name) {
                wideAlert('The name argument provided to ' +
                    'Macro.addNamespace is an empty string. Cannot execute.');
                return;
            }

            let handlerVal;
            if (handler !== undefined) {
                if (isFunction(handler)) {
                    handlerVal = handler;
                } else {
                    wideAlert('A handler argument was provided to ' +
                        'Macro.addNamespace, but does not appear to be a ' +
                        'function. Cannot execute.');
                    return;
                }
            }

            this[name] = {
                namespace: name,
            };

            if (handlerVal) {
                this[name].handler = handlerVal;
            }

            if (macros && typeof(macros) === 'object') {
                for (let macroName in macros) {
                    // add validation here?
                    this.add(macroName,
                        macros[macroName],
                        name);
                }
            }
        },

        delete(name) {
            delete this[name];
        },
    };

    Macro.add('set',
        {
            handler() {
                if (arguments.length === 0) {
                    wideAlert('The set macro cannot be empty!');
                    return;
                }

                for (let ii = 0; ii < arguments.length; ii++) {
                    if (typeof(arguments[ii]) !== 'string') {
                        wideAlert('The data type of each argument passed ' +
                            'to Macro.set must be a string. Skipping ' +
                            'triplet.');
                        continue;
                    }

                    const arg = arguments[ii];

                    const splitIndex = arg.search(/(\+=|-=|\*=|\/=|%=|=)/);
                    if (splitIndex === -1) {
                        wideAlert('No equals type was found in the ' +
                            'argument. Skipping triplet.');
                        continue;
                    }

                    let triplet = [];
                    if (arg[splitIndex + 1] === '=') {
                        triplet.push(arg.slice(0, splitIndex).trim());
                        triplet.push(
                            arg.slice(splitIndex, splitIndex + 2).trim());
                        triplet.push(arg.slice(splitIndex + 2, arg.length));
                    } else {
                        triplet.push(arg.slice(0, splitIndex).trim());
                        triplet.push(arg[splitIndex].trim());
                        triplet.push(arg.slice(splitIndex + 1, arg.length));
                    }

                    if (!triplet[0]) {
                        wideAlert('There is no variable in the set macro ' +
                            'within the triplet at position ' + ii);
                        continue;
                    } else if (/[^@$]/.test(triplet[0][0])) {
                        wideAlert('The first character of any variable name ' +
                            'passed to Macro.global.set must be a $ or @. ' +
                            'Cannot set.');
                        continue;
                    } else if (/[,'"()<>\s]/.test(triplet[0].slice(1))) {
                        wideAlert('A disallowed character was used in the ' +
                            'variable name, ' + triplet[0] + '. The ' +
                            'disallowed characters are , (comma), \', ", ' +
                            '(, ), <, >, any whitespace character, or ' +
                            'a $ or @ anywhere except as the first ' +
                            'character.');
                        continue;
                    } else if (equalTypes.indexOf(triplet[1]) === -1) {
                        wideAlert('The second component in each part of a ' +
                            'set assignment must be one of the following:\n' +
                            equalTypes.join('\n'));
                        continue;
                    }

                    triplet[2] = saferJSONParse(triplet[2]);

                    if (isNode(triplet[2])) {
                        triplet[2] = triplet[2].textContent;
                    }

                    State.setVariable(
                        triplet[0],
                        triplet[1],
                        triplet[2],
                        this.parentNode);
                }
            },
        },
        'global');

    Macro.add('print',
        {
            handler() {
                const match = document.createElement('tw-match');
                match.dataset.type= 'singleQuote';
                match.textContent = "'" +
                    from(arguments)
                        .map(arg => {
                            if (typeof(arg) === 'object') {
                                return JSON.stringify(arg);
                            } else {
                                return arg;
                            }
                        })
                        .join('') +
                    "'";

                return match;
            },
        },
        'global');

    Macro.add('compile',
        {
            handler() {

                const match = document.createElement('tw-match');
                match.className = 'compileContainer';
                match.textContent = from(arguments)
                    .map(arg => {
                        if (typeof(arg) === 'object') {
                            return JSON.stringify(arg);
                        } else {
                            return arg;
                        }
                    })
                    .join('');

                Compiler.compile(match);
                return match;
            },
        },
        'global');

    Macro.add('silently',
        {
            isDoubleTag: true,
            handler: noop,
        },
        'global');

    Macro.add('if',
        {
            isDoubleTag: true,
            dataType: 'html',

            handler(bool, ifBoolTrue, ifBoolFalse) {
                if (bool && bool.textContent) {
                    bool = saferEval(bool.textContent);
                } else {
                    bool = saferEval(bool);
                }

                if (bool) {
                    return ifBoolTrue;
                } else {
                    return ifBoolFalse;
                }
            },
        },
        'global');

    Macro.add('elseif',
        {
            isDoubleTag: true,
            handler: noop,
        });
    Macro.global['else-if'] = Macro.global.elseif;

    Macro.add('else',
        {
            isDoubleTag: true,
            handler: noop,
        },
        'global');

    Macro.add('click',
        {
            isDoubleTag: true,
            defer: true,
            handler(linkText, callback) {
                if (typeof(linkText) !== 'string' || !linkText) {
                    wideAlert('The linkText argument passed to ' +
                        'Macro.click is not valid.');
                    return;
                } else if (typeof(callback) !== 'string') {
                    wideAlert('The callback argument passed to ' +
                        'Macro.click is not valid.');
                    return;
                }

                const $node = $('<tw-link></tw-link>');
                $node.addClass('clickHandler click');
                
                const span = document.createElement('span');
                span.textContent = linkText;
                $node.append(span);

                const defer = document.createElement('tw-deferrer');
                defer.className = 'hidden';
                defer.textContent = callback;
                $node.append(defer);
           
                $node.click(e => {
                    const $deferred = $node.children('tw-deferrer');
                    if ($deferred.length) {
                        const temp = document.createElement('tw-match');
                        temp.textContent = $deferred.text();
                        const blockScope = State.getBlockScope($node[0]);
                        if (blockScope) {
                            temp.dataset.blockDefinitions =
                                JSON.stringify(blockScope);
                        }

                        Compiler.compile(temp);
                    }
                });

                return $node[0];
            },
        },
        'global');

    Macro.add('linkgoto',
        {
            handler(text, linkName) {
                let linkStr;
                if (linkName === undefined) {
                    linkStr = '<tw-link class="passage" passage-name="' +
                        text +
                        '">' +
                        text +
                        '</tw-link>';   
                } else {
                    linkStr = '<tw-link class="passage" passage-name="' +
                        linkName +
                        '">' +
                        text +
                        '</tw-link>';
                }

                return linkStr;
            },
        },
        'global');
    Macro.global['link-goto'] = Macro.global.linkgoto;

    Macro.add('linkreveal',
        {
            isDoubleTag: true,
            defer: true,

            handler(linkText, revealText) {
                const $node = $('<tw-link class="reveal"></tw-link>');
                $node.addClass('reveal');
            
                $node.append('<span>' + linkText + '</span>');
            
                $node.append('<tw-deferrer class="hidden">' + revealText +
                    '</tw-deferrer>');
           
                $node.click(e => {
                    const $deferred = $node.children('tw-deferrer');

                    if ($deferred.length) {
                        Compiler.compile($deferred);

                        if ($node[0].parentNode) {
                            const $children = $deferred.remove().children();

                            const $match = $('<tw-match></tw-match>');
                            $match.append($children);
                            $match.insertAfter($node);
                            $node.remove();
                        }
                    }
                });

                return $node[0];
            },
        },
        'global');
    Macro.global['link-reveal'] = Macro.global.linkreveal;

    Macro.add('cyclinglink',
        {
            handler() {
                let stringArgs = [];
                let variablePath;
                let passageName;
                const node = document.createElement('tw-link');
                node.className = 'cycling';

                if (arguments.length < 2) {
                    wideAlert('The cyclinglink macro needs at least two ' +
                        'arguments to function. Cannot create.');
                    return;
                } else if (arguments.length === 2) {
                    stringArgs = from(arguments);
                } else {
                    if (typeof(arguments[0]) === 'string' &&
                        /[$@]/.test(arguments[0][0]))
                    {
                        $node.addClass('setter');
                        variablePath = arguments[0];
                    } else {
                        stringArgs.unshift(arguments[0])
                    }

                    if (arguments.length > 3) {
                        stringArgs =
                            stringArgs.concat(from(arguments).slice(1, -2));
                        const out = arguments[arguments.length - 2];
                        passageName = arguments[arguments.length - 1];

                        if ((out === true || out === 'out') &&
                            typeof(passageName) === 'string')
                        {
                            $node.addClass('out');
                        } else {
                            stringArgs =
                                stringArgs.concat(from(arguments).slice(1, -2));
                        }
                    } else {
                        stringArgs = from(arguments).slice(1);
                    }
                }

                const present = State.stacks.present;

                if (variablePath) {
                    State.setVariable(variablePath, '=', stringArgs[0], node);
                }

                node.innerHTML = arguments[0];

                let currentIndex = 0;
                node.onclick = () => {
                    stringArgs.push(stringArgs.shift());

                    currentIndex++;
               
                    node.innerHTML = stringArgs[0];
                    
                    /* if the variable path was set, update the corresponding
                     * variable to reflect the current value of the link. */
                    if (variablePath) {
                        State.setVariable(variablePath, '=', stringArgs[0], node);
                    }

                    if (currentIndex === stringArgs.length) {
                        currentIndex = 0;
                        if (node.classList.contains('out')) {
                            Renderer.render(passageName);
                        }
                    }
                };

                return node;
            },
        },
        'global');
    Macro.global['cycling-link'] = Macro.global.cyclinglink;

    Macro.add('br',
        {
            handler() {
                return document.createElement('br');
            },
        },
        'global');
    Macro.global.newline = Macro.global.br;
    Macro.global['new-line'] = Macro.global.br;

    Macro.add('display',
        {
            handler(passageName) {
                const selector = 'tw-passagedata[name="' + passageName + '"]';
                const $passage = $(selector);
                if (!$passage.length) {
                    wideAlert('There is no passage to display named ' +
                        passageName +
                        '"]');
                    return;
                }

                return Renderer.doDisplay($passage);
            },
        },
        'global');

    Macro.add('replace',
        {
            handler(selectorOrMatch, replaceText) {
                if (typeof(selectorOrMatch) !== 'string' || !selectorOrMatch)
                {
                    wideAlert('The passageNameOrMatchText argument passed to ' +
                        'Macro.replace is not a string or is an empty ' +
                        'string. Cannot replace.');
                    return;
                } else if (typeof(replaceText) !== 'string' || !replaceText) {
                    wideAlert('The replaceText argument passed to ' +
                        'Macro.replace is not a string or is an empty ' +
                        'string. Cannot replace.');
                    return;
                } else if (selectorOrMatch === replaceText) {
                    wideAlert('The passageNameOrMatch and replaceText ' +
                        'arguments passed to Macro.replace are identical. ' +
                        'This will result in an infinite loop. ' +
                        'Cannot replace.');
                    return;
                }

                let $elem;
                // don't try searching if it's not an ID or class selector
                if (selectorOrMatch[0] === '#' || selectorOrMatch[0] === '.')
                {
                    try {
                        $elem = $(selectorOrMatch);
                    } catch (e) { /* do nothing */ }
                }

                if ($elem) {
                    $elem.html(replaceText);
                } else {
                    if (selectorOrMatch.indexOf(replaceText) !== -1) {
                        // TODO: fix this pls
                        wideAlert('The text to be replaced cannot contain ' +
                            'the replacement text in it. Cannot replace.');
                        return;
                    }

                    const currentPassage = $('tw-passage.current')[0];
                    if (!currentPassage) {
                        alert('The story has become corrupted – ' +
                            'the current passage cannot be found.')
                        throw new Error('The story has become corrupted – ' +
                            'the current passage cannot be found.');
                    }

                    const textNodes =
                        getAllContainedTextNodes(currentPassage);

                    for (let ii = 0; ii < textNodes.length; ii++) {
                        const node = textNodes[ii];

                        let text = node.textContent;
                        while (text.indexOf(selectorOrMatch) !== -1) {
                            text = text.replace(selectorOrMatch, replaceText);
                        }

                        node.textContent = text;
                    }
                }
            },
        },
        'global');

    Macro.add('textcolor',
        {
            isDoubleTag: true,
            handler(color, inner) {
                if (typeof(color) !== 'string' || color === '') {
                    wideAlert('The color argument passed to Macro.textcolor ' +
                        'is not a string or is an empty string. ' +
                        'Cannot change text color.');
                    return;
                } else if (typeof(inner) !== 'string' &&
                    inner === '' &&
                    !isElement(inner) &&
                    !isNode(inner))
                {
                    wideAlert('The innerText argument passed to ' +
                        'Macro.textcolor is not a string or is ' +
                        'an empty string. Cannot change text color.');
                    return;
                }

                let $elem;
                if (isElement(inner) || isNode(inner)) {
                    $elem = $(inner);
                } else {
                    $elem = $('<span></span>');
                    $elem.css('color', color);
                    $elem.text(inner);
                }

                return $elem[0];
            },
        },
        'global');
    Macro.global['text-color'] = Macro.global.textcolor;
    Macro.global.textcolour = Macro.global.textcolor;
    Macro.global['text-colour'] = Macro.global.textcolor;

    Macro.add('textstyle',
        {
            isDoubleTag: true,
            handler() {
                const inner = arguments[arguments.length - 1];
                if (typeof(inner) !== 'string' &&
                    inner === '' &&
                    !isElement(inner) &&
                    !isNode(inner))
                {
                    wideAlert('The innerText argument passed to ' +
                        'Macro.textcolor is not a string or is ' +
                        'an empty string. Cannot change text color.');
                    return;
                } else if (arguments.length === 1 || !arguments[1]) {
                    wideAlert('The first value argument passed to ' +
                        'Macro.textstyle is not a string or is an empty ' +
                        'string. Cannot change text style.');
                    return;
                }

                const toSearch = {
                    'font-family': [
                        'cursive',
                        'fantasy',
                        'monospace',
                        'sans-serif',
                        'serif',
                    ],

                    'font-size': [
                        'large',
                        'larger',
                        'medium',
                        'small',
                        'smaller',
                        'x-large',
                        'x-small',
                        'xx-large',
                        'xx-small',
                    ],

                    'font-stretch': [
                        'condensed',
                        'expanded',
                        'extra-condensed',
                        'extra-expanded',
                        'narrower',
                        'semi-condensed',
                        'semi-expanded',
                        'ultra-condensed',
                        'ultra-expanded',
                        'wider',
                    ],

                    'font-style': [
                        'italic',
                        'normal',
                        'oblique',
                    ],

                    'font-style': [
                        'small-caps',
                    ],

                    'font-weight': [
                        'bold',
                        'bolder',
                        'lighter',
                    ],

                    'text-combine-upright': [
                        'all',
                        'none',
                    ],

                    'text-decoration': [
                        'blink',
                        'line-through',
                        'overline',
                        'underline',
                    ],

                    'text-orientation': [
                        'mixed',
                        'sideways',
                        'upright',
                    ],

                    'text-overflow': [
                        'clip',
                        'ellipsis',
                    ],

                    'text-rendering': [
                        'auto',
                        'geometricPrecision',
                        'optimizeLegibility',
                        'optimizeSpeed',
                    ],

                    'text-transform': [
                        'capitalize',
                        'lowercase',
                        'uppercase',
                    ],
                };

                let $elem;
                if (isElement(inner) || isNode(inner)) {
                    $elem = $(inner);
                } else {
                    $elem = $('<span></span>');
                    $elem.text(inner);
                }

                let set = false;
                for (let ii = 0; ii < arguments.length - 1; ii++) {
                    const value = arguments[ii];
                    getOwnPropertyNames(toSearch).forEach(name => {
                        if (toSearch[name].indexOf(value) !== -1) {
                            $elem.css(name, value);
                            set = true;
                        }
                    });
                }

                if (!set) {
                    wideAlert('The value(s) passed to Macro.textstyle is/are not ' +
                        'currently supported. The element will be ' +
                        'displayed, but no style rules will be added.');
                }

                return $elem[0];
            },
        },
        'global');
    Macro.global['text-style'] = Macro.global.textstyle;

    // TODO: ADD OTHER STYLES
    Macro.add('transition',
        {
            isDoubleTag: true,
            handler(type, transitionArgsOrInOrOut, milliseconds, innerText) {
                type = type.trim();

                let str =
                    '<tw-transition-container></tw-transition-container>';
                const $node = $(str);

                let args;
                if (arguments.length === 0) {
                    wideAlert('No arguments were provided for ' +
                        'Macro.transition. Cannot transition.');
                    return;
                } else if (arguments.length === 1) {
                    wideAlert('Either no transition type was provided to ' +
                        'Macro.transition, or there is nothing provided to ' +
                        'transition.');
                    return;
                } else if (!type) {
                    wideAlert('The type argument provided to Macro.transition ' +
                        'is not valid. Cannot transition.');
                    return;
                } else if (arguments.length === 2) {
                    args = ['type', 'innerText'];
                    innerText = transitionArgsOrInOrOut;
                } else if (arguments.length === 3) {
                    args = ['type', 'transitionArgsOrInOrOut', 'innerText'];
                    innerText = milliseconds;
                } else {
                    args = ['type', 'transitionArgsOrInOrOut', 'milliseconds',
                        'innerText'];
                }

                if (args.indexOf('transitionArgsOrInOrOut') !== -1 &&
                    transitionArgsOrInOrOut)
                {
                    let arg = transitionArgsOrInOrOut;
                    let transitionObj = saferCombinedParse(arg);

                    if (typeof(transitionArgsOrInOrOut) === 'string') {
                        let timeVal = transitionArgsOrInOrOut;

                        if (args.indexOf('milliseconds') !== -1) {
                            let boolEval = saferEval(milliseconds);
                            if (boolEval === true ||
                                milliseconds === 'milliseconds')
                            {
                                timeVal += 'ms';
                            } else {
                                timeVal += 's';
                            }
                        } else {
                            timeVal += 's';
                        }

                        $node.css({
                            '-webkit-animation-duration': timeVal,
                            '-moz-animation-duration': timeVal,
                            '-o-animation-duration': timeVal,
                            'animation-duration': timeVal
                        });
                    } else if (typeof(transitionObj) === 'object') {
                        const transArgs = transitionArgsOrInOrOut;
                        let timeVal = transArgs.duration;

                        if (transArgs.duration !== undefined) {
                            let mills = transArgs.durationInMilliseconds;
                            if (args.indexOf('milliseconds') !== -1) {
                                let boolEval = saferEval(mills);
                                if (boolEval === true ||
                                    mills === 'milliseconds')
                                {
                                    timeVal += 'ms';
                                } else {
                                    timeVal += 's';
                                }
                            } else {
                                timeVal += 's';
                            }
                        } else {
                            timeVal += '1s';
                        }

                        $node.css({
                            '-webkit-animation-duration': timeVal,
                            '-moz-animation-duration': timeVal,
                            '-o-animation-duration': timeVal,
                            'animation-duration': timeVal
                        });

                        if (transArgs.delay !== undefined) {
                            let timeVal = transArgs.delay;
                            let mills = transArgs.delayInMilliseconds;
                            if (args.indexOf('milliseconds') !== -1) {
                                let boolEval = saferEval(mills);
                                if (boolEval === true ||
                                    mills === 'milliseconds')
                                {
                                    timeVal += 'ms';
                                } else {
                                    timeVal += 's';
                                }
                            } else {
                                timeVal += 's';
                            }

                            $node.css('transition-delay', timeVal);
                        }

                        if (typeof(transArgs.timing) === 'string' &&
                            transArgs.timing)
                        {
                            let timing = transArgs.timing;
                            $node.css({
                                '-webkit-animation-timing-function': timing,
                                '-moz-animation-timing-function': timing,
                                '-o-animation-timing-function': timing,
                                'animation-timing-function': timing,
                            });
                        }
                    }
                } else {
                    $node.css({
                        '-webkit-animation-duration': '1s',
                        '-moz-animation-duration': '1s',
                        '-o-animation-duration': '1s',
                        'animation-duration': '1s'
                    });
                }

                if (type === 'dissolve') {
                    $node.attr('data-transition-type', 'dissolve');
                } else {
                    // add default handler
                }

                let inOrOut = saferCombinedParse(transitionArgsOrInOrOut);
                if (inOrOut && inOrOut.toLowerCase) {
                    inOrOut = inOrOut.toLowerCase();
                }
                if (/^(in|out)$/i.test(inOrOut.trim())) {
                    $node.attr('data-transition-direction', inOrOut);
                }

                $node.html(innerText);

                return $node[0];
            },
        },
        'global');

    Macro.add('array',
        {
            handler() {
                const array = from(arguments).map(index => {
                    return saferJSONParse(index);
                });

                const elem = document.createElement('tw-argument');
                elem.textContent = JSON.stringify(array);
                return elem;
            },
        },
        'global');
    Macro.global.a = Macro.global.array;

    Macro.add('addatstart',
        {
            handler(arrOrStrOrVarPath) {
                if (!arrOrStrOrVarPath) {
                    wideAlert('The arrOrStrOrVarPath argument passed to ' +
                        'Macro.addatstart is not valid. Cannot add.');
                    return;
                } else if ((typeof(arrOrStrOrVarName) !== 'string' &&
                        typeof(arrOrStrOrVarName) !== 'object') ||
                    typeof(arrOrStrOrVarName.length) !== 'number' ||
                    !isFunction(arrOrStrOrVarName.slice))
                {
                    wideAlert('The arrOrStrOrVarPath argument passed to ' +
                        'Macro.addatstart is not a string or array. ' +
                        'Cannot add.');
                    return;
                } else if (arguments.length === 1) {
                    wideAlert('There is no value provided to add to the ' +
                        'array or string. Cannot add.');
                    return;
                }

                const present = State.stacks.present;
                let saving = false;
                let arrOrStr;
                if (typeof(arrOrStrOrVarPath) === 'string') {
                    if (/[$@]/.test(arrOrStr[0])) {
                        const smaller = arrOrStrOrVarPath.trim().slice(1);
                        const stored = State.getVariable(smaller);
                        const type = typeof(stored);
                        if ((type === 'string' || type === 'object') &&
                            typeof(stored.length) === 'number' &&
                            isFunction(stored.slice))
                        {
                            arrOrStr = stored;
                            saving = true;
                        } else {
                            wideAlert('The Twinescript variable referred ' +
                                'to by the arrOrStrOrVarName argument ' +
                                'passed to Macro.addatstart is not a ' +
                                'string or array. Cannot add.');
                            return;
                        }
                    } else {
                        let arrOrStr = saferCombinedParse(arrOrStrOrVarPath);
                        if (typeof(arrOrStr) !== 'object' ||
                            typeof(arrOrStr.length) !== 'number' ||
                            !isFunction(arrOrStr.slice))
                        {
                            arrOrStr = arrOrStrOrVarPath;
                        }
                    }
                }

                let temp = from(arguments).slice(1)
                    .map(index => {
                        const val = saferCombinedParse(index);
                        if (typeof(arrOrStr) === 'string') {
                            return val.toString();
                        } else {
                            return val;
                        }
                    }).concat(arrOrStr);

                if (typeof(arrOrStr) === 'string') {
                    arrOrStr = temp.join('');
                } else {
                    arrOrStr = temp;
                }

                if (saving) {
                    State.setVariable(arrOrStrOrVarPath,
                        '=',
                        arrOrStr,
                        this);
                }
            
                if (typeof(arrOrStr) === 'object') {
                    return JSON.stringify(arrOrStr);
                } else {
                    return arrOrStr;
                }
            },
        },
        'global');
    Macro.global['add-at-start'] = Macro.global.addatstart;
    Macro.global.addathead = Macro.global.addatstart;
    Macro.global['add-at-head'] = Macro.global.addatstart;
    Macro.global.prepend = Macro.global.addatstart;
    Macro.global.unshift = Macro.global.addatstart;

    Macro.add('addatend',
        {
            handler(arrOrStrOrVarPath) {
                if (!arrOrStrOrVarPath) {
                    wideAlert('The arrOrStrOrVarPath argument passed to ' +
                        'Macro.addatend is not valid. Cannot add.');
                    return;
                } else if ((typeof(arrOrStrOrVarPath) !== 'string' &&
                        typeof(arrOrStrOrVarPath) !== 'object') ||
                    typeof(arrOrStrOrVarPath.length) !== 'number' ||
                    !isFunction(arrOrStrOrVarPath.slice))
                {
                    wideAlert('The arrOrStrOrVarPath argument passed to ' +
                        'Macro.addatend is not a string or array. ' +
                        'Cannot add.');
                    return;
                } else if (arguments.length === 1) {
                    wideAlert('There is no value provided to add to the ' +
                        'array or string. Cannot add.');
                    return;
                }

                let val = arrOrStrOrVarPath;
                let variablePath = null;
                if (this.children[0] &&
                    this.children[0].dataset.variablePath)
                {
                    variablePath = this.children[0].dataset.variablePath;
                }

                if (typeof(val) === 'string') {
                    const temp = saferJSONParse(val);
                    if (temp &&
                        typeof(temp) === 'object' &&
                        isFunction(temp.concat))
                    {
                        val = temp;
                    }
                }

                const furtherArgs = from(arguments)
                    .slice(1)
                    .map(aa => saferJSONParse(aa))
                val = val.concat(furtherArgs);

                if (variablePath) {
                    State.setVariable(variablePath, '=', val, this);
                }
            
                if (typeof(val) === 'object') {
                    return JSON.stringify(val);
                } else {
                    return val;
                }
            },
        },
        'global');
    Macro.global['add-at-end'] = Macro.global.addatend;
    Macro.global.addattail = Macro.global.addatend;
    Macro.global['add-at-tail'] = Macro.global.addatend;
    Macro.global.append = Macro.global.addatend;
    Macro.global.push = Macro.global.addatend;

    Macro.add('addatindex',
        {
            handler(arrOrStrOrVarPath, index) {
                if (!arrOrStrOrVarPath) {
                    wideAlert('The arrOrStrOrVarPath argument passed to ' +
                        'Macro.global.addatindex is not valid. Cannot add.');
                    return;
                } else if ((typeof(arrOrStrOrVarPath) !== 'string' &&
                        typeof(arrOrStrOrVarPath) !== 'object') ||
                    typeof(arrOrStrOrVarPath.length) !== 'number' ||
                    !isFunction(arrOrStrOrVarPath.slice))
                {
                    wideAlert('The arrOrStrOrVarPath argument passed to ' +
                        'Macro.global.addatindex is not a string or array. ' +
                        'Cannot add.');
                    return;
                } else if (arguments.length === 2) {
                    wideAlert('There is no value provided to add to the ' +
                        'array or string. Cannot add.');
                    return;
                }

                let arrOrStr = arrOrStrOrVarPath;

                let saving = false;
                if (typeof(arrOrStr) === 'string') {
                    if (/[$@]/.test(arrOrStr[0])) {
                        const stored = State.getVariable(arrOrStr);
                        const type = typeof(stored);
                        if ((type === 'string' || type === 'object') &&
                            typeof(stored.length) === 'number' &&
                            isFunction(stored.slice))
                        {
                            arrOrStr = stored;
                            saving = true;
                        } else {
                            wideAlert('The Twinescript variable referred ' +
                                'to by the arrOrStrOrVarName argument ' +
                                'passed to Macro.addatindex is not a ' +
                                'string or array. Cannot add.');
                            return;
                        }
                    } else {
                        let arrOrStr = saferCombinedParse(arrOrStrOrVarPath);
                        if (typeof(arrOrStr) !== 'object' ||
                            typeof(arrOrStr.length) !== 'number' ||
                            !isFunction(arrOrStr.slice))
                        {
                            arrOrStr = arrOrStrOrVarPath;
                        }
                    }
                }

                let intIndex = Math.floor(Number(index));

                if (intIndex >= arrOrStr.length) {
                    wideAlert('The index provided is greater than the ' +
                        'length of the array or string represented by the ' +
                        'arrOrStrOrVarName argument passed to ' +
                        'Macro.addatindex. Cannot add.');
                    return;
                }

                let temp = arrOrStr.slice(0, intIndex)
                    .concat(
                        from(arguments).slice(2)
                            .map(index => {
                                const val = saferCombinedParse(index);
                                if (typeof(arrOrStr) === 'string') {
                                    return val.toString();
                                } else {
                                    return val;
                                }
                            }))
                    .concat(arrOrStr.slice(intIndex));

                if (typeof(arrOrStr) === 'string') {
                    arrOrStr = temp.join('');
                } else {
                    arrOrStr = temp;
                }

                if (saving) {
                    State.setVariable(arrOrStrOrVarPath,
                        '=',
                        arrOrStr,
                        this);
                }
            
                if (typeof(arrOrStr) === 'object') {
                    return JSON.stringify(arrOrStr);
                } else {
                    return arrOrStr;
                }
            },
        },
        'global');
    Macro.global['add-at-index'] = Macro.global.addatindex;

    Macro.add('removeathead',
        {
            handler(arrOrStrOrVarName, numberToRemove) {
                if (!arrOrStrOrVarName) {
                    wideAlert('The arrOrStrOrVarName argument passed to ' +
                        'Macro.removeathead is not valid. Cannot remove.');
                    return;
                } else if ((typeof(arrOrStrOrVarName) !== 'string' &&
                        typeof(arrOrStrOrVarName) !== 'object') ||
                    typeof(arrOrStrOrVarName.length) !== 'number' ||
                    !isFunction(arrOrStrOrVarName.slice))
                {
                    wideAlert('The arrOrStrOrVarName argument passed to ' +
                        'Macro.removeathead is not a string or array. ' +
                        'Cannot remove.');
                    return;
                }

                let arrOrStr = arrOrStrOrVarName;

                const present = State.stacks.present;
                let saving = false;
                if (typeof(arrOrStr) === 'string') {
                    if (arrOrStr[0] === '$') {
                        const smaller = arrOrStr.trim().slice(1);
                        const stored = present.storyVariables[smaller];
                        const type = typeof(stored);
                        if ((type === 'string' || type === 'object') &&
                            typeof(stored.length) === 'number' &&
                            isFunction(stored.slice))
                        {
                            arrOrStr = stored;
                            saving = true;
                        } else {
                            wideAlert('The Twinescript variable referred ' +
                                'to by the arrOrStrOrVarName argument ' +
                                'passed to Macro.removeathead is not a ' +
                                'string or array. Cannot remove.');
                            return;
                        }
                    } else {
                        let arrOrStr = saferCombinedParse(arrOrStrOrVarName);
                        if (typeof(arrOrStr) !== 'object' ||
                            typeof(arrOrStr.length) !== 'number' ||
                            !isFunction(arrOrStr.slice))
                        {
                            arrOrStr = arrOrStrOrVarName;
                        }
                    }
                }

                if (numberToRemove === undefined) {
                    arrOrStr = arrOrStr.slice(1);
                } else {
                    let intToRemove = Math.floor(Number(numberToRemove));
                    if (Number.isNaN(intToRemove) || intToRemove < 1) {
                        wideAlert('The numberToRemove argument passed to ' +
                            'Macro.removeathead is not a valid, >= 1 ' +
                            'number. Cannot remove.');
                        return;
                    }

                    arrOrStr = arrOrStr.slice(intToRemove);
                }

                if (saving) {
                    State.setVariable(arrOrStrOrVarName.slice(1), arrOrStr, this);
                }
            
                if (typeof(arrOrStr) === 'object') {
                    return JSON.stringify(arrOrStr);
                } else {
                    return arrOrStr;
                }
            },
        },
        'global');
    Macro.global['remove-at-head'] = Macro.global.removeathead;

    Macro.add('removeatend',
        {
            handler(arrOrStrOrVarName, numberToRemove) {
                if (!arrOrStrOrVarName) {
                    wideAlert('The arrOrStrOrVarName argument passed to ' +
                        'Macro.removeatend is not valid. Cannot remove.');
                    return;
                } else if ((typeof(arrOrStrOrVarName) !== 'string' &&
                        typeof(arrOrStrOrVarName) !== 'object') ||
                    typeof(arrOrStrOrVarName.length) !== 'number' ||
                    !isFunction(arrOrStrOrVarName.slice))
                {
                    wideAlert('The arrOrStrOrVarName argument passed to ' +
                        'Macro.removeatend is not a string or array. ' +
                        'Cannot remove.');
                    return;
                }

                let arrOrStr = arrOrStrOrVarName;

                const present = State.stacks.present;
                let saving = false;
                if (typeof(arrOrStr) === 'string') {
                    if (arrOrStr[0] === '$') {
                        const smaller = arrOrStr.trim().slice(1);
                        const stored = present.storyVariables[smaller];
                        const type = typeof(stored);
                        if ((type === 'string' || type === 'object') &&
                            typeof(stored.length) === 'number' &&
                            isFunction(stored.slice))
                        {
                            arrOrStr = stored;
                            saving = true;
                        } else {
                            wideAlert('The Twinescript variable referred ' +
                                'to by the arrOrStrOrVarName argument ' +
                                'passed to Macro.removeatend is not a ' +
                                'string or array. Cannot remove.');
                            return;
                        }
                    } else {
                        let arrOrStr = saferCombinedParse(arrOrStrOrVarName);
                        if (typeof(arrOrStr) !== 'object' ||
                            typeof(arrOrStr.length) !== 'number' ||
                            !isFunction(arrOrStr.slice))
                        {
                            arrOrStr = arrOrStrOrVarName;
                        }
                    }
                }

                if (numberToRemove === undefined) {
                    arrOrStr = arrOrStr.slice(0, arrOrStr.length - 1);
                } else {
                    let intToRemove = Math.floor(Number(numberToRemove));
                    if (Number.isNaN(intToRemove) || intToRemove < 1) {
                        wideAlert('The numberToRemove argument passed to ' +
                            'Macro.removeatend is not a valid, >= 1 ' +
                            'number. Cannot remove.');
                        return;
                    }
                    arrOrStr = 
                        arrOrStr.slice(0, arrOrStr.length - intToRemove);
                }

                if (saving) {
                    State.setVariable(arrOrStrOrVarName.slice(1),
                        '=',
                        arrOrStr,
                        this);
                }
            
                if (typeof(arrOrStr) === 'object') {
                    return JSON.stringify(arrOrStr);
                } else {
                    return arrOrStr;
                }
            },
        },
        'global');
    Macro.global['remove-at-end'] = Macro.global.removeatend;

    Macro.add('removeatindex',
        {
            handler(arrOrStrOrVarName, index, numberToRemove) {
                if (!arrOrStrOrVarName) {
                    wideAlert('The arrOrStrOrVarName argument passed to ' +
                        'Macro.removeatindex is not valid. Cannot remove.');
                    return;
                } else if ((typeof(arrOrStrOrVarName) !== 'string' &&
                        typeof(arrOrStrOrVarName) !== 'object') ||
                    typeof(arrOrStrOrVarName.length) !== 'number' ||
                    !isFunction(arrOrStrOrVarName.slice))
                {
                    wideAlert('The arrOrStrOrVarName argument passed to ' +
                        'Macro.removeatindex is not a string or array. ' +
                        'Cannot remove.');
                    return;
                }

                let arrOrStr = arrOrStrOrVarName;

                const present = State.stacks.present;
                let saving = false;
                if (typeof(arrOrStr) === 'string') {
                    if (arrOrStr[0] === '$') {
                        const smaller = arrOrStr.slice(1);
                        const stored = present.storyVariables[smaller];
                        const type = typeof(stored);
                        if ((type === 'string' || type === 'object') &&
                            typeof(stored.length) === 'number' &&
                            isFunction(stored.slice))
                        {
                            arrOrStr = stored;
                            saving = true;
                        } else {
                            wideAlert('The Twinescript variable referred ' +
                                'to by the arrOrStrOrVarName argument ' +
                                'passed to Macro.removeatindex is not a ' +
                                'string or array. Cannot remove.');
                            return;
                        }
                    } else {
                        let arrOrStr = saferCombinedParse(arrOrStrOrVarName);
                        if (typeof(arrOrStr) !== 'object' ||
                            typeof(arrOrStr.length) !== 'number' ||
                            !isFunction(arrOrStr.slice))
                        {
                            arrOrStr = arrOrStrOrVarName;
                        }
                    }
                }

                let intIndex = Math.floor(Number(index));
                while (intIndex < 0) {
                    intIndex = arrOrStr.length + index;
                }

                if (numberToRemove === undefined) {
                    arrOrStr = arrOrStr
                        .slice(0, intIndex)
                        .concat(arrOrStr.slice(intIndex + 1));
                } else {
                    let intToRemove = Math.floor(Number(numberToRemove));
                    if (Number.isNaN(intToRemove) || intToRemove < 1) {
                        wideAlert('The numberToRemove argument passed to ' +
                            'Macro.removeatindex is not a valid, >= 1 ' +
                            'number. Cannot remove.');
                        return;
                    }
                    arrOrStr = arrOrStr.slice(0, intIndex)
                        .concat(arrOrStr.slice(intIndex + intToRemove));
                }

                if (saving) {
                    State.setVariable(arrOrStrOrVarName.slice(1),
                        '=',
                        arrOrStr,
                        this);
                }
            
                if (typeof(arrOrStr) === 'object') {
                    return JSON.stringify(arrOrStr);
                } else {
                    return arrOrStr;
                }
            },
        },
        'global');
    Macro.global['remove-at-index'] = Macro.global.removeatindex;

    Macro.add('arrayfilter',
        {
            isDoubleTag: true,
            defer: true,
            handler(array, variable, callback) {
                const present = State.stacks.present;
                let arr;
                if (!array) {
                    wideAlert('The array parameter passed to Macro.arrayfilter ' +
                        'is not valid. Cannot filter.');
                    return;
                } else if (typeof(array) === 'object' && 'length' in object) {
                    arr = array;
                } else if (typeof(array) === 'string') {
                    const str = array.trim();
                    if (str[0] === '$' &&
                        str.slice(1) in present.storyVariableState)
                    {
                        const varName = str.slice(1);
                        const varInst = present.storyVariableState[varName];
                        if (varInst &&
                            typeof(varInst) === 'object' &&
                            'length' in varInst)
                        {
                            arr = varInst;
                        }
                    } else {
                        const temp = saferCombinedParse(array);
                        if (temp &&
                            typeof(temp) === 'object' &&
                            'length' in temp)
                        {
                            arr = temp;
                        }
                    }
                }

                arr = from(arr);
                if (!arr) {
                    wideAlert('The array argument passed to Macro.arrayfilter ' +
                        'is not valid. Cannot filter.');
                    return;
                }

                if (!callback) {
                    wideAlert('The callback parameter passed to ' + 
                        'Macro.arrayfilter is not valid. Cannot filter.');
                    return;
                } else if (!variable || typeof(variable) !== 'string') {
                    wideAlert('The variable argument passed to ' +
                        'Macro.arrayfilter is not valid. Cannot filter.');
                    return;
                }

                let callCopy;
                if (isElement(callback) || isNode(callback)) {
                    callCopy = callback.textContent;
                } else {
                    callCopy = callback.toString();
                }

                let filtered = [];
                for (let ii = 0; ii < arr.length; ii++) {
                    let string = arr[ii];
                    if (typeof(str) !== 'string') {
                        string = JSON.stringify(string);
                    }

                    if (variable.indexOf(string) !== -1) {
                        wideAlert('The variable argument, ' + variable +
                            ', contains an incidence of the array ' +
                            'index, ' + string + '. This will result in ' +
                            'an infinite loop when replacing variable ' +
                            'with index. Cannot filter.');
                        return;
                    } else if (string.indexOf(variable) !== -1) {
                        wideAlert('The array index, ' + string +
                            ', contains an incidence of the variable ' +
                            'argument, ' + variable + '. This will ' +
                            'result in an infinite loop when replacing ' +
                            'variable with index. Cannot filter.');
                        return;
                    }

                    let call = callCopy;
                    while (call.indexOf(variable) !== -1) {
                        call = call.replace(variable, string);
                    }  

                    let result = Parser.parseText(call);
                    if (result &&
                        result !== false &&
                        result.trim() !== 'false')
                    {
                        filtered.push(arr[ii]);
                    }
                }

                return JSON.stringify(filtered);
            },
        },
        'global');
    Macro.global['array-filter'] = Macro.global.arrayfilter;

    Macro.add('dict',
        {
            handler() {
                if (arguments.length && arguments.length % 2 !== 0) {
                    wideAlert('The Macro.dict constructor must have an ' +
                        'even number of arguments in the form key, value, ' +
                        'key, value, etc. Cannot construct.');
                    return;
                }

                const dict = {};
                for (let ii = 0; ii < arguments.length; ii += 2) {
                    const value = saferCombinedParse(arguments[ii + 1]);
                    dict[arguments[ii]] = value;
                }

                return JSON.stringify(dict);
            },
        },
        'global');
    Macro.global.dictionary = Macro.global.dict;
    Macro.global.datamap = Macro.global.dict;
    Macro.global.object = Macro.global.dict;
    Macro.global.obj = Macro.global.dict;

    Macro.add('in',
        {
            handler(toSearch, key) {
                if (!toSearch) {
                    return false;
                }

                const search = saferCombinedParse(toSearch);

                if (!search || typeof(search) !== 'object') {
                    wideAlert('The toSearch argument passed to Macro.in ' +
                        'is not an object and cannot be parsed to one. ' +
                        'Macro.in can only be used on objects or string ' +
                        'representations thereof. Cannot execute.');
                    return false;
                } else {
                    return key in toSearch;
                }
            },
        },
        'global');

    Macro.add('stringcontains',
        {
            handler(str, substr) {
                if (arguments.length === 0) {
                    wideAlert('No arguments were passed to ' +
                        'Macro.stringcontains. Cannot execute.');
                    return;
                } else if (typeof(str) !== 'string') {
                    wideAlert('The str argument passed to ' +
                        'Macro.stringcontains is not a string. Cannot ' +
                        'execute.');
                    return false;
                } else if (!str) {
                    gatelyLog('The str argument passed to ' +
                        'Macro.stringcontains is an empty string. Cannot ' +
                        'execute.');
                    return false;
                } else if (arguments.length === 1) {
                    wideAlert('The substr argument was not provided to ' +
                        'Macro.stringcontains. Cannot execute.');
                    return false;
                } else if (typeof(substr) !== 'string') {
                    wideAlert('The substr argument passed to ' +
                        'Macro.stringcontains is not a string. Cannot ' +
                        'execute.');
                    return false;
                } else if (!substr) {
                    gatelyLog('The substr argument passed to ' +
                        'Macro.stringcontains is an empty string. Cannot ' +
                        'execute.');
                    return false;
                }

                return str.indexOf(substr) !== -1;
            },
        },
        'global');
    Macro.global['string-contains'] = Macro.global.stringcontains;
    Macro.global['strcontains'] = Macro.global.stringcontains;
    Macro.global['str-contains'] = Macro.global.stringcontains;

    Macro.add('objectcontains',
        {
            handler(obj, value) {
                if (arguments.length === 0) {
                    wideAlert('No arguments were provided to ' +
                        'Macro.objectcontains. Cannot execute.');
                    return;
                } else if (arguments.length === 1) {
                    wideAlert('The value argument was not provided to ' +
                        'Macro.objectcontains. Cannot execute.');
                    return;
                } else if (typeof(obj) !== 'object' &&
                    typeof(obj) !== 'string')
                {
                    wideAlert('The obj argument passed to ' +
                        'Macro.objectcontains is not an object or string. ' +
                        'Cannot execute.');
                    return false;
                } else if (!obj) {
                    wideAlert('The obj argument passed to ' +
                        'Macro.objectcontains is not valid. Cannot execute.');
                    return false;
                } else if (arguments.length === 1) {
                    wideAlert('The value argument was not provided to ' +
                        'Macro.objectcontains. Cannot execute.');
                    return false;
                }

                const search = saferCombinedParse(obj);
                const parsedVal = saferJSONParse(value);
                const val = JSON.stringify(parsedVal);
                const names = getOwnPropertyNames(search);
                for (let ii = 0; ii < names.length; ii++) {
                    const name = names[ii];
                    const curVal = JSON.stringify(search[name]);
                    if (curVal === val) {
                        return true;
                    }
                }

                return false;
            },
        },
        'global');
    Macro.global['object-contains'] = Macro.global.objectcontains;
    Macro.global['objcontains'] = Macro.global.objectcontains;
    Macro.global['object-contains'] = Macro.global.objectcontains;

    Macro.add('arrayconcat',
        {
            handler() {
                let array = [];
                
                from(arguments).forEach(arg => {
                    const parsed = saferCombinedParse(arg);
                    if (parsed &&
                        typeof(parsed) === 'object' &&
                        isFunction(parsed.concat))
                    {
                        array = array.concat(parsed);
                    } else {
                        wideAlert('The argument provided to ' +
                            'Macro.arrayconcat, ' + arg + ', is not an ' +
                            'non-null object, and/or lacks a concat ' +
                            'method. Skipping argument.');
                        return;
                    }
                });

                return JSON.stringify(array);
            },
        },
        'global');
    Macro.global['array-concat'] = Macro.global.arrayconcat;
    Macro.global.arrconcat = Macro.global.arrayconcat;
    Macro.global['arr-concat'] = Macro.global.arrayconcat;

    Macro.add('stringconcat',
        {
            handler() {
                return from(arguments).join('');
            }
        },
        'global');
    Macro.global.strconcat = Macro.global.stringconcat;
    Macro.global['str-concat'] = Macro.global.stringconcat;

    Macro.add('slice',
        {
            handler(input, start, end) {
                if (!isFunction(input.slice)) {
                    wideAlert('The input argument provided to ' +
                        'Macro.global.slice is not valid.');
                    return;
                }

                return input.slice(start, end);
            },
        },
        'global');

    Macro.add('foreach',
        {
            isDoubleTag: true,
            defer: true,
            handler(collection, key, value, callback) {
                let processedCollection = collection;

                if (typeof(collection) === 'string') {
                    const trimmed = collection.trim();
                    if (trimmed.startsWith('[') && trimmed.endsWith(']')) {
                        processedCollection = saferEval(collection);
                    } else { 
                        processedCollection = saferJSONParse(collection);
                    }

                    if (!processedCollection ||
                        processedCollection === collection ||
                        (typeof(processedCollection) !== 'object' &&
                            typeof(processedCollection) !== 'string'))
                    {
                        if (collection.indexOf(',') === -1) {
                            wideAlert('The collection passed to ' +
                                'Macro.foreach cannot be parsed or ' +
                                'evalled, and is not comma-separated. ' +
                                'Cannot execute.');
                            return;
                        }

                        processedCollection = collection.split(',');
                    }
                } else if (typeof(collection) !== 'object') {
                    wideAlert('The collection argument ' +
                        'passed to Macro.foreach is not an object ' +
                        'or string. Cannot execute.');
                    return;
                }

                let pointer;
                if (State.isPointer(processedCollection)) {
                    pointer = processedCollection;
                    processedCollection = State.getVariable(pointer, this);
                }

                let keyVal = (key || '').toString();
                if (key && typeof(key) === 'string') {
                    if (keyVal[0] === '@') {
                        keyVal = keyVal.slice(1);
                    } else if (keyVal[0] === '$') {
                        wideAlert('Only local variables can be set with ' +
                            'the foreach macro.');
                        return;
                    }
                }

                let valueVal = (value || '').toString();
                if (value && typeof(value) === 'string') {
                    if (valueVal[0] === '@') {
                        valueVal = valueVal.slice(1);
                    } else if (valueVal[0] === '$') {
                        wideAlert('Only local variables can be set with ' +
                            'the foreach macro.');
                        return;
                    }
                }

                if (!callback || typeof(callback) !== 'string') {
                    wideAlert('TODO: foreach callback error');
                    return;
                }

                if (processedCollection.type === 'collection') {
                    processedCollection = processedCollection.children;
                }

                if (!processedCollection) {
                    wideAlert('TODO: foreach wmCollection error');
                    return;
                }

                const keys = Object.getOwnPropertyNames(processedCollection).
                    filter(function(key) {
                        // don't return the length property
                        return key !== 'length';
                    });

                const obj = {};
                const values = keys.map(key => processedCollection[key]);

                const returnElem = document.createElement('tw-match');
                returnElem.className = 'eachContainer';

                for (let ii = 0; ii < keys.length; ii++) {
                    const retNode = document.createElement('tw-match');
                    retNode.textContent = callback;

                    if (keyVal) {
                        const name = keys[ii];
                        State.setVariable('@' + keyVal, '=', name, retNode);
                    }

                    const blockScope = State.getBlockScope(this);
                    if (blockScope) {
                        getOwnPropertyNames(blockScope).forEach(name => {
                            if (State.isPointer(blockScope[name])) {
                                State.setVariable(
                                    '@' + name,
                                    '=',
                                    State.makePointer(blockScope[name].path +
                                        '[' + name + ']'),
                                    retNode);
                            } else {
                                State.setVariable(
                                    '@' + name,
                                    '=',
                                    blockScope[name],
                                    retNode);
                            }
                        });
                    }

                    if (valueVal) {
                        const val = values[ii];

                        if (pointer && val && typeof(val) === 'object') {
                            State.setVariable(
                                '@' + valueVal,
                                '=',
                                State.makePointer(pointer.path + '[' + keys[ii] + ']'),
                                retNode);
                        } else {
                            State.setVariable('@' + valueVal, '=', val, retNode);
                        }
                    }

                    Compiler.compile(retNode);

                    returnElem.appendChild(retNode);
                }

                return returnElem;
            },
        },
        'global');
    Macro.global['for-each'] = Macro.global.foreach;

    Macro.add('range',
        {
            handler(lower, upper, step) {
                if (arguments.length === 0) {
                    wideAlert('No arguments were provided to Macro.range. ' +
                        'Cannot execute.');
                    return;
                } else if (typeof(lower) !== 'number') {
                    if (typeof(lower) !== 'string') {
                        wideAlert('The lower argument provided to ' +
                            'Macro.range is not a number or string. Cannot ' +
                            'execute.');
                        return;
                    } else if (lower.length !== 1) {
                        wideAlert('The lower argument provided to ' +
                            'Macro.range is a string, but is not of length ' +
                            '1. Cannot execute.');
                        return;
                    }
                } else if (Number.isNaN(lower)) {
                    wideAlert('The lower argument provided to Macro.range ' +
                        'is NaN. Cannot execute.');
                    return;
                } else if (lower === Number.POSITIVE_INFINITY) {
                    wideAlert('The lower argument provided to Macro.range ' +
                        'is positive Infinity. Cannot execute.');
                    return;
                } else if (lower === Number.NEGATIVE_INFINITY) {
                    wideAlert('The lower argument provided to Macro.range ' +
                        'is negative Infinity. Cannot execute.');
                    return;
                } else if (arguments.length === 1) {
                    wideAlert('The upper argument was not provided to ' +
                        'Macro.range. Cannot execute.');
                    return;
                }

                if (typeof(upper) !== 'number') {
                    if (typeof(upper) !== 'string') {
                        wideAlert('The upper argument provided to ' +
                            'Macro.range is not a number or string. Cannot ' +
                            'execute.');
                        return;
                    } else if (upper.length !== 1) {
                        wideAlert('The upper argument provided to ' +
                            'Macro.range is a string, but is not of length ' +
                            '1. Cannot execute.');
                        return;
                    }
                } else if (Number.isNaN(upper)) {
                    wideAlert('The upper argument provided to Macro.range ' +
                        'is NaN. Cannot execute.');
                    return;
                } else if (upper === Number.POSITIVE_INFINITY) {
                    wideAlert('The upper argument provided to Macro.range ' +
                        'is positive Infinity. Cannot execute.');
                    return;
                } else if (upper === Number.NEGATIVE_INFINITY) {
                    wideAlert('The upper argument provided to Macro.range ' +
                        'is negative Infinity. Cannot execute.');
                    return;
                }

                if (typeof(lower) !== typeof(upper)) {
                    wideAlert('The type of the lower argument provided to ' +
                        'Macro.range does not match that of the upper ' +
                        'argument. Either both must be a number, or both ' +
                        'must be a string.');
                    return;
                }

                if (step !== undefined) {
                    if (typeof(step) !== 'number') {
                        wideAlert('The step argument is provided to ' +
                            'Macro.range, but is not a number. Cannot ' +
                            'execute.');
                        return;
                    } else if (Number.isNaN(step)) {
                        wideAlert('The step argument provided to ' +
                            'Macro.range is NaN. Cannot execute.');
                        return;
                    } else if (step === Number.POSITIVE_INFINITY) {
                        wideAlert('The step argument provided to Macro.range ' +
                            'is positive Infinity. Cannot execute.');
                        return;
                    } else if (step === Number.NEGATIVE_INFINITY) {
                        wideAlert('The step argument provided to Macro.range ' +
                            'is negative Infinity. Cannot execute.');
                        return;
                    } else if (step <= 0) {
                        wideAlert('The step argument provided to ' +
                            'Macro.range must be a number greater than 0. ' +
                            'Cannot execute.');
                        return;
                    }
                }

                const container = [];
                let stepNum = step;
                if (step === undefined) {
                    stepNum = 1;
                }

                if (typeof(lower) === 'number') {
                    if (lower >= upper) {
                        wideAlert('The lower argument passed to ' +
                            'Macro.range is greater than or equal to the ' +
                            'upper argument. Cannot execute.');
                        return;
                    }
                
                    let index = lower;

                    // upper-exclusive bound
                    while (index < upper) {
                        container.push(index);
                        index += stepNum;
                    }
                } else if (typeof(lower) === 'string') {
                    const lowerCodePoint = lower.codePointAt(0);
                    const upperCodePoint = upper.codePointAt(0);

                    if (lowerCodePoint >= upperCodePoint) {
                        wideAlert('The lower character argument passed to ' +
                            'Macro.range has a code point greater than or ' +
                            'equal to the upper character argument\'s code ' +
                            'point. Cannot execute.');
                        return;
                    }

                    if (stepNum % 1 !== 0) {
                        wideAlert('The step provided to Macro.range is not ' +
                        'an integer. Cannot execute.');
                        return;
                    }

                    let index = lowerCodePoint;

                    // upper-inclusive bound
                    while (index <= upperCodePoint) {
                        try {
                            const str = String.fromCodePoint(index);
                            if (str) {
                                container.push(str);
                            }
                        } catch (e) {
                            wideAlert('The following error was encountered ' +
                                'when trying to produce a string from the ' +
                                'following codepoint: ' + index);
                            wideAlert(e);
                        }

                        index += stepNum;
                    }
                }

                return JSON.stringify(container);
            },
        },
        'global');

    Macro.add('either',
        {
            handler(arraylike) {
                let array;

                if (arguments.length === 0) {
                    wideAlert('There were no arguments provided to ' +
                        'Macro.either. Cannot execute.');
                    return;
                } else if (arguments.length === 1) {
                    if (!arraylike) {
                        wideAlert('The only argument provided to ' +
                            'Macro.either is neither an object nor a ' +
                            '(non-empty) string. Cannot execute.');
                        return;
                    } else if (typeof(arraylike) === 'object') {
                        if (!('length' in arraylike)) {
                            wideAlert('An object was provided as the ' +
                                'only argument to Macro.either, but ' +
                                'it has no length property. Cannot execute.');
                            return;
                        } else if (!arraylike.length) {
                            wideAlert('An array-like object was provided ' +
                                'as the only argument to ' +
                                'Macro.either, but it is empty. Cannot ' +
                                'execute.');
                            return;
                        }

                        array = arraylike;
                    } else if (typeof(arraylike) === 'string') {
                        array = saferCombinedParse(arraylike);

                        if (!array || typeof(array) !== 'object') {
                            wideAlert('The only argument provided to ' +
                                'Macro.either does not parse to a non-null ' +
                                'object. Cannot execute.');
                            return;
                        } else if (!('length' in array)) {
                            wideAlert('An string was provided as the ' +
                                'only argument to Macro.either, but ' +
                                'the object parsed from it has no length ' +
                                'property. Cannot execute.');
                            return;
                        } else if (!array.length) {
                            wideAlert('An string was provided ' +
                                'as the only argument to ' +
                                'Macro.either, but the array-like object ' +
                                'parsed from it is empty. Cannot execute.');
                            return;
                        }
                    } else {
                        wideAlert('Only one argument was passed to ' +
                            'Macro.either, and this argument is not an array or ' +
                            'arraylike object. Cannot execute.');
                        return;
                    }
                } else {
                    array = arguments;
                }

                    
                const index = Math.floor(Math.random() * array.length);
                const either = array[index];

                if (typeof(either) === 'object') {
                    return JSON.stringify(either);
                } else {
                    return either;
                }
            },
        },
        'global');

    Macro.add('random',
        {
            handler(floor, ceiling, float, exclusive) {
                if (typeof(floor) !== 'number' &&
                    floor !== undefined)
                {
                    wideAlert('The floor argument provided to ' +
                        'Macro.random is not a number. ' +
                        'Cannot execute.');
                    return;
                } else if (Number.isNaN(floor)) {
                    wideAlert('The floor argument provided to ' +
                        'Macro.random is NaN. Cannot execute.');
                    return;
                } else if (floor === Number.POSITIVE_INFINITY) {
                    wideAlert('The floor argument provided to ' +
                        'Macro.random is positive Infinity. Cannot ' +
                        'execute.');
                    return;
                } else if (floor === Number.NEGATIVE_INFINITY) {
                    wideAlert('The floor argument provided to ' +
                        'Macro.random is negative Infinity. Cannot ' +
                        'execute.');
                    return;
                } else if (typeof(ceiling) !== 'number' &&
                    ceiling !== undefined)
                {
                    wideAlert('The ceiling argument provided to ' +
                        'Macro.random is not a number. ' +
                        'Cannot execute.');
                    return;
                } else if (Number.isNaN(ceiling)) {
                    wideAlert('The ceiling argument provided to ' +
                        'Macro.random is NaN. Cannot execute.');
                    return;
                } else if (ceiling === Number.POSITIVE_INFINITY) {
                    wideAlert('The ceiling argument provided to ' +
                        'Macro.random is positive Infinity. Cannot ' +
                        'execute.');
                    return;
                } else if (ceiling === Number.NEGATIVE_INFINITY) {
                    wideAlert('The ceiling argument provided to ' +
                        'Macro.random is negative Infinity. Cannot ' +
                        'execute.');
                    return;
                }

                let floorVal = floor;
                let ceilingVal = ceiling;

                if (floorVal >= ceilingVal) {
                    wideAlert('The floor argument provided to ' +
                        'Macro.random cannot be greater than or ' +
                        'equal to the ceiling argument. Cannot create ' +
                        'random number.');
                    return;
                }

                if (floor === undefined) {
                    floorVal = 0;
                }

                if (ceiling === undefined) {
                    ceilingVal = 1;
                }

                let floatVal = Boolean(float);
                if (typeof(float) === 'string' &&
                    float.toLowerCase() === 'float')
                {
                    floatVal = true;
                }

                let exclusiveVal;
                if (exclusive === true ||
                    (typeof(exclusive) === 'string' &&
                        exclusive.toLowerCase() === 'exclusive'))
                {
                    exclusiveVal = true;
                } else if (typeof(exclusive) === 'string' &&
                    exclusive.toLowerCase() === 'floor')
                {
                    exclusiveVal = 'floor';
                } else if (typeof(exclusive) === 'string' &&
                    exclusive.toLowerCase() === 'ceiling')
                {
                    exclusiveVal = 'ceiling';
                }

                if (!floatVal &&
                    floorVal + 1 >= ceilingVal &&
                    exclusiveVal)
                {
                    wideAlert('The arguments for Macro.random indicate ' +
                        'an integer product with exclusive bounds, but ' +
                        'the ceiling argument is 1 or less greater than ' +
                        'the floor argument. Cannot execute.');
                    return;
                }

                let rand = Math.random() * (ceilingVal - floorVal) + floorVal;

                if (!floatVal) {
                    rand = Math.round(rand);
                }

                if (rand <= floor) {
                    rand = floor;

                    if (exclusiveVal === true || exclusiveVal === 'floor') {
                        if (floatVal) {
                            rand += Number.EPSILON;
                        } else {
                            rand++;
                        }
                    }
                } else if (rand >= ceiling) {
                    rand = ceiling;

                    if (exclusiveVal === true || exclusiveVal === 'ceiling') {
                        if (floatVal) {
                            rand -= Number.EPSILON;
                        } else {
                            rand--;
                        }
                    }
                }

                if (rand === -0) {
                    rand = 0;
                }

                return rand;
            },
        },
        'global');
    Macro.global.rand = Macro.global.random;

    Macro.add('randomfloat',
        {
            handler(floor, ceiling, exclusive) {
                if (typeof(floor) !== 'number' && floor !== undefined) {
                    wideAlert('The floor argument provided to ' +
                        'Macro.randomfloat is not a number. ' +
                        'Cannot execute.');
                    return;
                } else if (Number.isNaN(floor)) {
                    wideAlert('The floor argument provided to ' +
                        'Macro.randomfloat is NaN. Cannot execute.');
                    return;
                } else if (floor === Number.POSITIVE_INFINITY) {
                    wideAlert('The floor argument provided to ' +
                        'Macro.randomfloat is positive Infinity. Cannot ' +
                        'execute.');
                    return;
                } else if (floor === Number.NEGATIVE_INFINITY) {
                    wideAlert('The floor argument provided to ' +
                        'Macro.randomfloat is negative Infinity. Cannot ' +
                        'execute.');
                    return;
                } else if (typeof(ceiling) !== 'number' &&
                    ceiling !== undefined)
                {
                    wideAlert('The ceiling argument provided to ' +
                        'Macro.randomfloat is not a number. ' +
                        'Cannot execute.');
                    return;
                } else if (Number.isNaN(ceiling)) {
                    wideAlert('The ceiling argument provided to ' +
                        'Macro.randomfloat is NaN. Cannot execute.');
                    return;
                } else if (ceiling === Number.POSITIVE_INFINITY) {
                    wideAlert('The ceiling argument provided to ' +
                        'Macro.randomfloat is positive Infinity. Cannot ' +
                        'execute.');
                    return;
                } else if (ceiling === Number.NEGATIVE_INFINITY) {
                    wideAlert('The ceiling argument provided to Macro.random ' +
                        'is negative Infinity. Cannot execute.');
                    return;
                }

                let floorVal = floor;
                let ceilingVal = ceiling;

                if (floorVal >= ceilingVal) {
                    wideAlert('The floor argument provided to ' +
                        'Macro.randomfloat cannot be greater than or equal ' +
                        'to the ceiling argument. Cannot execute.');
                    return;
                }

                if (floor === undefined) {
                    floorVal = 0;
                }

                if (ceiling === undefined) {
                    ceilingVal = 1;
                }

                let exclusiveVal;
                if (exclusive === true ||
                    (typeof(exclusive) === 'string' &&
                        exclusive.toLowerCase() === 'exclusive'))
                {
                    exclusiveVal = true;
                } else if (typeof(exclusive) === 'string' &&
                    exclusive.toLowerCase() === 'floor')
                {
                    exclusiveVal = 'floor';
                } else if (typeof(exclusive) === 'string' &&
                    exclusive.toLowerCase() === 'ceiling')
                {
                    exclusiveVal = 'ceiling';
                }

                let rand = Math.random() * (ceilingVal - floorVal) + floorVal;

                if (rand === floor) {
                    if (exclusiveVal === true || exclusiveVal === 'floor') {
                        rand += Number.EPSILON;
                    }
                } else if (rand === ceiling) {
                    if (exclusiveVal === true || exclusiveVal === 'ceiling') {
                        rand -= Number.EPSILON;
                    }
                }

                if (rand === -0) {
                    rand = 0;
                }

                return rand;
            },
        },
        'global');
    Macro.global['random-float'] = Macro.global.randomfloat;
    Macro.global.randfloat = Macro.global.randomfloat;
    Macro.global['rand-float'] = Macro.global.randomfloat;

    Macro.add('randominteger',
        {
            handler(floor, ceiling, exclusive, negative) {
                if (typeof(floor) !== 'number' && floor !== undefined) {
                    wideAlert('The floor argument provided to ' +
                        'Macro.randominteger is not a number. ' +
                        'Cannot execute.');
                    return;
                } else if (Number.isNaN(floor)) {
                    wideAlert('The floor argument provided to ' +
                        'Macro.randominteger is NaN. Cannot execute.');
                    return;
                } else if (floor === Number.POSITIVE_INFINITY) {
                    wideAlert('The floor argument provided to ' +
                        'Macro.randominteger is positive Infinity. Cannot ' +
                        'execute.');
                    return;
                } else if (floor === Number.NEGATIVE_INFINITY) {
                    wideAlert('The floor argument provided to ' +
                        'Macro.randominteger is negative Infinity. Cannot ' +
                        'execute.');
                    return;
                } else if (typeof(ceiling) !== 'number' &&
                    ceiling !== undefined)
                {
                    wideAlert('The ceiling argument provided to ' +
                        'Macro.randominteger is not a number. ' +
                        'Cannot execute.');
                    return;
                } else if (Number.isNaN(ceiling)) {
                    wideAlert('The ceiling argument provided to ' +
                        'Macro.randominteger is NaN. Cannot execute.');
                    return;
                } else if (ceiling === Number.POSITIVE_INFINITY) {
                    wideAlert('The ceiling argument provided to ' +
                        'Macro.randominteger is positive Infinity. Cannot ' +
                        'execute.');
                    return;
                } else if (ceiling === Number.NEGATIVE_INFINITY) {
                    wideAlert('The ceiling argument provided to ' +
                        'Macro.randominteger is negative Infinity. Cannot ' +
                        'execute.');
                    return;
                }

                let floorVal = floor;
                let ceilingVal = ceiling;

                if (floorVal >= ceilingVal) {
                    wideAlert('The floor argument provided to ' +
                        'Macro.randominteger cannot be greater than or ' +
                        'equal to the ceiling argument. Cannot execute.');
                    return;
                }

                if (floor === undefined) {
                    floorVal = 0;
                }

                if (ceiling === undefined) {
                    ceilingVal = 1;
                }

                let exclusiveVal;
                if (exclusive === true ||
                    (typeof(exclusive) === 'string' &&
                        exclusive.toLowerCase() === 'exclusive'))
                {
                    exclusiveVal = true;
                } else if (typeof(exclusive) === 'string' &&
                    exclusive.toLowerCase() === 'floor')
                {
                    exclusiveVal = 'floor';
                } else if (typeof(exclusive) === 'string' &&
                    exclusive.toLowerCase() === 'ceiling')
                {
                    exclusiveVal = 'ceiling';
                }

                if (floorVal + 1 >= ceilingVal && exclusiveVal) {
                    wideAlert('The arguments for Macro.randominteger ' +
                        'indicate exclusive bounds, but the ceiling ' +
                        'argument is 1 or less greater than the floor ' +
                        'argument. Cannot execute.');
                    return;
                }

                let rand = Math.round(Math.random() *
                    (ceilingVal - floorVal) +
                    floorVal);

                if (rand === floor) {
                    if (exclusiveVal === true || exclusiveVal === 'floor') {
                        rand++;
                    }
                } else if (rand === ceiling) {
                    if (exclusiveVal === true || exclusiveVal === 'ceiling') {
                        rand--;
                    }
                }

                if (rand === -0) {
                    rand = 0;
                }

                return rand;
            },
        },
        'global');
    Macro.global['random-integer'] = Macro.global.randominteger;
    Macro.global.randomint = Macro.global.randominteger;
    Macro.global['random-int'] = Macro.global.randominteger;
    Macro.global.randinteger = Macro.global.randominteger;
    Macro.global['rand-integer'] = Macro.global.randominteger;
    Macro.global.randint = Macro.global.randominteger;
    Macro.global['rand-int'] = Macro.global.randominteger;

    Macro.add('rolldice',
        {
            handler(dice, returnType, discard) {
                let diceVal;
                if (typeof(dice) === 'string') {
                    diceVal = saferCombinedParse(dice);

                    if (typeof(diceVal) === 'string') {
                        diceVal = dice.split(' ');
                    } else if (!diceVal || typeof(diceVal) !== 'object') {
                        wideAlert('The dice argument provied to ' +
                            'Macro.rolldice is not valid. A valid argument ' +
                            'is either an array-like object or a string.');
                        return;
                    } else {
                        diceVal = from(diceVal);       
                    }
                }

                if (!('length' in diceVal)) {
                    wideAlert('The object provided to Macro.rolldice as ' +
                        'the dice argument does not have a length property. ' +
                        'Cannot execute.');
                    return;
                } else if (!diceVal.length) {
                    wideAlert('The array provided to Macro.rolldice as the ' +
                        'dice argument is empty. Cannot execute.');
                    return;
                } else {
                    const filtered = diceVal.filter(function(aa) {
                        return typeof(aa) === 'string' && aa.trim() !== '';
                    });

                    if (filtered.length === 0) {
                        wideAlert('The dice argument provided to ' +
                            'Macro.rolldice is not valid. A valid argument ' +
                            'is either an array or string, either containing ' +
                            'one or more strings of the type XdY, where X ' +
                            'is the number of times to roll that die, and ' +
                            'Y is the number of faces on that die.');
                        return;
                    }
                }

                let rolls = [];
                const regex = /\d+d\d+/;
                for (let ii = 0; ii < diceVal.length; ii++) {
                    const portion = diceVal[ii];
                    if (typeof(portion) !== 'string') {
                        wideAlert('The portion of the dice argument ' +
                            'provided to Macro.diceroll, ' + portion + ', ' +
                            'is not a string. Cannot execute.');
                        continue;
                    } else if (!portion) {
                        wideAlert('The portion of the dice argument ' +
                            'provided to Macro.diceroll is empty. Ignoring ' +
                            'portion.');
                        continue;
                    } else if (!regex.test(portion)) {
                        wideAlert('The portion of the dice argument ' +
                            'provided to Macro.diceroll, ' + portion + ', ' +
                            'does not match the valid dice pattern. The ' +
                            'valid pattern is an integer, immediately ' +
                            'followed by the letter d, immediately ' +
                            'followed by another integer, e.g. 2d6, or ' +
                            '11d4, or 12d18.');
                        continue;
                    } else if (portion.length !==
                        portion.match(regex)[0].length)
                    {
                        wideAlert('The portion of the dice argument ' +
                            'provided to Macro.diceroll, ' + portion +
                            ', is not valid. It contains characters ' +
                            'extraneous to the valid dice pattern. The ' +
                            'valid pattern is an integer, immediately ' +
                            'followed by the letter d, immediately ' +
                            'followed by another integer, e.g. 3d4, or ' +
                            '6d11, or 10d12.');
                        continue;
                    }

                    const split = portion.split('d');
                    const times = Number(split[0]);
                    const sides = Number(split[1]);

                    for (let jj = 0; jj < times; jj++) {
                        rolls.push(Math.floor(Math.random() * sides) + 1);
                    }
                }

                rolls = rolls.sort();
                if (discard && typeof(discard) === 'string') {
                    let discardVal = discard
                        .toLowerCase()
                        .replace(/[ .-]/g, '');

                    const split = discardVal.toString().split(/\d/);
                    let constant = discardVal.split(/[^\d%]/).slice(-1)[0];
                    let percent = false;
                    if (constant[constant.length - 1] === '%') {
                        percent = true;
                        constant = Number(constant.slice(0, -1)) / 100;
                    }

                    constant = Number(constant);

                    if (percent) {
                        if (split[0] === 'lowest') {
                            const lowIndex = Math.round(
                                rolls.length * constant);

                            rolls = rolls.slice(
                                rolls.length * Number(split[1]));
                        } else if (split[0] === 'highest') {
                            const highIndex = Math.round(
                                rolls.length - rolls.length * constant);

                            rolls = rolls.slice(0, highIndex);
                        } else if (split[0] === 'both') {
                            const lowIndex = Math.round(
                                rolls.length * constant);
                            const highIndex = Math.round(
                                rolls.length - rolls.length * constant);

                            rolls = rolls.slice(lowIndex, highIndex);
                        } else {
                            wideAlert('The discard argument provided to ' +
                                'Macro.rolldice is not valid. Returning as ' +
                                'is.');
                        }
                    } else {
                        if (split[0] === 'lowest') {
                            const lowIndex = constant;

                            rolls = rolls.slice(lowIndex);
                        } else if (split[0] === 'highest') {
                            const highIndex = rolls.length - constant;

                            rolls = rolls.slice(0, highIndex);
                        } else if (split[0] === 'both') {
                            const lowIndex = constant;
                            const highIndex = rolls.length - constant;

                            rolls = rolls.slice(lowIndex, highIndex);
                        } else {
                            wideAlert('The discard argument provided to ' +
                                'Macro.rolldice is not valid. Returning as ' +
                                'is.');
                        }
                    }
                }

                if (!returnType ||
                    (typeof(returnType) === 'string' &&
                        returnType.toLowerCase() === 'sum'))
                {
                    return rolls.reduce((a, b) => a + b);
                } else {
                    return rolls;
                }
            }
        },
        'global');
    Macro.global['roll-dice'] = Macro.global.rolldice;
    Macro.global.diceroll = Macro.global.rolldice;
    Macro.global['dice-roll'] = Macro.global.rolldice;

    Macro.add('function',
        {
            isDoubleTag: true,
            defer: true,
            handler(name) {
                if (arguments.length < 2) {
                    wideAlert('At least two arguments, the function name ' +
                        'and the callback, must be provided to ' +
                        'Macro.function to create a function/widget.');
                    return;
                }

                const argNames = from(arguments).slice(1, -1)
                    .map(name => {
                        const bool = Boolean(name);
                        if (name) {
                            return name.toString();
                        } else {
                            return null;
                        }
                    })
                    .filter(name => name);
                
                State.addFunction.call(this,
                    name,
                    false,
                    argNames,
                    arguments[arguments.length - 1]);
            },
        },
        'global');
    Macro.global.widget = Macro.global.function;

    Macro.add('functionoverride',
        {
            isDoubleTag: true,
            defer: true,
            handler(name) {
                if (arguments.length < 2) {
                    wideAlert('At least two arguments, the function name ' +
                        'and the callback, must be provided to ' +
                        'Macro.function to create a function/widget.');
                    return;
                }

                const argNames = from(arguments)
                .slice(1, -1)
                .map(name => {
                    const bool = Boolean(name);
                    if (name) {
                        return name.toString();
                    } else {
                        return null;
                    }
                })
                .filter(name => name);
                
                State.addFunction.call(this,
                    name,
                    true,
                    argNames,
                    arguments[arguments.length - 1]);
            },
        },
        'global');
    Macro.global['function-override'] = Macro.global.functionoverride;
    Macro.global.widgetoverride = Macro.global.functionoverride;
    Macro.global['widget-override'] = Macro.global.functionoverride;
    
    Macro.add('return',
        {
            handler(value) {
                const present = State.stacks.present;
                if (!present.storyTheme.debug.failHard) {
                    wideAlert('Return statements can only be used with ' +
                        'State.stacks.current.storyTheme.debug.failHard set ' +
                        'to true.');
                    return;
                } else if (!$(this).parents('.functionContainer').length) {
                    // causes error, aborts
                    wideAlert('Illegal return statement. Returns must be ' +
                        'contained in user-created functions/widgets.');
                    return;
                }

                const match = document.createElement('tw-argument');
                if (isNode(value)) {
                    match.appendChild(value);
                } else {
                    match.innerHTML = value;
                }

                const returning = $(this).parents('.functionContainer')[0];
                if (returning) {
                    Matches.functionsReturning.push(returning);
                }

                return match;
            },
        },
        'global');

    Macro.add('bind',
        {
            isDoubleTag: true,
            defer: true,
            handler(name, callback) {
                if (!name || typeof(name) !== 'string') {
                    wideAlert('The name argument passed to ' +
                        'Macro.global.bind is not valid. Cannot execute.');
                    return;
                } else if (!callback || typeof(callback) !== 'string') {
                    wideAlert('There was no callback argument provided to ' +
                        'Macro.global.bind. Cannot execute.');
                    return;
                }

                const nameVal = name.trim();
                
                const match = document.createElement('tw-binding');
                this.appendChild(match);
                match.setAttribute('data-binding-name', nameVal);
                match.textContent = callback;
                Compiler.compile(match);

                State.addBinding(nameVal, callback);
                return match;
            },
        },
        'global');

    Macro.add('updatebinding',
        {
            handler(name) {
                if (!name || typeof(name) !== 'string') {
                    wideAlert('The name argument passed to ' +
                        'Macro.updatebinding is not valid. Not updating.');
                    return;
                } else if (!(name in State.stacks.present.bindingState)) {
                    wideAlert('The name argument passed to ' +
                        'Macro.updatebinding does not correspond with a ' +
                        'current binding. Cannot update.');
                    return;
                }

                State.updateBinding(name);
            },
        },
        'global');
    Macro.global['update-binding'] = Macro.global.updatebinding;

    Macro.add('updatebindings',
        {
            handler() {
                State.updateBindings();
            },
        },
        'global');
    Macro.global['update-bindings'] = Macro.global.updatebindings;

    Macro.add('getbinding',
        {
            handler(name) {
                if (!name || typeof(name) !== 'string') {
                    wideAlert('The name argument provided to ' +
                        'Macro.global.getbinding is not a valid string. ' +
                        'Cannot execute.');
                    return;
                }

                const present = State.stacks.present;
                const nameVal = name.trim();
                if (nameVal in present.bindingStates) {
                    return JSON.stringify(present.bindingStates[nameVal]);
                }

                return null;
            },
        },
        'global');
    Macro.global['get-binding'] = Macro.global.getbinding;

    Macro.add('requestanimationframe',
        {
            isDoubleTag: true,
            defer: true,
            handler(callback) {
                const $span = $('<span></span>');
                $(this).contents()
                    .filter((_, node) => {
                        $(node).attr('data-type') !== 'openTagTerminator'
                    }).appendTo($span);

                const func = () => {
                    Compiler.compile($span[0]);
                };

                requestAnimationFrame(func);

                return $span[0];
            },
        },
        'global');
    Macro.global['request-animation-frame'] =
        Macro.global.requestanimationframe;
    Macro.global.requestanimframe = Macro.global.requestanimationframe;
    Macro.global['request-anim-frame'] = Macro.global.requestanimationframe;
    Macro.global.raf = Macro.global.requestanimationframe;

    Macro.add('interval',
        {
            isDoubleTag: true,
            defer: true,
            handler(time, callback) {
                if (!time) {
                    wideAlert('The time argument passed to Macro.live is not ' +
                        'valid. Cannot perform live.');
                    return;
                } else if (!callback || typeof(callback) !== 'string') {
                    wideAlert('The callback argument passed to ' +
                        'Macro.global.live is not valid.');
                    return;
                }

                let timeObj = {
                    amt: 0,
                    unit: 'ms'
                };

                if (typeof(time) === 'string') {
                    time = time.trim().toLowerCase();
                    if (time.endsWith('ms')) {
                        const num =
                            Number(time.split('ms').slice(0, -1).join());
                        if (Number.isNaN(num) || num <= 0) {
                            wideAlert('The time value passed to Macro.live, ' +
                                time + ', is not valid.');
                            return;
                        }

                        timeObj.amt = num;
                    } else if (time.endsWith('s')) {
                        const num =
                            Number(time.split('s').slice(0, -1).join());
                        if (Number.isNaN(num) || num <= 0) {
                            wideAlert('The time value passed to Macro.live, ' +
                                time + ', is not valid.');
                            return;
                        }
                        timeObj.amt = num;
                        timeObj.unit = 's';
                    } else {
                        wideAlert('The time value passed to Macro.live, ' +
                            time + ', is not valid.');
                        return;
                    }
                } else if (typeof(time) === 'number') {
                    if (time <= 0) {
                        wideAlert('The time argument passed to Macro.live must ' +
                            'be greater than zero. Cannot perform live.');
                        return;
                    }

                    timeObj.amt = time;
                }

                const displayer = document.createElement('span');
                displayer.className = 'intervalDisplayer';

                const deferrer = document.createElement('tw-deferrer');
                deferrer.className = 'hidden';
                deferrer.textContent = callback;

                let timeVal = timeObj.amt;
                if (timeObj.unit === 's') {
                    timeVal *= 1000;
                }

                const intID = window.setInterval(() => {
                    if (!document.contains(displayer)) {
                        return;
                    }

                    displayer.textContent = deferrer.textContent;

                    Compiler.compile(displayer);
                }, timeVal);

                displayer.dataset.intervalId = intID;
                deferrer.dataset.intervalId = intID;

                const parent = document.createElement('span');
                parent.appendChild(displayer);
                parent.appendChild(deferrer);

                return parent;
            },
        },
        'global');
    Macro.global.setinterval = Macro.global.interval;
    Macro.global['set-interval'] = Macro.global.interval;
    Macro.global.live = Macro.global.interval;

    Macro.add('delay',
        {
            isDoubleTag: true,
            defer: true,
            handler(time, callback) {
                if (!time) {
                    wideAlert('The time argument passed to Macro.live is ' +
                        'not valid. Cannot execute.');
                    return;
                } else if (!callback) {
                    wideAlert('The callback argument passed to Macro.live ' +
                        'is not valid. Cannot execute.');
                    return;
                }

                let timeObj = {
                    amt: 0,
                    unit: 'ms',
                };

                if (typeof(time) === 'string') {
                    time = time.trim().toLowerCase();
                    if (time.endsWith('ms')) {
                        const num =
                            Number(time.split('ms').slice(0, -1).join());
                        if (Number.isNaN(num) || num <= 0) {
                            wideAlert('The time argument passed to ' +
                                'Macro.delay, ' + time + ', is not valid. ' +
                                'Cannot execute.');
                            return;
                        }

                        timeObj.amt = num;
                    } else if (time.endsWith('s')) {
                        const num =
                            Number(time.split('s').slice(0, -1).join());
                        if (Number.isNaN(num) || num <= 0) {
                            wideAlert('The time argument passed to ' +
                                'Macro.delay, ' + time + ', is not valid. ' +
                                'Cannot execute.');
                            return;
                        }

                        timeObj.amt = num;
                        timeObj.unit = 's';
                    } else {
                        wideAlert('The time argument passed to Macro.live, ' +
                            time + ', is not valid. Cannot execute.');
                        return;
                    }
                } else if (typeof(time) === 'number') {
                    if (time <= 0) {
                        wideAlert('The time argument passed to Macro.live ' +
                            'must be greater than zero. Cannot execute.');
                        return;
                    }

                    timeObj.amt = time;
                }

                const displayer = document.createElement('span');

                const deferrer = document.createElement('tw-deferrer');
                deferrer.className = 'hidden';
                deferrer.textContent = callback;

                let timeVal = timeObj.amt;
                if (timeObj.unit === 's') {
                    timeVal *= 1000;
                }

                const intID = window.setTimeout(() => {
                    if (!document.contains(displayer)) {
                        return;
                    }

                    displayer.textContent = deferrer.textContent;

                    Compiler.compile(displayer);
                }, timeVal);

                displayer.setAttribute('data-timeout-id', intID);
                deferrer.setAttribute('data-timeout-id', intID);

                const span = document.createElement('span');
                span.appendChild(displayer);
                span.appendChild(deferrer);

                return span;
            },
        },
        'global');
    Macro.global.timeout = Macro.global.delay;
    Macro.global.settimeout = Macro.global.delay;
    Macro.global['set-timeout'] = Macro.global.delay;

    Macro.add('stop',
        {
            handler() {
                const selector = '[data-interval-id]';
                const $elem = $(this).parents(selector);

                if (!$elem.length) {
                    wideAlert('Macro.stop/clear can only be used within ' +
                        'macros that also contain a live/interval or ' +
                        'delay/timeout macro. Cannot execute.');
                    return;
                }

                const id = $elem.attr('data-interval-id');
                if (id) {
                    const intervalIDParsed = Math.floor(Number(id));
                    window.clearInterval(intervalIDParsed);
                }

                $elem.parent().remove();
            },
        },
        'global');
    Macro.global.clear = Macro.global.stop;
    Macro.global.clearinterval = Macro.global.stop;
    Macro.global['clear-interval'] = Macro.global.stop;

    Macro.add('script',
        {
            isDoubleTag: true,
            defer: true,
            handler() {
                for (let ii = 0; ii < arguments.length; ii++) {
                    let processed = (arguments[ii] || '')
                        .toString()
                        .replace(/[\n\r\t]/g, '');

                    try {
                        eval(processed);
                    } catch (e) {
                        wideAlert(e);
                    }
                }
            },
        },
        'global');

    Macro.add('tojson',
        {
            isDoubleTag: true,
            defer: true,
            handler(value) {
                let processed = value.replace(/[\n\r\t]/g, '');

                // remove hooks
                if (this.getAttribute('data-dialect') === 'harlowe') {
                    processed = processed.replace(/^\s*\[|\s*]$/g, '');
                }

                processed = '(' + processed + ')';

                try {
                    return JSON.stringify(eval(processed));
                } catch (e) {
                    wideAlert(e);
                }
            },
        },
        'global');
    Macro.global['to-json'] = Macro.global.tojson;
    Macro.global.jsonstringify = Macro.global.tojson;
    Macro.global['json-stringify'] = Macro.global.tojson;

    Macro.add('alert',
        {
            handler() {
                alert(from(arguments).join(' '));
            },
        },
        'global');

    Macro.add('log',
        {
            handler() {
                const str = from(arguments)
                    .map(arg => {
                        const argVal = saferJSONParse(arg);
                        if (typeof(argVal) === 'object') {
                            if (State.isPointer(argVal)) {
                                const val = State.dereferencePointer(argVal);
                                if (typeof(val) === 'object') {
                                    return JSON.stringify(val);
                                } else {
                                    return val;
                                }
                            } else {
                                return JSON.stringify(argVal);
                            }
                        } else {
                            return argVal;
                        }
                    }).join(', ');

                console.log(str);
            },
        },
        'global');
    Macro.global.consolelog = Macro.global.log;
    Macro.global['console-log'] = Macro.global.log;

    Macro.add('debugger',
        {
            handler() {
                debugger;
            },
        });

    Macro.add('passageinfo',
        {
            handler(passageName, propName) {
                let passageNameVal;
                if (passageName) {
                    passageNameVal = passageName;
                } else {
                    passageNameVal = State.stacks.present.passageName;
                }

                const selector = 'tw-passagedata[name="' +
                    passageNameVal +
                    '"]';
                const $curPass = $(selector);
                if (!$curPass.length) {
                    if (passageName) {
                        wideAlert('There is no tw-passagedata element ' +
                            'corresponding to the name, ' + passageName +
                            ', passed to Macro.passageinfo. Cannot execute.');
                        return;
                    } else {
                        wideAlert('When calling Macro.passageinfo, it was ' +
                            'determined that the current state of the ' +
                            'story has become corrupted and the ' +
                            'State.stacks.present.passageName property is ' +
                            'inaccurate. Cannot execute.');
                        return;
                    }
                }

                const passObj = {
                    pid: $curPass.attr('pid'),
                    name: passageNameVal,
                    tags: $curPass.attr('tags'),
                    position: $curPass.attr('position'),
                };

                let retVal = JSON.stringify(passObj);
                if (propName) {
                    if (!(propName in passObj)) {
                        wideAlert('There was a propName argument, ' +
                            propName +
                            'provided, but it is not valid. Valid propName ' +
                            'values are ' +
                            getOwnPropertyNames(passObj).join(', '));
                        return;
                    }

                    retVal = passObj[propName];
                }

                return retVal;
            },
        },
        'global');
    Macro.global['passage-info'] = Macro.global.passageinfo;

    Macro.add('objectget',
        {
            handler(value, propName) {
                let val = saferJSONParse(value);
                if (!val || typeof(val) !== 'object') {
                    return null;
                } else {
                    return val[propName];
                }
            },
        },
        'global');
    Macro.global['object-get'] = Macro.global.objectget;
    Macro.global['objget'] = Macro.global.objectget;
    Macro.global['obj-get'] = Macro.global.objectget;

    Macro.add('getproperty',
        {
            handler(object, propName) {
                let obj;
                if (typeof(object) === 'string') {
                    if (!object) {
                        wideAlert('A string was provided as the object ' +
                            'argument to Macro.getproperty, but it is ' +
                            'empty. Cannot execute.');
                        return;
                    }

                    obj = Macro.saferCombinedParse(obj);

                    if (!obj || typeof(obj) !== 'object') {
                        wideAlert('The string provided as the object ' +
                            'argument to Macro.getproperty cannot be ' +
                            'parsed or evalled into a valid object. Cannot ' +
                            'execute.');
                        return;
                    }
                } else if (typeof(object) === 'object') {
                    if (!object) {
                        wideAlert('An object was provided as the object ' +
                            'argument to Macro.getproperty, but it is ' +
                            'null, and null does not have properties. ' +
                            'Cannot execute.');
                        return;
                    }

                    obj = object;
                } else {
                    wideAlert('The object argument provided to ' +
                        'Macro.getproperty is neither an object nor a ' +
                        'string. Cannot execute.');
                    return;
                }

                if (!propName || typeof(propName) !== 'string') {
                    wideAlert('The propName argument provided to ' +
                        'Macro.getproperty is not a string, or is an empty ' +
                        'string. Cannot execute.');
                    return;
                } else if (!(propName in obj)) {
                    wideAlert('There is no ' + propName + ' property in ' +
                        'the object argument provided to ' +
                        'Macro.getproperty. Cannot execute.');
                    return;
                }

                if (typeof(object[propName]) === 'object') {
                    return JSON.stringify(object[propName]);
                } else {
                    return propName;
                }
            },
        },
        'global');
    Macro.global['get-property'] = Macro.global.getproperty;
    Macro.global.getprop = Macro.global.getproperty;
    Macro.global['get-prop'] = Macro.global.getproperty;

    Macro.add('math',
        {
            handler(propName) {
                if (typeof(propName) !== 'string' || !propName) {
                    wideAlert('The propName argument passed to Macro.math is ' +
                        'not valid. Cannot perform action.');
                    return;
                }

                const prop = Math[propName];
                if (!prop) {
                    wideAlert('The propName argument passed to Macro.math does ' +
                        'not correspond to a property within the Math ' +
                        'object. Cannot perform action.');
                    return;
                }

                if (isFunction(prop)) {
                    return prop.apply(null, from(arguments).slice(1));
                } else {
                    return prop;
                }
            },
        },
        'global');

    Macro.add('number',
        {
            handler(propName) {
                if (typeof(propName) !== 'string' || !propName) {
                    wideAlert('The propName argument passed to Macro.number ' +
                        'is not valid. Cannot perform action.');
                    return;
                }

                const prop = Number[propName];
                if (!prop) {
                    wideAlert('The propName argument passed to Macro.number ' +
                        'does not correspond to a property within the ' +
                        'Number object. Cannot perform action.');
                    return;
                }

                if (isFunction(prop)) {
                    return prop.apply(null, from(arguments).slice(1));
                } else {
                    return prop;
                }
            },
        },
        'global');

    Macro.add('access',
        {
            handler(propName) {
                if (!propName || typeof(propName) !== 'string') {
                    wideAlert('The propName argument passed to ' +
                        'Macro.access is not valid. Cannot execute.');
                    return;
                }

                const split = propName.split('.');
                let parent = saferEval(split[0]);
                let child = parent[split[1]];

                for (let ii = 2; ii < split.length; ii++) {
                    parent = child;
                    child = parent[split[ii]];
                }

                let prop = child;

                if (parent === undefined || child === undefined) {
                    prop = saferEval(propName);
                }

                if (prop === undefined) {
                    wideAlert('The propName argument provided to ' +
                        'Macro.access does not correspond to any ' +
                        'properties. Cannot execute.');
                    return;
                }

                let result;
                if (isFunction(prop)) {
                    result = prop.apply(parent, from(arguments).slice(1));
                } else {
                    result = prop;
                }

                if (typeof(result) === 'object') {
                    return JSON.stringify(result);
                } else {
                    return result;
                }
            },
        },
        'global');

    Macro.add('storetracerygrammar',
        {
            isDoubleTag: true,
            handler(name, grammar, modifiers) {
                if (typeof(name) !== 'string' || name === '') {
                    wideAlert('The name argument passed to ' +
                        'Macro.storetracerygrammar is not valid. Cannot ' +
                        'add.');
                    return;
                } else if (!grammar) {
                    wideAlert('The grammar argument passed to ' +
                        'Macro.storetracerygrammar is not valid. Cannot ' +
                        'add.');
                    return;
                }
                State.addTraceryGrammar(name, grammar, modifiers);
            },
        },
        'global');
    Macro.global['store-tracery-grammar'] = Macro.global.storetracerygrammar;

    Macro.add('setgrammarmodifiers',
        {
            handler(name, modifiers) {
                if (typeof(name) !== 'string' || name === '') {
                    wideAlert('The name argument passed to ' +
                        'Macro.setgrammarmodifiers is not valid. Cannot ' +
                        'modify.');
                    return;
                } else if (!(name in State.traceryStore)) {
                    wideAlert('There is no tracery grammer by the name ' + name +
                        ' in State.traceryStore. Add the grammar before ' +
                        'attempting to modify it.');
                    return;
                } else if (!modifiers) {
                    wideAlert('The modifiers argument passed to ' +
                        'Macro.setgrammarmodifiers is not valid. Cannot ' +
                        'modify.');
                }

                const json = State.traceryStore[name].toJSON();
                delete State.traceryStore[name];
                State.addTraceryGrammar(name, modifiers);
            },
        },
        'global');
    Macro.global['set-grammar-modifiers'] = Macro.global.setgrammarmodifiers;

    Macro.add('printtracerygrammar',
        {
            handler(name, rule, allowEscapeChars) {
                if (typeof(name) !== 'string' || name === '') {
                    wideAlert('The name argument passed to ' +
                        'Macro.printtracerygrammar is not valid. Cannot ' +
                        'print.');
                    return;
                } else if (!(name in State.traceryStore)) {
                    wideAlert('There is no tracery grammer by the name ' + name +
                        ' in State.traceryStore. Add the grammar before ' +
                        'attempting to print it.');
                    return;
                }
                const grammar = State.traceryStore[name];
                if (!rule) {
                    rule = '#origin#';
                }

                return grammar.flatten(rule, allowEscapeChars);
            },
        },
        'global');
    Macro.global['print-tracery-grammar'] = Macro.global.printtracerygrammar;

    Macro.add('mouseevent',
        {
            handler(propName) {
                if (propName === undefined) {
                    return window.mouseEvent;
                } else if (propName in window.mouseEvent) {
                    return window.mouseEvent[propName];
                } else {
                    wideAlert('There is no ' + propName +
                        ' property in window.mouseEvent.');
                    return null;
                }
            },
        },
        'global');
    Macro.global['mouse-event'] = Macro.global.mouseevent;

    Macro.add('keyboardevent',
        {
            handler(key, propName) {
                if (!key || key === '*') {
                    return window.keyEvents;
                } else {
                    if (!(key in window.keyEvents)) {
                        wideAlert('There is no ' + key +
                            ' property in window.keyEvents.');
                        return null;
                    }
                    if (!propName) {
                        return window.keyEvents[key];
                    } else if (propName in window.keyEvents) {
                        return window.keyEvents[key][propName];
                    } else {
                        wideAlert('There is no ' + propName +
                            ' property in window.keyEvents.');
                        return null;
                    }
                }
            },
        },
        'global');
    Macro.global['keyboard-event'] = Macro.global.keyboardevent;

    Macro.add('makeelement',
        {
            handler(str) {
                if (!isElementRawString(str)) {
                    wideAlert('The str argument passed to ' +
                        'Macro.makeelement is not valid. Cannot make.');
                    return;
                }

                const elem = $(str)[0];
                elem.setAttribute('data-gately', '');
                return elem;
            },
        },
        'global');
    Macro.global['make-element'] = Macro.global.makeelement;

    Macro.add('maketextnode',
        {
            handler(content) {
                if (!content) {
                    wideAlert('The content argument passed to ' +
                        'Macro.maketextnode is not valid. Cannot make.');
                    return;
                }

                return document.createTextNode(content);
            },
        },
        'global');
    Macro.global['make-text-node'] = Macro.global.maketextnode;

    Macro.add('setelementcontent',
        {
            handler(element, content) {
                if (!element ||
                    (typeof(element) !== 'string' &&
                        !isElement(element)))
                {
                    wideAlert('The element argument passed to ' +
                        'Macro.setelementcontent is not valid. Cannot set ' +
                        'element content.');
                    return;
                } else if (!content) {
                    wideAlert('The content argument passed to ' +
                        'Macro.setelementcontent is not valid. Cannot set ' +
                        'element content.');
                    return;
                }

                let validElement = element;
                if (typeof(validElement) === 'string') {
                    try {
                        validElement = document.querySelector(validElement);
                    } catch (e) { /* do nothing */ }
                }

                if (isElement(content) || isNode(content)) {
                    $(element).contents().remove();
                    element.parentElement.appendChild(content);
                } else {
                    element.innerHTML = content;
                }

                return element;
            },
        },
        'global');
    Macro.global['set-element-content'] = Macro.global.setelementcontent;

    Macro.add('appendnode',
        {
            handler(parentElement, childElement) {
                if (!parentElement ||
                    (typeof(parentElement) !== 'string' &&
                        !isElement(parentElement)))
                {
                    wideAlert('The parentElement argument passed to ' +
                        'Macro.appendelement is not valid. Cannot append.');
                } else if (!childElement ||
                    (typeof(childElement) !== 'string' &&
                        !isElement(childElement) &&
                        !isNode(childElement)))
                {
                    wideAlert('The childElement argument passed to ' +
                        'Macro.appendelement is not valid. Cannot append.');
                }

                let validParent = parentElement;
                if (typeof(validParent) === 'string') {
                    try {
                        validParent = document.querySelector(parentElement);
                    } catch (e) { /* do nothing */ }

                    if (!validParent) {
                        wideAlert('The string passed as the parentElement ' +
                            'argument to Macro.appendelement is not a ' +
                            'valid selector.');
                        return;
                    }
                }

                let validChild = childElement;
                if (typeof(validChild) === 'string') {
                    validChild = $(childElement)[0];

                    if (!validChild) {
                        wideAlert('The string passed as the childElement ' +
                            'argument to Macro.appendelement is not a ' +
                            'valid selector.');
                        return;
                    }
                }

                validParent.appendChild(validChild);

                return validChild;
            },
        },
        'global');
    Macro.global['append-node'] = Macro.global.appendnode;

    Macro.add('removenode',
        {
            handler(node) {
                if (!node ||
                    (typeof(node) !== 'string' &&
                        !isNode(node) &&
                        !isElement(node)))
                {
                    wideAlert('The node argument passed to ' +
                        'Macro.removenode is not valid. Cannot append.');
                } else if (!node.parentElement) {
                    wideAlert('The node.parentElement passed to ' +
                        'Macro.removeelement is not valid. Cannot append.');
                }

                let validNode = node;
                if (typeof(node) === 'string') {
                    try {
                        validNode = document.querySelector(node);
                    } catch (e) { 
                        wideAlert('The string passed as the childElement ' +
                            'argument to Macro.removeelement is not a ' +
                            'valid selector.');
                        return;
                    }
                }

                validNode.parentElement.removeChild(validNode);

                return validNode;
            },
        },
        'global');
    Macro.global['remove-node'] = Macro.global.removenode;

    Macro.add('elementscss',
        {
            handler(elementsOrSelector, property, value) {
                if (!elementsOrSelector ||
                    (typeof(elementsOrSelector) !== 'string' &&
                        !isElement(elementsOrSelector)))
                {
                    wideAlert('The elementsOrSelector argument passed to ' +
                        'Macro.elementscss is not valid. Cannot change css.');
                    return;
                } else if (isElementRawString(validElements)) {
                    wideAlert('The Macro.elementscss method can only ' +
                        'be used to modify the inline style of ' +
                        'pre-existing elements, not construct new ' +
                        'elements. Cannot change css.');
                    return;
                } else if (!property) {
                    wideAlert('The property argument passed to ' +
                        'Macro.elementscss is not valid. Cannot change css.');
                    return;
                } else if (value === undefined) {
                    wideAlert('The value argument passed to ' +
                        'Macro.elementscss is not set. Cannot change css.');
                    return;
                }

                const $validElements = $(elementsOrSelector);

                $validElements.css(property, value);

                return from($(elementsOrSelector));
            },
        },
        'global');
    Macro.global['elements-css'] = Macro.global.elementscss;

    Macro.add('elementsattr',
        {
            handler(elementsOrSelector, property, value) {
                if (!elementsOrSelector ||
                    (typeof(elementsOrSelector) !== 'string' &&
                        !isElement(elementsOrSelector)))
                {
                    wideAlert('The elementsOrSelector argument passed to ' +
                        'Macro.elementsattr is not valid. Cannot change ' +
                        'attr.');
                    return;
                } else if (isElementRawString(validElements)) {
                    wideAlert('The Macro.elementsattr method can only ' +
                        'be used to modify the attributes of ' +
                        'pre-existing elements, not construct new ' +
                        'elements. Cannot change attr.');
                    return;
                } else if (!property) {
                    wideAlert('The property argument passed to ' +
                        'Macro.elementsattr is not valid. Cannot change ' +
                        'attr.');
                    return;
                } else if (value === undefined) {
                    wideAlert('The value argument passed to ' +
                        'Macro.elementsattr is not set. Cannot change attr.');
                    return;
                }

                const $validElements = $(elementsOrSelector);

                $validElements.attr(property, value);

                return from($(elementsOrSelector));
            },
        },
        'global');
    Macro.global['elements-attr'] = Macro.global.elementsattr;

    Macro.add('jquery',
        {
            handler(selector, extension) {
                if (!selector || typeof(selector) !== 'string') {
                    wideAlert('The selector argument provided to ' +
                        'Macro.global.jquery is not a valid string.');
                } else if (arguments.length < 2) {
                    wideAlert('The extension argument was not provided to ' +
                        'Macro.global.jquery.');
                    return;
                }

                const $jqObj = $(selector);
                    
                if (!(extension in $jqObj)) {
                    wideAlert('The property ' + extension + ' does not ' +
                        'exist in the jQuery ($) object.');
                    return;
                } else if (isFunction($jqObj[extension])) {
                    $jqObj[extension](...from(arguments).slice(2));
                    return;
                } else {
                    if (value === undefined) {
                        return $jqObj[extension];
                    } else {
                        wideAlert('The ' + extension + ' property in ' +
                            'the jQuery object is not a function. ' +
                            'Cannot execute.');
                        return;
                    }
                }
            },
        },
        'global');
    Macro.global.$ = Macro.global.jquery;

    Macro.add('addmacroalias',
        {
            handler(macroName, aliasName, disposeAfterPassage, override) {
                if (!macroName) {
                    wideAlert('The macroName argument passed to ' +
                        'Macro.addmacroalias is not valid. Cannot add macro alias.');
                    return;
                } else if (!aliasName) {
                    wideAlert('The aliasName argument passed to ' +
                        'Macro.addmacroalias is not valid. Cannot add macro alias.');
                    return;
                }

                State.addMacroAlias(
                    macroName,
                    aliasName,
                    disposeAfterPassage,
                    override);
            },
        },
        'global');
    Macro.global['add-macro-alias'] = Macro.global.addmacroalias;

    Macro.add('printmacroinfo',
        {
            handler(macroName) {
                if (!macroName) {
                    let retVal = '';
                    for (let nsName in Macro) {
                        if (typeof(Macro[nsName]) === 'object') {
                            const ns = Macro[nsName];
                            retVal += 'namespace: ' + nsName + '\n';


                            for (let macroName in Macro[nsName]) {
                                const macro = Macro[nsName][macroName];
                                if (macro && typeof(macro) === 'object' && macro.handler) {
                                    retVal += '&nbsp;&nbsp;&nbsp;&nbsp;macro: ' + macroName + '\n' +
                                        '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                                        '&nbsp;&nbsp;&nbsp;isDoubleTag: ' +
                                        (macro.isDoubleTag || false) +
                                        '\n' +
                                        '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                                        '&nbsp;&nbsp;&nbsp;defer: ' +
                                        (macro.defer || false) +
                                        '\n' +
                                        '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                                        '&nbsp;&nbsp;&nbsp;description: ' +
                                        (macro.description || 'Empty!') +
                                        '\n' +
                                        '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                                        '&nbsp;&nbsp;&nbsp;example: ' +
                                        (macro.example || 'Empty!') +
                                        '\n' +
                                        '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                                        '&nbsp;&nbsp;&nbsp;methodSignature: ' +
                                        (macro.methodSignature || 'Empty!') +
                                        '\n';
                                }
                            }
                        }
                    }

                    return retVal;
                } else {
                    let nsName = 'global';
                    if (/^.+\..+$/.test(macroName)) {
                        macroName = macroName.split('.').slice(1).join('.');
                        nsName = macroName.split('.')[0];
                    }

                    const ns = Macro[nsName];
                    if (!ns) {
                        wideAlert('There is no macro namespace by the name ' + nsName +
                            ' in the Macro object. Cannot print.');
                        return;
                    }

                    const macro = Macro[nsName][macroName];
                    if (!macro) {
                        wideAlert('There is no macro by the name ' + macroName +
                            ' in the Macro[' + nsName + '] namespace. Cannot print.');
                        return;
                    }

                    return 'macro: ' + macroName + '\n' +
                        '&nbsp;&nbsp;&nbsp;&nbsp;isDoubleTag: ' +
                        (macro.isDoubleTag || false) +
                        '\n' +
                        '&nbsp;&nbsp;&nbsp;&nbsp;defer: ' +
                        (macro.defer || false) +
                        '\n' +
                        '&nbsp;&nbsp;&nbsp;&nbsp;description: ' +
                        (macro.description || 'Empty!') +
                        '\n' +
                        '&nbsp;&nbsp;&nbsp;&nbsp;example: ' +
                        (macro.example || 'Empty!') +
                        '\n' +
                        '&nbsp;&nbsp;&nbsp;&nbsp;methodSignature: ' +
                        (macro.methodSignature || 'Empty!') +
                        '\n';
                }
            },
        },
        'global');
    Macro.global['print-macro-info'] = Macro.global.printmacroinfo;

    Macro.add('printnamespaceinfo',
        {
            handler(nsName) {
                let retVal = '';

                if (nsName) {
                    retVal += ' namespace: ' + nsName +
                        JSON.stringify(Macro[nsName]);
                } else {
                    for (let nsName in Macro) {
                        retVal += 'namespace: ' + nsName +
                            JSON.stringify(Macro[nsName]) +
                            '\n';
                    }

                }

                return retVal;
            },
        },
        'global');
    Macro.global['print-namespace-info'] = Macro.global.printnamespaceinfo;

    Macro.add('printaliasinfo',
        {
            handler(aliasName) {
                let retVal = '';

                if (aliasName) {
                    retVal += 'alias: ' + aliasName +
                        JSON.stringify(State.macroAliasStore[aliasName]);
                } else {
                    for (let aliasName in State.macroAliasStore) {
                        retVal += 'alias: ' + aliasName +
                            JSON.stringify(State.macroAliasStore[aliasName]) +
                            '\n';
                    }

                }

                return retVal;
            },
        },
        'global');
    Macro.global['print-alias-info'] = Macro.global.printaliasinfo;

    Macro.add('printfunctioninfo',
        {
            handler(functionName) {
                let retVal = '';

                if (functionName) {
                    retVal += 'function: ' + functionName +
                        JSON.stringify(State.functionStore[functionName]);
                } else {
                    for (let functionName in State.functionStore) {
                        retVal += 'function: ' + functionName +
                            JSON.stringify(State.functionStore[functionName]) +
                            '\n';
                    }
                }

                return retVal;
            },
        },
        'global');
    Macro.global['print-function-info'] = Macro.global.printfunctioninfo;

    Macro.add('edittheme',
        {
            handler(property, value) {
                if (!property) {
                    wideAlert('The property argument passed to Macro.config ' +
                        'is not valid. Cannot alter configuration.');
                    return;
                } else if (value === undefined) {
                    wideAlert('The value argument passed to Macro.config ' +
                        'is not valid. Cannot alter configuration.');
                    return;
                }

                const obj = {};
                obj[property] = value;
                Config.editTheme(obj);
            },
        },
        'global');
    Macro.global['edit-theme'] = Macro.global.settheme;

    Macro.add('savetheme',
        {
            handler(name, theme) {
                if (!name) {
                    wideAlert('The name argument passed to Macro.savetheme ' +
                        'is not valid or was not provided. Cannot save ' +
                        'theme.');
                    return;
                } else if (name in State.themeStore) {
                    wideAlert('The name argument passed to Macro.savetheme ' +
                        'already exists within State.themeStore. Cannot ' +
                        'save theme.');
                    return;
                }

                let themeObj = theme || State.stacks.present.storyTheme;
                if (typeof(themeObj) === 'string') {
                    themeObj = saferJSONParse(theme);
                }

                if (!themeObj || typeof(themeObj) !== 'object') {
                    wideAlert('The theme object is either not a valid object ' +
                        'or is a string that cannot be parsed into a valid ' +
                        'object. Cannot save theme.');
                    return;
                }

                State.saveTheme(name, themeObj);
            },
        },
        'global');
    Macro.global['save-theme'] = Macro.global.savetheme;

    Macro.add('savethemeoverride',
        {
            handler(name, theme) {
                if (!name) {
                    wideAlert('The name argument passed to Macro.savetheme ' +
                        'is not valid or was not provided. Cannot save ' +
                        'theme.');
                    return;
                }

                let themeObj = theme || State.stacks.present.storyTheme;
                if (typeof(themeObj) === 'string') {
                    themeObj = saferJSONParse(theme);
                }

                if (!themeObj || typeof(themeObj) !== 'object') {
                    wideAlert('The theme object is either not a valid object ' +
                        'or is a string that cannot be parsed into a valid ' +
                        'object. Cannot save theme.');
                    return;
                }

                State.setTheme(name, themeObj);
            },
        },
        'global');
    Macro.global['save-theme-override'] = Macro.global.savethemeoverride;

    Macro.add('loadtheme',
        {
            handler(name) {
                if (!name) {
                    wideAlert('The name argument provided to Macro.loadtheme ' +
                        'is not valid or was not provided. Cannot load ' +
                        'theme.');
                    return;
                } else if (!(name in State.themeStore)) {
                    wideAlert('The name argument provided to Macro.loadtheme ' +
                        'does not match any currently saved theme. Cannot ' +
                        'load theme.');
                    return;
                }

                State.loadTheme(name);
            },
        },
        'global');
    Macro.global['load-theme'] = Macro.global.loadtheme;

    Macro.add('configrendertype',
        {
            handler(value) {
                Config.editRenderType(value);
            },
        },
        'global');
    Macro.global['config-render-type'] = Macro.global.configrendertype;

    Macro.add('confignewlinetype',
        {
            handler(value) {
                Config.editNewlineType(value);
            },
        },
        'global');
    Macro.global['config-newline-type'] = Macro.global.confignewlinetype;

    Macro.add('configrewind',
        {
            handler(value) {
                Config.editRewind(value);
            },
        },
        'global');
    Macro.global['config-rewind'] = Macro.global.configrewind;

    Macro.add('configfastforward',
        {
            handler(value) {
                Config.editFastForward(value);
            },
        },
        'global');
    Macro['config-fast-forward'] = Macro.global.configfastforward;

    Macro.add('configerrorlevel',
        {
            handler(value) {
                Config.editErrorLevel(value);
            },
        },
        'global');
    Macro.global['config-error-level'] = Macro.global.configerrorlevel;

    Macro.add('configstyle',
        {
            isDoubleTag: true,
            defer: true,
            handler(value) {
                Config.editStyle(value);
            },
        },
        'global');
    Macro.global['config-style'] = Macro.global.configstyle;

    Macro.add('configprerender',
        {
            isDoubleTag: true,
            defer: true,
            handler(value) {
                Config.editPrerender(value);
            },
        },
        'global');
    Macro.global['config-prerender'] = Macro.global.configprerender;

    Macro.add('configpostrender',
        {
            isDoubleTag: true,
            defer: true,
            handler(value) {
                Config.editPostrender(value);
            },
        },
        'global');
    Macro.global['config-postrender'] = Macro.global.configpostrender;

    Macro.add('storyvolume',
        {
            handler(volume) {
                let val = Math.floor(Number(volume));
                if (volume === 'quieter') {
                    val = Math.max(0, Mixer.getStoryVolume() - 0.1);
                    Mixer.setStoryVolume(val);
                } else if (volume === 'louder') {
                    val = Math.min(1, Mixer.getStoryVolume() + 0.1);
                } else if (val <= 0) {
                    wideAlert('The volume argument passed to ' +
                        'Macro.soundvolume is not a valid, positive ' +
                        'number.');
                    return;
                }

                Mixer.setStoryVolume(val);
            },
        },
        'global');
    Macro.global['story-volume'] = Macro.global.storyvolume;

    Macro.add('newsound',
        {
            handler(name, channel, src, autoplay, loop, volume, fade) {
                new Sound(name, channel, src, autoplay, loop, volume, fade);
            },
        },
        'global');
    Macro.global['new-sound'] = Macro.global.newsound;

    Macro.add('soundduration',
        {
            handler(soundName, duration) {
                const sound = Mixer.sounds[soundName];
                if (!sound) {
                    wideAlert('There is no sound in Mixer.sounds with the ' +
                        'name ' + soundName + '. Cannot alter duration.');
                    return;
                }

                const val = Math.floor(Number(duration));
                if (val <= 0) {
                    wideAlert('The duration argument passed to ' +
                        'Macro.soundduration is not a valid, positive ' +
                        'number.');
                    return;
                }

                sound.duration = val;
            },
        },
        'global');
    Macro.global['sound-duration'] = Macro.global.soundduration;

    Macro.add('soundchannel',
        {
            handler(soundName, channelName) {
                const sound = Mixer.sounds[soundName];
                if (!sound) {
                    wideAlert('There is no sound in Mixer.sounds with the ' +
                        'name ' + soundName + '. Cannot assign to channel.');
                    return;
                }

                const channel = Mixer.channels[channelName];
                if (!channel) {
                    wideAlert('There is no channel in Mixer.channels with ' +
                        'the name ' + channelName + '. Cannot assign to ' +
                        'channel.');
                    return;
                }

                sound.channel = channel;
            },
        },
        'global');
    Macro.global['sound-channel'] = Macro.global.soundchannel;

    Macro.add('soundsrc',
        {
            handler(soundName, src) {
                const sound = Mixer.sounds[soundName];
                if (!sound) {
                    wideAlert('There is no sound in Mixer.sounds with the ' +
                        'name ' + soundName + '. Cannot assign to channel.');
                    return;
                }

                if (!src) {
                    wideAlert('The src argument passed to Macro.soundsrc ' +
                        'is not a valid, non-empty string.');
                    return;
                }

                sound.audioElement.src = src;
            },
        },
        'global');
    Macro.global['sound-src'] = Macro.global.soundsrc;

    Macro.add('soundloop',
        {
            handler(soundName, loop) {
                const sound = Mixer.sounds[soundName];
                if (!sound) {
                    wideAlert('There is no sound in Mixer.sounds with the ' +
                        'name ' + soundName + '. Cannot assign to channel.');
                    return;
                }

                if (loop || loop === undefined) {
                    sound.loop();
                } else {
                    sound.unloop();
                }
            },
        },
        'global');
    Macro.global['sound-loop'] = Macro.global.soundloop;

    Macro.add('soundvolume',
        {
            handler(soundName, volume) {
                const sound = Mixer.sounds[soundName];
                if (!sound) {
                    wideAlert('There is no sound in Mixer.sounds with the ' +
                        'name ' + soundName + '. Cannot adjust volume.');
                    return;
                }

                let val = Math.floor(Number(volume));
                if (val === 'quieter') {
                    sound.quieter();
                } else if (val === 'louder') {
                    sound.louder();
                } else if (val <= 0) {
                    wideAlert('The volume argument passed to ' +
                        'Macro.soundvolume is not a valid, positive ' +
                        'number.');
                    return;
                } else {
                    sound.setVolume(val);
                }
            },
        },
        'global');
    Macro.global['sound-volume'] = Macro.global.soundvolume;

    Macro.add('soundfade',
        {
            handler(soundName, fade) {
                const sound = Mixer.sounds[soundName];
                if (!sound) {
                    wideAlert('There is no sound in Mixer.sounds with the ' +
                        'name ' + soundName + '. Cannot assign to channel.');
                    return;
                }

                if (!fade) {
                    wideAlert('The fade argument passed to Macro.soundfade ' +
                        'is not valid.');
                    return;
                }

                sound.setFade(fade);
            },
        },
        'global');
    Macro.global['sound-fade'] = Macro.global.soundfade;

    Macro.add('crossfade',
        {
            handler(outSoundName, inSoundName, which) {
                const outSound = Mixer.sounds[outSoundName];
                if (!outSound) {
                    wideAlert('There is no sound in Mixer.sounds with ' +
                        'the name ' + outSoundName + '. Cannot crossfade.');
                    return;
                }

                const inSound = Mixer.sounds[inSoundName];
                if (!inSound) {
                    wideAlert('There is no sound in Mixer.sounds with the ' +
                        'name ' + inSoundName + '. Cannot crossfade.');
                    return;
                }

                outSound.crossfade(inSound, which);
            },
        },
        'global');
    Macro.global['cross-fade'] = Macro.global.crossfade;

    Macro.add('newchannel',
        {
            handler(name, defaultVolume, sounds) {
                if (!name) {
                    wideAlert('The name argument passed to ' +
                        'Macro.newchannel is not valid. Cannot create new ' +
                        'channel.');
                    return;
                }

                const soundObjs = [];

                for (let ii = 0; ii < sounds.length; ii++) {
                    const sound = sounds[ii];
                    if (sound in Mixer.sounds) {
                        soundObjs.push(Mixer.sounds[sound]);
                    } else {
                        gatelyLog('Could not find sound: ' + sound + '. ' +
                            'Cannot add to new channel: ' + name + '.');
                    }
                }

                new Channel(name, defaultVolume, soundObjs);
            },
        },
        'global');
    Macro.global['new-channel'] = Macro.global.newchannel;

    Macro.add('channelvolume',
        {
            handler(channelName, volume) {
                const channel = Mixer.channels[channelName];
                if (!sound) {
                    wideAlert('There is no channel in Mixer.channels with ' +
                        'the name ' + channelName + '. Cannot adjust ' +
                        'volume.');
                    return;
                }

                let val = Math.floor(Number(volume));
                if (val === 'quieter') {
                    channel.quieter();
                } else if (val === 'louder') {
                    channel.louder();
                } else if (val <= 0) {
                    wideAlert('The volume argument passed to ' +
                        'Macro.soundvolume is not a valid, positive ' +
                        'number.');
                    return;
                } else {
                    channel.setVolume(val);
                }
            },
        },
        'global');
    Macro.global['channel-volume'] = Macro.global.channelvolume; 

    Macro.add('makefade',
        {
            handler(isFading, easing, timing, steps) {
                const fade = new Fade(isFading, easing, timing, steps);
                return JSON.stringify(fade);
            },
        },
        'global');
    Macro.global['make-fade'] = Macro.global.makefade;

    Macro.add('compileroff',
        {
            handler() {
                State.compilerStatus.running = false;
            },
        },
        'global');
    Macro.global['compiler-off'] = Macro.global.compileroff;
    Macro.global.nocompile = Macro.global.compileroff;
    Macro.global['no-compile'] = Macro.global.compileroff;
    Macro.global.nocompiler = Macro.global.compileroff;
    Macro.global['no-compiler'] = Macro.global.compileroff;
    Macro.global.compilerstop = Macro.global.compileroff;
    Macro.global['compiler-stop'] = Macro.global.compileroff;

    Macro.add('compileron',
        {
            handler() {
                State.compilerStatus.running = true;
            },
        },
        'global');
    Macro.global['compiler-on'] = Macro.global.compileron;
    Macro.global.yescompile = Macro.global.compileron;
    Macro.global['yes-compile'] = Macro.global.compileron;
    Macro.global.yescompiler = Macro.global.compileron;
    Macro.global['yes-compiler'] = Macro.global.compileron;
    Macro.global.compilerstart = Macro.global.compileron;
    Macro.global['compiler-start'] = Macro.global.compileron;

    Macro.add('asis',
        {
            defer: true,
            isDoubleTag: true,
            handler() {
                return from(arguments).join('');
            },
        },
        'global');
    Macro.global['as-is'] = Macro.global.asis;

    Macro.add('failsoft',
        {
            defer: true,
            isDoubleTag: true,
            handler(callback) {
                const theme = State.stacks.present.storyTheme;
                if (callback === undefined) {
                    theme.debug.failHard = false;
                } else {
                    const original = theme.debug.failHard;
                    theme.debug.failHard = false;
                    const payload = document.createElement('div');
                    payload.textContent = callback;
                    Compiler.compile(payload);
                    theme.debug.failHard = original;
                }
            },
        },
        'global');
    Macro.global['fail-soft'] = Macro.global.failsoft;

    Macro.add('failhard',
        {
            defer: true,
            isDoubleTag: true,
            handler(callback) {
               if (callback === undefined) {
                    State.currentTheme.debug.failHard = true;
                } else {
                    const original = State.currentTheme.debug.failHard;
                    State.currentTheme.debug.failHard = true;
                    const payload = document.createElement('div');
                    payload.textContent = callback;

                    try {
                        Compiler.compile(payload);
                    } catch (e) {
                        // do nothing
                    }

                    State.currentTheme.debug.failHard = original;
                }
            },
        },
        'global');
    Macro.global['fail-hard'] = Macro.global.failhard;

    Macro.add('throw',
        {
            handler(message) {
                let msg = message;
                if (!msg) {
                    msg = '';
                }

                wideAlert(msg);
            }
        },
        'global');

    Macro.add('css',
        {
            isDoubleTag: true,
            description: 'Adds the provided rule/value pairs to all non-' +
                'tw-match direct children or the contained text node.',
            handler() {
                if (arguments.length % 2 !== 1) {
                    wideAlert('Must be even number of args.');
                    return;
                }

                const args = arguments;

                let seenTerminator = false;
                const hook = $(this).children('tw-match[data-type="hook"]');
                const nodes = $(hook)
                    .contents(':not(tw-match)')
                    .add(hook.children('tw-match').children(':not(tw-match)'))
                    .toArray();

                nodes.forEach(child => {
                    if (child.nodeType === 3) {
                        const str = '<span>' +
                            child.textContent +
                            '</span>';
                        const newElem = $(str)[0];
                        $(child).replaceWith(newElem);
                        child = newElem;
                    }

                    for (let ii = 0; ii < args.length; ii += 2) {
                        $(child).css(args[ii], args[ii + 1]);
                    }
                });

                return State.constants.LEAVE_AS_IS;
            },
        },
        'global');

    Macro.add('backlink',
        {
            handler(text) {
                if (!text) {
                    wideAlert('The text argument provided to ' +
                        'Macro.global.backlink is not valid.');
                    return;
                }

                const last = State.stacks.past.passageNames[0];
                if (!last || last === '_load') {
                    wideAlert('Cannot use a backlink to go back to _load!');
                    return;
                }

                const link = document.createElement('tw-link');
                link.className = 'passage';
                link.setAttribute('passage-name', last);
                link.textContent = text;

                return link;
            },
        },
        'global');
    Macro.global['back-link'] = Macro.global.backlink;

    Macro.add('strreplace',
        {
            handler(text, substr, replace) {
                const re = /[-\/\\^$*+?.()|[\]{}]/g;
                const pattern = substr.replace(re, '\\$&');
                const regex = new RegExp(pattern, 'g');
                return text.replace(regex, replace);
            }
        },
        'global');
    Macro.global['str-replace'] = Macro.global.strreplace;
}());