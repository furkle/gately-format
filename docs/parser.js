(function() {
    'use strict';

    let unusedMatchId = 0;

    window.Parser = {
        currentMatch: null,

        createMatchObject(dataType, parent, dialect, macroType) {
            return {
                dataType: dataType,
                macroType: macroType,
                parent: parent,
                children: [],
                id: Parser.getNewMatchId(),
                finishedStartTag: false,
                dialect: dialect,
            };
        },

        getCurrentMatchId() {
            if (Parser.currentMatch) {
                return Parser.currentMatch.id;
            } else {
                return null;
            }
        },

        getNewMatchId() {
            return unusedMatchId++;
        },

        getDataType(match) {
            if (match &&
                match.getAttribute &&
                match.getAttribute('data-type'))
            {
                return match
                    .getAttribute('data-type')
                    .replace(/&quot;|["']/g, '');
            } else if (match && match.dataType && match.dataType.replace) {
                return match.dataType.replace(/&quot;|["']/g, '');
            } else {
                return null;
            }
        },

        getMacroType(match) {
            if (match && match.getAttribute) {
                return match.getAttribute('data-macro-type');
            } else if (match && match.macroType) {
                return match.macroType;
            } else {
                return null;
            }
        },

        isEndingTag(tag) {
            return tag[0] === '/' || (tag.slice && tag.slice(0, 3) === 'end');
        },

        isDeferred(type) {
            if (!type) {
                return false;
            }

            let macroObj = Matches.getMacroObject(type);

            return macroObj && macroObj.defer === true;
        },

        createOpenTag(id, dataType, dialect, macroType) {
            let text = '<tw-match data-gately-id="' + id + '" ';
            if (Parser.isDeferred(macroType)) {
                text += 'class="defer" ';
            }

            if (dialect) {
                text += 'data-dialect="' + dialect + '" ';
            }

            if (macroType) {
                text += 
                    'data-type="macro" data-macro-type="' + macroType + '">';
            } else {
                text += 'data-type="' + dataType + '">';
            }

            return text;
        },

        addStringAtIndex(string, substring, index) {
            return string.slice(0, index) + substring + string.slice(index);
        },

        removeCharactersAtIndex(string, index, numToRemove) {
            return string.slice(0, index) + string.slice(index + numToRemove);
        },

        parseElement(element) {
            if (!isElement(element)) {
                wideAlert('The element argument provided to ' +
                    'Parser.parseElement is not a valid HTML element. ' +
                    'Cannot parse.');
            }

            State.parserStatus.running = true;
            State.parserStatus.element = element;
            State.parserStatus.text = element.innerHTML;
            State.parserStatus.index = 0;
            State.parserStatus.origIndex = 0;
            State.parserStatus.errors = [];

            let text = State.parserStatus.text;
            let index = State.parserStatus.index;
            let origIndex = State.parserStatus.origIndex;

            // supposed to be an invalid element error. test later.
            if (State.parserStatus.errors.length) {
                return;
            }

            let ii = 0;
            const elements = [];
            for (let jj = 0; jj < element.children.length; jj++) {
                const child = element.children[jj];
                // handle exact duplicates
                ii += text.slice(ii).indexOf(child.outerHTML);
                
                const start = ii;

                if (child.tagName === 'TW-ARGUMENT') {
                    elements.push(
                        text.slice(start, start + child.outerHTML.length));
                } else {
                    let newText =
                        Parser.createOpenTag(Parser.getNewMatchId(), 'html');
                    // add new text
                    text = Parser.addStringAtIndex(text, newText, ii);
                    // add the length of the new text
                    ii += newText.length;

                    ii += child.outerHTML.length;

                    newText = '</tw-match>';

                    text = Parser.addStringAtIndex(text, newText, ii);
                    // add the length of the new text
                    ii += newText.length;

                    elements.push(text.slice(start, ii));
                }
            }

            State.parserStatus.text = text;

            Parser.currentMatch = null;

            const origText = text;
            let matchOrigIndexes = [];

            let simpleMode = false;
            while (index < text.length) {
                const frameText = text;
                 if (/\[\[/.test(text.slice(index, index + 2))) {
                    Parser.handle(
                        text,
                        'linkOpener',
                        index,
                        origIndex);
                } else if (/]]/.test(text.slice(index, index + 2))) {
                    Parser.handle(
                        text,
                        'linkCloser',
                        index,
                        origIndex);
                } else if (/["']/.test(text[index])) {
                    Parser.handle(
                        text,
                        'quote',
                        index,
                        origIndex,
                        elements);
                } else if (text[index] === '<') {
                    Parser.handle(
                        text,
                        'lesserThanNonEntity',
                        index,
                        origIndex,
                        elements);
                } else if (/&lt;&lt;/.test(text.slice(index, index + 8))) {
                    Parser.handle(
                        text,
                        'sugarcaneMacroOpener',
                        index,
                        origIndex);
                } else if (/&gt;&gt;/.test(text.slice(index, index + 8))) {
                    Parser.handle(
                        text,
                        'sugarcaneMacroCloser',
                        index,
                        origIndex);
                } else if (/\(/.test(text[index])) {
                    Parser.handle(
                        text,
                        'harloweMacroOpener',
                        index,
                        origIndex);
                } else if (text[index] === ')') {
                    Parser.handle(
                        text,
                        'harloweMacroCloser',
                        index,
                        origIndex);
                } else if (/\[/.test(text[index])) {
                    Parser.handle(
                        text,
                        'harloweHookOpener',
                        index,
                        origIndex);
                } else if (/]/.test(text[index])) {
                    Parser.handle(
                        text,
                        'harloweHookCloser',
                        index,
                        origIndex);
                } else if (/\$/.test(text[index])) {
                    Parser.handle(
                        text,
                        'variable',
                        index,
                        origIndex);
                } else if (/\d/.test(text[index])) {
                    Parser.handle(
                        text,
                        'number',
                        index,
                        origIndex);
                } else if (/\n/.test(text[index])) {
                    Parser.handle(
                        text,
                        'newline',
                        index,
                        origIndex);
                } else if (/[^\s<>]/.test(text[index])) {
                    Parser.handle(
                        text,
                        'reservedWord',
                        index,
                        origIndex);   
                }

                State.parserStatus.index++;
                State.parserStatus.origIndex++;

                text = State.parserStatus.text;
                index = State.parserStatus.index;
                origIndex = State.parserStatus.origIndex;
            }

            element.innerHTML = text;

            if (Parser.currentMatch) {
                console.log('failed match:', Parser.currentMatch);
                const remainder = origText.slice(0, matchOrigIndexes[0]) +
                    '\n\nERROR FOLLOWS BELOW:\n' +
                    origText.slice(matchOrigIndexes[0]);
                wideAlert('A match was never successfully ended. Incomplete ' +
                    (Parser.getMacroType(Parser.currentMatch) ||
                        Parser.getDataType(Parser.currentMatch)) +
                    '.\n\n' +
                    remainder);
                Parser.currentMatch = null;
            }

            for (let name in State.parserStatus) {
                State.parserStatus[name] = null;
            }
        },

        handle(text, type, index, origIndex, tag) {
            if (State.parserStatus.running) {
                Parser.parseRules[type](text, index, origIndex, tag);
            }
        },

        parseRules: {
            linkOpener(text, index, origIndex) {
                // skip if there's no closing brackets
                if (text.indexOf(']]', index) === -1) {
                    return;
                }

                // skip over the opening [[
                text = Parser.removeCharactersAtIndex(text, index, 2);

                //matchOrigIndexes.unshift(origIndex);
                origIndex += 2;

                if (Parser.currentMatch) {
                    let newMatch = Parser.createMatchObject(
                        'link',
                        Parser.currentMatch,
                        'harlowe');
                    Parser.currentMatch.children.push(newMatch);
                    Parser.currentMatch = newMatch;
                } else {
                    Parser.currentMatch = Parser.createMatchObject(
                        'link',
                        null,
                        'harlowe');
                }

                let newText = Parser.createOpenTag(
                    Parser.getCurrentMatchId(),
                    'link',
                    'harlowe');

                text = Parser.addStringAtIndex(text, newText, index);
                // add the length of the new text
                index += newText.length - 1;

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            linkCloser(text, index, origIndex) {
                const currentMatch = Parser.currentMatch
                if (!currentMatch ||
                    Parser.getDataType(currentMatch) !== 'link')
                {
                    return;
                }

                // remove ]]
                text = Parser.removeCharactersAtIndex(text, index, 2);;

                let newText;
                let nextHook = text.slice(index).match(/\s*\[/);

                Parser.currentMatch = Parser.currentMatch.parent;

                newText = '</tw-match>';
                text = Parser.addStringAtIndex(text, newText, index);
                index += newText.length - 1;

                origIndex += 2;

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            quote(text, index, origIndex) {
                let quoteChar = text[index];
                let quoteType = 'doubleQuote';
                let otherType = 'singleQuote';

                if (text[index] === "'") {
                    quoteType = 'singleQuote';
                    otherType = 'doubleQuote';
                }

                // top-level quotes, single quotes inside valid double 
                // quote blocks, and escaped quotes aren't treated as 
                // semantic. there is probably a bug with successive
                // backslashes
                let current = Parser.currentMatch;
                if (!current ||
                    Parser.getDataType(current) === 'html' ||
                    Parser.getDataType(current) === 'hook' ||
                    Parser.getDataType(current) === otherType ||
                    text[index - 1] === '\\' ||
                    Matches.isFinishedStartTag(current))
                {
                    return;
                }

                let newText = Parser.createOpenTag(
                    Parser.getNewMatchId(),
                    quoteType);

                // add the new text
                text = Parser.addStringAtIndex(text, newText, index);

                // skip ahead the length of the added string
                index += newText.length + 1;

                //matchOrigIndexes.unshift(origIndex);
                origIndex++;
                // skip to the index after the next (unescaped)
                // double quote
                let backslashes = 0;
                while (index < text.length) {
                    if (text[index] === '\\') {
                        backslashes++;
                    } else if (text[index] === quoteChar &&
                        backslashes % 2 === 0)
                    {
                        break;
                    } else if (text[index] === '<' &&
                        backslashes % 2 === 0)
                    {
                        // handle valid html elements in strings
                        let after = text.slice(index);
                        let nodeMatches = arguments[3]
                            .map(aa => after.slice(0, aa.length) === aa);

                        for (let jj = 0; jj < nodeMatches.length; jj++) {
                            if (nodeMatches[jj]) {
                                index += arguments[3][jj].length - 1;
                                origIndex += arguments[3][jj].length - 1;
                                break;
                            }
                        }
                    } else {
                        backslashes = 0;
                    }

                    index++;
                    origIndex++;
                }

                // handle unclosed quotes
                if (index !== text.length) {
                    newText = '</tw-match>';
                    text = Parser.addStringAtIndex(text, newText, index + 1);
                    index += newText.length;
                    //matchOrigIndexes.shift();
                }

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            lesserThanNonEntity(text, index, origIndex) {
                let after = text.slice(index);
                let nodeMatches = arguments[3]
                    .map(aa => after.slice(0, aa.length) === aa);

                for (let jj = 0; jj < nodeMatches.length; jj++) {
                    if (nodeMatches[jj]) {
                        index += arguments[3][jj].length - 1;
                        origIndex += arguments[3][jj].length - 1;
                        break;
                    }
                }

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            sugarcaneMacroOpener(text, index, origIndex) {
                // macros names are terminated by a space or &gt;&gt;
                let macroType = text.slice(index + 8).split(/ |&gt;&gt;/)[0];
                if (!macroType) {
                    return;
                }

                let newText = '</tw-match>';

                // remove opening << brackets
                text = Parser.removeCharactersAtIndex(
                    text,
                    index,
                    8 + macroType.length);

                //matchOrigIndexes.unshift(origIndex);

                origIndex += 8 + macroType.length;

                // don't start a new macro if it's an ending tag
                if (Parser.currentMatch) {
                    let currentMatchType = 
                        Parser.getMacroType(Parser.currentMatch);
                    if (currentMatchType === 'elseif' ||
                        currentMatchType === 'else-if' ||
                        currentMatchType === 'else')
                    {
                        if (currentMatchType !== 'else') {
                            if (macroType === 'elseif' ||
                                macroType === 'else-if' ||
                                macroType === 'else')
                            {
                                text = Parser.addStringAtIndex(text,
                                    newText,
                                    index);
                                index += newText.length;

                                const parent = Matches.getCurrentParent(
                                    Parser.currentMatch);
                                Parser.currentMatch = parent;
                            }
                        } else {
                            if (macroType === '/if' ||
                                macroType === 'endif')
                            {
                                text = Parser.addStringAtIndex(
                                    text,
                                    newText,
                                    index);
                                index += newText.length;

                                const parent = Matches.getCurrentParent(
                                    Parser.currentMatch);
                                Parser.currentMatch = parent;
                            }
                        }
                    }
                   
                    if (Parser.isEndingTag(macroType)) {
                        // remove closing &gt;&gt; brackets
                        text = 
                            Parser.removeCharactersAtIndex(text, index, 8);
                        origIndex += 8;
                        text = Parser.addStringAtIndex(text, newText, index);
                        index += newText.length;

                        const parent = Matches.getCurrentParent(
                            Parser.currentMatch);
                        Parser.currentMatch = parent;
                        //matchOrigIndexes.shift();

                        State.parserStatus.index = index;
                        State.parserStatus.origIndex = origIndex;
                        return;
                    }

                    let newMatch = Parser.createMatchObject(
                        'macro',
                        Parser.currentMatch,
                        'sugarcane',
                        macroType);
                    Parser.currentMatch.children.push(newMatch);
                    Parser.currentMatch = newMatch;
                } else {
                    Parser.currentMatch = Parser.createMatchObject(
                        'macro',
                        null,
                        'sugarcane',
                        macroType);
                }

                newText = Parser.createOpenTag(
                    Parser.getCurrentMatchId(),
                    'macro',
                    'sugarcane',
                    macroType);
                // add new text
                text = Parser.addStringAtIndex(text, newText, index);
                // add the length of the new text
                index += newText.length - 1;

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            sugarcaneMacroCloser(text, index, origIndex) {
                if (!Parser.currentMatch) {
                    return;
                }

                let newText;

                const currentMatch = Parser.currentMatch;
                if (Matches.isDoubleTagMacroOpener(currentMatch)) {
                    Parser.currentMatch.finishedStartTag = true;
                }

                newText = Parser.createOpenTag(
                    Parser.getNewMatchId(),
                    'openTagTerminator',
                    'sugarcane');
                text = Parser.addStringAtIndex(text, newText, index);
                index += newText.length;

                // remove closing >> brackets
                text = Parser.removeCharactersAtIndex(text, index, 8);

                newText = '</tw-match>';
                text = Parser.addStringAtIndex(text, newText, index);
                index += newText.length - 1;

                if (!Parser.currentMatch.finishedStartTag) {
                    const parent = 
                        Matches.getCurrentParent(Parser.currentMatch);
                    Parser.currentMatch = parent;
                    //matchOrigIndexes.shift();
                }

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            harloweMacroOpener(text, index, origIndex) {
                // harlowe macro names are terminated by a colon
                let nextColon = text
                    .slice(index + 1)
                    .search(':');
                let nextNonEnder = text
                    .slice(index + 1)
                    .search(/[\s()>;'"]/);

                if (nextColon === -1 ||
                    nextNonEnder === -1 ||
                    nextNonEnder < nextColon)
                {
                    if (Parser.currentMatch &&
                        Parser.currentMatch.dialect === 'harlowe')
                    {
                        Matches.harloweParentheses++;
                    }

                    return;
                }

                let macroType = text.slice(index + 1).split(':')[0];

                // skip over the opening (, the macro name, and the
                // terminal colon
                text = Parser.removeCharactersAtIndex(
                    text,
                    index,
                    2 + macroType.length);

                //matchOrigIndexes.unshift(origIndex);
                origIndex += 1 + macroType.length;

                if (Parser.currentMatch) {
                    let newMatch = Parser.createMatchObject(
                        'macro',
                        Parser.currentMatch,
                        'harlowe',
                        macroType);
                    Parser.currentMatch.children.push(newMatch);
                    Parser.currentMatch = newMatch;
                } else {
                    Parser.currentMatch = Parser.createMatchObject(
                        'macro',
                        null,
                        'harlowe',
                        macroType);
                }

                let newText = Parser.createOpenTag(
                    Parser.getCurrentMatchId(),
                    'macro',
                    'harlowe',
                    macroType);

                text = Parser.addStringAtIndex(text, newText, index);
                // add the length of the new text
                index += newText.length - 1;

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            harloweMacroCloser(text, index, origIndex) {
                if (Matches.harloweParentheses > 0) {
                    Matches.harloweParentheses--;
                    return;
                } else if (!Parser.currentMatch ||
                    Parser.currentMatch.dialect === 'sugarcane')
                {
                    return;
                }

                // remove )
                text = Parser.removeCharactersAtIndex(text, index, 1);

                const currentMatch = Parser.currentMatch;
                let type = Parser.getMacroType(currentMatch) || '';
                let macroObj = Matches.getMacroObject(type);

                if (!macroObj) {
                    macroObj = State.macroAliasStore[type];
                }

                if (!macroObj) {
                    macroObj = State.functionStore[type];
                }

                if (!macroObj) {
                    /*wideAlert('There is no macro definition for the ' +
                        'macro type ' + type + '.'); */
                    macroObj = {};
                }

                let newText;
                let nextHook = text.slice(index).match(/\s*\[/);
                if (nextHook &&
                    text.slice(index).indexOf(nextHook[0]) === 0 &&
                    // breaks if replaced in any way???
                    macroObj.isDoubleTag)
                {
                    Parser.currentMatch.finishedStartTag = true;
                    newText = Parser.createOpenTag(
                        Parser.getNewMatchId(),
                        'openTagTerminator',
                        'harlowe') + '</tw-match>';
                    text = Parser.addStringAtIndex(text, newText, index);
                    index += newText.length - 1;
                } else {
                    Parser.currentMatch = Parser.currentMatch.parent;
                    newText = Parser.createOpenTag(
                        Parser.getNewMatchId(),
                        'openTagTerminator',
                        'harlowe') + '</tw-match>';
                    text = Parser.addStringAtIndex(text, newText, index);
                    index += newText.length;

                    newText = '</tw-match>';
                    text = Parser.addStringAtIndex(text, newText, index);
                    index += newText.length - 1;

                    //matchOrigIndexes.shift();
                }

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            harloweHookOpener(text, index, origIndex) {
                if (Parser.currentMatch) {
                    let newMatch = Parser.createMatchObject(
                        'hook',
                        Parser.currentMatch,
                        'harlowe');
                    Parser.currentMatch.children.push(newMatch);
                    Parser.currentMatch = newMatch;
                } else {
                    Parser.currentMatch = Parser.createMatchObject(
                        'hook',
                        null,
                        'harlowe');
                }

                text = Parser.removeCharactersAtIndex(text, index, 1);

                //matchOrigIndexes.unshift(origIndex);

                let newText = Parser.createOpenTag(
                    Parser.getCurrentMatchId(), 'hook', 'harlowe');
                // add new text
                text = Parser.addStringAtIndex(text, newText, index);
                // add the length of the new text
                index += newText.length - 1;

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            harloweHookCloser(text, index, origIndex) {
                if (!Parser.currentMatch) {
                    return;
                }

                const hookID = Parser.getCurrentMatchId();
                text = Parser.removeCharactersAtIndex(text, index, 1);

                let newText = '</tw-match>';
                text = Parser.addStringAtIndex(text, newText, index);
                index += newText.length - 1;
                Parser.currentMatch = 
                    Matches.getCurrentParent(Parser.currentMatch);

                let type = Parser.getMacroType(Parser.currentMatch);
                if (type &&
                    Matches.isFinishedStartTag(Parser.currentMatch))
                {
                    newText = '</tw-match>';
                    text = Parser.addStringAtIndex(text, newText, index + 1);
                    index += newText.length;
                    Parser.currentMatch = 
                        Matches.getCurrentParent(Parser.currentMatch);

                    //matchOrigIndexes.shift();
                }

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            variable(text, index, origIndex) {
                if (/\s/.test(text[index + 1])) {
                    return;
                }

                const enderRE = /[\s<>,()\[\]'"]|&lt;|&gt;|$/;
                let varLength = text
                    .slice(index)
                    .split(enderRE)[0].length;

                let accessorsLength;
                if (text[index + varLength] === '[') {
                    const match =
                        text.slice(index + varLength).match(/\[[^\s,]+\]/) || [''];
                    const nextChar = text[index + varLength + match[0].length];
                    if (!match[0].length || !enderRE.test(nextChar)) {
                        index += varLength;
                        origIndex += varLength;
                        State.parserStatus.index = index;
                        State.parserStatus.origIndex = origIndex;
                        return;
                    }

                    accessorsLength = match[0].length;
                }

                let newText = Parser.createOpenTag(
                    Parser.getNewMatchId(), 'variable');
                text = Parser.addStringAtIndex(text, newText, index);
                index += newText.length;

                index += varLength;
                origIndex += varLength;

                if (accessorsLength) {
                    const split = text.slice(index, index + accessorsLength)
                        .split(/[\[\]]/)
                        .filter(aa => aa !== '');
                    split.forEach(accessor => {
                        newText = Parser.createOpenTag(
                            Parser.getNewMatchId(), 'accessor');
                        text = Parser.addStringAtIndex(text, newText, index);
                        index += newText.length;

                        let accessorVal = accessor;

                        // remove "" or ''
                        let removingQuotes = false;
                        if (accessor[0] === accessor[accessor.length - 1] &&
                            (accessor[0] === '"' || accessor[0] === "'"))
                        {
                            removingQuotes = true;
                            accessorVal = accessor.slice(1, -1);
                            text = Parser.removeCharactersAtIndex(text, index + 1, 1);
                        }

                        // [ + accessor
                        index += 1 + accessorVal.length;

                        if (removingQuotes) {
                            text = Parser.removeCharactersAtIndex(text, index, 1);
                        }

                        // + ]
                        index++;

                        newText = '</tw-match>';
                        text = Parser.addStringAtIndex(text, newText, index);
                        index += newText.length;
                    });
                }

                newText = '</tw-match>';
                text = Parser.addStringAtIndex(text, newText, index);

                // figure out why you need a - 1 here lol
                index += newText.length - 1;

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            number(text, index, origIndex) {
                // skip anything outside of matches
                if (!Parser.currentMatch ||
                    Matches.isFinishedStartTag(Parser.currentMatch) ||
                    Parser.getDataType(Parser.currentMatch) === 'hook')
                {
                    return;
                }

                /* get the next non-number character
                 * match all whitespace and mathematical operators and
                 * macro closures and commas, and ensure that the next
                 * non-number character is one */
                let temp = text.slice(index).search(/\D/);
                const firstPeriod = text.slice(index).search(/\./);

                if (temp === -1) {
                    temp = text.slice(index).length;
                } else if (firstPeriod !== -1 && firstPeriod <= temp) {
                    temp += text.slice(index).match(/\.\d*/)[0].length;
                }

                /* skip anything outside of matches, or invalid matches. */
                if (!text[index + temp] ||
                    /[^\d.,]/.test(text.slice(index, index + temp)) ||
                    !/^([\s+\-*\/%(),>]|&gt;)/.test(text.slice(index + temp)))
                {
                    return;
                }

                let newText = Parser.createOpenTag(
                    Parser.getNewMatchId(),
                    'number');
                text = Parser.addStringAtIndex(text, newText, index);
                index += temp + newText.length;
                origIndex += temp;

                newText = '</tw-match>';
                text = Parser.addStringAtIndex(text, newText, index);
                index += newText.length - 1;

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            newline(text, index, origIndex) {
                State.parserStatus.lineNumber++;

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            reservedWord(text, index, origIndex) {
                // skip anything outside of matches
                if (!Parser.currentMatch ||
                    Matches.isFinishedStartTag(Parser.currentMatch) ||
                    Parser.getDataType(Parser.currentMatch) === 'hook')
                {
                    return;
                }

                // handle commas for argument lists
                if (text[index] === ',' &&
                    Parser.currentMatch.dataType === 'macro')
                {
                    let newText = Parser.createOpenTag(
                        Parser.getNewMatchId(),
                        'comma');
                    text = Parser.addStringAtIndex(text, newText, index);
                    // skip ahead the length of the added string
                    index += newText.length;

                    newText = '</tw-match>';
                    text = Parser.addStringAtIndex(text, newText, index + 1);
                    index += newText.length;

                    // TODO: test and fix for left-padded strings
                    // remove all spaces between comma-separated arguments
                    const regex = / /;
                    while (regex.test(text[index + 1])) {
                        text = Parser.removeCharactersAtIndex(
                            text, index + 1, 1);
                    }
                }

                let word = text.slice(index).split(/[\s<>)]|&lt;|&gt;/)[0];
                if (text[index - 1].match(/\s/) && 
                    Matches.reservedWordLookup[word]) 
                {
                    let newText = Parser.createOpenTag(
                        Parser.getNewMatchId(), 
                        'reservedWord');
                    text = Parser.addStringAtIndex(text, newText, index);
                    // skip ahead the length of the added string
                    index += newText.length;
                    // skip to the index after the reserved word completes
                    index += word.length;
                    origIndex += word.length;

                    newText = '</tw-match>';
                    text = Parser.addStringAtIndex(text, newText, index);
                    index += newText.length - 1;
                }

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            }
        },

        parseText(text) {
            const container = document.createElement('tw-match');
            container.className = 'parseContainer';
            container.innerHTML = text;
            Parser.parseElement(container);
            return container;
        },

        unparseElement(element) {
            const dataType = Parser.getDataType(element);
            const macroType = Parser.getMacroType(element);

            if (dataType === 'singleQuote' ||
                dataType === 'doubleQuote' ||
                dataType === 'reservedWord' ||
                dataType === 'comma' ||
                dataType === 'html' ||
                dataType === 'number' ||
                dataType === 'accessor') 
            {
                element.outerHTML = element.innerHTML;
            } else if (dataType === 'hook') {
                element.outerHTML = '[' + element.innerHTML + ']';
            } else if (dataType === 'variable') {
                element.outerHTML = element.innerHTML;
            } else if (dataType === 'macro') {
                const dialect = element.getAttribute('data-dialect');
                if (dialect === 'harlowe') {
                    element.outerHTML = 
                        '(' + Parser.getMacroType(element) + ': ' +
                            element.innerHTML;
                } else if (dialect === 'sugarcane') {
                    if (Macro[macroType].isDoubleTag) {
                        element.outerText = '&lt;&lt;' +
                            Parser.getMacroType(element) + ' ' +
                            element.innerHTML +
                            '&lt;&lt;end' + macroType + '&gt;&gt;';
                    } else {
                        element.outerText = '&lt;&lt;' +
                            Parser.getMacroType(element) + ' ' + element.innerHTML;
                    }
                } else {
                    // wut?
                    wideAlert('The data-dialect value was not recognized. ' +
                        'The only currently-supported dialects are ' + 
                        'sugarcane and Harlowe.');
                }
            } else if (dataType === 'openTagTerminator') {
                const dialect = element.getAttribute('data-dialect');
                if (dialect === 'harlowe') {
                    element.outerHTML = ')';
                } else if (dialect === 'sugarcane') {
                    element.outerHTML = '&gt;&gt;';
                } else {
                    // wut?
                    wideAlert('The data-dialect value was not recognized. The ' +
                        'only currently-supported dialects are sugarcane ' +
                        'and Harlowe.');
                }
            }
        },

        unparseText(text) {
            const div = document.createElement('div');
            div.innerHTML = text;
            Parser.unparseNode(div);
            return div.innerHTML;
        },

        parseSimple(text, vars) {
            State.parserStatus.running = true;
            State.parserStatus.text = text;
            State.parserStatus.index = 0;
            State.parserStatus.origIndex = 0;
            State.parserStatus.errors = [];

            let index = State.parserStatus.index;
            let origIndex = State.parserStatus.origIndex;

            let simpleVariables = null;
            if (vars && typeof(vars) === 'object') {
                simpleVariables = vars;
            }

            while (index < text.length) {
                const frameText = text;
                
                if (/^&lt;\?\s/.test(text.slice(index)) &&
                        /\s\?&gt;/.test(text.slice(index)))
                {
                    const simpleFrag = text
                        .slice(
                            index,
                            index + text.slice(index).search(/\s\?&gt;/) + 6)
                        .slice(6, -6);

                    text = Parser.removeCharactersAtIndex(
                        text,
                        index,
                        simpleFrag.length + 12);

                    simpleFrag.split(';').forEach(line => {
                        if (!line) {
                            return;
                        }

                        const obj = {
                            directive: '',
                            target: '',
                            modifier: '',
                            argument: '',
                            inArgument: false,
                        };

                        function stripQuotesAndPeriod(text) {
                            let strippedText = text;
                            // remove period
                            if (strippedText.endsWith('.')) {
                                text = text.slice(0, -1);
                            }

                            if (strippedText.startsWith('"') &&
                                strippedText.endsWith('"'))
                            {
                                return text.slice(1, -1);
                            } else {
                                return text;
                            }
                        }

                        const parts = line.split(' ')
                            .map(part => part.trim())
                            .filter(part => part);

                        obj.directive = stripQuotesAndPeriod(
                            (parts[0] || '').trim().toLowerCase());
                        obj.target = stripQuotesAndPeriod(
                            (parts[1] || '').trim().toLowerCase());
                        obj.modifier = stripQuotesAndPeriod(
                            (parts[2] || '').trim().toLowerCase());

                        const argText = (parts.slice(3) || '').join(' ').trim();

                        function isValidQuoteAtIndex(text, ii) {
                            if (text[ii] === '"') {
                                const match = text.slice(0, ii + 1).match(/\\+(?=")^/);
                                if (text[ii - 1] !== '\\' ||
                                    (match && match[0].length % 2 === 0))
                                {
                                    return true;
                                }
                            }

                            return false;
                        }

                        const args = [];
                        let quoteOpen = false;
                        let quoteOpenIndex = null;
                        let ii = 0;
                        while (ii < argText.length) {
                            if (argText[ii] === '"' &&
                                isValidQuoteAtIndex(argText, ii))
                            {
                                if (quoteOpen) {
                                    args.push(argText.slice(quoteOpenIndex + 1, ii));
                                } else {
                                    quoteOpenIndex = ii;
                                }

                                quoteOpen = !quoteOpen;
                            }

                            ii++;
                        }

                        if (/the|set/.test(obj.directive) &&
                            /is|to|are/.test(obj.modifier))
                        {
                            if (simpleVariables) {
                                if (args &&
                                    typeof(args) === 'object' &&
                                    args.length <= 1)
                                {
                                    simpleVariables[obj.target] = args[0] || '';
                                } else {
                                    simpleVariables[obj.target] = args;
                                }
                            }
                        } else if (/do/.test(obj.directive) &&
                            /to|with/.test(obj.modifier))
                        {
                            let str = '(' + obj.target + ': ';

                            for (let ii = 0; ii < args.length - 1; ii++) {
                                str += '"' + args[ii] + '", ';
                            }
                                
                            str += '"' + args[args.length - 1] + '")';

                            text = Parser.addStringAtIndex(text, str, index);
                        }
                    });

                    State.parserStatus.text = text;
                    State.parserStatus.index = index;
                    State.parserStatus.origIndex = origIndex;
                }

                State.parserStatus.index++;
                State.parserStatus.origIndex++;

                text = State.parserStatus.text;
                index = State.parserStatus.index;
                origIndex = State.parserStatus.origIndex;
            }

            return text;
        },
    };
}());