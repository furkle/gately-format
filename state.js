(function() {
    'use strict';

    window.State = {
        constants: {
            LEAVE_AS_IS: 'LEAVE_AS_IS',
        },

        compilerStatus: {
            passage: '',
            rootElement: null,
            element: null,
            running: false,
            errors: [],
        },

        parserStatus: {
            element: null,
            text: null,
            lineNumber: null,
            running: false,
            errors: [],
        },

        stacks: {
            past: {
                passageNames: [],
                storyVariableStates: [],
                virtualPassageStates: [],
                bindingStates: [],
                storyThemes: [],
                transcriptStates: [],
            },

            present: {
                passageName: '_load',
                storyVariableState: {},
                virtualPassageState: {},
                bindingState: {},
                storyTheme: Themes.getDefaultStoryTheme(),
                transcriptState: {},
            },

            future: {
                passageNames: [],
                storyVariableStates: [],
                virtualPassageStates: [],
                bindingStates: [],
                storyThemes: [],
                transcriptStates: [],
            },
        },

        functionStore: {},

        macroAliasStore: {},
        macroAliasDisposals: [],

        traceryStore: {},

        themeStore: {
            passage: Themes.getDefaultPassageThemes(),

            story: {
                default:  Themes.getDefaultStoryTheme(),
            }
        },

        simpleVariables: {},

        reapQuantity: -1,

        functionStore: {},

        macroAliasStore: {},
        macroAliasDisposals: [],

        traceryStore: {},

        simpleVariables: {},

        authorData: {},

        newState(passageName) {
            if (!passageName) {
                wideAlert('The passageName argument provided to ' +
                    'State.newState is not valid.');
                return;
            }

            State.serializeUserRenderFunctions();

            const past = State.stacks.past;
            const present = State.stacks.present;
            const future = State.stacks.future;

            // break all object references
            getOwnPropertyNames(present).forEach(name => {
                past[name + 's'].unshift(deepCopy(present[name]));

                present[name] = deepCopy(present[name]);

                future[name + 's'] = [];
            });

            present.passageName = passageName;

            State.deserializeUserRenderFunctions();

            if (window.SUPERDEBUG === true) {
                // shows basic memory allocations
                for (let name in State) {
                    if (isFunction(State[name])) {
                        continue;
                    }

                    const str = JSON.stringify(State[name]);
                    if (str) {
                        console.log(name, str.length);
                    } else {
                        console.log(name, 0);
                    }
                }

                console.log('total state', JSON.stringify(State).length);
                console.log('total HTML', $('html').html().length);
                console.log('__end of report__');
            }
        },

        rewind() {
            const past = State.stacks.past;
            const present = State.stacks.present;
            const future = State.stacks.future;

            if (past.passageNames.length === 1) {
                wideAlert('Can\'t rewind to _load.');
                return;
            }

            for (let name in present) {
                future[name + 's'] = [deepCopy(present[name])]
                    .concat(future[name + 's']);

                present[name] = deepCopy(past[name + 's'].shift());
            }

            State.deserializeUserRenderFunctions();
        },

        rewindTo(passageName) {
            const past = State.stacks.past;
            const present = State.stacks.present;
            const future = State.stacks.future;

            const index = past.passageNames.indexOf(passageName);
            if (index === -1) {
                wideAlert('The passage name ' +
                    passageName +
                    ' does not exist in the history. Cannot rewind.');
                return;
            }

            for (let ii = 0; ii < index + 1; ii++) {
                State.rewind();
            }
        },

        fastForward() {
            const past = State.stacks.past;
            const present = State.stacks.present;
            const future = State.stacks.future;

            if (future.passageNames.length === 0) {
                wideAlert('No passage to fast-forward to.');
                return;
            }

            for (let name in present) {
                past[name + 's'] = [deepCopy(present[name])]
                    .concat(past[name + 's']);

                present[name] = deepCopy(future[name + 's'].shift());
            }

            State.deserializeUserRenderFunctions();
        },

        fastForwardTo(passageName) {
            const past = State.stacks.past;
            const present = State.stacks.present;
            const future = State.stacks.future;

            const index = future.passageNames.indexOf(passageName);
            if (index === -1) {
                wideAlert('The passagename ' +
                    passageName +
                    ' does not exist in the history. Cannot rewind.');
                return;
            }

            for (let ii = 0; ii < index + 1; ii++) {
                State.fastForward();
            }
        },

        serializeUserRenderFunctions() {
            const storyTheme = State.stacks.present.storyTheme;

            storyTheme.prerenderString =
                (storyTheme.prerender || '').toString();

            storyTheme.postrenderString =
                (storyTheme.postrender || '').toString();

            storyTheme.loadString =
                (storyTheme.load || '').toString();

            storyTheme.unloadString =
                (storyTheme.unload || '').toString();
        },

        deserializeUserRenderFunctions() {
            const storyTheme = State.stacks.present.storyTheme;
            if (storyTheme.prerenderString) {
                storyTheme.prerender =
                    eval('(' + storyTheme.prerenderString + ')') || null;
            }

            if (storyTheme.postrenderString) {
                storyTheme.postrender =
                    eval('(' + storyTheme.postrenderString + ')') || null;
            }

            if (storyTheme.loadString) {
                storyTheme.load =
                    eval('(' + storyTheme.loadString + ')') || null;
            }

            if (storyTheme.unloadString) {
                storyTheme.unload =
                    eval('(' + storyTheme.unloadString + ')') || null;
            }
        },

        reapExcept() {
            if (State.reapQuantity === -1) {
                return;
            }

            let reapNum = Number(State.reapQuantity);
            if (Number.isNaN(reapNum)) {
                throw new Error('State.reapQuantity argument provided to ' +
                    'State.reapExcept is not valid.');
            }

            getOwnPropertyNames(State.stacks.past).forEach(name => {
                const list = State.stacks.past[name];
                for (let ii = reapNum; ii < list.length; ii++) {
                    list.pop();
                }
            });
        },

        getStoryVariables() {
            return State.stacks.present.storyVariableState;
        },

        getSimpleVariables() {
            const passageName = State.stacks.present.passageName;
            let obj = State.simpleVariables[passageName];
            if (!obj || typeof(obj) !== 'object') {
                obj = {};
            }

            return deepCopy(obj);
        },

        setSimpleVariable(name, value) {
            const passageName = State.stacks.present.passageName;
            if (!State.simpleVariables[passageName]) {
                State.simpleVariables[passageName] = {};
            }

            State.simpleVariables[passageName][name] = value;
        },

        getAllScopedVariables(element) {
            const storyVars = State.getStoryVariables();
            const blockScopedVars = State.getBlockScope(element) || {};
            const simpleVars = State.getSimpleVariables();

            const retObj = {};

            for (let name in storyVars) {
                retObj[name] = storyVars[name];
            }

            for (let name in blockScopedVars) {
                retObj[name] = blockScopedVars[name];
            }

            for (let name in simpleVars) {
                retObj[name] = simpleVars[name];
            }

            return retObj;
        },

        getVariable(variablePathOrPointer, blockElement) {
            let path;
            let type = typeof(variablePathOrPointer);
            if (!variablePathOrPointer ||
                (type !== 'object' && type !== 'string'))
            {
                wideAlert('The variableNameOrPointer argument provided to ' +
                    'State.getVariable is not a valid string or object. ' +
                    'Cannot execute.');
                return;
            } else if (type === 'object') {
                if (State.isPointer(variablePathOrPointer)) {
                    path = variablePathOrPointer.path;
                } else {
                    wideAlert('The variablePathOrPointer is an object, but ' +
                        'is not a valid pointer object. Cannot execute.');
                    return;
                }
            } else if (type === 'string') {
                path = variablePathOrPointer;
            }

            let retVal = null;
            if (path[0] === '$') {
                retVal = State.getStoryVariable(path, blockElement);
            } else if (path[0] === '@') {
                retVal = State.getLocalVariable(path, blockElement);
            } else if (path[0] === '#') {
                const pointer = {
                    path,
                    type: 'gatelyPointer',
                };

                retVal = State.dereferencePointer(pointer);
            } else {
                wideAlert('The varNameVal argument must start with ' +
                    '$, @, or #.');
            }

            return retVal;
        },

        getStoryVariable(variablePath, blockElement) {
            const present = State.stacks.present;

            const split = variablePath
                .split(/[[\]]/)
                .filter(aa => aa);

            let firstAccessor = split[0];
            if (firstAccessor[0] === '$') {
                firstAccessor = firstAccessor.slice(1);
            } else if (firstAccessor[0] === '@') {
                wideAlert('Local variables may not be set with the ' +
                    'State.getStoryVariable method. Please use ' +
                    'State.getLocalVariable instead.');
                return null;
            }

            let val = present.storyVariableState[firstAccessor];
            for (let ii = 1; ii < split.length; ii++) {
                if (val === undefined || val === null) {
                    wideAlert('Parent not found! Cannot get story variable.');
                    return null;
                }

                if (/[$@]/.test(split[ii][0])) {
                    split[ii] = State.getVariable(split[ii], blockElement);
                }

                val = val[split[ii]];
            }

            if (val === undefined) {
                val = null;
            } else if (State.isPointer(val)) {
                val = State.dereferencePointer(val);
            }

            return val;
        },

        getLocalVariable(variablePath, blockElement) {
            const split = variablePath.split(/[[\]]/).filter(aa => aa);

            const blockScope = State.getBlockScope(blockElement);
            let firstAccessor = split[0];
            if (firstAccessor[0] === '@') {
                firstAccessor = firstAccessor.slice(1);
            } else if (firstAccessor[0] === '$') {
                wideAlert('The variableName argument begins with an $, but ' +
                    'local variables must begin with an @. Cannot get local ' +
                    'variable.');
                return null;
            }

            let val = blockScope[firstAccessor];
            for (let ii = 1; ii < split.length; ii++) {
                if (val === undefined || val === null) {
                    return null;
                }

                if (/[$@]/.test(split[ii][0])) {
                    split[ii] = State.getVariable(split[ii], blockElement);
                }

                val = val[split[ii]];
            }

            if (val === undefined) {
                return null;
            } else if (State.isPointer(val)) {
                return State.dereferencePointer(val);
            } else {
                return val;
            }
        },

        makePointer(path, blockElement) {
            const variable = State.getVariable(path, blockElement);

            // null pointers are not allowed in gately
            if (variable === null || variable === undefined) {
                wideAlert('No variable or property exists at ' + path + '.');
                return null;
            }

            const retObj = {
                path: State.getTransformedPath(path, blockElement),
                type: 'gatelyPointer',
            };

            if (path[0] === '@') {
                let id;
                if (blockElement.dataset.gatelyPointerId) {
                    id = blockElement.dataset.gatelyPointerId;
                } else {
                    // only create a new pointer ID if the block doesn't
                    // already have one
                    id = State.getUnusedPointerID();

                    // add id so we can find the reference node again
                    blockElement.dataset.gatelyPointerId = id;
                }

                retObj.path = '#' + id + retObj.path;
            }

            return retObj;
        },

        unusedGatelyPointerId: 0,

        getUnusedPointerID() {
            return State.unusedGatelyPointerId++;
        },

        isPointer(maybePointer) {
            return maybePointer &&
                typeof(maybePointer) === 'object' &&
                maybePointer.type === 'gatelyPointer' &&
                maybePointer.path &&
                typeof(maybePointer.path) === 'string' &&
                /[$#]/.test(maybePointer.path[0]) &&
                getOwnPropertyNames(maybePointer).length === 2;
        },

        dereferencePointer(pointer) {
            if (!State.isPointer(pointer)) {
                wideAlert('The pointer argument passed to ' +
                    'State.dereferencePointer is not a pointer. Cannot ' +
                    'dereference.');
                return;
            }

            let obj;
            if (pointer.path[0] === '$') {
                obj = State.stacks.present.storyVariableState;

                // get path, remove $
                const path = pointer.path.slice(1);
                path.split(/[[\]]/).filter(aa => aa).forEach(portion => {
                    if (obj === null || obj === undefined) {
                        return;
                    }

                    obj = obj[portion];
                });

                return obj;
            } else {
                const id = pointer.path.slice(
                    1,
                    pointer.path.slice(1).search(/\D/) + 1);
                if (!id) {
                    wideAlert('The pointer has no reference node id.');
                    return null;
                }

                const node = document.querySelector(
                    '[data-gately-pointer-id="' + id + '"]');
                if (!node) {
                    wideAlert('A reference node with id ' + id + ' does not ' +
                        'exist in the document. Cannot dereference pointer.');
                    return null;
                }

                obj = State.getBlockScope(node);

                // get path, remove @
                const path = pointer.path.slice(pointer.path.indexOf('@') + 1);
                path.split(/[[\]]/).filter(aa => aa).forEach(portion => {
                    if (obj === null || obj === undefined) {
                        return;
                    }

                    obj = obj[portion];
                });
            }

            if (State.isPointer(obj)) {
                wideAlert('Forbidden double-pointer action -- a pointer has ' +
                    'been created that points to another pointer.');
                return null;
            } else if (obj === undefined) {
                return null;
            } else {
                return obj;
            }
        },

        getPointerAtPath(path, blockElement) {
            let retVal = null;
            if (path[0] === '$') {
                const present = State.stacks.present;

                const split = path.slice(1).split(/[[\]]/).filter(aa => aa);

                let firstAccessor = split[0];

                let val = present.storyVariableState[firstAccessor];
                for (let ii = 1; ii < split.length; ii++) {
                    if (val === undefined) {
                        wideAlert('Parent not found! Cannot get story variable.');
                        return null;
                    }

                    if (split[ii][0] === '$' || split[ii][0] === '@') {
                        split[ii] = State.getVariable(split[ii], blockElement);
                    }

                    val = val[split[ii]];
                }

                if (State.isPointer(val)) {
                    retVal = val;
                }
            } else if (path[0] === '@') {
                const split = path.slice(1).split(/[[\]]/).filter(aa => aa);

                const blockScope = State.getBlockScope(blockElement);
                let firstAccessor = split[0];

                let val = blockScope[firstAccessor];
                for (let ii = 1; ii < split.length; ii++) {
                    if (val === null || val === undefined) {
                        return null;
                    }

                    if (/[$@]/.test(split[ii][0])) {
                        split[ii] = State.getVariable(split[ii], blockElement);
                    }

                    val = val[split[ii]];
                }

                if (State.isPointer(val)) {
                    retVal = val;
                }
            } else {
                wideAlert('The path argument must start with ' +
                    '$ or @.');
            }

            return retVal;
        },

        getBlockScope(element) {
            if (!isElement(element)) {
                return null;
            }

            let blockScope = $('');

            const selector = '[data-block-definitions]';
            blockScope = $(element).parents(selector)
                .map((_, elem) => {
                    const scope = saferJSONParse(elem.dataset.blockDefinitions);
                    if (scope && typeof(scope) === 'object') {
                        return scope;
                    } else {
                        return {};
                    }
                }).toArray()
                .reverse();

            // if the element has a block scope, add it so it is evaluated last
            if ('blockDefinitions' in element.dataset) {
                const block = saferJSONParse(element.dataset.blockDefinitions);
                if (block && typeof(block) === 'object') {
                    blockScope.push(block);
                }
            }

            const returnScope = {};
            blockScope.forEach(scope => {
                getOwnPropertyNames(scope).forEach(name => {
                    returnScope[name] = scope[name];
                });
            });

            return returnScope;
        },

        variableInBlockScope(variablePath, blockElement) {
            const split = variablePath.split(/[[\]]/).filter(aa => aa);
            let variable = State.getBlockScope(blockElement);
            for (let ii = 0; ii < split.length - 1; ii++) {
                if (/[$@]/.test(split[ii][0])) {
                    split[ii] = State.getVariable(split[ii].slice(1),
                        blockElement);
                }

                if (!variable || typeof(variable) !== 'object') {
                    return false;
                }

                variable = variable[split[ii]];
            }

            return variable &&
                typeof(variable) === 'object' &&
                split[split.length - 1] in variable;
        },

        variableInStoryVarState(variablePath) {
            const split = variablePath.split(/[[\]]/).filter(aa => aa);
            let variable = State.stacks.present.storyVariableState;
            for (let ii = 0; ii < split.length - 1; ii++) {
                if (split[ii][0] === '$') {
                    split[ii] = State.getVariable(split[ii].slice(1));
                }

                if (!variable || typeof(variable) !== 'object') {
                    return false;
                }

                variable = variable[split[ii]];
            }

            return variable &&
                typeof(variable) === 'object' &&
                split[split.length - 1] in variable;
        },

        getTransformedPath(variablePath, blockElement) {
            const split = variablePath.split(/[[\]]/).filter(aa => aa);
            let fullPath = split[0];
            for (let ii = 1; ii < split.length; ii++) {
                if (/[$@]/.test(split[ii][0])) {
                    fullPath += '[' +
                        State.getVariable(split[ii], blockElement) +
                        ']';
                } else {
                    fullPath += '[' + split[ii] + ']';
                }
            }

            return fullPath;
        },

        setVariable(variableName, equalsType, newValue, blockElement, nesting) {
            if (!variableName || typeof(variableName) !== 'string') {
                wideAlert('The variableName argument provided to ' +
                    'State.setVariable is not a valid string or object. ' +
                    'Cannot execute.');
                return;
            }

            if (variableName[0] === '$') {
                State.setStoryVariable(
                    variableName,
                    equalsType,
                    newValue,
                    blockElement,
                    nesting);
            } else if (variableName[0] === '@') {
                State.setLocalVariable(
                    variableName,
                    equalsType,
                    newValue,
                    blockElement,
                    nesting);
            }
        },

        setStoryVariable(variablePath, equalsType, newValue, blockElement) {
            const present = State.stacks.present;

            const split = variablePath.split(/[[\]]/).filter(aa => aa);

            let firstAccessor = split[0];
            if (firstAccessor[0] === '$') {
                split[0] = split[0].slice(1);
                firstAccessor = split[0];
            } else if (firstAccessor[0] === '@') {
                wideAlert('Local variables, which start with @, cannot be ' +
                    'set using State.setStoryVariable. Use ' +
                    'State.setLocalVariable instead.');
                return;
            }

            let lastAccessor = split[split.length - 1];
            if (split.length === 1 && lastAccessor[0] === '$') {
                lastAccessor = lastAccessor.slice(1);
            } else if (/[$@]/.test(lastAccessor[0])) {
                lastAccessor = State.getVariable(lastAccessor, blockElement);
            }

            let obj = present.storyVariableState;

            for (let ii = 0; ii < split.length - 1; ii++) {
                if (!split[ii]) {
                    wideAlert('You provided an undefined variable as an ' +
                        'accessor. Cannot set.');
                    return;
                } else if (!obj || typeof(obj) !== 'object') {
                    wideAlert('There is a missing parent property ' +
                        'somewhere. Error will be improved. Cannot set.');
                    return;
                }

                let accessor = split[ii];
                // don't dereference the first portion
                if (ii > 0 && /[$@]/.test(accessor[0])) {
                    accessor = State.getVariable(accessor, blockElement);
                }

                obj = obj[accessor];
            }

            if (!obj || typeof(obj) !== 'object') {
                wideAlert('Failed to find variable to be set. The parent of ' +
                    'the property to be set evaluates to null.');
                return;
            }

            if (equalsType === '=') {
                obj[lastAccessor]  = newValue;
            } else if (assignType === '+=') {
                obj[lastAccessor] += newValue;
            } else if (assignType === '-=') {
                obj[lastAccessor] -= newValue;
            } else if (assignType === '*=') {
                obj[lastAccessor] *= newValue;
            } else if (assignType === '/=') {
                obj[lastAccessor] /= newValue;
            } else if (assignType === '%=') {
                obj[lastAccessor] %= newValue;
            } else {
                wideAlert('The equalsType argument is not ' +
                    'valid. Cannot execute.');
                return;
            }

            const transformedPath = State.getTransformedPath(variablePath,
                blockElement);

            if (State.getBinding(transformedPath)) {
                State.updateBinding(transformedPath);
            }
        },

        setLocalVariable(variablePath, equalsType, newValue, blockElement, nesting) {
            const present = State.stacks.present;

            let firstAccessor = variablePath.split(/[[\]]/)[0];
            let split = [firstAccessor].concat( 
                variablePath
                    .split(/[[\]]/)
                    .slice(1)
                    .filter(aa => aa)
                    .map(aa => {
                        if (/[$@]/.test(aa[0])) {
                            return State.getVariable(aa, blockElement);
                        } else {
                            return aa;
                        }
                    }));

            let lastAccessor = split[split.length - 1];

            if (firstAccessor[0] === '@') {
                firstAccessor = firstAccessor.slice(1);
            }

            if (split.length === 1 && lastAccessor[0] === '@') {
                lastAccessor = lastAccessor.slice(1);
            }

            let block = null;
            let node = blockElement;
            const blockScope = State.getBlockScope(blockElement);
            if (firstAccessor in blockScope) {
                const selector = '[data-block-definitions]';
                let foundParent = false;
                const $elems = $(blockElement).add(
                    $(blockElement).parents(selector));
                $elems.each((_, elem) => {
                    // skip all scopes after the nearest
                    if (foundParent) {
                        return;
                    }

                    const obj = saferJSONParse(elem.dataset.blockDefinitions);
                    if (obj &&
                        typeof(obj) === 'object' &&
                        firstAccessor in obj)
                    {
                        node = elem;
                        block = obj;
                        foundParent = true;
                    }
                });
            } else {
                node = blockElement;

                block = saferJSONParse(node.dataset.blockDefinitions);
                if (!block || typeof(block) !== 'object') {
                    block = {};
                }
            }

            let variable = block[firstAccessor];
            let wasLocal = true;
            const isAPointer = State.isPointer(variable);
            if (isAPointer) {
                if (nesting) {
                    wideAlert('Cannot perform double-pointer sets (setting ' +
                        'a pointer to a pointer).');
                    return;
                }

                if (variable.path[0] === '#') {
                    const nonNumber = variable.path.slice(1).search(/\D/) + 1;
                    const id = variable.path.slice(1, nonNumber);
                    let selector = '[data-gately-pointer-id="' + id + '"]';
                    node = document.querySelector(selector);
                    if (!node) {
                        wideAlert('Cannot find node in document with ' +
                            'Gately pointer id ' + id);
                        return;
                    }

                    const addedPath = split
                        .slice(1)
                        .map(aa => '[' + aa + ']')
                        .join('');

                    const path = variable.path.slice(
                        variable.path.indexOf('@')) + addedPath;

                    State.setVariable(path, equalsType, newValue, node, true);
                } else {
                    const addedPath = split
                        .slice(1)
                        .map(aa => '[' + aa + ']')
                        .join('');

                    State.setVariable(variable.path + addedPath,
                        equalsType,
                        newValue,
                        node,
                        true);
                }

                const transformedPath = State.getTransformedPath(variablePath,
                    blockElement);

                if (State.getBinding(transformedPath)) {
                    State.updateBinding(transformedPath);
                }

                return;
            }

            for (let ii = 1; ii < split.length - 1; ii++) {
                if (!variable || typeof(variable) !== 'object') {
                    wideAlert('There is a missing parent property ' +
                        'somewhere. Cannot set.');
                    return;
                }

                let accessor = split[ii];
                if (/[$@]/.test(accessor[0])) {
                    accessor = State.getVariable(accessor, blockElement);
                }

                variable = variable[accessor];
            }

            // If lastAccessor is firstAccessor, we need to go to 
            // the -1th index, not the 0th.
            if (split.length === 1) {
                variable = block;
            }

            if (!variable || typeof(variable) !== 'object') {
                wideAlert('Failed to find variable to be set. The parent of ' +
                    'the property to be set evaluates to null.');
                return;
            }

            if (equalsType === '=') {
                variable[lastAccessor]  = newValue;
            } else if (assignType === '+=') {
                variable[lastAccessor] += newValue;
            } else if (assignType === '-=') {
                variable[lastAccessor] -= newValue;
            } else if (assignType === '*=') {
                variable[lastAccessor] *= newValue;
            } else if (assignType === '/=') {
                variable[lastAccessor] /= newValue;
            } else if (assignType === '%=') {
                variable[lastAccessor] %= newValue;
            } else {
                wideAlert('The equalsType argument is not ' +
                    'valid. Cannot execute.');
                return;
            }

            if (wasLocal) {
                node.dataset.blockDefinitions = JSON.stringify(block);
            }

            const transformedPath = State.getTransformedPath(variablePath,
                blockElement);

            if (!nesting && State.getBinding(transformedPath)) {
                State.updateBinding(transformedPath);
            }
        },

        addMacroAlias(macroName, aliasName, disposeAfterPassage, override) {
            if (!macroName) {
                wideAlert('The macroName argument passed to ' +
                    'Macro.global.addmacroalias is not valid. Cannot add macro alias.');
                return;
            } else if (!(macroName in Macro)) {
                wideAlert('The macroName argument passed to ' +
                    'Macro.global.addmacroalias does not correspond to an ' +
                    'existing macro. Cannot add macro alias.');
                return;
            } else if (!aliasName) {
                wideAlert('The aliasName argument passed to ' +
                    'Macro.global.addmacroalias is not valid. Cannot add macro alias.');
                return;
            } else if (aliasName in Macro) {
                wideAlert('A macro with the same name as the aliasName ' +
                    'argument, ' + aliasName + ', already exists in the ' +
                    'Macro object. Cannot execute.');
                return;
            } else if (aliasName in State.macroAliasStore &&
                override !== true &&
                (typeof(override) !== 'string' ||
                    override.toLowerCase() !== 'override'))
            {
                wideAlert('An alias with the same name as the aliasName ' +
                    'argument, ' + aliasName + ', already exists in the ' +
                    'State.macroAliasStore object. Set the override ' +
                    'argument to true or "override" to override this error.');
                return;
            }

            State.macroAliasStore[aliasName] = Macro[macroName];

            if (disposeAfterPassage === true ||
                (typeof(disposeAfterPassage) === 'string' &&
                disposeAfterPassage.toLowerCase() === 'disposeafterpassage'))
            {
                State.macroAliasDisposals.push(aliasName);
            }
        },

        addFunction(name, override, argNames, callback) {
            if (!name) {
                wideAlert('A TwineScript function cannot exist without a ' +
                    'valid name.');
                return;
            }  else if (name in Macro) {
                wideAlert('There is already a property in the Macro object ' +
                    'named ' + name + '. Cannot execute.');
                return;
            } else if (name in State.macroAliasStore) {
                wideAlert('There is already a property in the ' +
                    'State.macroAliasStore named ' + name + '. Cannot ' +
                    'execute.');
                return;
            } else if (name in State.functionStore &&
                override !== true &&
                (typeof(override) !== 'string' ||
                    override.toLowerCase() !== 'override'))
            {
                wideAlert('There is already a property in ' +
                    'State.functionStore named ' + name + '. Set the ' +
                    'override argument to true or "override" to ' +
                    'override this error.');
                return;
            } else if (typeof(callback) !== 'string' || !callback) {
                wideAlert('The callback argument passed to ' +
                    'State.addFunction is not a valid, non-empty string.');
                return;
            }

            State.functionStore[name] = {
                pointerPassthrough: true,
                handler() {
                    const match = document.createElement('tw-match');
                    match.className = 'functionContainer';
                    match.textContent = callback;

                    const args = from(arguments)
                        .map(arg => saferJSONParse(arg));

                    const blockScope = State.getBlockScope(this);
                    if (blockScope) {
                        getOwnPropertyNames(blockScope).forEach(name => {
                            if (State.isPointer(blockScope[name])) {
                                State.setVariable(
                                    '@' + name, 
                                    '=',
                                    State.makePointer(
                                        blockScope[name].path +
                                        '[' + name + ']'),
                                    match);
                            } else {
                                State.setVariable(
                                    '@' + name,
                                    '=',
                                    blockScope[name],
                                    match);
                            }
                        });
                    }
                    
                    State.setVariable('@args', '=', args, match);

                    for (let ii = 0; ii < argNames.length; ii++) {
                        if (ii === args.length) {
                            break;
                        }

                        let argName = argNames[ii];
                        if (argName[0] === '@') {
                            argName = argName.slice(1);
                        } else if (argName[0] === '$') {
                            wideAlert('Cannot only set local variables ' +
                                'through the function macro.');
                            continue;
                        }

                        State.setVariable('@' + argName, '=', args[ii], match);
                    }

                    Compiler.compile(match);
                    return match;
                },
            }
        },

        getBinding(name) {
            if (typeof(name) !== 'string' || !name) {
                wideAlert('The name argument passed to ' +
                    'State.getBinding was not valid.');
                return;
            }

            return State.stacks.present.bindingState[name];
        },

        addBinding(name, callback) {
            if (!name || typeof(name) !== 'string') {
                wideAlert('The name argument provided to State.addBinding ' +
                    'is not valid.');
            }

            State.stacks.present.bindingState[name] = {
                callback,
                lastValue: null,
            };
        },

        updateBinding(name) {
            if (!name || typeof(name) !== 'string') {
                wideAlert('The name argument passed to ' +
                    'State.updateBinding is not valid.');
                return;
            }

            const binding = State.stacks.present.bindingState[name];
            if (!binding) {
                wideAlert('There is no binding tied to the variable name or ' +
                    'expression ' + name + '. ' +
                    'Cannot update binding.');
                return;
            } else if (!binding.callback) {
                wideAlert('The callback property of the binding referred ' +
                    'to by the name argument ' +
                    'passed to State.updateBinding is not valid.');
                return;
            }

            const selector = 'tw-binding[data-binding-name="' +
                name +
                '"]';
            const elem = document.querySelector(selector);

            if (!elem || !document.contains(elem)) {
                /*gatelyLog('Binding named ' + name + ' could not be found ' +
                    'in the document.');*/
                State.removeBinding(name);
                return;
            }

            let retVal;
            const present = State.stacks.present;
            if (name[0] === '$' &&
                name.slice(1) in present.storyVariableState)
            {
                retVal = present.storyVariableState[name.slice(1)];
            }

            if (JSON.stringify(retVal) !== JSON.stringify(binding.lastValue) ||
                binding.lastValue === undefined ||
                binding.lastValue === null)
            {
                elem.innerHTML = '';
                const match = document.createElement('tw-match');
                match.className = 'bindingContainer';
                match.textContent = binding.callback;
                elem.appendChild(match);
                Compiler.compile(match);
            }

            if (retVal && typeof(retVal) === 'object') {
                binding.lastValue = deepCopy(retVal);
            } else if (retVal !== undefined) {
                binding.lastValue = retVal;
            }
        },

        updateBindings() {
            for (let name in State.stacks.present.bindingState) {
                if (name !== 'passageName') {
                    State.updateBinding(name);
                }
            }
        },

        removeBinding(name) {
            delete State.stacks.present.bindingState[name];
        },

        addTraceryGrammar(name, grammar, modifiers) {
            if (!name || typeof(name) !== 'string') {
                wideAlert('The name argument passed to State.addTraceryGrammar ' +
                    'is not valid. Cannot add.');
                return;
            }

            let object;
            if (!grammar) {
                wideAlert('The grammar argument passed to ' +
                    'State.addTraceryGrammar is not valid. Cannot add.');
                return;
            } else if (typeof(grammar) === 'string') {
                try {
                    object = eval('(' + grammar + ')');
                } catch (evalError) {
                    wideAlert('Could not eval in-passage tracery source' + 
                    '(store). ' + evalError.message);
                    return;
                }
            } else if (typeof(grammar) === 'object') {
                object = grammar;
            }

            const traceried = tracery.createGrammar(object);

            let mods = modifiers;
            if (typeof(modifiers) === 'string') {
                mods = saferCombinedParse(modifiers);
            } 

            if (!mods) {
                mods = baseEngModifiers;
            }
        
            traceried.addModifiers(mods);
            State.traceryStore[name] = traceried;
        },

        validateStateIntegrity() {
            let val = State.stacks.past.passageNames.length;
            for (name in State.stacks.past) {
                if (State.stacks.past[name].length !== val) {
                    throw new Error('One of the state stacks (' +
                        'State.stacks.past[' + name + '] has a ' +
                        'different length than one or more of the other ' +
                        'stacks. This prevents the story from continuing.');
                }
            }

            for (name in State.stacks.present) {
                if (!State.stacks.present[name]) {
                    throw new Error('One of the state present properties (' +
                        'State.stacks.present[' + name + '] has an ' +
                        'invalid value. This prevents the story from ' +
                        'continuing.');
                }
            }

            val = State.stacks.future.passageNames.length;
            for (name in State.stacks.future) {
                if (State.stacks.future[name].length !== val) {
                    throw new Error('One of the state history stacks (' +
                        'State.stacks.past[' + name + '] has a ' +
                        'different length than one or more of the other ' +
                        'stacks. This prevents the story from continuing.');
                }
            }

            const selector = 'tw-storydata > tw-passagedata[name="' +
                State.stacks.present.passageName +
                '"]';
            if (State.stacks.present.passageName !== '_load' &&
                !document.querySelector(selector))
            {
                wideAlert('The State.stacks.present.passageName property ' +
                    'does not correspond to a tw-passagedata element. This ' +
                    'may prevent the story from functioning and continuing ' +
                    'on will likely result in further bugs.');
                return;
            }
        },

        getUserData(name) {
            return State.userData[name];
        },

        setUserData(name, value) {
            State.userData[name] = value;
        },
    };

    State.stacks.present.storyTheme = State.themeStore.story.default;

    function deleteText(obj) {
        delete obj._text;
        for (let name in obj) {
            if (obj[name] && typeof(obj[name]) === 'object') {
                deleteText(obj[name]);
            }
        }

        return obj;
    }
}());