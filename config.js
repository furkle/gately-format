(function() {
    'use strict';

    window.Config = {
        renderTypes: {
            jonah: 'jonah',
            sugarcane: 'sugarcane',
        },

        newlineTypes: {
            newlinetobr: 'newlinetobr',
            newlineToBr: 'newlinetobr',
            noNewlines: 'nonewlines',
            nonewlines: 'nonewlines',
        },

        chronologyButtonDirections: {
            rewind: 'rewind',
            fastforward: 'fastforward',
            fastForward: 'fastforward',
        },

        errorLevels: {
            alert: 'alert',
            console: 'console',
            inline: 'inline',
        },

        saveStoryTheme(name, theme) {
            if (!name) {
                wideAlert('The name argument passed to ' +
                    'Config.saveStoryTheme is not valid or was not ' +
                    'provided. Cannot save theme.');
                return;
            } else if (!theme) {
                wideAlert('The theme argument passed to ' +
                    'Config.saveStoryTheme is not valid or was not ' +
                    'provided. Cannot save theme');
                return;
            }

            let themeObj = theme;
            if (typeof(theme) === 'string') {
                themeObj = saferJSONParse(theme);
            }

            if (!themeObj || typeof(themeObj) !== 'object') {
                wideAlert('The theme object is either not a valid object ' +
                    'or is a string that cannot be parsed into a valid ' +
                    'object. Cannot save theme.');
                return;
            }

            State.themeStore.story[name] = theme;
        },

        loadStoryTheme(name) {
            if (!name) {
                wideAlert('The name argument passed to ' +
                    'Config.loadStoryTheme is not valid or was not ' +
                    'provided. Cannot load theme.');
                return;
            } else if (!(name in State.themeStore.story)) {
                wideAlert('The name argument provided to ' +
                    'Config.loadStoryTheme does not match any presently ' +
                    'saved theme. Cannot load theme.');
                return;
            }

            const oldTheme = State.stacks.present.storyTheme;
            if (isFunction(oldTheme.unload)) {
                oldTheme.unload();
            }

            const themeObj = State.themeStore.story[name];
            if (isFunction(themeObj.load)) {
                themeObj.load();
            }

            const curTheme = Themes.getDefaultStoryTheme();
            getOwnPropertyNames(themeObj).forEach(name => {
                curTheme[name] = themeObj[name];
            });

            if (isFunction(curTheme.prerender)) {
                curTheme.prerenderString = curTheme.prerender.toString();
            } else if (curTheme.prerenderString) {
                curTheme.prerender = eval('(' + curTheme.prerenderString + ')');
            } else {
                curTheme.prerenderString = null;
            }

            if (isFunction(curTheme.postrender)) {
                curTheme.postrenderString = curTheme.postrender.toString();
            } else if (curTheme.postrenderString) {
                curTheme.postrender = eval('(' + curTheme.postrenderString + ')');
            } else {
                curTheme.postrenderString = null;
            }

            if (isFunction(curTheme.load)) {
                curTheme.loadString = curTheme.load.toString();
            } else if (curTheme.loadString) {
                curTheme.load = eval('(' + curTheme.loadString + ')');
            } else {
                curTheme.loadString = null;
            }

            if (isFunction(curTheme.unload)) {
                curTheme.unloadString = curTheme.unload.toString();
            } else if (curTheme.unloadString) {
                curTheme.unload = eval('(' + curTheme.unloadString + ')');
            } else {
                curTheme.unloadString = null;
            }

            State.stacks.present.storyTheme = curTheme;

            Renderer.initializeStoryTheme();
        },

        editStoryTheme(theme) {
            if (typeof(theme) === 'string') {
                if (!theme) {
                    wideAlert('The theme argument provided to ' +
                        'Config.editTheme is a string, but it is empty. ' +
                        'Cannot execute.');
                    return;
                }

                const lowered = theme.toLowerCase();
                if (lowered in Config.renderTypes) {
                    handleRenderTypeChange(lowered);
                } else if (lowered in Config.newlineTypes) {
                    handleNewlineTypeChange(lowered);
                } else if (lowered in chronologyButtonDirections) {
                    handleRewindFastForwardChange(lowered);
                } else if (lowered in Config.errorLevels) {
                    const errorLevels = State.stacks.present.errorLevels;
                    handleErrorLevelsChange(lowered, !errorLevels[lowered]);
                }
            } else if (typeof(theme) === 'object') {
                if (!theme) {
                    wideAlert('The theme argument provided to ' +
                        'Config.editTheme is an object, but it is null. ' +
                        'Cannot execute.');
                    return;
                }

                if ('length' in theme) {
                    const arraylike = theme;
                    if (!arraylike.length) {
                        wideAlert('The theme argument provided ' +
                            'to Config.editTheme is an array-like object, ' +
                            'its length is 0. Cannot execute.');
                        return;
                    }

                    for (let ii = 0; ii < arraylike.length; ii++) {
                        const index = arraylike[ii].toLowerCase();
                        if (!index || typeof(index) !== 'string') {
                            continue;
                        }

                        if (index in Config.renderTypes) {
                            handleRenderTypeChange(index);
                        } else if (index in Config.newlineTypes) {
                            handleNewlineTypeChange(index);
                        } else if (index in chronologyButtonDirections) {
                            handleRewindFastForwardChange(index);
                        } else if (index in Config.errorLevels) {
                            const errorLevels =
                                State.stacks.current.storyTheme.errorLevels;
                            handleErrorLevelsChange(index, !errorLevels[index]);
                        }
                    }
                } else {
                    const obj = configObjOrString;

                    const renderType = (obj.renderType || '')
                        .toString()
                        .toLowerCase();
                    if (renderType in Config.renderTypes) {
                        handleRenderTypeChange(renderType);
                    }

                    const newlineType = (obj.newlineType || '')
                        .toString()
                        .toLowerCase();
                    if (obj.newlineType in Config.newlineTypes) {
                        handleNewlineTypeChange(newlineType);
                    }

                    if (typeof(obj.rewind) === 'boolean') {
                        handleRewindFastForwardChange('rewind', obj.rewind);
                    }

                    if (typeof(obj.fastForward) === 'boolean') {
                        handleRewindFastForwardChange('fastforward',
                            obj.fastForward);
                    }

                    if (obj.errorLevels &&
                        typeof(obj.errorLevels) === 'object')
                    {
                        if (typeof(obj.errorLevels.alert) === 'boolean') {
                            handleErrorLevelsChange('alert',
                                obj.errorLevels.alert);
                        }

                        if (typeof(obj.errorLevels.console) === 'boolean') {
                            handleErrorLevelsChange('console',
                                obj.errorLevels.console);
                        }

                        if (typeof(obj.errorLevels.inline) === 'boolean') {
                            handleErrorLevelsChange('inline',
                                obj.errorLevels.inline);
                        }
                    }
                }
            } else {
                wideAlert('The configObjOrString argument passed to ' + 
                    'Config.newTheme is neither an object nor a string. ' +
                    'Cannot execute.');
                return;
            }
        },

        addPassageTheme(name, obj) {
            State.themeStore.passage[name] = obj;
        },

        editRenderType(renderType) {
            if (!renderType) {
                wideAlert('The renderType argument provided to ' +
                    'Config.editRenderType argument is not a string or is ' +
                    'empty. Cannot execute.');
                return;
            } else if (typeof(renderType) === 'string') {
                if (renderType.toLowerCase() in Config.renderTypes) {
                    handleRenderTypeChange(renderType.toLowerCase());
                } else {
                    wideAlert('The renderType argument provided to ' +
                        'Config.editRenderType is a string, but does not ' +
                        'match one of the render types in ' +
                        'Config.renderTypes. Cannot execute.');
                    return;
                }
            } else {
                wideAlert('The renderType argument provided to ' +
                    'Config.editRenderType is not a string. Cannot execute.');
                return;
            }
        },

        editNewlineType(newlineType) {
            if (!newlineType) {
                wideAlert('The newlineType argument provided to ' +
                    'Config.editNewlineType argument is not a string or is ' +
                    'empty. Cannot execute.');
                return;
            } else if (typeof(newlineType) === 'string') {
                if (newlineType.toLowerCase() in Config.newlineTypes) {
                    handleNewlineTypeChange(newlineType.toLowerCase());
                } else {
                    wideAlert('The newlineType argument provided to ' +
                        'Config.editNewlineType does not match one of the ' +
                        'newline types in Config.newlineTypes. ' +
                        'Cannot execute.');
                    return;
                }
            } else {
                wideAlert('The newlineType argument provided to ' +
                    'Config.editNewlineType is not a string. Cannot ' +
                    'execute.');
                return;
            }
        },

        editRewind(value) {
            if (typeof(value) !== 'boolean') {
                wideAlert('The value argument provided to ' +
                    'Config.editRewind is not a boolean. Cannot execute.');
                return;
            }

            handleRewindFastForwardChange('rewind', value);
        },

        editFastForward(value) {
            if (typeof(value) !== 'boolean') {
                wideAlert('The value argument provided to ' +
                    'Config.editFastForward is not a boolean. Cannot ' +
                    'execute.');
                return;
            }

            handleRewindFastForwardChange('fastForward', value);
        },

        editErrorLevel(level, value) {
            if (level.toLowerCase() in Config.errorLevels) {
                if (typeof(value) !== 'boolean') {
                    wideAlert('The value argument provided to ' +
                        'Config.editErrorLevel is not a boolean. Cannot ' +
                        'execute.');
                    return;
                }

                handleErrorLevelsChange(level.toLowerCase(), value);
            } else {
                wideAlert('The level argument provided to ' +
                    'Config.editErrorLevel does not match one of the ' +
                    'errorLevel types in Config.errorLevels. Cannot ' +
                    'execute.');
                return;
            }
        },

        editStoryStyle(styleText) {
            if (!styleText) {
                wideAlert('The styleText argument provided to ' +
                    'Config.editStyle is not a valid string. Cannot edit ' +
                    'style.');
                return;
            } else if (typeof(styleText) !== 'string') {
                wideAlert('The styleText argument provided to ' +
                    'Config.editStyle is not of type string. Cannot edit ' +
                    'style.');
                return;
            }

            State.stacks.present.storyTheme.style = styleText;
            Renderer.initializeStoryTheme();
        },

        editLoad(strOrFunc) {
            const storyTheme = State.stacks.present.storyTheme;

            if (isFunction(strOrFunc)) {
                storyTheme.load = strOrFunc;
                storyTheme.loadString = storyTheme.load.toString();
            } else if (typeof(strOrFunc) === 'string') {
                storyTheme.loadString = strOrFunc;
                storyTheme.load = new Function(storyTheme.loadString);
            } else {
                wideAlert('The strOrFunc argument provided to ' +
                    'Config.editPrerender is not a valid string or ' +
                    'function. Cannot edit theme prerender function.');
                return;
            }
        },

        editUnload(strOrFunc) {
            const storyTheme = State.stacks.present.storyTheme;

            if (isFunction(strOrFunc)) {
                storyTheme.unload = strOrFunc;
                storyTheme.loadString = storyTheme.unload.toString();
            } else if (typeof(strOrFunc) === 'string') {
                storyTheme.unloadString = strOrFunc;
                storyTheme.unload = new Function(storyTheme.unloadString);
            } else {
                wideAlert('The strOrFunc argument provided to ' +
                    'Config.editPrerender is not a valid string or ' +
                    'function. Cannot edit theme prerender function.');
                return;
            }
        },

        editPrerender(strOrFunc) {
            const storyTheme = State.stacks.present.storyTheme;

            if (isFunction(strOrFunc)) {
                storyTheme.prerender = strOrFunc;
                storyTheme.prerenderString = storyTheme.prerender.toString();
            } else if (typeof(strOrFunc) === 'string') {
                storyTheme.prerenderString = strOrFunc;
                storyTheme.prerender =
                    new Function(storyTheme.prerenderString);
            } else {
                wideAlert('The strOrFunc argument provided to ' +
                    'Config.editPrerender is not a valid string or ' +
                    'function. Cannot edit theme prerender function.');
                return;
            }
        },

        editPostrender(strOrFunc) {
            const storyTheme = State.stacks.present.storyTheme;

            if (isFunction(strOrFunc)) {
                storyTheme.postrender = strOrFunc;
                storyTheme.postrenderString = storyTheme.postrender.toString();
            } else if (typeof(strOrFunc) === 'string') {
                storyTheme.postrenderString = strOrFunc;
                storyTheme.postrender =
                    new Function(storyTheme.postrenderString);
            } else {
                wideAlert('The strOrFunc argument provided to ' +
                    'Config.editPostrender is not a valid string or ' +
                    'function. Cannot edit theme postrender function.');
                return;
            }
        },
    }

    function handleRenderTypeChange(renderType) {
        State.stacks.present.storyTheme.renderType = renderType;

        Renderer.handleBackAndForwardButtons();
    }

    function handleNewlineTypeChange(newlineType) {
        const presentTheme = State.stacks.present.storyTheme;
        if (newlineType === presentTheme.newlineType) {
            return;
        }

        presentTheme.newlineType = newlineType;

        Renderer.undoNewlineHandling($('tw-passage.current'));

        if (newlineType === Config.newlineTypes.newlineToBr) {
            Renderer.newlineToBr($currentNode);
        }
    }

    function handleRewindFastForwardChange(property, value) {
        const $currentNode = $('tw-passage.current');

        if (property === 'rewind') {
            if (value) {
                $currentNode.addClass('rewind');
            } else {
                $currentNode.addClass('rewind');
            }
        } else if (property === 'fastForward') {
            if (value) {
                $currentNode.addClass('fastForward');
            } else {
                $currentNode.removeClass('fastForward');
            }
        } else {
            return;
        }

        State.stacks.present.storyTheme.rewind = Boolean(value);
    }

    function handleErrorLevelsChange(level, value) {
        State.stacks.present.storyTheme.errorLevels[level] = value;
    }
}());