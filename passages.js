(function() {
    'use strict';

    State.stacks.present.virtualPassageState = {
        all: {},
    };
    
    State.getVirtualPassage = nameOrObj => {
        if (!nameOrObj) {
            wideAlert('No nameOrObj provided to State.getVirtualPassage.');
            return null;
        }

        let name = nameOrObj;
        if (typeof(name) === 'object') {
            name = nameOrObj.name;
        }

        if (!name) {
            return null;
        }

        const present = State.stacks.present;
        const vpo = present.virtualPassageState.all[name];
        if (!vpo || typeof(vpo) !== 'object') {
            return null;
        }

        const selector = 'tw-passagedata[name="' + name + '"]';
        vpo.passage = document.querySelector(selector);

        vpo.selector = () => {
            return '#' + vpo.name;
        };

        let _name = name;
        Object.defineProperty(vpo, 'name', {
            get() {
                return _name;
            },

            writeable: true,
            configurable: true,
        });

        Object.defineProperty(vpo, 'shortName', {
            get() {
                return vpo.name.split('/').slice(-1)[0];
            },

            writeable: true,
            configurable: true,
        });

        let _text = vpo.text;
        Object.defineProperty(vpo, 'text', {
            set(value) {
                _text = value.toString();
                vpo.passage.textContent = value;
                return vpo;
            },

            writeable: true,
            configurable: true,
        });

        vpo.ascendants = () => {
            const collection = {
                type: collection,
                children: {},
            };

            const present = State.stacks.present;
            for (let _name in present.virtualPassageState.all) {
                const temp = present.virtualPassageState.all[_name];
                if (_name in temp.descendants) {
                    collection.children[_name] =
                        State.getVirtualPassage(temp.name);
                }
            }

            return collection;
        };

        vpo.parent = () => {
            const present = State.stacks.present;
            for (let name in present.virtualPassageState.all) {
                const temp = present.virtualPassageState.all[name];
                if (!temp.children || typeof(temp.children) !== 'object') {
                    return null;
                }

                if (vpo.name in temp.children) {
                    return State.getVirtualPassage(temp.name);
                }
            }

            return null;
        };

        vpo.descendantOf = name => {
            const ascendant = State.getVirtualPassage(name);
            if (!ascendant) {
                return false;
            }

            return vpo.name in ascendant.descendants;
        };

        vpo.descendantOfShortName = shortName => {
            const present = State.stacks.present;
            const vpoAll = present.virtualPassageState.all;
            let parents = getOwnPropertyNames(vpoAll)
                .map(aa => State.getVirtualPassage(aa))
                .filter(aa => aa.shortName === shortName)
                .map(parent => getOwnPropertyNames(parent.descendants));

            if (!parents.length) {
                return false;
            }

            return Boolean(parents
                .reduce((a, b) => (a || []).concat(b))
                .map(child => State.getVirtualPassage(child))
                .filter(desc => desc.name === vpo.name)
                .length);
        };

        vpo.childOf = name => {
            const parent = State.getVirtualPassage(name);
            if (!parent) {
                return false;
            }

            return vpo.name in parent.children;
        };

        vpo.childOfShortName = shortName => {
            const present = State.stacks.present;
            const vpoAll = present.virtualPassageState.all;
            let parents = getOwnPropertyNames(vpoAll)
                .map(aa => State.getVirtualPassage(aa))
                .filter(aa => aa.shortName === shortName)
                .map(parent => getOwnPropertyNames(parent.children));

            if (!parents.length) {
                return false;
            }

            return Boolean(parents
                .reduce((a, b) => (a || []).concat(b))
                .map(child => State.getVirtualPassage(child))
                .filter(child => child.name === vpo.name)
                .length);
        };

        vpo.hasChild = name => {
            const child = State.getVirtualPassage(name);
            if (!child) {
                return false;
            }

            return child.name in vpo.children;
        };

        vpo.hasChildShortName = shortName => {
            const child = getOwnPropertyNames(vpo.children)
                .map(childName => State.getVirtualPassage(childName))
                .filter(child => child.shortName === shortName);

            return Boolean(child.length);
        }

        vpo.hasDescendant = name => {
            const descendant = State.getVirtualPassage(name);
            if (!descendant) {
                return false;
            }

            return descendant.name in vpo.descendants;
        };

        vpo.hasDescendantShortName = shortName => {
            const desc = getOwnPropertyNames(vpo.descendants)
                .map(childName => State.getVirtualPassage(childName))
                .filter(desc => desc.shortName === shortName);

            return Boolean(child.length);
        }

        vpo.getChildrenAll = () => {
            const collection = {
                type: 'collection',
                children: {},
            };

            for (let name in vpo.children) {
                collection.children[name] = State.getVirtualPassage(name);
            }

            return collection;
        };

        vpo.getChildrenOne = shortName => {
            const children = getOwnPropertyNames(vpo.children)
                .map(aa => State.getVirtualPassage(aa));
            for (let ii = 0; ii < children.length; ii++) {
                if (children[ii].shortName === shortName) {
                    return children[ii];
                }
            }

            return null;
        };

        vpo.getDescendantsAll = () => {
            const collection = {
                type: 'collection',
                children: {},
            };

            for (let name in vpo.descendants) {
                collection.children[name] = State.getVirtualPassage(name);
            }

            return collection;
        };

        vpo.getDescendantsOne = shortName => {
            const descs = getOwnPropertyNames(vpo.descendants)
                .map(aa => State.getVirtualPassage(aa));
            for (let ii = 0; ii < descs.length; ii++) {
                if (descs[ii].shortName === shortName) {
                    return descs[ii];
                }
            }

            return null;
        };

        let _tags = vpo.tags;
        Object.defineProperty(vpo, 'tags', {
            get() {
                return _tags;
            },

            set(value) {
                if (!value) {
                    wideAlert('Invalid value passed to a VPO\'s setTags ' +
                        'method.');
                    return null;
                } else if (value &&
                    typeof(value) === 'object' &&
                    value.length)
                {
                    _tags = value;
                    vpo.passage.setAttribute(tags, value.join(' '));
                } else if (typeof(value) === 'string') {
                    _tags = value.split(' ');
                    vpo.passage.setAttribute('tags', value);
                }

                return vpo;
            },

            writeable: true,
            configurable: true,
        });

        vpo.addTag = value => {
            if (vpo.tags.indexOf(value) === -1) {
                vpo.tags.push(value);
                vpo.passage.setAttribute(tags, vpo.tags.join(' '));
            }

            return vpo;
        };

        vpo.removeTag = value => {
            if (vpo.tags.indexOf(value) !== -1) {
                vpo.tags.splice(vpo.tags.indexOf(value), 1);
                vpo.passage.setAttribute(tags, vpo.tags.join(' '));
            }

            return vpo;
        };

        let attributes = vpo.attributes;
        Object.defineProperty(vpo, 'attributes', {
            get() {
                return attributes;
            },

            set(value) {
                attributes = value;

                try {
                    vpo.passage.dataset.attributes = JSON.stringify(value);
                } catch (e) {
                    wideAlert('Failed to serialize VPO attributes: ' + e.message);
                }

                return vpo;
            },

            writeable: true,
            configurable: true,
        });

        vpo.getAttr = name => {
            return vpo.attributes[value];
        };

        vpo.setAttr = (name, value) => {
            vpo.attributes[name] = value;

            try {
                vpo.passage.dataset.attributes =
                    JSON.stringify(vpo.attributes);
            } catch (e) {
                wideAlert('Failed to serialize vpo attributes: ' + e.message);
            }
     
            return vpo;
        };

        vpo.removeAttr = name => {
            delete vpo.attributes[name];

            try {
                vpo.passage.dataset.attributes =
                    JSON.stringify(vpo.attributes);
            } catch (e) {
                wideAlert('Failed to serialize vpo attributes: ' + e.message);
            }

            return vpo;
        };

        return vpo;
    }

    State.getCurrentVirtualPassage = () => {
        return State.getVirtualPassage(State.stacks.present.passageName);
    };
    
    State.getVirtualPassageWithoutText = name => {
        return deleteText(State.getVirtualPassage(name));
    };

    const $storyData = $('tw-storydata');
    const selector = 'tw-storydata > tw-passagedata';
    from(document.querySelectorAll(selector)).forEach(passage => {
        const name = passage.getAttribute('name');
        if (name[0] === '_') {
            wideAlert('Passages cannot have a passage name starting in _.');
        }

        const present = State.stacks.present;

        let currentObject = present.virtualPassageState.all;
        const split = name.split('/');
        for (let ii = 0; ii < split.length; ii++) {
            if (!currentObject) {
                return;
            }

            const section = split[ii];

            const name = split.slice(0, ii + 1).join('/');
            const $elem = $('tw-passagedata[name="' + name + '"]');
            const text = $elem.text();
            let tags = ($elem.attr('tags') || '').split(' ');
            if (tags[0] === '') {
                tags = [];
            }

            const newObj = {
                name,
                text,
                tags,
                children: {},
                descendants: {},
                attributes: {},
            };

            const regex = /(.+)=(.+)/
            tags.forEach(tag => {
                const match = tag.match(regex);
                if (match) {
                    newObj.attributes[match[1]] = match[2];
                }
            });

            if (currentObject !== present.virtualPassageState.all) {
                if (!currentObject.children) {
                    currentObject.children = {};
                }

                currentObject.children[newObj.name] = '#' + newObj.name;
            }

            for (let jj = 0; jj < ii; jj++) {
                const name = split.slice(0, jj + 1).join('/');
                const curObj = present.virtualPassageState.all[name];
                if (!curObj.descendants) {
                    curObj.descendants = {};
                }

                curObj.descendants[newObj.name] = '#' + newObj.name;
            }
            
            if (!present.virtualPassageState.all[newObj.name]) {
                present.virtualPassageState.all[newObj.name] = newObj;
            } else {
                const curObj = present.virtualPassageState.all[newObj.name];
                for (let name in newObj.children) {
                    curObj.children[name] = newObj[name];
                }

                for (let name in newObj.descendants) {
                    curObj.descendants[name] = newObj[name];
                }
            }

            currentObject = present.virtualPassageState .all[newObj.name];
        }
    });
}());