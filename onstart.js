(function() {
    'use strict';

    // prevent FOUC; make window visible now that loading is complete
    $('html').removeClass('unloaded');

    if ($('tw-passagedata[name="_load"]').length) {
        wideAlert('It\'s inadvisable to name a passage "_load" – the ' +
            'compiler uses this name for the initial, non-passage frame, ' +
            'and naming a passage this will most likely cause unfixable ' +
            'bugs.');
    }

    // Gately is now loading.
    window.gatelyLoading = true;

    // The global debug flag for the story.
    window.DEBUG = false;

    // The time at which gately began loading.
    window.gatelyLoadTime = new Date().getTime();

    let selector;

    // Process flags within the story <tw-storydata>.
    const flags = $storydata.attr('flags');
    let processed = [];
    if (flags) {
        const splitFlags = flags.split(' ');
        splitFlags.forEach(aa => {
            if (processed.indexOf(aa) === -1) {
                processed.push(aa);
            }
        });
    }

    // Check for DEBUG signs.
    if (processed.indexOf('debug') !== -1 ||
        $storydata.find('[name="debug"]').length)
    {
        window.DEBUG = true;
    }

    function proxyRecordState(name, target, property, value) {
        const debug = window.DEBUG;
        const recording = State.recordingTranscript;
        const stateChanges = Options.recordingLevels.stateChanges;
        const load = Options.recordingLevels.load;
        const isLoading = window.isLoading;
        const render = Options.recordingLevels.render;
        const isRendering = Renderer.isRendering;

        if (debug ||
            (recording &&
                ((isLoading && load && stateChanges) || 
                (isRendering && render && stateChanges) ||
                (!isLoading && !isRendering && stateChanges))
            ))
        {
            Options.captureStateChange(name, target, property, value);
        }
    }

    /* If the Proxy constructor is available, attach a Proxy to the State
     * object and each of the objects therein. This allows state changes
     * to be captured by the debugger. */
    if (isFunction(window.Proxy) && 'Options' in window) {
        for (let name in State) {
            if (typeof(State[name]) === 'object' && State[name]) {
                State[name] = new window.Proxy(State[name], {
                    set: (target, property, value) => {
                        proxyRecordState(name, target, property, value);
                        target[property] = value;
                        return true;
                    }
                });
            }
        }

        State = new window.Proxy(State, 
            {
                set: (target, property, value) => {
                    proxyRecordState(name, target, property, value);
                    target[property] = value;
                    return true;
                }
            }
        );
    }

    const frame = document.querySelector('#defaultTranscriptFrame');

    let $displayPane = null;
    let passagePane = null;
    let frameHTML = null;
    let frameHead = null;
    let frameBody = null;
    if (frame && frame.contentDocument) {
        frameHTML = frame.contentDocument.body.parentElement;
        frameHead = frameHTML.querySelector('head');
        frameBody = frameHTML.querySelector('body');
        $displayPane = $(frameBody).find('#displayPane');
        passagePane = $(frameBody).find('#passagePane');
        $displayPane.on('click', 'tw-tab', e => {
            const $tabControl = $(e.currentTarget).parent();
            $tabControl.find('tw-tab').removeClass('active');

            const $tgt = $(e.currentTarget);
            $tgt.addClass('active')

            const selector = $tgt.attr('data-selector');
            $displayPane.children('div').removeClass('active');
            $displayPane.children(selector).addClass('active');
        });
    }

    if (!State.stacks.present.storyTheme.debug.allowTranscripts && !DEBUG) {
        $('#printTranscriptToConsole').addClass('hidden');
    }

    const $twStory = $('tw-story');
    if (!$twStory) {
        throw new Error('Could not find tw-story! Aborting start.');
    }

    // an enumeration of all known jquery handlers
    const jqueryEvents = [
        'click',
        'blur',
        'contextmenu',
        'dblclick',
        'error',
        'focusin',
        'focusout',
        'hover',
        'keydown',
        'keypress',
        'keyup',
        'load',
        'mousedown',
        'mouseup',
        'mouseenter',
        'mouseleave',
        'mousemove',
        'mouseout',
        'mouseover',
        'mouseon',
        'ready',
        'scroll',
        'select',
        'submit',
        'toggle',
        'unload',
    ];

    // checks for link clicks leading to a new passage and a new state head
    // (as in the top of the stack)
    $twStory.on('click', 'tw-link.passage', e => {
        // clear intervals
        $(e.currentTarget).parents('tw-passage').nextAll().each((_, node) => {
            const intervalEls = $(node).find('[data-interval-id]');
            intervalEls.each((_, intervalEl) => {
                if (!intervalEl.getAttribute) {
                    return;
                }

                intervalEl = intervalEl.getAttribute('data-interval-id');
                window.clearInterval(intervalEl);
            });
        });

        // clear timeouts
        $(e.currentTarget).parents('tw-passage').nextAll().each((_, node) => {
            const timeoutEls = $(node).find('[data-timeout-id]');
            timeoutEls.each((_, timeoutEl) => {
                if (!timeoutEl.getAttribute) {
                    return;
                }

                timeoutEl = timeoutEl.getAttribute('data-timeout-id');
                window.clearTimeout(timeoutEl);
            });
        });

        // render binding selectors temporarily unusable
        $('tw-binding').each((_, binding) => {
            const $binding = $(binding);
            const selector = 'data-binding-name';
            let name = $binding.attr(selector);
            if (!name.startsWith('***')) {
                $binding.attr(selector, '***' + name);
            }
            
            name = $binding.attr(selector);
            if (!name.endsWith('***')) {
                $binding.attr(selector, name + '***');
            }
        });

        // delete all auto-disposable macro aliases
        while (State.macroAliasDisposals.length) {
            const name = State.macroAliasDisposals.pop();
            delete State.macroAliasStore[name];
        }

        // render the new passage
        Renderer.render($(e.currentTarget).attr('passage-name'));

        // fix
        /* $('html, body').animate({
            scrollTop: 10000
        }, 1250); */
    });

    // checks for jonah-style rewind actions
    const pastSelector = 'tw-passage:not(.current):not(.future)';
    $twStory.on('click', pastSelector, e => {
        // render binding selectors temporarily unusable
        $('tw-binding').each((_, binding) => {
            const $binding = $(binding);
            const selector = 'data-binding-selector';
            let attr = $binding.attr(selector);
            if (!attr.startsWith('***')) {
                $binding.attr(selector, '***' + attr);
            }

            attr = $binding.attr(selector);
            if (!attr.endsWith('***')) {
                $binding.attr(selector, attr + '***');
            }
        });
        // remove ***  ***, reactivating bindings on the current passage
        $(e.currentTarget).find('tw-binding').each((_, binding) => {
            const $binding = $(binding);
            const selector = 'data-binding-selector';
            const attr = $binding.attr(selector);
            $binding.attr(selector, attr.slice(3, -3));
        });

        const id = $(e.currentTarget).attr('data-id');
        Renderer.rewindTo(id);
    });

    // checks for jonah-style fast-forward actions
    $twStory.on('click', 'tw-passage.future', e => {
        // render binding selectors temporarily unusable
        $('tw-binding').each((_, binding) => {
            const $binding = $(binding);
            const selector = 'data-binding-selector';
            let attr = $binding.attr(selector);
            if (!attr.startsWith('***')) {
                $binding.attr(selector, '***' + attr);
            }

            attr = $binding.attr(selector);
            if (!attr.endsWith('***')) {
                $binding.attr(selector, attr + '***');
            }
        });

        // remove ***  ***, reactivating bindings on the current passage
        $(e.currentTarget).find('tw-binding').each((_, binding) => {
            const $binding = $(binding);
            const selector = 'data-binding-selector';
            const attr = $binding.attr(selector);
            $binding.attr(selector, attr.slice(3, -3));
        });

        const id = $(e.target).attr('data-id');
        Renderer.fastForwardTo(id);
    });

    // checks for sugarcube-style rewind actions
    $twStory.on('click', 'tw-history-button.back', Renderer.rewind);

    // checks for sugarcube-style fast-forward actions
    $twStory.on('click', 'tw-history-button.forward', Renderer.fastForward);

    window.mouseEvents = {
        isDown: false,
    };

    $('html').mousedown(e => {
        window.mouseEvents.isDown = true;
        window.mouseEvents[e.which] = {
            event: e,
            isDown: true,
        };
    });

    $('html').mousemove(e => {
        if (!e.which) {
            window.mouseEvents = {
                isDown: false,
            };
        }
        
        if (e.which in window.mouseEvents) {
            window.mouseEvents[e.which].event = e;
        } else {
            window.mouseEvents[e.which] = {
                event: e,
                isDown: false,
            }
        }
    });

    $('html').mouseup(e => {
        window.mouseEvents[e.which] = {
            event: e,
            isDown: false,
        };

        $('tw-element-resizer').removeClass('active');
        window.gatelyElementResizing = null;
    });

    $('html').add(frameHTML).mousemove(e => {
        if (e.which in window.mouseEvents &&
            window.mouseEvents[e.which].isDown &&
            window.gatelyElementResizing)
        {
            widthElementToPercentage(window.gatelyElementResizing, e);
        }
    });

    window.keyEvents = {};
    $('html').keydown(e => {
        window.keyEvents[e.which] = {
            event: e,
            isDown: false
        }

        window.keyEvents.isDown = true;
    });

    $('html').keyup(e => {
        window.keyEvents[e.which] = {
            event: e,
            isDown: false
        }

        let foundTrue = false;
        for (let key in window.keyEvents) {
            if (typeof(window.keyEvents[key]) === 'object' &&
                window.keyEvents[key] &&
                'keyDown' in window.keyEvents[key] &&
                window.keyEvents[key].keyDown === true)
            {
                foundTrue = true;
                break;
            }
        }
        if (!foundTrue) {
            window.keyEvents.isDown = false;
        }
    });

    if (processed.indexOf('save') !== -1) {
        const saveElem = document.querySelector('tw-save#defaultSave');

        if (saveElem && saveElem.childCount !== 0) {
            const stateObj = saferJSONParse(saveElem.dataset.save);

            if (typeof(stateObj) === 'object' && stateObj) {
                for (let prop in stateObj) {
                    try {
                        State[prop] = stateObj[prop];
                    } catch (e) {
                        console.log('Failed reserializing!', e);
                    }
                }
            }

            const present = State.stacks.present;
            if (present.storyTheme.prerenderString) {
                present.storyTheme.prerender = new Function(
                    present.storyTheme.prerenderString);
            }

            if (present.storyTheme.postrenderString) {
                present.storyTheme.postrender = new Function(
                    present.storyTheme.postrenderString);
            }

            Macro.world.restoreModelProtoFuncs.handler();

            Mixer.deserializeMetadata();

            $('tw-save#defaultSave').remove();

            return;
        } else {
            throw new Error('There is a save flag on this HTML file but no ' +
                'tw-save#defaultSave element that contains a state.');
        }
    }

    /******
     everything that follows must be non-event logic unneeded for 
     save launches
     ******/
    // evals all story scripts
    selector = 'script, tw-passagedata[tags~="script"]';
    $storydata.find(selector).each((_, aa) => {
        $(aa).attr('data-src', 'user-added script');
        eval(aa.textContent);
    });

    const startNode = $storydata.attr('startNode');
    const $startPassage = $('tw-passagedata[pid="' + startNode + '"]');

    // appends all story styles
    selector = 'style, tw-passagedata[tags~="style"]';
    $storydata.find(selector).each((_, aa) => {
        $('body').append(
            '<style data-src="user-added style">' +
            aa.textContent +
            '</style>');
    });

    // fire all passages tagged load
    selector = 'tw-passagedata[tags~=load]';
    $storydata.find(selector).each((_, aa) => {
        const clone = aa.cloneNode(true);
        Compiler.compile(clone);
    });

    let fastMode;
    if (State.fastMode) {
        fastMode = true;
    }

    if (!fastMode) {
        // Add the history buttons to the <tw-story> element.
        let str ='<tw-history-button class="back hidden">' +
            '\<' +
            '</tw-history-button>';
        $('tw-story').append(str);

        str = '<tw-history-button class="forward hidden">' +
            '\>' +
            '</tw-history-button>';
        $('tw-story').append(str);
    }

    // wire up all passages tagged unload to the beforeunload event
    selector = 'tw-passagedata[tags~=unload]';
    $storydata.find(selector).each((_, aa) => {
        $(window).on('beforeunload', e => {
            return Parser.parseText(aa.innerHTML);
        });
    });

    // add tracery grammars in special html elements
    selector = 'tw-passagedata[tags~="_tracerystore"], ' +
        'tw-passagedata[tags~="_tracery-store"]';
    $storydata.find(selector).each((_, trace) => {
        const name = trace.getAttribute('name');
        if (!name) {
            wideAlert('A passage tagged tracery-store lacks a name attribute. ' +
                'Cannot add to tracery store.');
        }

        State.addTraceryGrammar(name, trace.innerHTML);
    });

    // initialize the default story style
    Renderer.initializeStoryTheme();

    // renders the start passage
    Renderer.render($startPassage.attr('name'));

    // prevent FOUC; make window visible now that loading is complete
    $('html').removeClass('unloaded');

    // gately is no longer loading
    window.isLoading = false;
}());