(function() {
    window.Themes = {
        getDefaultStoryTheme() {
            return deepCopy({
                renderType: Config.renderTypes.sugarcane,

                rewind: true,
                fastForward: true,

                passageName: 'load',

                newlineType: Config.newlineTypes.newlineToBr,

                debug: {
                    allowTranscripts: true,
                    allowSaves: true,
                    failHard: false,
                },

                errorLevels: {
                    alert: true,
                    console: true,
                    inline: true,
                },

                load: null,
                loadString: null,

                unload: null,
                unloadString: null,

                prerender: null,
                prerenderString: null,

                postrender: null,
                postrenderString: null,

                transformVariablesInAttributes: true,

                style: 
                    '/* global containers */\n' +
                    'html {\n' +
                        '\tbackground-color: white;\n' +
                    '}\n' +
                    'html, body {\n' +
                        '\twidth: 100%;\n' +
                        '\theight: 100%;\n' +
                    '}\n' +
                    'body {\n' +
                        '\tposition: relative;\n' +
                        '\tmargin: 0;\n' +
                        '\tfont: 1rem "Helvetica Neue", Helvetica, Arial, sans-serif;\n' +
                    '}\n' +
                    '/* basic story elements */\n' +
                    'tw-storydata {\n' +
                        '\tdisplay: none;\n' +
                    '}\n' +
                    'tw-story, #passage {\n' +
                        '\tposition: absolute;\n' +
                        '\twidth: 80%;\n' +
                        '\tpadding: 2.5rem;\n' +
                        '\tline-height: 145%;\n' +
                    '}\n' +
                    '#passage *, #passagePane *, #displayPane *, #defaultMenu * {\n' +
                        '\t/*font-size: inherit;*/\n' +
                    '}\n' +
                    'tw-passage {\n' +
                        '\tposition: relative;\n' +
                        '\tdisplay: inline-block;\n' +
                        '\twidth: 100%;\n' +
                        '\tpadding: 1.5rem;\n' +
                        '\tmargin-bottom: 0.33rem;\n' +
                        '\tborder: 0.2rem solid black;\n' +
                        '\tbox-sizing: border-box;\n' +
                    '}\n' +
                    'tw-passage.current.jonah {\n' +
                        '\tborder: 0.2rem solid gold;\n' +
                    '}\n' +
                    'tw-passage:not(.current) {\n' +
                        '\topacity: 0.35;\n' +
                        '\tcursor: pointer;\n' +
                    '}\n' +
                    'tw-passage:not(.current):hover {\n' +
                        '\tbox-shadow: 0 0 0.33rem black;\n' +
                    '}\n' +
                    'tw-passage:not(.current) {\n' +
                        '\tborder: 0.2rem solid black;\n' +
                    '}\n' +
                    'tw-passage:not(.current) * {\n' +
                        '\tpointer-events: none;\n' +
                    '}\n' +
                    'tw-link {\n' +
                        '\tcolor: red;\n' +
                        '\tfont-weight: bold;\n' +
                        '\tcursor: pointer;\n' +
                    '}\n' +
                    'tw-history-button.back {\n' +
                        '\ttop: 2rem;\n' +
                    '}\n' +
                    'tw-history-button.forward {\n' +
                        '\ttop: 5rem;\n' +
                    '}\n' +
                    'tw-history-button {\n' +
                        '\tposition: absolute;\n' +
                        '\tleft: -2rem;\n' +
                        '\tfont-size: 1.5rem;\n' +
                        '\tfont-weight: bold;\n' +
                        '\tcursor: pointer;\n' +
                    '}\n' +
                    'tw-header, tw-footer {\n' +
                        '\tdisplay: block;\n' +
                    '}\n' +
                    'tw-transition-container[data-transition-type="dissolve"] {\n' +
                        '\t-webkit-animation-name: dissolve;\n' +
                        '\t-moz-animation-name: dissolve;\n' +
                        '\t-o-animation-name: dissolve;\n' +
                        '\tanimation-name: dissolve;\n' +
                    '}\n' +
                    'tw-save {\n' +
                        '\tdisplay: none;\n' +
                    '}\n' +
                    '/* global helpers */\n' +
                    '.hidden {\n' +
                        '\tdisplay: none !important;\n' +
                    '}\n' +
                    '.centerHorizontally {\n' +
                        '\tleft: 50%;\n' +
                        '\t-webkit-transform: translateX(-50%);\n' +
                        '\t-moz-transform: translateX(-50%);\n' +
                        '\t-o-transform: translateX(-50%);\n' +
                        '\ttransform: translateX(-50%);\n' +
                    '}\n' +
                    '.centerVertically {\n' +
                        '\ttop: 50%;\n' +
                        '\t-webkit-transform: translateY(-50%);\n' +
                        '\t-moz-transform: translateY(-50%);\n' +
                        '\t-o-transform: translateY(-50%);\n' +
                        '\ttransform: translateY(-50%);\n' +
                    '}\n' +
                    '.centerVertically {\n' +
                        '\tleft: 50%;\n' +
                        '\ttop: 50%;\n' +
                        '\t-webkit-transform: translate(-50%, -50%);\n' +
                        '\t-moz-transform: translateY(-50%, -50%);\n' +
                        '\t-o-transform: translateY(-50%, -50%);\n' +
                        '\ttransform: translateY(-50%, -50%);\n' +
                    '}\n' +
                    '/* named animations */\n' +
                    '@-webkit-keyframes dissolve {\n' +
                        '\tfrom {\n' +
                            '\t\topacity: 0;\n' +
                        '\t}\n' +
                        '\tto {\n' +
                            '\t\topacity: 1;\n' +
                        '\t}\n' +
                    '}\n' +
                    '@-moz-keyframes dissolve {\n' +
                        '\tfrom {\n' +
                            '\t\topacity: 0;\n' +
                        '\t}\n' +
                        '\tto {\n' +
                            '\t\topacity: 1;\n' +
                        '\t}\n' +
                    '}\n' +
                    '@-o-keyframes dissolve {\n' +
                        '\tfrom {\n' +
                            '\t\topacity: 0;\n' +
                        '\t}\n' +
                        '\tto {\n' +
                            '\t\topacity: 1;\n' +
                        '\t}\n' +
                    '}\n' +
                    '@keyframes dissolve {\n' +
                        '\tfrom {\n' +
                            '\t\topacity: 0;\n' +
                        '\t}\n' +
                        '\tto {\n' +
                            '\t\topacity: 1;\n' +
                        '\t}\n' +
                    '\t}\n' +
                    '@keyframes dissolve {\n' +
                        '\tfrom {\n' +
                            '\t\topacity: 0;\n' +
                        '\t}\n' +
                        '\tto {\n' +
                            '\t\topacity: 1;\n' +
                        '\t}\n' +
                    '}\n',
            });
        },
        
        getDefaultPassageThemes() {
            const dflt = {
                _traceryrender: {
                    render: function(text, passageData) {
                        let object;
                        try {
                            object = eval('(' + text + ')');
                        } catch (e) {
                            wideAlert('Could not eval in-passage tracery ' +
                                'source.\n' + e.message);
                        }

                        if (object && typeof(object) === 'object') {
                            const grammar = tracery.createGrammar(object);
                        
                            grammar.addModifiers(baseEngModifiers);
                            return this.style + grammar.flatten("#origin#");
                        }
                    },
                    
                    style: null,
                },
            };

            dflt['_tracery-render'] = dflt._traceryrender;

            return dflt;
        },
    };
}());