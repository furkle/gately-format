(function() {
    'use strict';

    const $twStory = $('tw-story');

    window.Renderer = {
        isRendering: false,

        initializeStoryTheme() {    
            $('#themeStyle').replaceWith(
                $('<style id="themeStyle">' +
                        (State.stacks.present.storyTheme.style || '') +
                    '</style>'));
        },

        passagesSeenThisRender: [],

        PASSAGE_RECURSION_LIMIT: 500,

        render(passageName) {
            State.validateStateIntegrity();

            /* this should be accurate as long as render is never called 
             * recursively. */
            Renderer.isRendering = true;

            Renderer.passagesSeenThisRender = [];

            if (!passageName || typeof(passageName) !== 'string') {
                // being loaded in a frame; wait for further instructions
                if (!('Options' in window)) {
                    return;
                }

                // not in a frame, but an error
                wideAlert('The passageName argument passed to ' +
                    'Renderer.render is not valid.');
                return;
            }
            
            // search for the correct tw-passagedata only in tw-storydata
            const passage = document.querySelector('tw-storydata > ' +
                'tw-passagedata[name="' + passageName + '"]');
            if (!passage) {
                // passage does not exist in document
                wideAlert('The passage referred to by the passageName ' +
                    'argument, ' + passageName + ', passed to ' +
                    'Renderer.render is not valid.');
                return;
            }

            const tags = (passage.getAttribute('tags') || '')
                .split(' ')
                .filter(aa => aa);

            Renderer.passagesSeenThisRender = [passageName];

            Macro.world.restoreModelProtoFuncs.handler();

            const renderTypes = Config.renderTypes;
            const theme = State.stacks.present.storyTheme;

            /* Clicking a link removes all future state, both in terms of
             * visible jonah passages, and objects in State.future
             * arrays. */
            const future = document.querySelectorAll('tw-passage.future');
            for (let ii = 0; ii < future.length; ii++) {
                future[ii].parentNode.removeChild(future[ii]);
            }

            const historyButs = document.querySelectorAll('tw-history-button');
            for (let ii = 0; ii < historyButs.length; ii++) {
                $twStory.append(historyButs[ii]);
            }

            Renderer.makeNode(passageName);

            Renderer.handleBackAndForwardButtons();

            const current = document.querySelector('tw-passage.current');
            if ((tags.indexOf('nobr') === -1 &&
                tags.indexOf('no-br') === -1 &&
                tags.indexOf('nonewlines') === -1 &&
                tags.indexOf('no-newlines') === -1) &&
                State.stacks.present.storyTheme.newlineType ===
                    Config.newlineTypes.newlineToBr)
            {
                Renderer.newlineToBr(current);
            }

            if (State.recordingTranscript === true) {
                Options.updateTranscriptVisuals();
            }

            current.normalize();

            Renderer.isRendering = false;
        },

        rewind() {
            if (State.stacks.past.passageNames.length === 1) {
                wideAlert('Cannot rewind to _load.');
                return;
            }

            const passageName = State.stacks.past.passageNames[0];
            Renderer.rewindTo(passageName);
        },

        rewindTo(passageName) {
            State.validateStateIntegrity();

            // search for the correct tw-passagedata only in tw-storydata
            const $passage = $('tw-storydata > tw-passagedata[name="' +
                passageName + '"]');
            if (!$passage.length) {
                throw new Error();
            }

            State.rewindTo(passageName);

            const tags = ($passage.attr('tags')|| '').split(' ');
            if (tags.indexOf('sugarcane') !== -1 &&
                tags.indexOf('jonah') !== -1)
            {
                wideAlert('You\'ve tagged a passage both "sugarcane" and ' +
                    '"jonah," so I can\'t do anything.');
                return;
            } else if (tags.indexOf('sugarcane') !== -1) {
                sugarcane();
            } else if (tags.indexOf('jonah') !== -1) {
                jonah();
            } else if (State.stacks.present.storyTheme.renderType ===
                Config.renderTypes.sugarcane)
            {
                sugarcane();
            } else if (State.stacks.present.storyTheme.renderType ===
                Config.renderTypes.jonah)
            {
                jonah();
            }

            function sugarcane() {
                // the true denotes not altering State
                Renderer.makeNode(passageName, true);
            }

            function jonah() {
                $('tw-history-button')
                    .addClass('hidden')
                    .appendTo($('tw-story'));

                const $rewindTo = $('tw-passage[data-id="' +
                    passageName +
                    '"]');

                $('tw-passage').removeClass('current');

                $rewindTo
                    .addClass('current')
                    .nextAll()
                    .addClass('future');

                $('body').scrollTop($('tw-passage.current').offset().top - 5);
            }
        },

        fastForward() {
            if (State.stacks.future.passageNames.length === 0) {
                wideAlert('There is no passage to fast-forward to.');
                return;
            }

            const passageName = State.stacks.future.passageNames[0];
            Renderer.fastForwardTo(passageName);
        },

        fastForwardTo(passageName) {
            State.validateStateIntegrity();

            // search for the correct tw-passagedata only in tw-storydata
            const $passage = $('tw-storydata > tw-passagedata[name="' +
                passageName + '"]');
            if (!$passage.length) {
                wideAlert('The passage named ' + passageName + ' cannot be ' +
                    'found. Cannot fast-forward.');
                return;
            }

            State.fastForwardTo(passageName);

            const tags = ($passage.attr('tags')|| '').split(' ');
            if (tags.indexOf('sugarcane') !== -1 &&
                tags.indexOf('jonah') !== -1)
            {
                wideAlert('You\'ve tagged a passage both "sugarcane" and ' +
                    '"jonah," so I can\'t do anything.');
                return;
            } else if (tags.indexOf('sugarcane') !== -1) {
                sugarcane();
            } else if (tags.indexOf('jonah') !== -1) {
                jonah();
            } else if (State.stacks.present.storyTheme.renderType ===
                Config.renderTypes.sugarcane)
            {
                sugarcane();
            } else if (State.stacks.present.storyTheme.renderType ===
                Config.renderTypes.jonah)
            {
                jonah();
            }

            function sugarcane() {
                // the true denotes not altering State
                Renderer.makeNode(passageName, true);
            }

            function jonah() {
                $('tw-history-button')
                    .addClass('hidden')
                    .appendTo($('tw-story'));

                const $fastForwardTo = $('tw-passage[data-id="' +
                    passageName +
                    '"]');

                $('tw-passage').removeClass('current');

                $fastForwardTo
                    .addClass('current')
                    .prevAll('tw-passage')
                    .add($fastForwardTo)
                    .removeClass('future');
            }
        },

        makeNode(passageName, noState) {
            if (noState !== true) {
                State.newState(passageName);
                State.reapExcept(State.reapQuantity);
            }

            const $passageData =
                $('tw-passagedata[name="' + passageName + '"]');

            $twStory.append($('tw-history-button'));
    
            const present = State.stacks.present;
            if (isFunction(present.storyTheme.prerender)) {
                present.storyTheme.prerender($passageData[0]);
            }

            const renderTypes = Config.renderTypes;

            const headerPassages = $storydata
                .find('tw-passagedata[tags~="header"]')
                .toArray();
            const footerPassages = $storydata
                .find('tw-passagedata[tags~="footer"]')
                .toArray();

            // add a valid passage for rendering if one does not already exist
            if (State.stacks.present.storyTheme.renderType === renderTypes.jonah ||
                !$('tw-passage.current').length)
            {
                Renderer.addPassage(passageName, $passageData.html());
            } 

            const $currentNode = $('tw-passage.current');
            if (!$currentNode) {
                throw new Error('No tw-passage.current element could be ' +
                    'found. Aborting rendering in Renderer.makeNode.');
            }

            $currentNode.html('');

            if ('Options' in window) {
                Options.endTranscriptEntry();
                Options.startTranscriptEntry();
            }

            headerPassages.forEach(header => {
                Renderer.handlePassage(header, 'header');
            });

            Renderer.handlePassage($passageData[0]);

            footerPassages.forEach(footer => {
                Renderer.handlePassage(footer, 'footer');
            });

            Renderer.handleBackAndForwardButtons();

            if ('Options' in window) {
                const textChanges = Options.recordingLevels.textChanges;
                if (window.DEBUG || textChanges) {
                    Options.captureTextChanges();
                }

                const pageEvents = Options.recordingLevels.pageEvents;
                if (window.DEBUG || pageEvents) {
                    Options.capturePageEvents();
                }
            }

            if (isFunction(present.storyTheme.postrender)) {
                present.storyTheme.postrender($passageData[0]);
            }
        },

        addPassage(passageName, text) {
            $('tw-passage').removeClass('current');

            const newPassage = document.createElement('tw-passage');
            newPassage.setAttribute('data-id', passageName);
            newPassage.className = 'current ';
            
            const present = State.stacks.present;
            if (!State.fastMode) {
                if (present.storyTheme.rewind) {
                    newPassage.className += 'rewind ';
                }

                if (present.storyTheme.fastForward) {
                    newPassage.className += 'fastForward ';
                }
            }

            // remove terminal space
            newPassage.className = newPassage.className.slice(0, -1);

            const type = present.storyTheme.renderType;
            newPassage.dataset.renderType = type;

            $twStory.append(newPassage);

            newPassage.innerHTML = text;
        },

        handlePassage(passageData, headerOrFooter) {
            const name = passageData.getAttribute('name');
            const text = passageData.textContent;

            let elem;
            if (headerOrFooter === 'header') {
                elem = document.createElement('tw-header');
                elem.id = 'header_' + name;
            } else if (headerOrFooter === 'footer') {
                elem = document.createElement('tw-footer');
                elem.id = 'footer_' + name;
            } else {
                elem = document.createElement('tw-passage-temp');
                elem.id = name;
            }

            const tags = (passageData.getAttribute('tags') || '').split(' ');

            $('tw-passage.current').append(elem); 
            elem.textContent = text;
            Compiler.compile(elem);

            if (tags.indexOf('header') !== -1 ||
                tags.indexOf('footer') !== -1)
            {
                for (let ii = 0; ii < tags.length; ii++) {
                    const tag = tags[ii];
                    const match = tag.match(
                        /^require-([^~|^$*]+)([~|^$*]?=)(.+)$/);
                    if (match) {
                        const isSkipping = !$(passageData).is(
                            '[' +
                            match[1] +
                            match[2] +
                            '"' +
                            match[3] +
                            '"]');

                        if (isSkipping) {
                            return;
                        }
                    }
                }
            }
            
            tags.forEach(tag => {    
                if (tag in State.themeStore.passage) {
                    const theme = State.themeStore.passage[tag];
                    const rendered = theme.render(passageData);
                    rendered.className += ' ' + tag + '-element';
                    if (isNode(rendered) || isElement(rendered)) {
                        elem.appendChild(rendered);
                    } else {
                        wideAlert('Theme returntype is not node!');
                        return;
                    }

                    const $style =
                        $('<style class="' + tag + '-style">' +
                            (theme.style || '') +
                        '</style>');

                    elem.appendChild($style[0]);
                }
            });

            if (elem.tagName === 'TW-PASSAGE-TEMP') {
                const $elem = $(elem);
                $elem.contents().each((_, el) => {
                    $elem.before(el);
                });

                $elem.remove();
            }

            const present = State.stacks.present;
        },

        doDisplay(passage) {
            const $passage = $(passage);

            // A value of -1 will skip all these checks.
            if (Renderer.PASSAGE_RECURSION_LIMIT !== -1) {
                Renderer.passagesSeenThisRender.push($passage.attr('name'));

                const seen = Renderer.passagesSeenThisRender;
                const dict = {};
                seen.forEach(name => {
                    if (!dict[name]) {
                        dict[name] = 1;
                    } else {
                        dict[name]++;
                    }
                });

                const vals = getOwnPropertyNames(dict).map(name => dict[name]);
                const max = Math.max(vals);

                if (max >= Renderer.PASSAGE_RECURSION_LIMIT) {
                    wideAlert('Recursion limit reached. It is likely that a ' +
                        'passage is at some level displaying itself, which ' +
                        'excepting interruption would create an infinite ' +
                        'loop. Increase Renderer.PASSAGE_RECURSION_LIMIT if ' +
                        'needed, but beware that this dramatically ' +
                        'increases the risk of fatal browser stack errors.');
                    return;
                }
            }

            const display = document.createElement('tw-display');
            display.id = $passage.attr('name');
            display.innerHTML = $passage.html();
            Compiler.compile(display);
            return display;
        },

        handleBackAndForwardButtons() {
            const $current = $('tw-passage.current');
            const $backButton = $('tw-history-button.back');
            const $forwardButton = $('tw-history-button.forward');

            // rewinds and fastforwards are impossible in fast mode
            if (State.fastMode === true) {
                $backButton.add($forwardButton).addClass('hidden');
                return;
            }

            const past = State.stacks.past;
            const present = State.stacks.present;
            const future = State.stacks.future;

            const configObj = present.storyTheme;
            if (configObj.renderType === Config.renderTypes.sugarcane) {
                // first is _load. can't go to _load
                const prevTheme = past.storyThemes[1];

                if (past.passageNames.length > 1 &&
                    prevTheme.renderType === Config.renderTypes.sugarcane)
                {
                    $backButton.removeClass('hidden').appendTo($current);
                } else {
                    $backButton.addClass('hidden').appendTo($current);
                }

                if (future.passageNames.length > 0 &&
                    future.storyThemes[0].renderType ===
                        Config.renderTypes.sugarcane)
                {
                    $forwardButton.removeClass('hidden').appendTo($current);
                } else {
                    $forwardButton.addClass('hidden').appendTo($current);
                }

                $current.removeClass('jonah').addClass('sugarcane');
            } else if (configObj.renderType === Config.renderTypes.jonah) {
                $backButton.add($forwardButton)
                    .addClass('hidden')
                    .appendTo($twStory);

                $current.removeClass('sugarcane').addClass('jonah');
            }
        },

        newlineToBr(parent) {
            let parentElem = parent;
            if (parentElem[0]) {
                parentElem = parentElem[0];
            }

            const treeWalker = document.createTreeWalker(
                parentElem,
                NodeFilter.SHOW_TEXT,
                {
                    acceptNode: node => {
                        const parent = node.parentNode;
                        if (parent &&
                            parent.tagName !== 'SCRIPT' &&
                            parent.tagName !== 'STYLE')
                        {
                            return NodeFilter.FILTER_ACCEPT;
                        } else {
                            return NodeFilter.FILTER_REJECT;
                        }
                    },
                },
                false
            );

            const nodeList = [];
            while (treeWalker.nextNode()) {
                nodeList.push(treeWalker.currentNode);
            }

            nodeList.forEach(node => {
                const nodes = [];
                const text = node.textContent;
                let lastIndex = 0;
                let index = 0;
                for (; index < text.length; index++) {
                    if (text[index] === '\n') {
                        const segment = text.slice(lastIndex, index);
                        if (segment) {
                            nodes.push(document.createTextNode(segment));
                        }

                        const br = document.createElement('br');
                        br.className = 'gately';
                        nodes.push(br);

                        lastIndex = index + 1;
                    }
                }

                if (index !== lastIndex) {
                    const segment = text.slice(lastIndex, index);
                    if (segment) {
                        nodes.push(document.createTextNode(segment));
                    }
                }

                if (nodes.length) {
                    $(node).replaceWith(nodes);
                }
            });
        },

        undoNewlineHandling(parent) {
            const $tags = $(parent).find('br.gately');
            $tags.each((_, elem) => {
                elem.outerHTML = '\n';
            });
        },
    }
}());