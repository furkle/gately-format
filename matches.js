(function() {
    'use strict';

    window.Matches = {
        currentMatch: null,

        getDataType(match) {
            if (match &&
                match.getAttribute &&
                match.getAttribute('data-type'))
            {
                return match
                    .getAttribute('data-type')
                    .replace(/&quot;|["']/g, '');
            } else if (match && match.dataType && match.dataType.replace) {
                return match.dataType.replace(/&quot;|["']/g, '');
            } else {
                return null;
            }
        },

        getMacroType(match) {
            if (match && match.getAttribute) {
                return match.getAttribute('data-macro-type');
            } else if (match && match.macroType) {
                return match.macroType;
            } else {
                return null;
            }
        },

        getMacroObject(type) {
            if (!type) {
                console.log('Invalid type received by ' +
                    'Matches.getMacroObject.');
                return null;
            }

            let namespace;
            let macroObj;
            const typeSplit = type.split('.');
            if (typeSplit.length === 1) {
                namespace = Macro.global;
                macroObj = Macro.global[type];
            } else {
                namespace = Macro[typeSplit[0]];
                if (!namespace) {
                    return null;
                }

                macroObj = namespace[typeSplit.slice(1).join('.')];
            }

            return macroObj;
        },

        getCurrentParent(match) {
            if (match && match.parent) {
                return match.parent;
            } else if (match && match.parentNode) {
                return match.parentNode;
            } else {
                return null;
            }
        },

        isDoubleTagMacroOpener(match) {
            if (!match ||
                !match.dataType ||
                match.dataType !== 'macro' ||
                match.finishedStartTag)
            {
                return false;
            }

            const macro = Matches.getMacroObject(match.macroType);
            return macro && macro.isDoubleTag;
        },

        isFinishedStartTag(match) {
            if (match) {
                return match.finishedStartTag;
            } else {
                return false;
            }
        },

        reservedWordLookup: {
            'is': '===',
            '===': '===',

            'isnot': '!==',
            '!==': '!==',

            'gt': '>',
            'greaterthan': '>',
            '>': '>',

            'gte': '>=',
            'greaterthanequals': '>=',
            '>=': '>=',

            'lt': '<',
            'lessthan': '<',
            'lesserthan': '<',
            '<': '<',

            'lte': '<=',
            'lessthanequals': '<=',
            'lesserthanequals': '<=',
            '<=': '<=',

            'and': '&&',
            '&&': '&&',

            'or': '||',
            '||': '||',

            'not': '!',
            '!': '!',

            'to': '=',
            '=': '=',

            'it': 'it',

            'plus': '+',
            '+': '+',

            'minus': '-',
            '-': '-',

            'times': '*',
            'multiply': '*',
            '*': '*',
            
            'divide': '/',
            '/': '/',

            'modulo': '%',
            '%': '%',
        },

        functionsReturning: [],

        link(match) {
            let text = match.textContent;
            const regexOne = /\s*[^[\]\s][^[\]]*\|\s*[^[\]\s][^[\]]*/;
            const regexTwo = /\s*[^[\]\s][^[\]]*->\s*[^[\]\s][^[\]]*/;
            const regexThree = /\s*[^[\]\s][^[\]]*<-\s*[^[\]\s][^[\]]*/;
            const regexFour = /\s*[^[\]\s][^[\]]*/;
            if (regexOne.test(text)) {
                // sugarcane-style links
                const split = text.split('|');
                text = '<tw-link class="passage" passage-name="' +
                    split.slice(1, split.length).join('|') +
                    '">' + split[0] + '</tw-link>';
            } else if (regexTwo.test(text)) {
                // harlowe-style forward-arrow links
                const split = text.split(/-(?:>|&gt;)/);
                text = '<tw-link class="passage" passage-name="' +
                    split[split.length - 1] +
                    '">' + split.slice(0, split.length - 1).join('->') +
                    '</tw-link>';
            } else if (regexThree.test(text)) {
                // harlowe-style backward-arrow links
                const split = text.split(/(?:<|&lt;)-/);
                text = '<tw-link class="passage" passage-name="' +
                    split[0] + '">' +
                    split.slice(1, split.length).join('<-') +
                    '</tw-link>';
            } else {
                // simple links
                text = '<tw-link class="passage" passage-name="' +
                    text + '">' + text + '</tw-link>';
            }

            match.innerHTML = text;
        },

        macro(match) {
            const parents = $(match).parents('.functionContainer');
            for (let ii = 0; ii < Matches.functionsReturning.length; ii++) {
                const ret = Matches.functionsReturning[ii];
                if (parents.index(ret) !== -1) {
                    match.parentNode.removeChild(match);
                    return;
                }
            }

            if (!match) {
                wideAlert('Something went wrong! The match passed to ' +
                    'Matches.macro is undefined.');
                return;
            }

            let type = Matches.getMacroType(match);
            let macroObj = Matches.getMacroObject(type);

            if (!macroObj) {
                macroObj = State.macroAliasStore[type];
            }

            if (!macroObj) {
                macroObj = State.functionStore[type];
            }

            if (!macroObj) {
                wideAlert('There is no macro object, alias, or ' +
                    'function/widget for a token of type: ' + type + '.');
                return;
            }

            const macroHandler = macroObj.handler;
            if (!macroHandler) {
                wideAlert('There is no macro handler for a token of type: ' +
                    type + '.');
                return;
            }

            if (type === 'if') {
                Matches.if(match);
                return;
            }

            let tokenHTML = '';
            const tokens = match.childNodes;
            const args = [];
            for (let ii = 0; ii < tokens.length; ii++) {
                let varToSet;
                while (ii < tokens.length) {
                    const dataType = Matches.getDataType(tokens[ii]);
                    if (dataType === 'comma' ||
                        dataType === 'openTagTerminator')
                    {
                        break;
                    } else if (!tokens[ii].textContent.trim() &&
                        tokens[ii].nodeType === 3)
                    {
                        ii++;
                        continue;
                    } else if (dataType === 'variable' &&
                        /^(set|let|put)$/.test(type) &&
                        tokenHTML.indexOf('=') === -1)
                    {
                        tokens[ii].innerHTML = tokens[ii].dataset.variablePath;
                    } else if (dataType === 'reservedWord' &&
                        tokens[ii].textContent === 'it' &&
                        varToSet)
                    {
                        let val = varToSet.trim();
                        if (val[0] === '$') {
                            val = val.slice(1);
                        }

                        val = State.getVariable(val, match);
                        const valType = typeof(val);
                        if (val &&
                            valType === 'object' &&
                            val.type !== 'gatelyPointer')
                        {
                            val = State.makePointer(val, match);
                        }

                        if (valType === 'null') {
                            tokens[ii].innerHTML = 'null';
                            tokens[ii].dataset.javascriptType = 'null';
                        } else {
                            tokens[ii].innerHTML = JSON.stringify(val);
                            tokens[ii].dataset.javascriptType = valType;
                        }
                    }

                    let token;
                    if (tokens[ii].tagName === 'TW-ARGUMENT') {
                        isTwArgument = true;
                        break;
                    } else if (dataType === 'variable' &&
                        tokens[ii].dataset.javascriptType === 'object')
                    {
                        token = tokens[ii].firstChild.textContent;
                    } else if (dataType) {
                        token = tokens[ii].textContent;
                    } else if (tokens[ii].nodeType === 1) {
                        if (tokens[ii].hasAttribute('gately')) {
                            token = tokens[ii];
                        } else {
                            token = tokens[ii].outerHTML;
                        }
                    } else if (tokens[ii].nodeType === 8) {
                        // skip comments
                        ii++;
                        continue;
                    } else {
                        token = tokens[ii].textContent;
                    }

                    if (typeof(token) === 'undefined') {
                        token = tokens[ii].textContent || '';
                    }
                    
                    tokenHTML += token;
                    ii++;
                }

                const retVal = saferEval(tokenHTML);
                if (typeof(retVal) === 'undefined') {
                    args.push(tokenHTML);
                } else {
                    args.push(retVal);
                }

                tokenHTML = '';
            }

            if (args.length === 1 && args[0] === '') {
                args.splice(0, 1);
            }

            for (let ii = 0; ii < args.length; ii++) {
                const maybePointer = saferJSONParse(args[ii]);
                if (State.isPointer(maybePointer)) {
                    args[ii] = State.getVariable(maybePointer.path, match);
                }
            }

            const typeSplit = type.split('.');
            const actionType = typeSplit[typeSplit.length - 1];
            let namespace;
            if (typeSplit.length === 1) {
                namespace = Macro.global;
            } else {
                namespace = Macro;

                const ns = typeSplit.slice(0, typeSplit.length - 1);

                for (let ii = 0; ii < ns.length; ii++) {
                    namespace = namespace[ns[ii]];
                }

                if (namespace === Macro) {
                    namespace = {};
                }
            }

            let retVal;
            if (isFunction(namespace.handler)) {
                retVal = namespace.handler.apply(
                    match, [actionType].concat(args));
            } else {
                retVal = macroHandler.apply(match, args);
            }

            if (typeof(retVal) === 'undefined') {
                retVal = '';
            }

            // remove all arguments
            $(match).find('[data-type="openTagTerminator"]')
                .prevAll()
                .remove();
            
            if (isNode(retVal)) {
                match.innerHTML = '';
                match.appendChild(retVal);
            } else {
                if (retVal === State.constants.LEAVE_AS_IS) {
                    // do nothing
                } else if (typeof(retVal) === 'string') {
                    match.textContent = retVal;
                } else {
                    match.innerHTML = retVal;
                }
            }

            if (window.DEBUG ||
                (State.recordingTranscript && Options.recordingLevels.render))
            {
                if (!State.stacks.present.transcriptState.macros) {
                    State.stacks.present.transcriptState.macros = [];
                }

                State.stacks.present.transcriptState.macros.push({
                    passageName: State.stacks.present.passageName,
                    macroName: type,
                    arguments: args,
                    time: new Date().getTime().toString(),
                    type: 'macro',
                    errors: from(State.compilerStatus.errors),
                });

                State.compilerStatus.errors = [];
            }

            if (Matches.functionsReturning.indexOf(match) !== -1) {
                Matches.functionsReturning.splice(
                    Matches.functionsReturning.indexOf(match),
                    1);
            }
        },

        if: match => {
            Matches.processIfBoolean(match, 'nest');

            const before = match.childNodes[0].textContent;
            let after;
            try {
                after = saferEval(before);
            } catch (e) { /* do nothing */ }

            if (after) {
                const nodes = from(match.childNodes);
                const openTT = 'openTagTerminator';
                let terminatorIndex = nodes
                    .map(node => Matches.getDataType(node) === openTT)
                    .indexOf(true);

                let hook = match.childNodes[terminatorIndex + 1];

                if (!hook) {
                    wideAlert('Hook ([ ]) not found on if statement. Cannot ' +
                        'execute.');
                    return;
                }

                // get the next non-text node
                if (hook.nodeType === 3) {
                    hook = hook.nextElementSibling;
                }

                Matches.handle(hook);

                while (hook.nextSibling) {
                    hook.parentNode.removeChild(hook.nextSibling);
                }
            } else {
                // must be ahead of if: boolean argument and tag terminator
                for (let ii = 2; ii < match.children.length; ii++) {
                    const child = match.children[ii];
                    const childMT = Matches.getMacroType(child);

                    if (childMT === 'elseif' ||
                        childMT === 'else-if' ||
                        childMT === 'else')
                    {
                        let before = child.childNodes[0].textContent;
                        let after;

                        try {
                            after = saferEval(before);
                        } catch (e) { /* do nothing */ }

                        if (after || childMT === 'else') {
                            // skip the open tag terminator
                            const subMatch = document.createElement('tw-match');
                            subMatch.innerHTML = child.children[1].outerHTML;

                            $(match.children[ii]).replaceWith(subMatch);

                            Matches.handle(subMatch);

                            $(subMatch).nextAll().remove();
                            $(subMatch).replaceWith($(subMatch).contents());
                        } else {
                            $(child).remove();
                            ii--;
                        }
                    }
                }
            }

            const args = [match.childNodes[0]]
                .concat(from(match.children).slice(1));
            const value = Macro.global.if.handler.apply(this, args);
            $(match).replaceWith(value);
        },

        processIfBoolean(match, nest) {
            let pastOpenTag = false;
            let point = 'elseif';

            $(match).addClass('processedIf');

            let handleStr = '';
            for (let ii = 0; ii < match.childNodes.length; ii++) {
                const child = match.childNodes[ii];
                const childDT = Matches.getDataType(child);
                const childMT = Matches.getMacroType(child);
                
                if (handleStr !== null) {
                    if (childDT !== 'openTagTerminator') {
                        if (child.outerHTML) {
                            handleStr += match.childNodes[ii].outerHTML;
                        } else {
                            handleStr += match.childNodes[ii].textContent;
                        }
                    } else {
                        // remove all prior nodes
                        $(match.childNodes[ii]).prevAll().remove();

                        const subMatch = document.createElement('tw-match');
                        subMatch.innerHTML = handleStr;

                        $(match.childNodes[0]).before(subMatch);

                        Matches.handle(subMatch);

                        from(subMatch.children).forEach(child => {
                            if (child.dataset.javascriptType === 'string') {
                                child.textContent = '"' + child.textContent + '"';
                            }
                        });

                        subMatch.outerHTML = subMatch.textContent;

                        // prevent looking for else-ifs within else-ifs
                        if (nest !== true && nest !== 'nest') {
                            return;
                        }

                        handleStr = null;
                        ii = 0;
                    }
                } else {
                    if (childMT === 'elseif' || childMT === 'else-if') {
                        if (point === 'else') {
                            wideAlert('An else-if macro cannot follow an ' +
                                'else macro! Ignoring else-if.');
                            child.outerHTML = '';
                        } else {
                            Matches.processIfBoolean(child);
                        }
                    } else if (childMT === 'else') {
                        point = 'else';
                    }
                }
            }
        },

        variable(match) {
            const path = match.textContent;
            match.dataset.variablePath = path;

            let variable = State.getVariable(path, match.parentNode);
            let jsType;
            jsType = typeof(variable);
            if (variable === undefined || variable === null) {
                variable = 'null';
                jsType = 'null';
            } else if (typeof(variable) === 'object') {
                const maybePointer = State.getPointerAtPath(path, match);
                if (maybePointer) {
                    // copy the pointer at that position rather than creating a new one
                    variable = maybePointer;
                } else {
                    if (match.parentNode.dataset.macroType === 'set') {
                        variable = State.makePointer(path,
                            match.parentNode.parentNode);
                    } else {
                        variable = State.makePointer(path, match.parentNode);
                    }
                }

                jsType = 'pointer';
                match.dataset.pointer = true;
            }

            if (isElement(variable)) {
                match.innerHTML = variable.outerHTML;
            } else if (isNode(variable)) {
                match.innerHTML = variable.textContent;
            } else if (jsType === 'pointer') {
                // if it is bare, print it
                if (match.parentNode === State.parserStatus.element ||
                    $(match).parents('[data-macro-type]').length === 0)
                {
                    const result = State.getVariable(variable, match);
                    if (typeof(result) === 'object') {
                        match.innerHTML = JSON.stringify(result);
                    } else {
                        match.innerHTML = result;
                    }
                } else {
                    match.innerHTML = JSON.stringify(variable);
                }
            } else {
                match.innerHTML = variable;
            }

            match.dataset.javascriptType = jsType;
        },

        reservedWord(match) {
            if (match.textContent in Matches.reservedWordLookup) {
                match.textContent = Matches.reservedWordLookup[match.textContent];
            } else {
                gatelyLog('There is no entry in the reserved word lookup ' +
                    'table for the match: ' + match.textContent + '.');
            }
        },

        html(match) {
            const present = State.stacks.present;
            const theme = present.storyTheme;
            if (theme.transformVariablesInAttributes === true) {
                const exclusions = [
                    'data-block-definitions',
                    'data-variable-path',
                ];

                getOwnPropertyNames(match.attributes).forEach(name => {
                    let attr = match.attributes[name];
                    const re = /[$@][^,.'"()[\]<>\s$@]+(\[[@$]*[^,.'"()[\]<>\s$@]+])*/g;
                    let hit;
                    while (hit = re.exec(attr.value)) {
                        if (exclusions.indexOf(attr.name) === -1) {
                            attr.value = attr.value.replace(
                                re, State.getVariable(hit[0], match));
                        }
                    }
                });
            }

            if (match.tagName === 'SCRIPT') {
                // remove compiler markup from view
                const copy = document.createElement('tw-temp');
                copy.innerHTML = match.textContent;
                try {
                    eval(copy.textContent);
                } catch (e) {
                    wideAlert(e);
                }
            }
        },

        handle(match) {
            if (!State.compilerStatus.running) {
                return;
            }

            const ifs = ['if', 'elseif', 'else-if', 'else'];
            const unfinishedIf = 
                ifs.indexOf(Matches.getMacroType(match)) !== -1 &&
                !$(match).hasClass('processedIf');
            const defer = $(match).hasClass('defer');

            let seenTerminator = false;

            if (!unfinishedIf || (defer && seenTerminator)) {
                from(match.children).forEach(childMatch => {
                    if (!State.compilerStatus.running &&
                        !/compiler-?on/.test(childMatch.dataset.macroType))
                    {
                        return;
                    } else if ('children' in childMatch) {
                        Matches.handle(childMatch);
                    }
                   
                    const type = Matches.getDataType(childMatch);
                    // skip all plain, non-parsed nodes
                    if (!type) {
                        Matches.html(childMatch);
                        return;
                    } else if (type === 'openTagTerminator') {
                        seenTerminator = true;
                    }

                    const handler = Matches[type];

                    if (!handler) {
                        wideAlert('There is no match handler for a token of ' +
                            'type: ' + type + '.');
                    } else {
                        handler(childMatch);
                    }
                });
            }
        }
    };

    Matches.singleQuote = noop;
    Matches.doubleQuote = noop;
    Matches.comma = noop;
    Matches.number = noop;
    Matches.openTagTerminator = noop;
    Matches.hook = noop;
    Matches.accessor = noop;
}())