(function() {
    'use strict';

    const getOwnPropertyNames = Object.getOwnPropertyNames;

    window.updateAudioDisplay = () => {
        // add audio channels
        const $audioDisplay = $('#defaultAudioDisplay');
        $audioDisplay.children().remove();

        let selector = '<tw-audio-channel>';
        const $global = $(selector);
        $('<span>Master Volume</span>').appendTo($global);
        $('<br>').appendTo($global);

        const $input = $('<input>');
        $input.attr({
            type: 'range',
            value: 1,
            min: '0',
            max: '1',
            step: '0.01',
        });

        $input.on('change mousemove', function(e) {
            Mixer.setUserVolume(e.currentTarget.value);
            Mixer.updateVolumes();
        });

        $input.appendTo($global);
        $('<span class="rangeReadout">1</span>').appendTo($global);
        $global.appendTo($audioDisplay);

        getOwnPropertyNames(Mixer.channels).forEach(function(name) {
            // don't display none channel
            if (name === 'none') {
                return;
            }

            const channelData = Mixer.channels[name];

            const $channel = $('<tw-audio-channel>');

            const $title = $('<span>');
            $title.text(channelData.getName());
            $title.appendTo($channel);
               
            const $range = $('<input></input>');
            $range.attr({
                type: 'range',
                step: '0.01',
                min: '0',
                max: '1',
                value: channelData.getVolume().toString(),
            });

            $range.appendTo($channel);

            $range.on('change mousemove', function(e) {
                channelData.setVolume(e.currentTarget.value, 'noupdate');
            });

            const $readout = $('<span class="rangeReadout">');
            $readout.text(channelData.getVolume());
            $readout.appendTo($channel);

            $channel.appendTo($audioDisplay);
        });
    };

    let easings = {
        /** Move at a linear rate from start to finish. Note: timing does not
        * affect linear fading. volume = x. */
        LINEAR: 'linear',
        /** Move at a quadratic rate from start to finish. volume = x^2. */
        QUADRATIC: 'quadratic',
        /** Move at a cubic rate from start to finish. volume = x^3. */
        CUBIC: 'cubic',
        /** Move at a quartic rate from start to finish. volume = x^4. */
        QUARTIC: 'quartic',
        /** Move at a quintic rate from start to finish. volume = x^5. */
        QUINTIC: 'quintic',
        /** Move at a exponential rate from start to finish. volume = x^10. */
        EXPONENTIAL: 'exponential'
    };
    easings = Object.freeze(Object.seal(easings));
    if (isFunction(window.Proxy)) {
        easings = new window.Proxy(easings, {
            set: function set() {
                gatelyLog('Mixer.easings is frozen and ' +
                    'sealed, and cannot be altered.');
            }
        });
    }

    const FADING_EASING_TYPE = easings.LINEAR;

    /** 
     * An immutable enum-like object containing the possible values for 
     * timings used to fade sounds. It is impossible to change anything
     * within this object or add to it. In ES6, attempting to do so will
     * produce a log message; changes made in earlier versions of ES will
     * fail silently.
     * @global
     * @enum {string}
     * @const
     */
    let timings = {
        /** Perform easing during the first half of the fade. */
        EASEIN: 'easeIn',
        /** Perform easing during the second half of the fade. */
        EASEOUT: 'easeOut',
        /** Perform easing at for the whole of the fade. */
        EASEINOUT: 'easeInOut'
    };
    timings = Object.freeze(Object.seal(timings));
    if (isFunction(window.Proxy)) {
        timings = new window.Proxy(timings, {
            set: function set() {
                gatelyLog('Mixer.timings is frozen and sealed, ' +
                    'and cannot be altered.');
            }
        });
    }
    const FADING_TIMING_TYPE = timings.EASEINOUT;

    let userVolume = 1;
    let storyVolume = 1;
    window.Mixer = {
        easings,
        timings,
        channels: {},
        sounds: {},

        getUserVolume() {
            return userVolume;
        },
        setUserVolume(value) {
            const val = Number(value);
            if (Number.isNaN(val)) {
                userVolume = 1;
            } else {
                userVolume = value;
            }
        },

        getStoryVolume() {
            return storyVolume;
        },
        setStoryVolume(value) {
            storyVolume = Number(value);
            Mixer.updateVolumes();
        },

        updateVolume(sound) {
            const totalVolume = 
                userVolume *
                storyVolume *
                Mixer.channels[sound.channel].getVolume() *
                sound.getVolume();
            sound.audioElement.volume = totalVolume;
        },
        updateVolumes() {
            getOwnPropertyNames(Mixer.channels).forEach(name => {
                const channel = Mixer.channels[name];

                getOwnPropertyNames(channel.sounds).forEach(name => {
                    Mixer.updateVolume(Mixer.sounds[name]);
                });
            });
        },

        serializeMetadata() {
            State.mixerMetadata = deepCopy(Mixer);

            for (let soundName in State.mixerMetadata.sounds) {
                const sound = State.mixerMetadata.sounds[soundName];
                sound.audioElement = serializeElement(
                    Mixer.sounds[soundName].audioElement);
            }
        },
        deserializeMetadata() {
            for (let name in State.mixerMetadata) {
                const prop = State.mixerMetadata[name];
                Mixer[name] = prop;
            }

            for (let name in State.mixerMetadata.channels) {
                const channel = State.mixerMetadata.channels[name];

                const tempProto = Channel.prototype;
                for (let name in tempProto) {
                    const prop = tempProto[name];
                    if (isFunction(prop)) {
                        channel[name] = prop;
                    }
                }
            }

            Mixer.channels = State.mixerMetadata.channels;

            for (let name in State.mixerMetadata.sounds) {
                const sound = State.mixerMetadata.sounds[name];

                const tempProto = Sound.prototype;
                for (let name in tempProto) {
                    const prop = tempProto[name];
                    if (isFunction(prop)) {
                        sound[name] = prop;
                    }
                }

                const audioElem = new Audio();
                for (let name in sound.audioElement) {
                    try {
                        audioElem[name] = sound.audioElement[name];
                    } catch (e) { /* do nothing */ }
                }

                if (!sound.audioElement.paused) {
                    audioElem.play();
                }

                sound.audioElement = audioElem;
            }

            Mixer.sounds = State.mixerMetadata.sounds;

            delete State.mixerMetadata;
        },
    };

    class Channel {
        constructor(name, defaultVolume, sounds) {
            if (!name || typeof(name) !== 'string' ) {
                throw new Error('The name argument passed to the Channel ' +
                    'constructor is not valid. Cannot add.');
            } else if (name in Mixer.channels) {
                throw new Error('A channel by that name already exists. ' +
                    'Cannot add.');
            }

            let channelName = name;
            this.getName = function getName() {
                return channelName;
            };

            let channelVolume = Number(defaultVolume);
            if (channelVolume > 1) {
                throw new Error('The default volume of a Channel cannot be ' +
                    'greater than 1. Cannot create Channel.');
            } else if (channelVolume < 0) {
                throw new Error('The default volume of a Channel cannot be ' +
                    'less than 0. Cannot create Channel.');
            } else if (Number.isNaN(channelVolume)) {
                if (defaultVolume === undefined) {
                    channelVolume = 1;
                } else {
                    throw new Error('The defaultVolume argument passed to ' +
                        'the Channel constructor is not valid. Cannot ' +
                        'create Channel.');
                }
            }

            this.getVolume = function getVolume() {
                return channelVolume;
            };

            this.setVolume = function setVolume(value, noupdate) {
                const val = Number(value);

                if (!validateVolume(val, 'setVolume method')) {
                    return;
                }
                
                channelVolume = Math.min(1, Math.max(0, val));

                const soundVals = getOwnPropertyNames(this.sounds)
                    .map(function(name) {
                        return Mixer.sounds[name];
                    });
                soundVals.forEach(function(sound) {
                    Mixer.updateVolume(sound);
                });

                if (noupdate !== true && noupdate !== 'noupdate') {
                    window.updateAudioDisplay();
                }
            };

            this.quieter = function quieter() {
                channelVolume = Math.max(0, channelVolume - 0.1);
            };

            this.louder = function louder() {
                channelVolume = Math.min(1, channelVolume + 0.1);
            };

            Mixer.channels[name] = this;

            this.sounds = {};
            let soundObjs = [];
            if (!sounds) {
                soundObjs = [];
            } else {
                soundObjs = from(sounds);
            }
            
            for (let ii = 0; ii < soundObjs.length; ii++) {
                const sound = soundObjs[ii];
                if (!('name' in sound)) {
                    wideAlert('There is no name property in the Sound ' +
                        'object ' + sound + '. Cannot add Sound to Channel.');
                    continue;
                } else if (!sound.getName()) {
                    wideAlert('The name property in the Sound object ' +
                        sound + ' is not valid. Cannot add Sound to ' +
                        'Channel.');
                    continue;
                }

                const channel = Mixer.channels[sound.channel];
                const soundName = sound.getName();
                delete channel.sounds[soundName];
                
                sound.channel = this.getName();
                this.sounds[soundName] = sound;
            }

            this.playlist = [];
            this.playlistIndex = 0;
            
            Mixer.updateVolumes();

            updateAudioDisplay();
        }
    }
    window.Channel = Channel;

    Mixer.channels.default = new Channel('default');

    /* ===================================================================== *
     * Adapted from Robert Penner's Easing Equations v2.01                   *
     * (c) 2003 Robert Penner, all rights reserved.                          *
     * These equations are subject to the terms in                           *
     * http://www.robertpenner.com/easing_terms_of_use.html.                 *
     * ===================================================================== */
    function linearEasing(time, initial, change, duration) {
        return change * time / duration + initial;
    }

    function quadEasing(time, initial, change, duration, timing) {
        if (timing === Mixer.timings.EASE_IN) {
            return change * (time /= duration) * time + initial;
        } else if (timing === Mixer.timings.EASE_OUT) {
            return -change * (time /= duration) * (time - 2) + initial;
        } else if (timing === Mixer.timings.EASE_IN_OUT) {
            if ((time /= duration / 2) < 1) {
                return change / 2 * time * time + initial;
            }

            return -change / 2 * ((--time) * (time - 2) - 1) + initial;
        }
    }

    function cubeEasing(time, initial, change, duration, timing) {
        if (timing === Mixer.timings.EASE_IN) {
            return change * (time /= duration) * time * time + initial;
        } else if (timing === Mixer.timings.EASE_OUT) {
            return change * ((time = time / duration - 1) * time * time + 1) +
                initial;
        } else if (timing === Mixer.timings.EASE_IN_OUT) {
            if ((time /= duration / 2) < 1) {
                return change / 2 * time * time * time + initial;
            }

            return change / 2 * ((time -= 2) * time * time + 2) + initial;
        }
    }

    function quartEasing(time, initial, change, duration, timing) {
        if (timing === Mixer.timings.EASE_IN) {
            return change * (time /= duration) * time * time * time + initial;
        } else if (timing === Mixer.timings.EASE_OUT) {
            return -change * 
                ((time = time / duration - 1) * time * time * time - 1) +
                initial;
        } else if (timing === Mixer.timings.EASE_IN_OUT) {
            if ((time /= duration / 2) < 1) {
                return change / 2 * time * time * time * time + initial;
            }

            return -change / 2 * ((time -= 2) * time * time * time - 2) +
                initial;
        }
    }

    function quintEasing(time, initial, change, duration, timing) {
        if (timing === Mixer.timings.EASE_IN) {
            return change * (time /= duration) *
                time * time * time * time + initial;
        } else if (timing === Mixer.timings.EASE_OUT) {
            return change * 
                ((time = time / duration - 1) *
                    time * time * time * time + 1) + initial;
        } else if (timing === Mixer.timings.EASE_IN_OUT) {
            if ((time /= duration / 2) < 1) {
                return change / 2 * time * time * time * time * time +
                    initial;
            }
            
            return change / 2 * ((time -= 2) *
                time * time * time * time + 2) +
                initial;
        }
    }

    function expEasing(time, initial, change, duration, timing) {
        if (timing === Mixer.timings.EASE_IN) {
            return (time === 0) ? initial : change *
                Math.pow(2, 10 * (time / duration - 1)) + initial;
        } else if (timing === Mixer.timings.EASE_OUT) {
            return (time === duration) ? initial + change :
                change * (-Math.pow(2, -10 * time / duration) + 1) + initial;
        } else if (timing === Mixer.timings.EASE_IN_OUT) {
            if (time === 0) {
                return initial;
            } else if (time === duration) {
                return initial + change;
            } else if ((time /= duration / 2) < 1) {
                return change / 2 * Math.pow(2, 10 * (time - 1)) + initial;
            }
            
            return change / 2 * (-Math.pow(2, -10 * --time) + 2) + initial;
        }
    }

    const easingFunctions = {};
    easingFunctions[Mixer.easings.LINEAR] = linearEasing;
    easingFunctions[Mixer.easings.QUADRATIC] = quadEasing;
    easingFunctions[Mixer.easings.CUBIC] = cubeEasing;
    easingFunctions[Mixer.easings.QUARTIC] = quartEasing;
    easingFunctions[Mixer.easings.QUINTIC] = quintEasing;
    easingFunctions[Mixer.easings.EXPONENTIAL] = expEasing;
    function doFade(soundObj, action, value) {
        if (action === 'volume' && !validateVolume(value)) {
            return;
        } else if (Number(value) === soundObj.getVolume()) {
            wideAlert('Can\'t fade to volume (' + value + ') – ' +
                'the currently set volume is the same.');
            return;
        }

        const fade = soundObj.fade;
        if (!fade) {
            wideAlert('The fade does not exist.');
            return;
        }

        if (fade &&
            typeof(fade.timing) !== 'string' &&
            fade.easing !== Mixer.easings.LINEAR)
        {
            gatelyLog('The sound object\'s fade.easing property is set ' +
                'to ' + fade.easing + ', which requires the sound ' +
                'object\'s fade.timing property to be set to a valid ' +
                'timing property. However, the current value of ' +
                'fade.timing is ' + fade.timing);
            return;
        }

        let time = 0;
        let initial;
        const volume = soundObj.getVolume();
        let change;
        if (action === 'play') {
            initial = 0;
            change = volume;
        } else if (action === 'pause' || action === 'stop') {
            initial = volume;
            change = -volume;
        } else if (action === 'volume') {
            initial = volume;
            change = value - volume;
        }

        const duration = fade.duration;
        const step = duration / fade.steps;
        const timing = fade.timing;
        const func = easingFunctions[fade.easing.toLowerCase()];
        if (!isFunction(func)) {
            wideAlert('The easing function does not exist.');
            return;
        }

        const soundObjDup = soundObj;

        const fadeInterval = window.setInterval(function() {
            const stepVolume = func(time, initial, change, duration, timing);
            if (stepVolume === undefined ||
                Number.isNaN(Number(stepVolume)))
            {
                window.clearInterval(fadeInterval);
                return;
            }

            soundObjDup.setVolume(stepVolume);

            time += step;
            if (time >= duration) {
                soundObjDup.setVolume(initial + change);
                window.clearInterval(fadeInterval);
                return;
            }
        }, step);
    }

    function createAudioWithErrorHandling() {
        const audio = new Audio();

        audio.addEventListener('error', function(e) {
            const err = e.target.error;
            switch (err.code) {
                case err.MEDIA_ERR_ABORTED:
                    gatelyLog('You aborted the audio playback.');
                    break;
                case err.MEDIA_ERR_NETWORK:
                    gatelyLog('A network error caused the audio download ' +
                        'to fail.');
                    break;
                case err.MEDIA_ERR_DECODE:
                    gatelyLog('The audio playback was aborted due to a ' +
                        'corruption problem or because the audio used ' +
                        'features your browser did not support.');
                    break;
                case err.MEDIA_ERR_SRC_NOT_SUPPORTED:
                    gatelyLog('The audio cannot be loaded, either ' +
                        'because the server or network failed or because ' +
                        'the format is not supported.');
                    break;
                default:
                    gatelyLog('An unknown error occurred.');
                    break;
            }
        });

        return audio;
    }

    function validateVolume(value, source, suppressLogs) {
        if (!source) {
            source = '(source not provided)';
        }

        if (!value && value !== 0 && value !== '1') {
            if (!suppressLogs) {
                gatelyLog('The volume value parameter (' + value + '), ' +
                    'in the ' + source + ', is not valid. The parameter ' +
                    'must be a Number or a string representation of a ' +
                    'valid Number.');
            }

            return false;
        } else if (Number.isNaN(Number(value))) {
            if (!suppressLogs) {
                gatelyLog('The volume value parameter (' + value + '), ' +
                    'in the ' + source + ', is not a number.');
            }

            return false;
        } else if (Number(value) < 0) {
            if (!suppressLogs) {
                gatelyLog('The volume value parameter (' + value + '), ' +
                    'in the ' + source + ', is less than 0.');
            }

            return false;
        } else if (Number(value) > 1) {
            if (!suppressLogs) {
                gatelyLog('The volume value parameter (' + value + '), ' +
                    'in the ' + source + ', is greater than 1.');
            }

            return false;
        }
        
        return true;
    }

    updateAudioDisplay();

    // default Sound constants
    const MASTER_VOLUME_LEVEL = 1;
    const MASTER_VOLUME_STEP = 0.01;
    const ELEMENT_VOLUME = 1;
    const FADING_STATUS = false;
    const FADING_DURATION = 1000;
    const FADING_STEPS = 100;

    class Sound {
        constructor(name, channel, src, autoplay, loop, volume, fade) {
            if (typeof(name) !== 'string' || !name) {
                wideAlert('The name property (' + name + '), in the Sound ' +
                    'constructor, is not a valid string.');
                return null;
            } else if (typeof(src) !== 'string' || !src) {
                wideAlert('The src property (' + src + '), in the Sound ' +
                    'constructor, is not a valid string.');
                return null;
            }

            /**
             * @type {string}
             */
            let soundName = name;
            this.getName = function getName() {
                return soundName;
            };

            Mixer.sounds[name] = this;

            if (autoplay === true) {
                this.audioElement.autoplay = true;
            }

            if (loop === true) {
                this.audioElement.loop = true;
            }

            /**
             * @type {HTMLAudioElement}
             */
            this.audioElement = createAudioWithErrorHandling();
            this.audioElement.src = src;

            if (channel) {
                if (!channel in Mixer.channels) {
                    gatelyLog('A channel id was provided, but no channel ' +
                        'matching that id exists.');
                    return;
                }

                this.channel = channel;
                Mixer.channels[channel].sounds[soundName] = soundName;
            } else {
                this.channel = 'default';
                Mixer.channels.default.sounds[soundName] = soundName;
            }
            
            /**
             * @type {number}
             */
            let soundVolume;
            this.getVolume = function getVolume() {
                return soundVolume;
            };

            /**
             * Sets the volume of the Sound and updates its real volume.
             * @global
             * @param {string|number} value
             * @param {...string|Sound} [variadic]
             * @returns {undefined}
             */
            this.setVolume = function setVolume(value) {
                const val = Number(value);

                if (!validateVolume(val, 'setVolume method')) {
                    return;
                }
                
                soundVolume = Math.min(1, Math.max(0, val));

                Mixer.updateVolume(this);
            };

            if (typeof(volume) === 'number') {
                this.setVolume(volume);
            } else {
                this.setVolume(1);
            }

            /** Contains the
             * @type {object}
             * @property {string} [fade.easing='linear']
             * @property {string} [fade.timing='easeInOut']
             * @property {number} [fade.duration=1000]
             * @property {number} [fade.steps=100]
             */
            const source = Object.freeze(Object.seal(
                new Fade(
                    FADING_EASING_TYPE,
                    FADING_TIMING_TYPE,
                    FADING_DURATION,
                    FADING_STEPS
                )));

            /**
             * Sets the fade options for the Sound. Details on the forms the
             * fade argument can take can be found {@link fuMakeSound here}.
             * @global
             * @param {boolean|string|object} fade {@link fuMakeSound link}
             * @param {...string|Sound} [variadic]
             * @returns {undefined}
             */
            this.setFade = function setFade(fade) {
                if (!fade) {
                    /* gatelyLog('The fade argument (' + fade + ') passed to ' +
                        'the Sound constructor is not valid.'); */
                    return;
                } 

                if (!this.fade) {
                    this.fade = JSON.parse(JSON.stringify(source));
                }

                if (typeof(fade) === 'string') {
                    let fadeVal = saferJSONParse(fade);
                    if (validateFade(fade)) {
                        for (let name in fadeVal) {
                            this.fade[name] = fadeVal[name];
                        }
                    } else {
                        const easeNames = getOwnPropertyNames(Mixer.easings);
                        const timeNames = getOwnPropertyNames(Mixer.timings);
                        const fadeStr = fade.toUpperCase();
                        if (fadeStr in Mixer.easings) {
                            this.fade.easing = Mixer.easings[fadeStr];
                        } else if (fadeStr in Mixer.timings) {
                            this.fade.timing = Mixer.timings[fadeStr];
                        } else {
                            gatelyLog('The string passed to the arg ' +
                                'parameter (' + fade + ') is not a valid ' +
                                'easing or timing.');
                            return;
                        }
                    }
                } else if (typeof(fade) === 'object') {
                    if (typeof(fade.easing) === 'string') {
                        const easing = fade.easing.toUpperCase();
                        if (easing in Mixer.easings) {
                            this.fade.easing = Mixer.easings[easing];
                        }
                    } else if (typeof(fade.easing) === 'object') {
                        // custom easing support here
                    }

                    if (typeof(fade.timing) === 'string') {
                        const timing = fade.timing.toUpperCase();
                        if (timing in Mixer.timings) {
                            this.fade.timing = Mixer.timings[timing];
                        }
                    }

                    if (fade.duration > 0) {
                        this.fade.duration = fade.duration;
                    } else {
                        gatelyLog('The duration argument passed to ' +
                            'Sound.setFade must be a number greater than 0.');
                    }

                    if (fade.steps > 0) {
                        this.fade.steps = fade.steps;
                    } else {
                        gatelyLog('The steps argument passed to ' +
                            'Sound.setFade must be a number greater than 0.');
                    }
                }

                /* const easings = getOwnPropertyNames(Mixer.easings)
                    .map(function(name) {
                        return Mixer.easings[name];
                    });

                const timings = getOwnPropertyNames(Mixer.timings)
                    .map(function(name) {
                        return Mixer.timings[name];
                    });

                if (this.fade.isFading !== false &&
                    typeof(this.fade.isFading) === 'boolean' &&
                    easings.indexOf(this.fade.easing.name) !== -1 &&
                    timings.indexOf(this.fade.timing) !== -1 &&
                    this.fade.duration > 0 &&
                    this.fade.steps > 0)
                {
                    this.fade.isFading = true;
                } */
            }

            this.setFade(fade);

            updateAudioDisplay();
        }

        /**
         * Plays the audio element. Will fade if the Sound's fade object is
         * valid.
         * and the 
         * @global
         * @param {...string|Sound} [variadic]
         * @returns {undefined}
         */
        play(toFade, newFade) {
            if (!this.audioElement.paused) {
                wideAlert('You cannot play a Sound that is already playing.');
                return;
            }

            if (newFade !== undefined) {
                this.setFade(newFade);
            }

            this.audioElement.play();
            if (toFade === true || toFade === 'toFade') {
                doFade(this, 'play');
            }
        }

        /**
         * Pauses the audio element.
         * @global
         * @param {...string|Sound} [variadic]
         * @returns {undefined}
         */
        pause(toFade, newFade) {
            if (this.audioElement.paused) {
                wideAlert('You cannot pause a Sound that is already paused.');
                return;
            }
            
            if (typeof(newFade) !== 'undefined') {
                this.setFade(newFade);
            }

            this.audioElement.pause();

            if (toFade === true || toFade === 'toFade') {
                doFade(this, 'pause');
            }
        }

        /**
         * Stops the audio element. In effect, this is pausing the element
         * and setting the position to 0.
         * @global
         * @param {...string|Sound} [variadic]
         * @returns {undefined}
         */
        stop(toFade, newFade) {
            if (this.audioElement.paused &&
                this.audioElement.currentTime === 0)
            {
                wideAlert('You cannot stop a Sound that is already stopped.');
                return;
            }
            
            if (typeof(newFade) !== 'undefined') {
                this.setFade(fade);
            }
            
            this.pause();
            this.setPosition(0);

            if (toFade === true || toFade === 'toFade') {
                doFade(this, 'play', 'fromaction');
            }
        }

        /**
         * Rewinds the audio element to the beginning, without pausing it.
         * @global
         * @param {...string|Sound} [variadic]
         * @returns {undefined}
         */
        restart() {
            this.setPosition(0);
        }

        /**
         * Sets the currentTime position of the audio element.
         * @global
         * @param {...string|Sound} [variadic]
         * @returns {undefined}
         */
        setPosition(position) {
            const posi = Number(position);
            if (Number.isNaN(posi)) {
                wideAlert('The position argument passed to ' +
                    'Sound.rewindSound is not a valid number. Cannot ' +
                    'set position.');
                return;
            } else {
                this.audioElement.currentTime = posi;
            }
        }

        /**
         * Lowers the volume of the Sound by 0.1. The lowest possible value is
         * 0; lowering it beyond this will have no effect or warning.
         * @global
         * @param {...string|Sound} [variadic]
         * @returns {undefined}
         */
        quieter() {
            this.setVolume(this.getVolume() - 0.1);
        }

        /**
         * Raises the volume of the Sound by 0.1. The highest possible value
         * is 1; lowering it beyond this will have no effect or warning.
         * @global
         * @param {...string|Sound} [variadic]
         * @returns {undefined}
         */
        louder() {
            this.setVolume(this.getVolume() + 0.1);
        }

        /**
         * Sets the audio element's loop property to true.
         * @global
         * @param {...string|Sound} [variadic]
         * @returns {undefined}
         */
        loop() {
            this.audioElement.loop = true;
        }
        
        /**
         * Sets the audio element's loop property to false.
         * @global
         * @param {...string|Sound} [variadic]
         * @returns {undefined}
         */
        unloop() {
            this.audioElement.unloop = false;
        }

        /**
         * Deletes all references to the Sound in the Mixer and Channels.
         * @global
         * @param {...string|Sound} [variadic]
         * @returns {undefined}
         */
        delete() {
            if (!this.getName()) {
                wideAlert('This Sound does not have a valid name property. ' +
                    'Cannot delete.');
                return;
            } else if (!this.channel) {
                wideAlert('This Sound does not have a valid channel ' +
                    'property. Cannot delete.');
                return;
            } else if (!this.channel.sounds) {
                wideAlert('This Sound does not have a valid channel.sounds ' +
                    'property. Cannot delete.');
                return;
            }

            this.stop();

            delete Mixer.sounds[this.getName()];
            delete Mixer.channels[this.channel].sounds[this.getName()];
        }

        /**
         * Stops a playing Sound and plays a stopped or paused Sound, fading
         * the two in/out in an identical fashion. The Sound being faded out
         * must be playing, and the Sound being faded in must be paused,
         * otherwise the crossfade will not be performed. By default, the
         * specific manner in which the fades are performed on both Sounds
         * are taken from the sound being faded out and applied to both.
         * However, if the which argument is either "in" or equal to the Sound
         * being faded in, the fade information will be taken from the Sound
         * being faded in.
         * @global
         * @param {string|Sound} [which] If the value of which is "in," or
         * the value of which is equal to the inSoundObj argument,
         * inSoundObj.fade will be used to determine how the fade is
         * performed, rather than outSoundObj.fade.
         * @returns {undefined}
         */
        crossFade(inSoundObj, which) {
            if (!inSoundObj ||
                (typeof(inSoundObj) !== 'string' &&
                    typeof(inSoundObj) !== 'object'))
            {
                wideAlert('The inSoundObj argument is invalid.');
                return;
            }
            
            if (typeof(inSoundObj) === 'string') {
                inSoundObj = Mixer.sounds[inSoundObj];
            }
            
            if (this.audioElement.paused) {
                wideAlert('Cannot fade – the outSoundObj is not playing.');
                return;
            } else if (!inSoundObj.audioElement.paused) {
                wideAlert('Cannot fade – the inSoundObj is already ' +
                    'playing.');
                return;
            }
            
            let fade = this.fade;

            if (which === 'in' || which === inSoundObj) {
                fade = inSoundObj.fade;
            }

            if (typeof(fade.easing) !== 'string') {
                wideAlert('The fade\'s easing is not a string. Cannot ' +
                    'crossfade.');
                return;
            } else if (!(fade.easing.toUpperCase() in Mixer.easings)) {
                wideAlert('The fade\'s easing is not in Mixer.easings. ' +
                    'Cannot crossfade.');
                return;
            } else if (typeof(fade.timing) !== 'string') {
                wideAlert('The fade\'s timing is not a string. Cannot ' +
                    'crossfade.');
                return;
            } else if (!(fade.timing.toUpperCase() in Mixer.timings)) {
                wideAlert('The fade\'s timing is not in Mixer.timing. ' +
                    'Cannot crossfade.');
                return;
            } else if (Number.isNaN(Number(fade.duration))) {
                wideAlert('The fade\'s duration is not a number. Cannot ' +
                    'crossfade.');
                return;
            } else if (fade.duration <= 0) {
                wideAlert('The fade\'s duration is not greater than 0. ' +
                    'Cannot crossfade.');
                return;
            } else if (Number.isNaN(Number(fade.steps))) {
                wideAlert('The fade\'s steps is not a number. Cannot ' +
                    'crossfade.');
                return;
            } else if (fade.steps <= 0) {
                wideAlert('One or more of the values in the ' +
                    'outSoundObj\'s fade property is invalid. Cannot ' +
                    'crossfade.');
                return;
            }

            const duration = fade.duration;
            const step = duration / fade.steps;
            const timing = fade.timing;

            const easer = easingFunctions[fade.easing.toLowerCase()];
            if (!isFunction(easer)) {
                wideAlert('The easing function does not exist. Cannot ' +
                    'crossfade.');
                return;
            }

            const outSoundObj = this;
            const outTime = outSoundObj.audioElement.currentTime * 1000;
            const outInitial = this.getVolume();
            const outChange = -this.getVolume();

            const outFadeInterval = window.setInterval(function() {
                const stepVolume = easer(
                    outTime,
                    outInitial,
                    outChange,
                    duration,
                    timing);

                if (typeof(stepVolume) === 'undefined' ||
                    Number.isNaN(Number(stepVolume)))
                {
                    window.clearInterval(outFadeInterval);
                    return;
                }

                outSoundObj.setVolume(stepVolume);

                if (outSoundObj.audioElement.paused ||
                    outTime + duration >= outSoundObj.audioElement.currentTime)
                {
                    outSoundObj.setVolume(outInitial + outChange);
                    Mixer.updateVolume(outSoundObj);

                    if (outSoundObj.audioElement.paused) {
                        outSoundObj.setPosition(0);
                    } else {
                        outSoundObj.pause();
                    }

                    window.clearInterval(outFadeInterval);
                    return;
                }
            }, step);

            const inTime = inSoundObj.audioElement.currentTime * 1000;
            let inInitial = 0;
            let inChange = outInitial;

            const inFadeInterval = window.setInterval(function() {
                const stepVolume = easer(
                    inTime,
                    inInitial,
                    inChange,
                    duration,
                    timing);

                if (stepVolume === undefined ||
                    Number.isNaN(Number(stepVolume)))
                {
                    window.clearInterval(inFadeInterval);
                    return;
                }

                inSoundObj.setVolume(stepVolume);

                if (inSoundObj.audioElement.paused ||
                    inTime + duration >= outSoundObj.audioElement.currentTime)
                {
                    inSoundObj.setVolume(inInitial + inChange);
                    Mixer.updateVolume(inSoundObj);
                    window.clearInterval(inFadeInterval);
                    return;
                }
            }, step);

            inSoundObj.play();
        }
    }
    window.Sound = Sound;

    class Fade {
        constructor(easing, timing, duration, steps) {
            this.easing = easing;
            this.timing = timing;
            this.duration = duration;
            this.steps = steps;
        }
    }
    window.Fade = Fade;

    function validateFade(fade) {
        return fade &&
            typeof(fade) === 'object' &&
            fade.duration > 0;
    }

    class Playlist {
        constructor(sounds, autoplay, repeat, fade, queryInterval) {
            this.sounds = sounds;
            this.autoplay = autoplay;
            this.repeat = repeat;
            this.fade = fade;
            this.index = 0;
            this.intervalID = null;
            this.currentlyPlaying = null;

            this.queryInterval = 250;
            if (!Number.isNaN(Number(queryInterval))) {
                this.queryInterval = Math.floor(Number(queryInterval));
            }

            if (this.autoplay === true || this.autoplay === 'autoplay') {
                this.play();
            }
        }

        getQueryInterval() {
            return this.queryInterval;
        }

        play() {
            let currentSound = this.sounds[this.index];

            const validatePSR = validatePlaylistSoundReadiness;
            if (!validatePSR(currentSound)) {
                return;
            }

            currentSound.play(Boolean(this.fade), this.fade);

            this.currentlyPlaying = currentSound;

            const playlist = this;
            this.intervalID = window.setInterval(function() {
                // retain 'this' reference
                playlist.poll.call(playlist);
            }, this.getQueryInterval());
        }

        poll() {
            let currentSound = this.currentlyPlaying;

            const duration = currentSound.audioElement.duration * 1000;
            const currentTime = currentSound.audioElement.currentTime * 1000;
            const playbackRate = currentSound.audioElement.playbackRate;

            let outTooShort;
            let inTooShort;
            if (this.fade) {
                outTooShort = duration / 2 <= this.fade.duration;
                if (outTooShort && currentTime <= this.getQueryInterval()) {
                    wideAlert('Out sound too short for specified fade. Any ' +
                        'sound being crossfaded in a Playlist must be more ' +
                        'than twice the length of the playlist\'s fade ' +
                        'duration. Not fading.');
                    return;
                }

                const inSound = this.sounds[this.index + 1] || this.sounds[0];
                inTooShort = inSound.audioElement.duration * 1000 / 2 <=
                    this.fade.duration;
                if (inTooShort && currentTime <= this.getQueryInterval()) {
                    wideAlert('In sound too short for specified fade. Any ' +
                        'sound being crossfaded in a Playlist must be more ' +
                        'than twice the length of the playlist\'s fade ' +
                        'duration. Not fading.');
                    return;
                }
            }
            
            if (this.fade &&
                this.fade.duration &&
                !inTooShort &&
                !outTooShort)
            {
                playlistPlayWithFade(this);
            } else if (duration - currentTime < this.getQueryInterval()) {
                playlistPlayWithoutFade(this);
            }
        }

        pause() {
            if (this.currentlyPlaying &&
                isFunction(this.currentlyPlaying.pause))
            {
                this.currentlyPlaying.stop();
            }

            resetPlaylist(this);
        }
    }
    window.Playlist = Playlist;

    function playlistPlayWithFade(playlist) {
        const validatePSR = validatePlaylistSoundReadiness;

        let currentSound = playlist.currentlyPlaying;

        const duration = currentSound.audioElement.duration * 1000;
        const currentTime = currentSound.audioElement.currentTime * 1000;
        const totalTime = duration - playlist.fade.duration;

        if (currentTime + playlist.getQueryInterval() >= totalTime) {
            currentSound.fade = playlist.fade;

            playlist.index++;
            currentSound = playlist.sounds[playlist.index];

            if (currentSound) {
                if (validatePSR(currentSound)) {
                    playlist.currentlyPlaying.crossFade(currentSound);
                } else {
                    resetPlaylist(playlist);
                }
            } else {
                if (playlist.repeat === true ||
                    playlist.repeat === 'repeat')
                {
                    playlist.index = 0;
                    currentSound = playlist.sounds[playlist.index];

                    if (playlist.sounds.length > 1) {
                        if (validatePSR(currentSound)) {
                            playlist.currentlyPlaying.crossFade(currentSound);
                        } else {
                            resetPlaylist(playlist);
                        }
                    } else {
                        if (validatePSR(currentSound)) {
                            currentSound.play('fade');
                        } else {
                            resetPlaylist(playlist);
                        }
                    }
                } else {
                    resetPlaylist(playlist);
                }
            }
                
            playlist.currentlyPlaying = currentSound;
        }
    }

    function playlistPlayWithoutFade(playlist) {
        const validatePSR = validatePlaylistSoundReadiness;

        let currentSound = playlist.currentlyPlaying;

        currentSound.stop();

        playlist.index++;
        currentSound = playlist.sounds[playlist.index];

        if (currentSound) {
            if (validatePSR(currentSound)) {
                currentSound.play(Boolean(playlist.fade), playlist.fade);
                playlist.currentlyPlaying = currentSound;
            } else {
                resetPlaylist(playlist);
            }
        } else {
            playlist.index = 0;

            if (playlist.repeat === true ||
                playlist.repeat === 'repeat')
            {
                currentSound = playlist.sounds[playlist.index];

                if (validatePSR(currentSound)) {
                    currentSound.play(Boolean(playlist.fade), playlist.fade);
                    playlist.currentlyPlaying = currentSound;
                } else {
                    resetPlaylist(playlist);
                }
            } else {
                resetPlaylist(playlist);
            }
        }
    }

    function validatePlaylistSoundReadiness(sound) {
        if (!sound) {
            gatelyLog('The current index of this Playlist does not ' +
                'correspond to a valid Sound in the this.sounds ' +
                'property. Cannot execute.');
            return false;
        } else if (!sound.audioElement.paused) {
            gatelyLog('The current Sound object in this Playlist is ' +
                'already playing. A Playlist can only play if every ' +
                'Sound object is paused when it is reached.');
            return false;
        } /* else if (sound.audioElement.currentTime !== 0) {
            gatelyLog('The current Sound object in this Playlist is ' +
                'not at time 0. A Playlist can only play if every ' +
                'Sound object is paused and at position 0 when it is ' +
                'reached.');
            return false;
        } */

        return true;
    }

    function resetPlaylist(playlist) {
        if (!playlist) {
            gatelyLog('Invalid playlist argument provided to resetPlaylist.');
            return;
        }

        playlist.index = 0;
        playlist.currentlyPlaying = null;
        window.clearInterval(playlist.intervalID);
        playlist.intervalID = null;
    }
}());