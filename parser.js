(function() {
    'use strict';

    let unusedMatchId = 0;

    window.Parser = {
        currentMatch: null,

        createMatchObject(dataType, parent, dialect, macroType) {
            return {
                dataType: dataType,
                macroType: macroType,
                parent: parent,
                children: [],
                id: Parser.getNewMatchId(),
                finishedStartTag: false,
                dialect: dialect,
            };
        },

        getCurrentMatchId() {
            if (Parser.currentMatch) {
                return Parser.currentMatch.id;
            } else {
                return null;
            }
        },

        getNewMatchId() {
            return unusedMatchId++;
        },

        getDataType(match) {
            if (match &&
                match.getAttribute &&
                match.getAttribute('data-type'))
            {
                return match
                    .getAttribute('data-type')
                    .replace(/&quot;|["']/g, '');
            } else if (match && match.dataType && match.dataType.replace) {
                return match.dataType.replace(/&quot;|["']/g, '');
            } else {
                return null;
            }
        },

        getMacroType(match) {
            if (match && match.getAttribute) {
                return match.getAttribute('data-macro-type');
            } else if (match && match.macroType) {
                return match.macroType;
            } else {
                return null;
            }
        },

        isEndingTag(tag) {
            return tag[0] === '/' || (tag.slice && tag.slice(0, 3) === 'end');
        },

        isDeferred(type) {
            if (!type) {
                return false;
            }

            const macroObj = Matches.getMacroObject(type);

            return macroObj && macroObj.defer === true;
        },

        createOpenTag(id, dataType, dialect, macroType) {
            let text = '<tw-match '; //data-gately-id="' + id + '" ';
            if (Parser.isDeferred(macroType)) {
                text += 'class="defer" ';
            }

            if (dialect) {
                text += 'data-dialect="' + dialect + '" ';
            }

            if (macroType) {
                text += 
                    'data-type="macro" data-macro-type="' + macroType + '">';
            } else {
                text += 'data-type="' + dataType + '">';
            }

            return text;
        },

        addStringAtIndex(string, substring, index) {
            return string.slice(0, index) + substring + string.slice(index);
        },

        removeCharactersAtIndex(string, index, numToRemove) {
            return string.slice(0, index) + string.slice(index + numToRemove);
        },

        parseElement(element) {
            if (!isElement(element)) {
                wideAlert('The element argument provided to ' +
                    'Parser.parseElement is not a valid HTML element. ' +
                    'Cannot parse.');
                return;
            }

            State.parserStatus.running = true;
            State.parserStatus.element = element;
            State.parserStatus.text = element.textContent;
            State.parserStatus.index = 0;
            State.parserStatus.origIndex = 0;
            State.parserStatus.errors = [];

            let text = State.parserStatus.text;
            let index = State.parserStatus.index;
            let origIndex = State.parserStatus.origIndex;

            // supposed to be an invalid element error. test later.
            if (State.parserStatus.errors.length) {
                return;
            }

            Parser.currentMatch = null;

            const origText = text;

            while (index < text.length) {
                const frameText = text;
                 if (/\[\[/.test(text.slice(index, index + 2))) {
                    Parser.handle(
                        text,
                        'linkOpener',
                        index,
                        origIndex);
                } else if (/]]/.test(text.slice(index, index + 2))) {
                    Parser.handle(
                        text,
                        'linkCloser',
                        index,
                        origIndex);
                } else if (/["']/.test(text[index])) {
                    Parser.handle(
                        text,
                        'quote',
                        index,
                        origIndex);
                } else if (/^<([a-z]|[A-Z]|\/)+(\s|>)/.test(
                    text.slice(index, text.length)))
                {
                    Parser.handle(
                        text,
                        'htmlElement',
                        index,
                        origIndex);
                } else if (/^<!--\s[^>]+-->/.test(text.slice(index))) {
                    Parser.handle(
                        text,
                        'htmlComment',
                        index,
                        origIndex);
                } else if (/<</.test(text.slice(index, index + 2))) {
                    Parser.handle(
                        text,
                        'sugarcaneMacroOpener',
                        index,
                        origIndex);
                } else if (/>>/.test(text.slice(index, index + 2))) {
                    Parser.handle(
                        text,
                        'sugarcaneMacroCloser',
                        index,
                        origIndex);
                } else if (/\(/.test(text[index])) {
                    Parser.handle(
                        text,
                        'harloweMacroOpener',
                        index,
                        origIndex);
                } else if (text[index] === ')') {
                    Parser.handle(
                        text,
                        'harloweMacroCloser',
                        index,
                        origIndex);
                } else if (/\[/.test(text[index])) {
                    Parser.handle(
                        text,
                        'harloweHookOpener',
                        index,
                        origIndex);
                } else if (/]/.test(text[index])) {
                    Parser.handle(
                        text,
                        'harloweHookCloser',
                        index,
                        origIndex);
                } else if (/[$@]/.test(text[index])) {
                    Parser.handle(
                        text,
                        'variable',
                        index,
                        origIndex);
                } else if (/[\d-]/.test(text[index])) {
                    Parser.handle(
                        text,
                        'number',
                        index,
                        origIndex);
                } else if (/\n/.test(text[index])) {
                    Parser.handle(
                        text,
                        'newline',
                        index,
                        origIndex);
                } else if (/#gs{/.test(text.slice(index, index + 4))) {
                    Parser.handle(
                        text,
                        'simple',
                        index,
                        origIndex)
                } else if (/[^\s<>]/.test(text[index])) {
                    Parser.handle(
                        text,
                        'reservedWord',
                        index,
                        origIndex);   
                }

                State.parserStatus.index++;
                State.parserStatus.origIndex++;

                text = State.parserStatus.text;
                index = State.parserStatus.index;
                origIndex = State.parserStatus.origIndex;
            }

            element.innerHTML = text;

            if (Parser.currentMatch) {
                console.log('failed match:', Parser.currentMatch);
                wideAlert('A match was never successfully ended. Incomplete ' +
                    (Parser.getMacroType(Parser.currentMatch) ||
                        Parser.getDataType(Parser.currentMatch)) + '.');
                Parser.currentMatch = null;
            }

            for (let name in State.parserStatus) {
                State.parserStatus[name] = null;
            }
        },

        handle(text, type, index, origIndex, tag) {
            if (State.parserStatus.running) {
                Parser.parseRules[type](text, index, origIndex, tag);
            }
        },

        parseRules: {
            linkOpener(text, index, origIndex) {
                // skip if there's no closing brackets
                if (text.indexOf(']]', index) === -1) {
                    return;
                }

                // skip over the opening [[
                text = Parser.removeCharactersAtIndex(text, index, 2);

                origIndex += 2;

                if (Parser.currentMatch) {
                    const newMatch = Parser.createMatchObject(
                        'link',
                        Parser.currentMatch,
                        'harlowe');
                    Parser.currentMatch.children.push(newMatch);
                    Parser.currentMatch = newMatch;
                } else {
                    Parser.currentMatch = Parser.createMatchObject(
                        'link',
                        null,
                        'harlowe');
                }

                const newText = Parser.createOpenTag(
                    Parser.getCurrentMatchId(),
                    'link',
                    'harlowe');

                text = Parser.addStringAtIndex(text, newText, index);
                // add the length of the new text
                index += newText.length - 1;

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            linkCloser(text, index, origIndex) {
                const currentMatch = Parser.currentMatch
                if (!currentMatch ||
                    Parser.getDataType(currentMatch) !== 'link')
                {
                    return;
                }

                // remove ]]
                text = Parser.removeCharactersAtIndex(text, index, 2);;

                let newText;
                let nextHook = text.slice(index).match(/\s*\[/);

                Parser.currentMatch = Parser.currentMatch.parent;

                newText = '</tw-match>';
                text = Parser.addStringAtIndex(text, newText, index);
                index += newText.length - 1;

                origIndex += 2;

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            quote(text, index, origIndex) {
                let quoteChar = text[index];
                let quoteType = 'doubleQuote';
                let otherType = 'singleQuote';

                if (text[index] === "'") {
                    quoteType = 'singleQuote';
                    otherType = 'doubleQuote';
                }

                // top-level quotes, single quotes inside valid double 
                // quote blocks, and escaped quotes aren't treated as 
                // semantic. there is probably a bug with successive
                // backslashes
                let current = Parser.currentMatch;
                if (!current ||
                    Parser.getDataType(current) === 'html' ||
                    Parser.getDataType(current) === 'hook' ||
                    Parser.getDataType(current) === 'link' ||
                    Parser.getDataType(current) === otherType ||
                    quoteIsEscaped(text, index) ||
                    Matches.isFinishedStartTag(current))
                {
                    return;
                }

                let newText = Parser.createOpenTag(
                    Parser.getNewMatchId(),
                    quoteType);

                // add the new text
                text = Parser.addStringAtIndex(text, newText, index);

                // skip ahead the length of the added string
                index += newText.length + 1;

                origIndex++;
                // skip to the index after the next (unescaped)
                // double quote
                let backslashes = 0;

                while (index < text.length) {
                    if (text[index] === '<') {
                        // escape lesser than
                        text = Parser.removeCharactersAtIndex(text, index, 1);
                        text = Parser.addStringAtIndex(text, '&lt;', index);
                        index += 3;
                    } else if (text[index] === '>') {
                        // escape greater than
                        text = Parser.removeCharactersAtIndex(text, index, 1);
                        text = Parser.addStringAtIndex(text, '&gt;', index);
                        index += 3;
                    } else if (text[index] === '\\') {
                        backslashes++;
                    } else if (text[index] === quoteChar &&
                        backslashes % 2 === 0)
                    {
                        break;
                    } else {
                        backslashes = 0;
                    }

                    index++;
                    origIndex++;
                }

                // handle unclosed quotes
                if (index !== text.length) {
                    newText = '</tw-match>';
                    text = Parser.addStringAtIndex(text, newText, index + 1);
                    index += newText.length;
                }

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            htmlElement(text, index, origIndex) {
                let openQuoteCharacter = null;
                for (; index < text.length; index++) {
                    if (text[index] === '>' &&
                        !openQuoteCharacter)
                    {
                        break;
                    } else if (text[index] === '"' ||
                        text[index] === "'")
                    {
                        if (openQuoteCharacter === text[index]) {
                            openQuoteCharacter = null;
                        } else if (!openQuoteCharacter) {
                            openQuoteCharacter = text[index];
                        }
                    }
                }

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            htmlComment(text, index, origIndex) {
                for (; index < text.length; index++) {
                    if (/^\s-->/.test(text.slice(index))) {
                        index += 4;
                        origIndex += 4;
                        break;
                    }

                    origIndex++;
                }

                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            sugarcaneMacroOpener(text, index, origIndex) {
                // macros names are terminated by a space or >>
                let macroType = text.slice(index + 2).split(/ |>>/)[0];
                if (!macroType) {
                    return;
                }

                let newText = '</tw-match>';

                // remove opening << brackets
                text = Parser.removeCharactersAtIndex(
                    text,
                    index,
                    2 + macroType.length);

                origIndex += 2 + macroType.length;

                // don't start a new macro if it's an ending tag
                if (Parser.currentMatch) {
                    let currentMatchType = 
                        Parser.getMacroType(Parser.currentMatch);
                    if (currentMatchType === 'elseif' ||
                        currentMatchType === 'else-if' ||
                        currentMatchType === 'else')
                    {
                        if (currentMatchType !== 'else') {
                            if (macroType === 'elseif' ||
                                macroType === 'else-if' ||
                                macroType === 'else')
                            {
                                text = Parser.addStringAtIndex(text,
                                    newText,
                                    index);
                                index += newText.length;

                                const parent = Matches.getCurrentParent(
                                    Parser.currentMatch);
                                Parser.currentMatch = parent;
                            }
                        } else {
                            if (macroType === '/if' ||
                                macroType === 'endif')
                            {
                                text = Parser.addStringAtIndex(
                                    text,
                                    newText,
                                    index);
                                index += newText.length;

                                const parent = Matches.getCurrentParent(
                                    Parser.currentMatch);
                                Parser.currentMatch = parent;
                            }
                        }
                    }
                   
                    if (Parser.isEndingTag(macroType)) {
                        // remove closing >> brackets
                        text = 
                            Parser.removeCharactersAtIndex(text, index, 2);
                        origIndex += 2;
                        text =
                            Parser.addStringAtIndex(text, newText, index);
                        index += newText.length;

                        const parent = Matches.getCurrentParent(
                            Parser.currentMatch);
                        Parser.currentMatch = parent;
                        
                        State.parserStatus.text = text;
                        State.parserStatus.index = index;
                        State.parserStatus.origIndex = origIndex;
                        return;
                    }

                    let newMatch = Parser.createMatchObject(
                        'macro',
                        Parser.currentMatch,
                        'sugarcane',
                        macroType);
                    Parser.currentMatch.children.push(newMatch);
                    Parser.currentMatch = newMatch;
                } else {
                    Parser.currentMatch = Parser.createMatchObject(
                        'macro',
                        null,
                        'sugarcane',
                        macroType);
                }

                newText = Parser.createOpenTag(
                    Parser.getCurrentMatchId(),
                    'macro',
                    'sugarcane',
                    macroType);
                // add new text
                text = Parser.addStringAtIndex(text, newText, index);
                // add the length of the new text
                index += newText.length - 1;

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            sugarcaneMacroCloser(text, index, origIndex) {
                if (!Parser.currentMatch) {
                    return;
                }

                let newText;

                const currentMatch = Parser.currentMatch;
                if (Matches.isDoubleTagMacroOpener(currentMatch)) {
                    Parser.currentMatch.finishedStartTag = true;
                }

                newText = Parser.createOpenTag(
                    Parser.getNewMatchId(),
                    'openTagTerminator',
                    'sugarcane');
                text = Parser.addStringAtIndex(text, newText, index);
                index += newText.length;

                // remove closing >> brackets
                text = Parser.removeCharactersAtIndex(text, index, 2);

                newText = '</tw-match>';
                text = Parser.addStringAtIndex(text, newText, index);
                index += newText.length - 1;

                let type = Parser.getMacroType(currentMatch) || '';
                let macroObj = Matches.getMacroObject(type);

                if (!macroObj) {
                    macroObj = State.macroAliasStore[type];
                }

                if (!macroObj) {
                    macroObj = State.functionStore[type];
                }

                if (!macroObj) {
                    /*wideAlert('There is no macro definition for the ' +
                        'macro type ' + type + '.'); */
                    macroObj = {};
                }

                if (!macroObj.isDoubleTag) {
                    newText = '</tw-match>';
                    text = Parser.addStringAtIndex(text, newText, index + 1);
                    index += newText.length;

                    Parser.currentMatch = Parser.currentMatch.parent;
                }

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            harloweMacroOpener(text, index, origIndex) {
                // harlowe macro names are terminated by a colon
                let nextColon = text
                    .slice(index + 1)
                    .search(':');
                let nextNonEnder = text
                    .slice(index + 1)
                    .search(/[\s()>;'"]/);

                if (nextColon === -1 ||
                    nextNonEnder === -1 ||
                    nextNonEnder < nextColon)
                {
                    return;
                }

                let macroType = text.slice(index + 1).split(':')[0];

                // skip over the opening (, the macro name, and the
                // terminal colon
                text = Parser.removeCharactersAtIndex(
                    text,
                    index,
                    2 + macroType.length);

                origIndex += 2 + macroType.length;

                if (Parser.currentMatch) {
                    let newMatch = Parser.createMatchObject(
                        'macro',
                        Parser.currentMatch,
                        'harlowe',
                        macroType);
                    Parser.currentMatch.children.push(newMatch);
                    Parser.currentMatch = newMatch;
                } else {
                    Parser.currentMatch = Parser.createMatchObject(
                        'macro',
                        null,
                        'harlowe',
                        macroType);
                }

                let newText = Parser.createOpenTag(
                    Parser.getCurrentMatchId(),
                    'macro',
                    'harlowe',
                    macroType);

                text = Parser.addStringAtIndex(text, newText, index);
                // add the length of the new text
                index += newText.length - 1;

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            harloweMacroCloser(text, index, origIndex) {
                if (!Parser.currentMatch ||
                    Parser.currentMatch.dialect === 'sugarcane')
                {
                    return;
                }

                // remove )
                text = Parser.removeCharactersAtIndex(text, index, 1);

                const currentMatch = Parser.currentMatch;
                let type = Parser.getMacroType(currentMatch) || '';
                let macroObj = Matches.getMacroObject(type);

                if (!macroObj) {
                    macroObj = State.macroAliasStore[type];
                }

                if (!macroObj) {
                    macroObj = State.functionStore[type];
                }

                if (!macroObj) {
                    /*wideAlert('There is no macro definition for the ' +
                        'macro type ' + type + '.'); */
                    macroObj = {};
                }

                let newText;
                let nextHook = text.slice(index).match(/\s*\[/);
                if (nextHook &&
                    text.slice(index).indexOf(nextHook[0]) === 0 &&
                    // breaks if replaced in any way???
                    macroObj.isDoubleTag)
                {
                    Parser.currentMatch.finishedStartTag = true;
                    newText = Parser.createOpenTag(
                        Parser.getNewMatchId(),
                        'openTagTerminator',
                        'harlowe') + '</tw-match>';
                    text = Parser.addStringAtIndex(text, newText, index);
                    index += newText.length - 1;
                } else {
                    Parser.currentMatch = Parser.currentMatch.parent;
                    newText = Parser.createOpenTag(
                        Parser.getNewMatchId(),
                        'openTagTerminator',
                        'harlowe') + '</tw-match>';
                    text = Parser.addStringAtIndex(text, newText, index);
                    index += newText.length;

                    newText = '</tw-match>';
                    text = Parser.addStringAtIndex(text, newText, index);
                    index += newText.length - 1;
                }

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            harloweHookOpener(text, index, origIndex) {
                if (Parser.currentMatch) {
                    let newMatch = Parser.createMatchObject(
                        'hook',
                        Parser.currentMatch,
                        'harlowe');
                    Parser.currentMatch.children.push(newMatch);
                    Parser.currentMatch = newMatch;
                } else {
                    Parser.currentMatch = Parser.createMatchObject(
                        'hook',
                        null,
                        'harlowe');
                }

                text = Parser.removeCharactersAtIndex(text, index, 1);

                let newText = Parser.createOpenTag(
                    Parser.getCurrentMatchId(), 'hook', 'harlowe');
                // add new text
                text = Parser.addStringAtIndex(text, newText, index);
                // add the length of the new text
                index += newText.length - 1;

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            harloweHookCloser(text, index, origIndex) {
                if (!Parser.currentMatch) {
                    return;
                }

                const hookID = Parser.getCurrentMatchId();
                text = Parser.removeCharactersAtIndex(text, index, 1);

                let newText = '</tw-match>';
                text = Parser.addStringAtIndex(text, newText, index);
                index += newText.length - 1;
                Parser.currentMatch = 
                    Matches.getCurrentParent(Parser.currentMatch);

                let type = Parser.getMacroType(Parser.currentMatch);
                if (type &&
                    Matches.isFinishedStartTag(Parser.currentMatch))
                {
                    newText = '</tw-match>';
                    text = Parser.addStringAtIndex(text, newText, index + 1);
                    index += newText.length;
                    Parser.currentMatch = 
                        Matches.getCurrentParent(Parser.currentMatch);
                }

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            variable(text, index, origIndex) {
                if (/\s/.test(text[index + 1])) {
                    return;
                }

                const enderRE = /[\s,.'"()<>\s$@[\]]/;
                const varLength = text
                    .slice(index + 1)
                    .split(enderRE)[0].length + 1;

                let accessorsLength;
                if (text[index + varLength] === '[') {
                    const match = text.slice(index + varLength)
                        .match(/^(\[[@$]*[^,.'"()[\]<>\s$@]+])+/) || [''];
                    const nextChar = text[index + varLength + match[0].length];
                    if (!match[0].length || !enderRE.test(nextChar)) {
                        index += varLength;
                        origIndex += varLength;
                        State.parserStatus.index = index;
                        State.parserStatus.origIndex = origIndex;
                        return;
                    }

                    accessorsLength = match[0].length;
                }

                let newText = Parser.createOpenTag(
                    Parser.getNewMatchId(), 'variable');
                text = Parser.addStringAtIndex(text, newText, index);
                index += newText.length;

                index += varLength;
                origIndex += varLength;

                if (accessorsLength) {
                    const split = text.slice(index, index + accessorsLength)
                        .split(/[\[\]]/)
                        .filter(aa => aa !== '');
                    split.forEach(accessor => {
                        newText = Parser.createOpenTag(
                            Parser.getNewMatchId(), 'accessor');
                        text = Parser.addStringAtIndex(text, newText, index);
                        index += newText.length;

                        let accessorVal = accessor;

                        // remove "" or ''
                        let removingQuotes = false;
                        if (accessor[0] === accessor[accessor.length - 1] &&
                            (accessor[0] === '"' || accessor[0] === "'"))
                        {
                            removingQuotes = true;
                            accessorVal = accessor.slice(1, -1);
                            text = Parser.removeCharactersAtIndex(text, index + 1, 1);
                        }

                        // [ + accessor
                        index += 1 + accessorVal.length;

                        if (removingQuotes) {
                            text = Parser.removeCharactersAtIndex(text, index, 1);
                        }

                        // + ]
                        index++;

                        newText = '</tw-match>';
                        text = Parser.addStringAtIndex(text, newText, index);
                        index += newText.length;
                    });
                }

                newText = '</tw-match>';
                text = Parser.addStringAtIndex(text, newText, index);

                index += newText.length - 1;

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            number(text, index, origIndex) {
                // skip anything outside of matches
                if (!Parser.currentMatch ||
                    Matches.isFinishedStartTag(Parser.currentMatch) ||
                    Parser.getDataType(Parser.currentMatch) === 'hook' ||
                    (text[index] === '-' && !/\d/.test(text[index + 1])))
                {
                    return;
                }

                /* get the next non-number character
                 * match all whitespace and mathematical operators and
                 * macro closures and commas, and ensure that the next
                 * non-number character is one */
                let temp = text.slice(index).search(/[^\d-]/);
                const firstPeriod = text.slice(index).search(/\./);

                if (temp === -1) {
                    temp = text.slice(index).length;
                } else if (firstPeriod !== -1 && firstPeriod <= temp) {
                    temp += text.slice(index).match(/\.\d*/)[0].length;
                }

                /* skip anything outside of matches, or invalid matches. */
                if (!text[index + temp] ||
                    /[^\d.,-]/.test(text.slice(index, index + temp)) ||
                    !/^([\s+\-*\/%(),>]|>)/.test(text.slice(index + temp)))
                {
                    return;
                }

                let newText = Parser.createOpenTag(
                    Parser.getNewMatchId(),
                    'number');
                text = Parser.addStringAtIndex(text, newText, index);
                index += temp + newText.length;
                origIndex += temp;

                newText = '</tw-match>';
                text = Parser.addStringAtIndex(text, newText, index);
                index += newText.length - 1;

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            newline(text, index, origIndex) {
                State.parserStatus.lineNumber++;

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            reservedWord(text, index, origIndex) {
                // skip anything outside of matches
                if (!Parser.currentMatch ||
                    Matches.isFinishedStartTag(Parser.currentMatch) ||
                    Parser.getDataType(Parser.currentMatch) === 'hook')
                {
                    return;
                }

                // handle commas for argument lists
                if (text[index] === ',' &&
                    Parser.currentMatch.dataType === 'macro')
                {
                    let newText = Parser.createOpenTag(
                        Parser.getNewMatchId(),
                        'comma');
                    text = Parser.addStringAtIndex(text, newText, index);
                    // skip ahead the length of the added string
                    index += newText.length;

                    newText = '</tw-match>';
                    text = Parser.addStringAtIndex(text, newText, index + 1);
                    index += newText.length;

                    // TODO: test and fix for left-padded strings
                    // remove all spaces between comma-separated arguments
                    const regex = / /;
                    while (regex.test(text[index + 1])) {
                        text = Parser.removeCharactersAtIndex(
                            text, index + 1, 1);
                    }
                }

                let word = text.slice(index).split(/[\s<>)]/)[0];
                if (text[index - 1].match(/\s/) && 
                    Matches.reservedWordLookup[word]) 
                {
                    let newText = Parser.createOpenTag(
                        Parser.getNewMatchId(), 
                        'reservedWord');
                    text = Parser.addStringAtIndex(text, newText, index);
                    // skip ahead the length of the added string
                    index += newText.length;
                    // skip to the index after the reserved word completes
                    index += word.length;
                    origIndex += word.length;

                    newText = '</tw-match>';
                    text = Parser.addStringAtIndex(text, newText, index);
                    index += newText.length;
                }                           

                State.parserStatus.text = text;
                State.parserStatus.index = index;
                State.parserStatus.origIndex = origIndex;
            },

            simple(text, index, origIndex) {
                // retain index of start of block
                const startIndex = index;

                // Skip #gs{
                index += 4;

                // skip whitespace before start of directives
                while (/\s/.test(text[index])) {
                    index++;
                }

                /* Check if there is a closing bracket anywhere in the
                 * text. If not, skip it.*/
                if (!/}/.test(text.slice(index))) {
                    return;
                }

                let objects = [];
                for (; index < text.length; index++) {
                    const obj = {
                        argument: [],
                    };

                    const sliced = text.slice(index);

                    if (/^the /i.test(sliced)) {
                        obj.directive = 'set';
                        const match = sliced.match(/^the\s+(\S+)\s+is\s+/i);
                        if (!match) {
                            wideAlert('Sorry, I don\'t understand the ' +
                                'sentence "' + sliced + '"');
                            return;
                        }

                        obj.varName = match[1];

                        index += match[0].length;
                    } else if (/^do /i.test(sliced)) {
                        obj.directive = 'macro';
                        const match = sliced.match(/^do\s+(\S+)\s+/i);
                        if (!match) {
                            wideAlert('Sorry, I don\'t understand the ' +
                                'sentence "' + sliced + '"');
                            return;
                        }
                        
                        const macroName = match[1];
                        index += match[0].length;

                        // TODO: add macro handling
                    } else if (/}/.test(text[index])) {
                        let returnHTML = '';
                        objects.forEach(function(obj) {
                            if (obj.directive === 'set') {
                                const cpn = State.stacks.present.passageName;
                                if (!(cpn in State.simpleVariables)) {
                                    State.simpleVariables[cpn] = {};
                                }

                                const varName = obj.varName.toLowerCase();
                                State.simpleVariables[cpn][varName] =
                                    obj.argument;
                            } else if (obj.directive === 'macro') {
                                const ns = obj.namespace || 'global';
                                // returnHTML += Macro[ns][obj.macroName].handler.call(null, /* add args here */);
                            }
                        });

                        State.parserStatus.text =
                            text.slice(0, startIndex) +
                            returnHTML +
                            text.slice(index + 1);
                        return;
                    } else {
                        continue;
                    }

                    if (text[index] !== '"') {
                        wideAlert('Gately simple error! I got as far as ' +
                            text.slice(startIndex, index) +
                            ' before I encountered an error. Skipping ' +
                            'malformed statement.');
                        continue;
                    }

                    if (collectArgument() === null) {
                        continue;
                    }

                    objects.push(obj);

                    function collectArgument() {
                        let open = true;

                        // skip opening "
                        index++;

                        let openingIndex = index;
                        for (; index < text.length; index++) {
                            if (text[index] === '"') {
                                if (open) {
                                    if (!/[,.]/.test(text[index + 1])) {
                                        wideAlert('Gately simple error! I ' +
                                            'expected a period or comma ' +
                                            'after:\n\n\'' +
                                            text.slice(startIndex, index + 1) +
                                            '\'\n\nbut one was not found. ' +
                                            'Skipping malformed statement.');
                                        return null;
                                    }

                                    if (!quoteIsEscaped(text, index)) {
                                        obj.argument.push(
                                            text.slice(openingIndex, index));
                                        open = false;
                                    }
                                } else {
                                    open = true;
                                    openingIndex = index + 1;
                                }
                            } else if (text[index] === ',') {
                                index++;
                                while (/\s/.test(text[index])) {
                                    index++;
                                }

                                continue;
                            } else if (text[index] === '.') {
                                break;
                            } else if (text[index] === '}') {
                                return;
                            } else if (obj.directive === 'macro' &&
                                /^from /i.test(text.slice(index)))
                            {
                                index += 5;
                                if (text[index] !== '"') {
                                    wideAlert('Gately simple error! ' +
                                        'I got as far as ' +
                                        text.slice(startIndex, index) +
                                        'before I encountered an ' +
                                        'error. The problem was that ' +
                                        'I expected a quote ' +
                                        'following the "from" ' +
                                        'modifier, but none was ' +
                                        'found. Skipping malformed ' +
                                        'statement.');
                                    return null;
                                }

                                index++;

                                let namespace = '';
                                while (text[index] !== '"' ||
                                    !quoteIsEscaped(text, index))
                                {
                                    namespace += text[index];
                                }
                            }
                        }

                        if (obj.argument.length === 1) {
                            obj.argument = obj.argument[0];
                        }
                    }
                }
            },
        },

        parseText(text) {
            const container = document.createElement('tw-match');
            container.className = 'parseContainer';
            container.innerHTML = text;
            Parser.parseElement(container);
            return container;
        },

        unparseElement(element) {
            const dataType = Parser.getDataType(element);
            const macroType = Parser.getMacroType(element);

            if (!dataType) {
                $(element).replaceWith(
                    document.createTextNode(element.outerHTML));
            } else if (dataType === 'singleQuote' ||
                dataType === 'doubleQuote' ||
                dataType === 'reservedWord' ||
                dataType === 'comma' ||
                dataType === 'number' ||
                dataType === 'accessor') 
            {
                element.outerHTML = element.innerHTML;
            } else if (dataType === 'hook') {
                element.outerHTML = '[' + element.innerHTML + ']';
            } else if (dataType === 'variable') {
                element.outerHTML = element.innerHTML;
            } else if (dataType === 'macro') {
                const dialect = element.getAttribute('data-dialect');
                if (dialect === 'harlowe') {
                    element.outerHTML = 
                        '(' + Parser.getMacroType(element) + ': ' +
                            element.innerHTML;
                } else if (dialect === 'sugarcane') {
                    const macroObj = Matches.getMacroObject(macroType);
                    if (macroObj && macroObj.isDoubleTag) { 
                        const str =
                            '<<' +
                            Parser.getMacroType(element) + ' ' +
                            element.textContent +
                            '<<end' + macroType + '>>';
                        const textNode = document.createTextNode(str);
                        $(element).replaceWith(textNode);
                    } else {
                        const str =
                            '<<' +
                            Parser.getMacroType(element) +
                            ' ' +
                            element.textContent;
                        const textNode = document.createTextNode(str);
                        $(element).replaceWith(textNode);
                    }
                } else {
                    // wut?
                    wideAlert('The data-dialect value was not recognized. ' +
                        'The only currently-supported dialects are ' + 
                        'Sugarcane and Harlowe.');
                    return;
                }
            } else if (dataType === 'openTagTerminator') {
                const dialect = element.getAttribute('data-dialect');
                if (dialect === 'harlowe') {
                    element.outerHTML = ')';
                } else if (dialect === 'sugarcane') {
                    element.outerHTML = '>>';
                } else {
                    wideAlert('The data-dialect value was not recognized. The ' +
                        'only currently-supported dialects are Sugarcane ' +
                        'and Harlowe.');
                }
            }
        },

        unparseText(text) {
            const div = document.createElement('div');
            div.innerHTML = text;
            Parser.unparseNode(div);
            return div.innerHTML;
        },
    };
}());