(function() {
    'use strict';

    window.Compiler = {
        stop() {
            State.compilerStatus.running = false;
        },

        compile(element) {
            if (!isElement(element)) {
                wideAlert('The element argument to be parsed is not a valid ' +
                    'HTML element.',
                    elem);
                return;
            }

            State.compilerStatus.running = true;
            State.compilerStatus.errors = [];

            let elem = element;

            if (!State.compilerStatus.rootElement) {
                State.compilerStatus.rootElement = element;
            }

            State.compilerStatus.element = elem;
            State.compilerStatus.text = elem.textContent;

            for (let ii = 0; ii < Compiler.preParse.length; ii++) {
                if (!isFunction(Compiler.preParse[ii])) {
                    throw new Error('invalid preparse function.');
                }

                Compiler.preParse[ii](elem);
            }

            Parser.parseElement(elem);

            for (let ii = 0; ii < Compiler.postParse.length; ii++) {
                if (!isFunction(Compiler.postParse[ii])) {
                    throw new Error('invalid preparse function.');
                }

                Compiler.postParse[ii](elem);
            }

            if (State.compilerStatus.error) {
                throw new Error(State.compilerStatus.error);
            }

            if (element === State.compilerStatus.rootElement) {
                State.compilerStatus.running = false;
            }
        },

        preParse: [
            function resetCompilerStatus() {
                State.compilerStatus.passage = State.currentPassageName;
            },
        ],

        postParse: [
            function removeCommentNodes(element) {
                const treeWalker = document.createTreeWalker(
                    element,
                    NodeFilter.SHOW_COMMENT,
                    {
                        acceptNode: node => {
                            return NodeFilter.FILTER_ACCEPT;
                        }
                    },
                    false
                );

                const nodeList = [];
                while (treeWalker.nextNode()) {
                    nodeList.push(treeWalker.currentNode);
                }

                nodeList.forEach(node => {
                    node.parentNode.removeChild(node);
                });
            },

            function renderTraceries(element) {
                // process tracery containers
                const traceries = from(
                    element.querySelectorAll('tw-tracery-render'));

                traceries.forEach(trace => {
                    let object;
                    try {
                        object = eval('(' + trace.innerHTML + ')');
                    } catch (e) {
                        wideAlert('Could not eval in-passage tracery source' + 
                            '(header).\n' +
                            e.message);
                        return;
                    }

                    if (typeof(object) === 'object' && object !== null) {
                        const grammar = tracery.createGrammar(object);
                    
                        grammar.addModifiers(baseEngModifiers);
                        const flattened = grammar.flatten("#origin#");
                        trace.outerHTML = flattened;
                    }
                });
            },

            function storeTraceries(element) {
                const traceries = from(
                    element.querySelectorAll('tw-tracery-store'));

                traceries.forEach(trace => {
                    const name = trace.getAttribute('name');
                    if (!name) {
                        wideAlert('A tw-tracery-store element does not have a name ' +
                            'attribute. Cannot store grammar.');
                        trace.outerHTML = '';
                        return;
                    }

                    State.addTraceryGrammar(name, trace.innerHTML);
                    trace.outerHTML = '';
                });
            },

            function putSubIfsInIfBlocks(element) {
                let ifs = element
                    .querySelectorAll('tw-match[data-macro-type="if"]') || [];
                for (let ii = 0; ii < ifs.length; ii++) {
                    let ifElement = ifs[ii];
                    let parent = ifElement.parentNode;
                    let found = false;
                    for (let jj = 0; jj < parent.children.length; jj++) {
                        let child = parent.children[jj];
                        if (child === ifElement) {
                            found = true;
                        } else if (found) {
                            let type = Matches.getMacroType(child);
                            if (type === 'elseif' || type === 'else-if') {
                                ifElement.appendChild(parent.removeChild(child));
                                jj--;
                            } else if (type === 'else') {
                                ifElement.appendChild(parent.removeChild(child));
                                break;
                            } else {
                                break;
                            }
                        }
                    }
                }
            },

            function unparseDeferrals(element) {
                const deferred = from(
                    element.querySelectorAll('tw-match.defer'));
                for (let ii = 0; ii < deferred.length; ii++) {
                    const openTag = 'openTagTerminator';
                    const defer = deferred[ii];

                    const descendants = defer.querySelectorAll('*');
                    let jj = 0;
                    while (jj < descendants.length && 
                        (Matches.getDataType(descendants[jj - 1]) !== openTag ||
                        descendants[jj - 1].parentNode !== defer))
                    {
                        jj++;
                    }

                    const $hooks = $(defer).children(
                        '[data-type="openTagTerminator"] + [data-type="hook"]');

                    if (descendants[jj]) {
                        const children = from(descendants)
                            .slice(jj)
                            .reverse();
                        for (let kk = 0; kk < children.length; kk++) {
                            Parser.unparseElement(children[kk]);
                        }
                    }

                    if ($hooks.length) {
                        const text = defer.lastChild.textContent.trim();
                        if (text.startsWith('[') && text.endsWith(']')) {
                            defer.lastChild.textContent = text.slice(1, -1);
                        }
                    }
                }
            },

            function handleMatches(element) {
                Matches.handle(element);
            },

            function removeQuotationMarks(element) {
                const quotes = element.querySelectorAll(
                    'tw-match[data-type="singleQuote"], ' +
                    'tw-match[data-type="doubleQuote"]');
                from(quotes).forEach(quoteMatch => {
                    quoteMatch.textContent =
                        quoteMatch.textContent.slice(1, -1);
                });
            },

            function transmuteRemainingVars(element) {
                const $remainingVars = $(element)
                    .children('tw-match[data-type="variable"]');
                $remainingVars.each((_, variable) => {
                    const maybePointer = saferCombinedParse(variable.innerHTML);
                    if (State.isPointer(maybePointer)) {
                        variable.innerHTML =
                            JSON.stringify(State.getVariable(maybePointer, variable));
                    }
                });
            },

            function handleBlockScope(element) {
                /* Collect all tw-match nodes created by the parser, and 
                 * reverse their order so that children are handled before 
                 * parents. */
                const matches = $(element)
                    .find('tw-match, tw-argument').toArray().reverse();
                matches.forEach(match => {
                    const $match = $(match);

                    let html = $match.html();

                    if ($match.is('[data-block-definitions]')) {
                        const defs = saferJSONParse($match[0].dataset.blockDefinitions);
                        if (defs && typeof(defs) === 'object') {
                            $match.children().each((_, element) => {
                                let childDefs =
                                    saferJSONParse(element.dataset.blockDefinitions);
                                if (!childDefs || typeof(childDefs) !== 'object') {
                                    childDefs = {};
                                }

                                getOwnPropertyNames(defs).forEach(name => {
                                    if (!(name in childDefs)) {
                                        childDefs[name] = defs[name];
                                    }
                                });

                                element.dataset.blockDefinitions =
                                    JSON.stringify(childDefs);
                            });
                        }
                    }
                });
            },

            function removeTwMatchAndArgumentContainers(element) {
                /* Collect all tw-match nodes created by the parser, and 
                 * reverse their order so that children are handled before 
                 * parents. */
                const matches = $(element)
                    .find('tw-match, tw-argument').toArray().reverse();
                matches.forEach(match => {
                    const $match = $(match);

                    /* Insert each child element within match element before 
                     * the element itself. */
                    $match.contents().each((_, element) => {
                        $match.before(element);
                    });

                    // Remove all now-empty tw-match tags.
                    $match.remove();
                });
            },
        ],
    }
}());